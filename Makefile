#!/usr/bin/make -f
DOXYGEN:=doxygen
RM:=rm
RSYNC:=rsync

DEPLOY_TGT:=hirn.fritz.box:/home/maxx/pipenet/doc

.PHONY: default
default:
	@echo TODO
	@false

.PHONY: clean
clean:
	$(RM) -rvf include lib

.PHONY: distclean
distclean: clean
	cd nsntl && $(MAKE) distclean
	cd posxx && $(MAKE) distclean

.PHONY: debug
debug:
	cd nsntl && $(MAKE) distclean dep debug install
	cd posxx && $(MAKE) distclean dep debug install test

.PHONY: doc
doc:
	$(DOXYGEN) doxygen.conf

.PHONY: deploy-doc
deploy-doc:
	$(RSYNC) -vaH --delete --ignore-times doc/ $(DEPLOY_TGT)/
