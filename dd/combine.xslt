<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <!--<xsl:output method="xml" version="1.0" indent="yes" encoding="utf-8" doctype-system="./dd.dtd"/>-->
  <xsl:output method="xml" version="1.0" indent="yes" encoding="utf-8"/>
  <xsl:param name="XML_DOC_DIR" />
  <xsl:template match="/">
    <InheritanceSet xmlns:xsi = "http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation = "./dd.xsd"
>
      <xsl:for-each select="doxygenindex/compound">
        <xsl:for-each select="document(concat($XML_DOC_DIR,'/',@refid,'.xml'))/doxygen/compounddef[@kind='class'][basecompoundref]">
          <xsl:for-each select="basecompoundref">
            <Inheritance>
              <xsl:attribute name="base">
                <xsl:value-of select="current()"/>
              </xsl:attribute>
              <xsl:attribute name="derived">
                <xsl:value-of select="../compoundname"/>
              </xsl:attribute>
              <xsl:attribute name="virtual">
                <xsl:value-of select="@virt"/>
              </xsl:attribute>
            </Inheritance>
          </xsl:for-each>
        </xsl:for-each>
      </xsl:for-each>
    </InheritanceSet>
  </xsl:template>
</xsl:stylesheet>
