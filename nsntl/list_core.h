#ifndef nsntl_list_core_h
#define nsntl_list_core_h

#include <stdint.h>

namespace nsntl {

class CoreListEntry;

class CoreList {
  friend CoreListEntry;
  private:
    CoreListEntry* frst;
    CoreListEntry* lst;

  public:
    CoreList();
    ~CoreList();

    void attach(CoreListEntry* entry, bool toFront = false);
    void attachBefore(CoreListEntry* entry, CoreListEntry* before);
    void attachAfter(CoreListEntry* entry, CoreListEntry* after);
    void detach(CoreListEntry* entry);

    CoreListEntry* firstEntry() const;
    CoreListEntry* lastEntry() const;
    bool isEmpty() const;
    void forEach(int f(CoreListEntry* li));
    void forEach(void* self, int f(void* self, CoreListEntry* li));
};

class CoreListEntry {
  friend CoreList;
  protected:
    CoreList* lst;
    CoreListEntry* prv;
    CoreListEntry* nxt;

  public:
    CoreListEntry(CoreList* list = 0);
    ~CoreListEntry();

    CoreList* list() const;
    CoreListEntry* next() const;
    CoreListEntry* prev() const;
};

};

#endif
