#ifndef nsntl_exception_h
#define nsntl_exception_h

#include "types.h"

namespace nsntl {

class ErrorCodeException {
  private:
    uint64_t errorCode;
  public:
    ErrorCodeException(uint64_t errorCode);
    uint64_t getErrorCode() const;
};

enum class LogicError : uint64_t {
  UNREACHABLE_CODE = 0x955ae875e5ae2e79ULL,
  ASSERT_FAILED = 0x44b74274d93d1db4ULL,
  COPY_ELISION_ENFORCED = 0x0713bad554992d60ULL,
  NOT_IMPLEMENTED = 0x899e50f9cbfe2c52ULL,
  PAGE_SIZE_TOO_SMALL = 0xbe494193e159fa37ULL,
  STRDUP_FAILS = 0xf2783a16bd422c93ULL,
  MALLOC_FAILS = 0xb95e2cd90663245cULL,
  REALLOC_FAILS = 0xa45f8082617b2fcaULL,
  MEMORY_OVERLAP = 0xc8dd3bf6e0ec5705ULL,
  EMPTY = 0x7296984a7112aae3ULL,
  ALREADY_ATTACHED = 0x0308c90309df218fULL,
  ALREADY_DETACHED = 0x198ae848d5fa9fcfULL,
  NO_DUPES = 0x427b4358a7b90522ULL,
  INVALID_FLAGS = 0x0c3b54e393c561f2ULL,
  NOT_FOUND = 0x8ee8385e271d8e45ULL,
  POOL_WITH_USED_DATA = 0x0aa4ff2301b79dbdULL,
  INVALID_SIZE = 0xe596adf094f7b1f1ULL,
  NOT_MY_ENTRY = 0xdd092f8e0035f3b4ULL,
  INTERNAL_STATE_INCONSISTENT = 0xffb63bffa46cb18eULL,
  RUNTIME_TYPE_CHECK_FAILS = 0x808b613893d72938ULL,
  DYNAMIC_BLOCK_CAN_NOT_BE_MOVED = 0x97ab3766d0a325a7ULL,
  NULLPTR_IN_COLLECTION = 0xc7c887170987ecdaULL,
  CANNOT_DEL_FIXED_DATA = 0x6bedcef0a55ed31aULL
};

void TODO(); // AVOID_TODO_WARN

class LogicException : public ErrorCodeException {
  public:
    LogicException(LogicError errorCode);
};

};

#endif
