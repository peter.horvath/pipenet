#ifndef nsntl_list_h
#define nsntl_list_h

#include <stdint.h>

#include "collection_core.h"
#include "list_core.h"

namespace nsntl {

class ListEntry;

class List : protected virtual Collection, protected CoreList {
  friend ListEntry;

  private:
    uint64_t _size;

  protected:
    List(Allocator* entryAllocator = 0);
    virtual ~List() override;

    void derAttach(CollectionEntry* entry) override;
    void derDetach(CollectionEntry* entry) override;

  public:
    const void* getEntryType() const override;
    uint64_t getSizeofEntry() const override;
    bool isEmpty() const override;
    uint64_t size() const override;
    Collection* getCollection() const override;
    CollectionEntry* firstEntry() const override;
    CollectionEntry* lastEntry() const override;
    CollectionEntry* nextOf(const CollectionEntry* entry) const override;
    CollectionEntry* prevOf(const CollectionEntry* entry) const override;

  private:
    void initEntry(void* p) override;
    void uninitEntry(void* p) override;

    /** Uses sequential search, O(n), SLOW */
    CollectionEntry* genFindEntry(const void* p, FindRelation relation = FIND_EQ) const override;
    CollectionEntry* getEntryFromContent(const void* p) const override;

  public:
    void attach(ListEntry* entry);
    void attach(ListEntry* entry, bool toFront);
    void attachBefore(ListEntry* entry, ListEntry* before);
    void attachAfter(ListEntry* entry, ListEntry* after);
    void detach(ListEntry* entry);
    void forEach(CollectionEntry* f(ListEntry* list)); // TODO
    void forEach(void* self, CollectionEntry* f(void* self, ListEntry* li)); // TODO
};

class ListEntry : protected CollectionEntry, protected CoreListEntry {
  friend List;

  public:
    ListEntry(List* list=0);
    virtual ~ListEntry() override;

    List* list() const;
    ListEntry* next() const;
    ListEntry* prev() const;

    Collection* getCollection() const override;
    const void* getEntryType() const override;

  private:
    CollectionEntry* getCollectionEntry() const override;
};

};

#endif
