#ifndef nsntl_collection_core_h
#define nsntl_collection_core_h

#include <stdint.h>

#include "compare.h"
#include "malloc.h"

namespace nsntl {

// collection flags
enum class CollectionFlags : uint8_t {
  ALLOW_DUPLICATES = 1
};

class CollectionEntry;

class Collection {
  protected:
    Allocator *entryAllocator;

    Collection();
    Collection(Allocator* entryAllocator);
    virtual ~Collection();

  public:
    void detachAll();
    void deleteAll();
    void* getContentP(const CollectionEntry* e) const;
    CollectionEntry* buildEntry(const void* src=0);
    void deleteEntry(CollectionEntry* entry);

    void attach(CollectionEntry* entry);
    void detach(CollectionEntry* entry);
    bool genHas(const void* p, FindRelation relation = FIND_EQ) const;
    CollectionEntry* genAdd(const void* p);
    bool genRemove(const void* p);
    virtual void genSet(CollectionEntry* entry, const void* p);
    void genForEachEntry(void* self, CollectionEntry* cb(void* self, CollectionEntry* entry)); // TODO
    void genForEachEntry(CollectionEntry* cb(CollectionEntry* entry)); // TODO
    void genForEach(void* self, int cb(void* self, void* p)); // TODO
    void genForEach(int cb(void* p)); // TODO

  /** Provided by Collection implementations (List, Set, etc) */
  private:
    virtual void derAttach(CollectionEntry* entry) = 0;
    virtual void derDetach(CollectionEntry* entry) = 0;

  public:
    virtual const void* getEntryType() const=0;
    virtual uint64_t getSizeofEntry() const=0; // TODO: constexpr ?
    virtual bool isEmpty() const=0;
    virtual uint64_t size() const=0;
    virtual Collection* getCollection() const=0;
    virtual CollectionEntry* firstEntry() const=0;
    virtual CollectionEntry* lastEntry() const=0;
    virtual CollectionEntry* nextOf(const CollectionEntry* entry) const=0;
    virtual CollectionEntry* prevOf(const CollectionEntry* entry) const=0;

  protected:
    virtual void initEntry(void* p)=0;
    virtual void uninitEntry(void* p)=0;
    virtual CollectionEntry* genFindEntry(const void* p, FindRelation relation = FIND_EQ) const=0;
    virtual CollectionEntry* getEntryFromContent(const void* p) const=0;
    
  /** Provided by Content implementations (Uint64Content, etc) */
  protected:
    virtual uint64_t getSizeofContent() const=0;
    virtual void initContent(void* p, const void* src=0)=0;
    virtual void uninitContent(void* p)=0;
    virtual int compareContent(const void* a, const void* b) const=0;
};

class CollectionEntry : public InplaceNew {
  protected:
    virtual ~CollectionEntry();

  public:
    /** Provided by *CollectionEntry */
    virtual Collection* getCollection() const=0;
    virtual const void* getEntryType() const=0;

  private:
    /** Provided by *CollectionEntry */
    virtual CollectionEntry* getCollectionEntry() const=0;
};

};

#endif
