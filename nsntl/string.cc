#include <string.h>

#include "exception.h"
#include "malloc.h"
#include "string.h"
#include "util.h"

using namespace nsntl;

int nsntl::compare(const string a, const string b) {
  if (!a.c->p && !b.c->p)
    return 0;
  else if (!a.c->p)
    return -1;
  else if (!b.c->p)
    return 1;
  else {
    for (uint64_t i=0; i<a.c->len && i<b.c->len; i++) {
      if (a.c->p[i] < b.c->p[i])
        return -1;
      else if (a.c->p[i] > b.c->p[i])
        return 1;
    }
    return nsntl::compare(a.c->len, b.c->len);
  };
};

nsntl::string_core::string_core() {
  this->p = NULL;
  this->ref = 1;
  this->len = 0;
};

nsntl::string_core::string_core(const char* s) {
  if (!s) {
    this->p = NULL;
  } else {
    this->p = Strdup(s);
  }
  this->ref = 1;
  this->len = Strlen(s);
};

nsntl::string_core::~string_core() {
  if (this->p)
    Free(this->p);
};

nsntl::string::string() {
  this->c = new string_core();
};

nsntl::string::string(const char* s) {
  this->c = new string_core(s);
};

nsntl::string::string(const uint128_t& n) {
  string s = n.toString();
  this->c = s.c;
  this->c->ref++;
};

nsntl::string::string(uint64_t n) : string(uint128_t(n)) {
  // nothing now
};

nsntl::string::~string() {
  this->detach_core();
};

void nsntl::string::detach_core() {
  if (this->c->ref > 0)
    this->c->ref--;
  else
    delete this->c;
};

const nsntl::string& nsntl::string::operator=(const string& s) {
  this->detach_core();
  this->c = s.c;
  this->c->ref++;
  return *this;
};

const nsntl::string& nsntl::string::operator=(char const * s) {
  this->detach_core();
  this->c->p = Strdup(s);
  this->c->ref = 1;
  this->c->len = Strlen(s);
  return *this;
}

const nsntl::string nsntl::string::operator+(const nsntl::string& s) const {
  nsntl::string ret;
  ret.c->len = this->c->len + s.c->len;
  ret.c->p = (char*)nsntl::Malloc(ret.c->len + 1);
  nsntl::Strcpy(ret.c->p, this->c->p);
  nsntl::Strcpy(ret.c->p + this->c->len, s.c->p);
  ret.c->p[ret.c->len]=0;
  return ret;
};

const char* nsntl::string::c_str() const {
  return this->c->p;
};

uint64_t nsntl::string::length() const {
  if (this->c)
    return this->c->len;
  else
    return 0;
};

uint64_t nsntl::StringContent::getSizeofContent() const {
  return sizeof(string);
};

void nsntl::StringContent::initContent(void* p, const void* src) {
  if (!src)
    new(p) string();
  else
    new(p) string(*reinterpret_cast<string*>(const_cast<void*>(src)));
};

void nsntl::StringContent::uninitContent(void* p) {
  reinterpret_cast<string*>(p)->~string();
};

int nsntl::StringContent::compareContent(const void* a, const void* b) const {
  return compare(*reinterpret_cast<const string*>(a), *reinterpret_cast<const string*>(b));
};

bool nsntl::StringContent::has(const string p, FindRelation relation) const {
  return genFindEntry(&p, relation);
};

string nsntl::StringContent::find(const string p, FindRelation relation) const {
  CollectionEntry* entry = findEntry(p, relation);
  if (entry)
    return get(entry);
  else
    throw LogicException(LogicError::NOT_FOUND);
};

CollectionEntry* nsntl::StringContent::findEntry(const string p, FindRelation relation) const {
  return genFindEntry(&p, relation);
};

CollectionEntry* nsntl::StringContent::add(const string p) {
  return genAdd(&p);
};

bool nsntl::StringContent::remove(const string p) {
  return genRemove(&p);
};

string nsntl::StringContent::get(const CollectionEntry* e) const {
  return *reinterpret_cast<string*>(getContentP(e));
};

void nsntl::StringContent::set(CollectionEntry* entry, const string p) {
  genSet(entry, &p);
};
