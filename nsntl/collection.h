#ifndef nsntl_collection_h
#define nsntl_collection_h

#include "list.h"
#include "map.h"
#include "set.h"
#include "smartptr.h"
#include "types.h"

namespace nsntl {

class Uint64List : public List, public Uint64Content {
};

class StringList : public List, public StringContent {
};

class PtrList : public List, public PtrContent {
};

class SmartPtrList : public List, public SmartPtrContent {
};

class Uint64Set : public Set, public Uint64Content {
  public:
    using Uint64Content::find;
};

class StringSet : public Set, public StringContent {
  public:
    using StringContent::find;
};

class PtrSet : public Set, public PtrContent {
  public:
    using PtrContent::find;
};

class SmartPtrSet : public Set, public SmartPtrContent {
  public:
    using SmartPtrContent::find;
};

class Array : public MapKeyUint64, public MapValueSmartPtr { //TODO;
};

class Dict : public MapKeyString, public MapValueString { //TODO;
};

};

#endif
