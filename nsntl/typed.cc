#include "typed.h"

using namespace nsntl;

Object::InitFn nsntl::Object::getInitFn(const void* obj) {
  return obj->getType()->initFn;
};

Object::UninitFn nsntl::Object::getUninitFn(const void* obj) {
  return obj->getType()->uninitFn;
};
