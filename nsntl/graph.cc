#include <stddef.h>
#include <stdlib.h>

#include "graph.h"

#pragma GCC diagnostic ignored "-Winvalid-offsetof"

using namespace nsntl;

nsntl::GraphException::GraphException(GraphError code) : ErrorCodeException((uint64_t)code) {
  // nothing now
};

nsntl::Edge::Edge(Vertex *src, Vertex *dst) {
  if (src || dst) {
    if (!src || !dst)
      throw GraphException(GraphError::HALF_ATTACHED_EDGE_NOT_ALLOWED);
    if (src != dst)
      throw GraphException(GraphError::CROSS_GRAPH_EDGE_NOT_ALLOWED);
    attach(src, dst);
  }
};

nsntl::Edge::~Edge() {
  // nothing now
};

nsntl::Graph* nsntl::Edge::getGraph() const {
  if (graphEntry.list())
    return Graph::fromEdgeList(graphEntry.list());
  else
    return 0;
};

nsntl::Vertex* nsntl::Edge::getSrc() const {
  if (srcEntry.list())
    return Vertex::fromDstList(srcEntry.list());
  else
    return 0;
};

nsntl::Vertex* nsntl::Edge::getDst() const {
  if (dstEntry.list())
    return Vertex::fromSrcList(dstEntry.list());
  else
    return 0;
};

bool nsntl::Edge::isAttached() const {
  return getGraph();
};

void nsntl::Edge::attach(Vertex *src, Vertex *dst) {
  if (isAttached()) {
    throw LogicException(LogicError::ALREADY_ATTACHED);
  }
  if (!src || !dst) {
    throw GraphException(GraphError::VERTEX_NOT_EXISTS);
  }
  if (!src->getGraph() || !dst->getGraph()) {
    throw GraphException(GraphError::GRAPH_NOT_EXISTS);
  }
  if (src->getGraph() != dst->getGraph()) {
    throw GraphException(GraphError::CROSS_GRAPH_EDGE_NOT_ALLOWED);
  }
  src->getGraph()->edgeList.attach(&graphEntry);
  src->dstList.attach(&srcEntry);
  dst->srcList.attach(&dstEntry);
};

void nsntl::Edge::detach() {
  if (!srcEntry.list() || !dstEntry.list() || !graphEntry.list()) {
    throw LogicException(LogicError::ALREADY_DETACHED);
  };
  getGraph()->edgeList.detach(&graphEntry);
  getSrc()->dstList.detach(&srcEntry);
  getDst()->dstList.detach(&dstEntry);
};

nsntl::Edge* nsntl::Edge::fromSrcEntry(CoreListEntry* srcEntry) {
  size_t offset = offsetof(Edge, srcEntry);
  return reinterpret_cast<Edge*>(reinterpret_cast<uint8_t*>(srcEntry) - offset);
};

nsntl::Edge* nsntl::Edge::fromDstEntry(CoreListEntry* dstEntry) {
  size_t offset = offsetof(Edge, dstEntry);
  return reinterpret_cast<Edge*>(reinterpret_cast<uint8_t*>(dstEntry) - offset);
};

nsntl::Edge* nsntl::Edge::fromGraphEntry(CoreListEntry* graphEntry) {
  size_t offset = offsetof(Edge, graphEntry);
  return reinterpret_cast<Edge*>(reinterpret_cast<uint8_t*>(graphEntry) - offset);
};

nsntl::Vertex::Vertex(Graph* graph) : srcList(), dstList(), graphEntry() {
  if (graph)
    attach(graph);
};

nsntl::Vertex::~Vertex() {
  detach();
};

nsntl::Graph* nsntl::Vertex::getGraph() const {
  if (graphEntry.list())
    return Graph::fromVertexList(graphEntry.list());
  else
    return 0;
};

bool nsntl::Vertex::isAttached() const {
  return graphEntry.list();
};

void nsntl::Vertex::attach(Graph *graph) {
  if (isAttached()) {
    throw LogicException(LogicError::ALREADY_ATTACHED);
  };
  graph->vertexList.attach(&graphEntry);
};

void nsntl::Vertex::detach() {
  if (!isAttached()) {
    throw LogicException(LogicError::ALREADY_DETACHED);
  }
  if (!srcList.isEmpty() || !dstList.isEmpty()) {
    throw GraphException(GraphError::CAN_NOT_DETACH_VERTEX_WITH_EDGES);
  }
  getGraph()->vertexList.detach(&graphEntry);
};

nsntl::Vertex* nsntl::Vertex::fromSrcList(CoreList* srcList) {
  size_t offset = offsetof(Vertex, srcList);
  return reinterpret_cast<Vertex*>(reinterpret_cast<uint8_t*>(srcList) - offset);
};

nsntl::Vertex* nsntl::Vertex::fromDstList(CoreList* dstList) {
  size_t offset = offsetof(Vertex, dstList);
  return reinterpret_cast<Vertex*>(reinterpret_cast<uint8_t*>(dstList) - offset);
};

nsntl::Vertex* nsntl::Vertex::fromGraphEntry(CoreListEntry* graphEntry) {
  size_t offset = offsetof(Vertex, graphEntry);
  return reinterpret_cast<Vertex*>(reinterpret_cast<uint8_t*>(graphEntry) - offset);
};

nsntl::Graph::Graph() {
  // nothing now
};

nsntl::Graph::~Graph() {
  if (!edgeList.isEmpty())
    abort();
  if (!vertexList.isEmpty())
    abort();
};

nsntl::Graph* nsntl::Graph::fromEdgeList(CoreList* edgeList) {
  size_t offset = offsetof(Graph, edgeList);
  return reinterpret_cast<Graph*>(reinterpret_cast<uint8_t*>(edgeList) - offset);
};

nsntl::Graph* nsntl::Graph::fromVertexList(CoreList* vertexList) {
  size_t offset = offsetof(Graph, vertexList);
  return reinterpret_cast<Graph*>(reinterpret_cast<uint8_t*>(vertexList) - offset);
};
