#include "block.h"
#include "exception.h"
#include "util.h"

using namespace nsntl;

nsntl::Block::Block() {
  this->addr = 0;
  this->size = 0;
};

nsntl::Block::Block(void* addr, uint64_t size) {
  this->addr = addr;
  this->size = size;
};

nsntl::Block::Block(string s) {
  this->addr = Strdup(s.c_str());
  this->size = s.length();
};

nsntl::Block::~Block() {
  // nothing now
};

void* nsntl::Block::getAddr() const {
  return addr;
};

void nsntl::Block::setAddr(void* addr) {
  this->addr = addr;
};

uint64_t nsntl::Block::getSize() const {
  return size;
};

void nsntl::Block::setSize(uint64_t size) {
  this->size = size;
};

nsntl::Block::operator nsntl::string() const {
  char* p = (char*)Malloc(size+1);
  Strncpy(p, (char*)addr, size);
  p[size] = 0;
  string ret(p);
  Free(p);
  return ret;
};

nsntl::DynBlock::DynBlock(uint64_t size) : Block(0, size) {
  if (size)
    addr=Malloc(size);
  else
    addr=0;
};

nsntl::DynBlock::DynBlock(const Block& block) : DynBlock(block.getSize()) {
  Memcpy(getAddr(), block.getAddr(), block.getSize());
};

nsntl::DynBlock::~DynBlock() {
  if (addr)
    Free(addr);
};

void nsntl::DynBlock::setAddr(void* addr) {
  throw LogicException(LogicError::DYNAMIC_BLOCK_CAN_NOT_BE_MOVED);
};

void nsntl::DynBlock::setSize(uint64_t size) {
  if (size == this->size)
    return;

  if (!size) {
    if (this->size) {
      Free(this->addr);
      this->addr=0;
    }
  } else {
    if (!this->size)
      addr=Malloc(size);
    else
      addr=Realloc(this->addr, size);
  }
  this->size=size;
};
