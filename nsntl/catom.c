#include <stdatomic.h>

#include "catom.h"

void nsntl_atomic_u16_init(volatile uint16_t* p, volatile uint16_t desired) {
  atomic_init(p, desired);
};

volatile uint16_t nsntl_atomic_u16_load(volatile uint16_t* p) {
  return atomic_load_explicit(p, memory_order_seq_cst);
};

void nsntl_atomic_u16_store(volatile uint16_t* p, volatile uint16_t n) {
  return atomic_store_explicit(p, n, memory_order_seq_cst);
};

volatile uint16_t nsntl_atomic_u16_inc(volatile uint16_t* p) {
  return atomic_fetch_add_explicit(p, 1, memory_order_seq_cst) + 1;
};

volatile uint16_t nsntl_atomic_u16_exchange(volatile uint16_t* p, volatile uint16_t v) {
  return atomic_exchange_explicit(p, v, memory_order_seq_cst);
};

void nsntl_atomic_u32_init(volatile uint32_t* p, volatile uint32_t desired) {
  atomic_init(p, desired);
};

volatile uint32_t nsntl_atomic_u32_load(volatile uint32_t* p) {
  return atomic_load_explicit(p, memory_order_seq_cst);
};

void nsntl_atomic_u32_store(volatile uint32_t* p, volatile uint32_t n) {
  return atomic_store_explicit(p, n, memory_order_seq_cst);
};

volatile uint32_t nsntl_atomic_u32_inc(volatile uint32_t* p) {
  return atomic_fetch_add_explicit(p, 1, memory_order_seq_cst) + 1;
};

volatile uint32_t nsntl_atomic_u32_exchange(volatile uint32_t* p, volatile uint32_t v) {
  return atomic_exchange_explicit(p, v, memory_order_seq_cst);
};

void nsntl_atomic_u64_init(volatile uint64_t* p, volatile uint64_t desired) {
  atomic_init(p, desired);
};

volatile uint64_t nsntl_atomic_u64_load(volatile uint64_t* p) {
  return atomic_load_explicit(p, memory_order_seq_cst);
};

void nsntl_atomic_u64_store(volatile uint64_t* p, volatile uint64_t n) {
  return atomic_store_explicit(p, n, memory_order_seq_cst);
};

volatile uint64_t nsntl_atomic_u64_inc(volatile uint64_t* p) {
  return atomic_fetch_add_explicit(p, 1, memory_order_seq_cst) + 1;
};

volatile uint64_t nsntl_atomic_u64_exchange(volatile uint64_t* p, volatile uint64_t v) {
  return atomic_exchange_explicit(p, v, memory_order_seq_cst);
};
