#ifndef nsntl_map_h
#define nsntl_map_h

#include "avl.h"

/** Example:
 * MyObj *myObj = *anyMap[12];
 *
 * While a Map has various textual methods (get, del, etc), these are all protected or private. The intention is
 * to go everything in a Map by overloaded operators.
 *
 * These are:
 *
 * 1. Array subscription:
 *
 * anyMap[12] --> returns a MapEntry& to the element indexed by 12, or throws exception if none
 *
 * 2. Dereference:
 * *anyMap[12] --> the overloaded dereference operator on a MapEntry returns its value.
 *
 * Internally, a Map has various derived clases which tricks this with virtual functions.
 *
 * These have 3 groups:
 *
 * 1. Specifying a Map Key type. These implement the Map  operator[].
 * 2. Specifying the Map Value type. These implement the MapEntry dereference operator.
 * 3. Specifying the allocations/deallocations of the MapEntries. A MapSoftEntry only detaches deallocated MapEntries, while MapHardEntry deletes them.
 *
 * MapEntry creation happens by the (really) generic buildEntry method, removal happens by unsetEntry provided by the deallocation classes.
 */

namespace nsntl {

class MapEntry;

class Map : protected Avl { // TODO: this should be a Collection
  friend MapEntry;

  private:
    Allocator* allocator;

  protected:
    /** Temporary entry for handling things like myMap[idx]=thing; */
    MapEntry* hotEntry;

    Map();
    Map(Allocator& allocator);
    ~Map();

    int compare(const AvlEntry* a, const AvlEntry* b) const final override;
    static int compareEntryKey(const AvlEntry* a, const void* key);

    MapEntry* firstEntry() const;
    MapEntry* lastEntry() const;

    // Yes, this is how it should be done. This is the most generic with the least overhead.

    /** Shows the byte size of a key */
    virtual uint64_t sizeofKey() const = 0;
    /** Initializes a key on undefined data. Argument is the address of the key. Default is no-op. */
    virtual void initKey(void* p) const;
    /** Uninitializes a key. Is called only on valid keys. Default is no-op. */
    virtual void uninitKey(void* p) const;
    /** Compares two keys given by their void* address. */
    virtual int compareKeys(const void* a, const void* b) const=0;
    /** Gives the address of a key in a MapEntry */
    void* getKeyP(const MapEntry* e) const;

    /** Shows the byte size of a value */
    virtual uint64_t sizeofValue() const = 0;
    /** Initializes a value on random data. Argument is the address of the value. Default is no-op. */
    virtual void initValue(void* p) const;
    /** Uninitializes a value. Is called only on initialized values. Default is no-op. */
    virtual void uninitValue(void* p) const;
    /** Gives the address of a value in a MapEntry */
    void* getValueP(const MapEntry* e) const;

    /** Generic size of a map entry */
    uint64_t sizeofEntry() const;
    /** Initializes an entry for the map (but it does not attach()-es) */
    MapEntry* buildEntry();
    /** Uninitializes an entry: detach, uninitValue, uninitKey, ~MapEntry */
    void deleteEntry(MapEntry* entry);

    /** Consistency check of hotEntry. Throws exception if it is NULL or points to another Map */
    void checkHot() const;

    /** Changes hotEntry to argument. Possibly removes already existing unattached hot element. */
    void setHot(MapEntry* entry);

    MapEntry* genFindEntry(const void* key, FindRelation rel = FIND_EQ) const;

    void genFind(const void* key, FindRelation rel = FIND_EQ);

    static Map* fromAvl(Avl* a);

  public:
    uint64_t size();
    Map& first();
    Map& last();
    Map& next();
    Map& prev();
    Map& unset();
    operator bool() const;
};

class MapEntry : public AvlEntry {
  friend class Map;

  protected:
    MapEntry();
    ~MapEntry();

    Map* map() const;
    MapEntry* next() const;
    MapEntry* prev() const;

    void* operator new(size_t size, void* p);
};

class MapKeyUint64 : protected virtual Map {
  protected:
    uint64_t sizeofKey() const;
    int compareKeys(const void* a, const void* b) const;

  public:
    Map& operator[](uint64_t i);
    Map& find(uint64_t i, FindRelation rel = FIND_EQ) const;
    uint64_t getKey() const;
};

class MapKeyString : protected virtual Map { //TODO;
};

class MapKeyPtr : protected virtual Map { //TODO;
};

class MapKeySmartPtr : protected virtual Map { //TODO;
};

class MapValueUint64 : protected virtual Map { //TODO;
};

class MapValueString : protected virtual Map { //TODO;
};

class MapValuePtr : protected virtual Map { //TODO;
};

class MapValueSmartPtr : protected virtual Map { //TODO;
};

// MapKey... : operator[] -> gives back itself. 
// myMap[5]; myMap=thing;
/*

...won't work. What works:

myMap.key(5).value(thing);

myMap.key(5) : tmpEntry.key := 5
myMap.value(thing) : tmpEntry.value = thing; tmpEntry.attach(this);

Ok, now try to convert it to operator-overloads:

myMap.key(5) will be myMap[5]. DONE!

*/

/*
class MapKeySoftPtr : protected virtual Map {
  public:
    bool has(Comparable* key) const;
};

class MapKeySoftPtrEntry : protected virtual MapEntry {
};

class MapKeyHardPtr : protected virtual Map {
};

class MapKeyHardPtrEntry : protected virtual MapEntry {
};

class MapKeyString : protected virtual Map {
  public:
};

class MapValueSoftPtr : protected virtual Map {
  private:
    SoftPtr v;

  public:
    SoftPtr operator*() const;
};

class MapValueHardPtr : protected virtual Map {
};

class MapValueString : protected virtual Map {
};

class MapEntrySoft : protected virtual Map {
  protected:
    virtual void unsetEntry(MapEntry* e);
};

class MapEntrySoftEntry : protected virtual MapEntry {
};

class MapEntryHard : protected virtual Map {
  protected:
    virtual void unsetEntry(MapEntry* e);
};

class MapEntryHardEntry : protected virtual MapEntry {
};

*/

/**

class SysFds : public MapKeyUint64, public MapValueHardPtr {
};

class Dict : public MapKeyString, public MapValueString {
};

Dict dict;
dict["alpha"]="one";
std::cout << *dict["alpha"];

*/

};
#endif
