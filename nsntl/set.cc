#include <stdlib.h>

#include "exception.h"
#include "set.h"

using namespace nsntl;

static const void* const setEntryType = 0;

nsntl::Set::Set(uint8_t flags) : Set(0, flags) {
  // nothing now
};

nsntl::Set::Set(Allocator* entryAllocator, uint8_t flags) : Collection(entryAllocator), Avl(flags) {
  // nothing now
};

nsntl::Set::~Set() {
  // non-empty Collections can not be deleted, use deleteAll/detachAll
  // Yeah, that sucks in c++
  if (!this->isEmpty())
    abort();
};

void nsntl::Set::genSet(CollectionEntry* entry, const void* v) {
  if (getEntryType() != entry->getEntryType())
    throw LogicException(LogicError::RUNTIME_TYPE_CHECK_FAILS);
  SetEntry* setEntry = static_cast<SetEntry*>(entry);
  detach(setEntry);
  Collection::genSet(entry, v);
  attach(setEntry);
};

void nsntl::Set::derAttach(CollectionEntry* entry) {
  attach(static_cast<SetEntry*>(entry));
};

void nsntl::Set::derDetach(CollectionEntry* entry) {
  detach(static_cast<SetEntry*>(entry));
};

const void* nsntl::Set::getEntryType() const {
    return reinterpret_cast<const void*>(&setEntryType);
};

uint64_t nsntl::Set::getSizeofEntry() const {
  return sizeof(SetEntry);
};

bool nsntl::Set::isEmpty() const {
  return Avl::isEmpty();
};

uint64_t nsntl::Set::size() const {
  return root ? root->getSize() : 0;
};

Collection* nsntl::Set::getCollection() const {
  return static_cast<Collection*>(const_cast<Set*>(this));
};

CollectionEntry* nsntl::Set::firstEntry() const {
  return static_cast<SetEntry*>(Avl::firstEntry());
};

CollectionEntry* nsntl::Set::lastEntry() const {
  return static_cast<SetEntry*>(Avl::lastEntry());
};

CollectionEntry* nsntl::Set::nextOf(const CollectionEntry* entry) const {
  const SetEntry* se = static_cast<const SetEntry*>(entry);
  return static_cast<SetEntry*>(se->next());
};

CollectionEntry* nsntl::Set::prevOf(const CollectionEntry* entry) const {
  const SetEntry* se = static_cast<const SetEntry*>(entry);
  return static_cast<SetEntry*>(se->prev());
};

void nsntl::Set::initEntry(void* p) {
  new (p) SetEntry();
};

void nsntl::Set::uninitEntry(void* p) {
  SetEntry* entry = reinterpret_cast<SetEntry*>(p);
  entry->~SetEntry();
};

nsntl::CollectionEntry* nsntl::Set::genFindEntry(const void* p, FindRelation relation) const {
  AvlEntry* avlEntry = Avl::find(p, relation, Set::findHelper);
  SetEntry* setEntry = static_cast<SetEntry*>(avlEntry);
  return setEntry;
};

CollectionEntry* nsntl::Set::getEntryFromContent(const void* p) const {
  return reinterpret_cast<const SetEntry*>(reinterpret_cast<const uint8_t*>(p) - getSizeofEntry());
};

void nsntl::Set::attach(SetEntry* entry) {
  entry->AvlEntry::attach(this);
};

void nsntl::Set::detach(SetEntry* entry) {
  entry->AvlEntry::detach();
};

void nsntl::Set::forEach(CollectionEntry* f(SetEntry* list)) {
  TODO(); // nsntl::Set::forEach
};

void nsntl::Set::forEach(void* self, CollectionEntry* f(void* self, SetEntry* li)) {
  TODO(); // nsntl::Set::forEach
};

int nsntl::Set::findHelper(const AvlEntry* a, const void* v) {
  const SetEntry* setEntry = static_cast<const SetEntry*>(a);
  Set* set = setEntry->getSet();
  void* aContent = set->getContentP(setEntry);
  return set->compareContent(aContent, v);
};

int nsntl::Set::compare(const AvlEntry* a, const AvlEntry* b) const {
  const SetEntry* setEntryA = SetEntry::fromAvlEntry(const_cast<AvlEntry*>(a));
  const SetEntry* setEntryB = SetEntry::fromAvlEntry(const_cast<AvlEntry*>(b));
  return compareContent(getContentP(setEntryA), getContentP(setEntryB));
};

nsntl::Avl* nsntl::Set::getAvl() const {
  return const_cast<Set*>(this);
};

nsntl::SetEntry::SetEntry() : AvlEntry() {
  // nothing now
};

nsntl::SetEntry::SetEntry(Set* s) : AvlEntry(s->getAvl()) {
  // nothing now
};

nsntl::SetEntry::~SetEntry() {
  // nothing now
};

nsntl::Set* nsntl::SetEntry::getSet() const {
  return static_cast<Set*>(avl());
};

nsntl::SetEntry* nsntl::SetEntry::next() const {
  return static_cast<SetEntry*>(AvlEntry::next());
};

nsntl::SetEntry* nsntl::SetEntry::prev() const {
  return static_cast<SetEntry*>(AvlEntry::prev());
};

nsntl::Collection* nsntl::SetEntry::getCollection() const {
  return getSet();
};

const void* nsntl::SetEntry::getEntryType() const {
    return reinterpret_cast<const void*>(&setEntryType);
};

nsntl::SetEntry* nsntl::SetEntry::fromAvlEntry(AvlEntry* a) {
  return static_cast<SetEntry*>(a);
};

nsntl::CollectionEntry* nsntl::SetEntry::getCollectionEntry() const {
  return const_cast<SetEntry*>(this);
};
