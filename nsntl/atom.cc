#include "atom.h"

using namespace nsntl;

nsntl::atomic_bool::atomic_bool(bool v) {
  this->assign(v);
};

nsntl::atomic_bool& nsntl::atomic_bool::operator=(bool v) {
  this->assign(v);
  return *this;
};

void nsntl::atomic_bool::assign(bool v) {
  nsntl_atomic_u32_init(&this->v, v?1:0);
}

bool nsntl::atomic_bool::exchange(bool v) {
  return nsntl_atomic_u32_exchange(&this->v, v)?true:false;
};

nsntl::atomic_u16::atomic_u16(uint16_t v) {
  this->assign(v);
};

nsntl::atomic_u16& nsntl::atomic_u16::operator=(uint16_t v) {
  this->assign(v);
  return *this;
};

nsntl::atomic_u16& nsntl::atomic_u16::operator++() {
  nsntl_atomic_u16_inc(&this->v);
  return *this;
};

nsntl::atomic_u16::operator uint16_t() {
  return nsntl_atomic_u16_load(&this->v);
};

void nsntl::atomic_u16::assign(uint16_t v) {
  nsntl_atomic_u16_init(&this->v, v);
};

uint16_t nsntl::atomic_u16::exchange(uint16_t v) {
  return nsntl_atomic_u16_exchange(&this->v, v);
};

nsntl::atomic_u64::atomic_u64(uint64_t v) {
  this->assign(v);
};

nsntl::atomic_u64& nsntl::atomic_u64::operator=(uint64_t v) {
  this->assign(v);
  return *this;
};

nsntl::atomic_u64& nsntl::atomic_u64::operator++() {
  nsntl_atomic_u64_inc(&this->v);
  return *this;
};

nsntl::atomic_u64::operator uint64_t() {
  return nsntl_atomic_u64_load(&this->v);
};

void nsntl::atomic_u64::assign(uint64_t v) {
  nsntl_atomic_u64_init(&this->v, v);
};

uint64_t nsntl::atomic_u64::exchange(uint64_t v) {
  return nsntl_atomic_u64_exchange(&this->v, v);
};

nsntl::Counter::Counter(uint64_t next) {
  init(next);
};

void nsntl::Counter::init(uint64_t next) {
  this->next = next;
};

uint64_t nsntl::Counter::getId() {
  return ++this->next;
};

nsntl::LockException::LockException(uint64_t errorCode) : ErrorCodeException(errorCode) {
};

nsntl::Lock::Lock() {
  lock = false;
};

nsntl::Lock::~Lock() {
  // nothing now
};

bool nsntl::Lock::tryLock() {
  return lock.exchange(true);
};

void nsntl::Lock::unlock() {
  bool result = lock.exchange(false);
  if (!result)
    throw LockException(ALREADY_UNLOCKED);
  return;
};

nsntl::Spinlock::Spinlock(Lock& lock) {
  this->lock = &lock;
  if (!lock.tryLock())
    throw LockException(CANT_LOCK);
};

nsntl::Spinlock::~Spinlock() {
  lock->unlock();
};
