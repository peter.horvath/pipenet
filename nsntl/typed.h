#ifndef nsntl_typed_h
#define nsntl_typed_h

#include <stdint.h>

namespace nsntl {

typedef struct {
  uint64_t size;
  uint64_t offset;
} Type;

class Object {
  public:
    void* operator new(size_t size, void* p);

    virtual ~Object();
    virtual const Type* getType() const=0;

  private:
    virtual int compare(const Object* obj) const=0;
    virtual void set(const void* tgt)=0;
};

int compare(const Object& a, const Object& b);
int compare(const Object& a, const Object* b);
int compare(const Object* a, const Object& b);
int compare(const Object* a, const Object* b);

void set(Object* o, const void* tgt);

class Parent {
  public:
    virtual setMember(Object* o, const void* tgt)=0;
    virtual ~Parent();
};

/* What is needed:

1. Getting type, size, offset and Object* from void*
2. Getting type, size, offset and void* from Object*
3. These all should be static methods (functions)

What all Object has to be able to do:
1. Initialized on a fixed memory address.
2. Uninitialized there. Uninitialization should leave the Object in a consistent, but non-interacting, free-able state.

(1) and (2) can be done by virtual functions. How to do (3)?

1. Q: How to get void* from Object* ? A: From a virtual method in Object.
2. Q: But how to do this by a global method? A: It is impossible. Objects must be in a valid state until destruction, to make the virtual method working.
3. Q: How to get Object* from void*? A: We need to have a Container class.

Alternate solution: somehow it should be guaranteed, that Object is always at the start of the memory area of its derived classes.

Q: Ok, but how to have a global init/uninit/sizeof then?
A: Object has a pointer to a type struct. For all the types, such a struct must exist.

Q: How are then objects constructed?
A: The current allocators are refactored to builders. They allocate/deallocate and call the in-place constructor/destructor.

Q: How about collections?
A: They will have a close relation with the allocators.

Q: Ok, but generic object constructors require variadic argument passing.
A: Yes. Constructors will be callable only by a null-terminated list of Objects as parameters.

Yet another idea: allocators will also construct/destruct the Objects. The will give back Object*. The conversion between Object* and void* will happen
by a global function registered in the Type descriptor struct. The destructor of the Objects will let them in a valid, but resource/pointer/interaction-less
state, which can be safely free-d. The constructor will create a valid object, based on the data given to it in a void* pointer.

Q: How to handle statically allocated Objects (stack/global/data member)?
A: They don't need to be Objects.

Q: How about Collections, again?
A: They will have a pointer to the type struct of their entries. On insert of an Object with a different type, they will throw fatal exception.

Q: Why should be all Object also comparable (by the virtual method "int compare(Object*) const=0")?
A: I did not found a single case when the comparison would be impossible (by their adreses if there is no better), and also not a single case when it would not be needed.

Q: Why does even a List collection require comparison?
A: Linear search requires at least a comparison for equality. Although it will be highly discouraged, due to its O(n) nature - but if the size of the List is O(logn) or O(1),
     it is not a problem.

Q: How does the interaction with the parents (collections, containing objects etc) happen?
A: All object has a pointer to its parent Object. It can be 0. (TODO: decide, what to do if it is)

Call flow, if a SmartPtd, refered by SmartPtr CollectionEntries, is deleted:

1. SmartPtd destructor walks on the SmartPtr s refering it and sets them all to 0. This setting happens by a global setter.
2. The global setter checks, if the Obj has a parent. If it has, its setMember(Object* member, const void* src)=0 virtual method is called.

So. We have a set(const void* src) on all Objects, and a setMember(Object*, const void* src) on all parents.

Setting to a 0 means the deletion of the Object.

*/
};

#endif
