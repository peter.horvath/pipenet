#include <stdlib.h>

#include "collection_core.h"
#include "exception.h"

using namespace nsntl;

nsntl::Collection::Collection() : Collection(0) {
  // nothing now
};

nsntl::Collection::Collection(Allocator* entryAllocator) {
  if (entryAllocator)
    this->entryAllocator = entryAllocator;
  else
    this->entryAllocator = const_cast<Allocator*>(static_cast<const Allocator*>(&defaultAllocator));
};

nsntl::Collection::~Collection() {
  // TODO: something should be done with the destruction of non-empty collections
};

void nsntl::Collection::detachAll() {
  while (CollectionEntry* entry = firstEntry())
    detach(entry);
};

void nsntl::Collection::deleteAll() {
  while (CollectionEntry* entry = firstEntry())
    deleteEntry(entry);
};

void* nsntl::Collection::getContentP(const CollectionEntry* entry) const {
  uint8_t* entryP = const_cast<uint8_t*>(reinterpret_cast<const uint8_t*>(entry));
  uint8_t* contentP = entryP + getSizeofEntry();
  return reinterpret_cast<void*>(contentP);
};

CollectionEntry* nsntl::Collection::buildEntry(const void* src) {
  void* pEntry = entryAllocator->alloc(getSizeofEntry() + getSizeofContent());
  CollectionEntry* entry = reinterpret_cast<CollectionEntry*>(pEntry);
  initEntry(entry);
  void* c = getContentP(entry);
  initContent(c, src);
  return entry;
};

void nsntl::Collection::deleteEntry(CollectionEntry* entry) {
  detach(entry);
  uninitContent(getContentP(entry));
  uninitEntry(entry);
  entryAllocator->free(entry);
};

void nsntl::Collection::attach(CollectionEntry* entry) {
  if (entry->getCollection())
    throw LogicException(LogicError::ALREADY_ATTACHED);
  if (getEntryType() != entry->getEntryType())
    throw LogicException(LogicError::RUNTIME_TYPE_CHECK_FAILS);
  derAttach(entry);
};

void nsntl::Collection::detach(CollectionEntry* entry) {
  if (entry->getCollection() != this)
    throw LogicException(LogicError::NOT_MY_ENTRY);
  if (getEntryType() != entry->getEntryType())
    throw LogicException(LogicError::RUNTIME_TYPE_CHECK_FAILS);
  derDetach(entry);
};

bool nsntl::Collection::genHas(const void* p, FindRelation relation) const {
  return genFindEntry(p, relation);
};

CollectionEntry* nsntl::Collection::genAdd(const void* p) {
  CollectionEntry* ret = buildEntry(p);
  attach(ret);
  return ret;
};

bool nsntl::Collection::genRemove(const void* p) {
  CollectionEntry* ret = genFindEntry(p);
  if (!ret)
    throw LogicException(LogicError::NOT_FOUND);
  deleteEntry(ret);
  return ret;
};

void nsntl::Collection::genSet(CollectionEntry* entry, const void* p) {
  uninitContent(getContentP(entry));
  initContent(getContentP(entry), p);
};

nsntl::CollectionEntry::~CollectionEntry() {
  // TODO: something generic should be done to CollectionEntries
};
