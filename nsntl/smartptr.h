#ifndef nsntl_smartptr_h
#define nsntl_smartptr_h

/** Reference-counting smart pointer implementation. */

#include "list_core.h"
#include "collection_core.h"
#include "compare.h"

namespace nsntl {

/**
 * Target of smart pointers. Maintains an internal list of pointers pointed to it.
 * On deletion, all the incoming pointers are invalidated (set to zero).
 *
 * If the list of the incoming pointers becomes empty, and the object is not fixed, the
 * object is deleted (constructor called + Allocator frees it up)
 */

class SmartPtr;

class SmartPtd {
  friend SmartPtr;

  private:
    CoreList pointedBy;
    Allocator* allocator;
    bool fixed;

  protected:
    SmartPtd();
    SmartPtd(bool fixed);
    SmartPtd(const Allocator& allocator);
    SmartPtd(const Allocator& allocator, bool fixed);
    virtual ~SmartPtd();

  public:
    bool isFixed() const;
    void setFixed(bool fixed=false);
    Allocator* getAllocator();
    void setAllocator(const Allocator& allocator);

    static void del(SmartPtd& p);
    static void del(SmartPtd* p);

  private:
    static SmartPtd* fromSmartPtdBy(CoreList* pointedBy);
};

class SmartPtr : public InplaceNew { // TODO: it should be split to RefcPtr and GcPtr
  friend SmartPtd;

  private:
    CoreListEntry entry;

  public:
    typedef void (*Setter)(SmartPtr* p, SmartPtd* tgt);

    SmartPtr();
    SmartPtr(SmartPtd& target);
    SmartPtr(SmartPtd* target);
    SmartPtr(const SmartPtr& ptr);
    virtual ~SmartPtr();

    SmartPtd& get() const;
    SmartPtd* getPtr() const;
    void set(SmartPtd& tgt);
    void set(SmartPtd* tgt);
    bool isNull() const;
    void setNull();

    static void defaultSetter(SmartPtr* ptr, SmartPtd* ptd);
    virtual Setter getSetter() const;

    static void set(SmartPtr& ptr, SmartPtd& target);
    static void set(SmartPtr& ptr, SmartPtd* target);
    static void set(SmartPtr* ptr, SmartPtd& target);
    static void set(SmartPtr* ptr, SmartPtd* target);

  private:
    static SmartPtr* fromEntry(CoreListEntry* entry);
};

class ComparableSmartPtd : public SmartPtd, public Comparable {
  protected:
    ComparableSmartPtd();
    ComparableSmartPtd(bool fixed);
    ComparableSmartPtd(const Allocator& allocator);
    ComparableSmartPtd(const Allocator& allocator, bool fixed);
    virtual ~ComparableSmartPtd() override;
};

/** A pointer pointing to a ComparableSmartPtd object. It is also a Comparable - the comparison method
 * forwards it to the pointed Comparables (NULLs are smaller than everything, and equal only
 * themselves).
 *
 * !!! The default compare method works only on ComparableSmartPtrs!
*/
class ComparableSmartPtr : public virtual SmartPtr, public Comparable {
  public:
    ComparableSmartPtr();
    ComparableSmartPtr(ComparableSmartPtd& target);
    ComparableSmartPtr(ComparableSmartPtd* target);
    ComparableSmartPtr(const ComparableSmartPtr& ptr);
    virtual ~ComparableSmartPtr();

    /** As usual, b should be a pointer to a ComparableSmartPtr! We can not ensure this without sacrificing performance. */
    virtual int compare(const Comparable* b) const override; // TODO: find a way to ensure type safety in comparisons in O(1)
};

class CollectionSmartPtr : public ComparableSmartPtr {
  private:
    CollectionEntry* entry;

    CollectionSmartPtr(CollectionEntry* entry, ComparableSmartPtd& tgt);
    ~CollectionSmartPtr() override;

    static void collectionSetter(SmartPtr* ptr, SmartPtd* ptd);
    SmartPtr::Setter getSetter() override;
};

class SmartPtrContent : public virtual Collection {
  private:
    uint64_t getSizeofContent() const override;
    void initContent(void* p, const void* src) override;
    void uninitContent(void* p) override;
    int compareContent(const void* a, const void* b) const override;

  public:
    bool has(const Comparable& p, FindRelation relation = FIND_EQ) const;
    ComparableSmartPtd& find(const Comparable& p, FindRelation relation = FIND_EQ) const;
    CollectionEntry* findEntry(const Comparable& p, FindRelation relation = FIND_EQ) const;
    CollectionEntry* add(ComparableSmartPtd& p);
    bool remove(const Comparable& p);
    ComparableSmartPtd& get(const CollectionEntry* e) const;
    void set(CollectionEntry* e, ComparableSmartPtd& p);
};

};

#endif
