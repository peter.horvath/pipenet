#include "avl.h"
#include "exception.h"

using namespace nsntl;

int nsntl::Avl::_compare(int compare(const AvlEntry*, const void*), const AvlEntry* a, const void* b) const {
  if (compare)
    return compare(a, b);
  else
    return this->compare(a, reinterpret_cast<const AvlEntry*>(b));
};

AvlEntry* nsntl::Avl::find(const void* v, FindRelation relation, int compare(const AvlEntry*, const void*)) const {
  if (!root)
    return 0;

  if (relation == FIND_EQ) {
    AvlEntry* r = root;
    while (r) {
      int c = _compare(compare, r, v);
      if (c > 0)
        r = r->left;
      else if (c < 0)
        r = r->right;
      else
        break;
    }
    return r;
  }
  else if (relation == FIND_LT)
    return root->findLt(v, compare);
  else if (relation == FIND_LE)
    return root->findLe(v, compare);
  else if (relation == FIND_GE)
    return root->findGe(v, compare);
  else if (relation == FIND_GT)
    return root->findGt(v, compare);
  else
    throw LogicException(LogicError::INVALID_FLAGS);
};

AvlEntry* nsntl::Avl::find(const void* v, int compare(const AvlEntry*, const void*)) const {
  return find(v, FIND_EQ, compare);
};

/** Returns the largest child (or this) which is lighter than v */
AvlEntry* nsntl::AvlEntry::findLt(const void* v, int compare(const AvlEntry*, const void*)) const {
  int c = t->_compare(compare, this, v);
  if (c < 0) {
    if (!right)
      return const_cast<AvlEntry*>(this);
    else {
      AvlEntry* ret = right->findLt(v, compare);
      if (!ret)
        return const_cast<AvlEntry*>(this);
      else
        return ret;
    }
  } else {
    if (!left)
      return 0;
    else
      return left->findLt(v, compare);
  }
};

/** Returns the largest child (or this) which is lighter than, or equal to v */
AvlEntry* nsntl::AvlEntry::findLe(const void* v, int compare(const AvlEntry*, const void*)) const {
  int c = t->_compare(compare, this, v);
  if (c < 0) {
    if (!right)
      return const_cast<AvlEntry*>(this);
    else {
      AvlEntry* ret = right->findLe(v, compare);
      if (!ret)
        return const_cast<AvlEntry*>(this);
      else
        return ret;
    }
  } else if (!c) {
    return const_cast<AvlEntry*>(this);
  } else {
    if (!left)
      return 0;
    else
      return left->findLe(v, compare);
  }
};

/** Returns the smallest child (or this) which is greater than, or equal to v */
AvlEntry* nsntl::AvlEntry::findGe(const void* v, int compare(const AvlEntry*, const void*)) const {
  int c = t->_compare(compare, this, v);
  if (c < 0) {
    if (!right)
      return 0;
    else
      return right->findGe(v, compare);
  } else if (!c) {
    return const_cast<AvlEntry*>(this);
  } else {
    if (!left)
      return const_cast<AvlEntry*>(this);
    else {
      AvlEntry* ret = left->findGe(v, compare);
      if (!ret)
        return const_cast<AvlEntry*>(this);
      else
        return ret;
    }
  }
};

/** Returns the smallest child (or this) which is greater than v */
AvlEntry* nsntl::AvlEntry::findGt(const void* v, int compare(const AvlEntry*, const void*)) const {
  int c = t->_compare(compare, this, v);
  if (c <= 0) {
    if (!right)
      return 0;
    else
      return right->findGt(v, compare);
  } else {
    if (!left)
      return const_cast<AvlEntry*>(this);
    else {
      AvlEntry* ret = left->findGt(v, compare);
      if (!ret)
        return const_cast<AvlEntry*>(this);
      else
        return ret;
    }
  }
};
