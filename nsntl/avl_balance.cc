/**
 * Core functionality: functions needed to balance the Avl tree.
 */

#include "avl.h"
#include "exception.h"

using namespace nsntl;

bool nsntl::AvlEntry::fixHs() {
  uint64_t s = size;
  uint32_t h = height;
  uint32_t lh = leftHeight();
  uint32_t rh = rightHeight();

  if (lh < rh)
    height = rh + 1;
  else
    height = lh + 1;

  size = leftSize() + rightSize() + 1;

  return s == size && h == height;
};

void nsntl::AvlEntry::fixHsRec() {
  for (AvlEntry* i = this; i; i = i->parent)
    if (i->fixHs())
      break;
};

void nsntl::AvlEntry::rotateLeft() {
  if (!right)
    throw LogicException(LogicError::UNREACHABLE_CODE);

  AvlEntry* p = parent;
  AvlEntry* b = right;
  AvlEntry* c = b->left;

  if (p) {
    if (p->left == this)
      p->left = b;
    else
      p->right = b;
  }

  b->parent = p;
  b->left = this;

  parent = b;
  right = c;

  if (c) {
    c->parent = this;
  }

  fixHs();
  b->fixHsRec();
};

void nsntl::AvlEntry::rotateRight() {
  if (!left)
    throw LogicException(LogicError::UNREACHABLE_CODE);

  AvlEntry* p = parent;
  AvlEntry* a = left;
  AvlEntry* c = a->right;

  if (p) {
    if (p->left == this)
      p->left = a;
    else
      p->right = a;
  }

  a->parent = p;
  a->right = this;

  parent = a;
  left = c;

  if (c)
    c->parent = this;

  fixHs();
  a->fixHsRec();
};

bool nsntl::AvlEntry::balance() {
  uint32_t lh = leftHeight();
  uint32_t rh = rightHeight();

  if (rh > lh + 1) {
    if (right->leftHeight() > right->rightHeight())
      right->rotateRight();
    rotateLeft();
    return true;
  } else if (lh > rh + 1) {
    if (left->rightHeight() > left->leftHeight())
      left->rotateLeft();
    rotateRight();
    return true;
  } else
    return false;
};

void nsntl::AvlEntry::balanceRec() {
  AvlEntry* c = this;
  for (;;) {
    if (!c)
      break;
    c->balance();
    c = c->parent;
  }
};

void nsntl::AvlEntry::fixRoot() {
  AvlEntry* i = this;
  for (;;) {
    if (!i->parent)
      break;
    i = i->parent;
  }
  t->root = i;
};
