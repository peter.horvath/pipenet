#include "exception.h"

using namespace nsntl;

void nsntl::TODO() { // AVOID_TODO_WARN
  throw LogicException(LogicError::NOT_IMPLEMENTED);
};

nsntl::ErrorCodeException::ErrorCodeException(uint64_t errorCode) {
  this->errorCode = errorCode;
};

uint64_t nsntl::ErrorCodeException::getErrorCode() const {
  return errorCode;
};

nsntl::LogicException::LogicException(LogicError errorCode) : ErrorCodeException((uint64_t)errorCode) {
  // nothing now
};
