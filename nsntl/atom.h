#ifndef nsntl_atom_h
#define nsntl_atom_h

#include "catom.h"
#include "exception.h"
#include "types.h"

namespace nsntl {

class atomic_bool {
  private:
    volatile uint32_t v;

  public:
    atomic_bool(bool v = false);
    atomic_bool& operator=(bool v);
    operator bool();

    void assign(bool v);
    bool exchange(bool v);
};

class atomic_u16 {
  private:
    volatile uint16_t v;

  public:
    atomic_u16(uint16_t v = 0);
    atomic_u16& operator=(uint16_t v);
    operator uint16_t();
    atomic_u16& operator++();

    void assign(uint16_t v);
    uint16_t exchange(uint16_t v);
};

class atomic_u64 {
  private:
    volatile uint64_t v;

  public:
    atomic_u64(uint64_t v = 0);
    atomic_u64& operator=(uint64_t v);
    operator uint64_t();
    atomic_u64& operator++();

    void assign(uint64_t v);
    uint64_t exchange(uint64_t v);
};

/**
  * Atomic & concurrency things
  */

/**
 * atomic, lock-free Id class
 */
class Counter {
  private:
    atomic_u64 next;

  public:
    Counter(const uint64_t next = 1);
    void init(uint64_t next = 1);
    uint64_t getId();
};

/**
  * Simple locking exception class. Actually, it differs from ErrorCode only in its rtti.
  */
class LockException : public ErrorCodeException {
  public:
    LockException(uint64_t errorCode);
};

typedef enum {
  ALREADY_UNLOCKED = 0x7fea6d2b10970760ULL,
  CANT_LOCK = 0xbd5af9bfd3e88c6dULL
} LockErrorCodes;

/**
 * 
 */
class Lock {
  private:
    atomic_bool lock;

  public:
    Lock();
    ~Lock();

    /**
      * Tries to lock the Lock. On success, returns true. If unsuccessful, returns false.
      */
    bool tryLock();

    /**
      * Tries to unlock. If it was already unlocked, an exception is thrown with errorCode
      * ALREADY_UNLOCKED.
      */
    void unlock();
};

/**
 * Tries to spinlock an existing Lock on constructor, if fails throws exception.
 * Destructor automatically unlocks.
 */
class Spinlock {
  private:
    Lock* lock;

  public:
    /**
      * Spinlocks an existing Lock object on constructor. If fails, CANT_LOCK is thrown.
      */
    Spinlock(Lock& lock);

    /**
      * Drops the spinlock on destructor.
      */
    ~Spinlock();
};

/**
 * An array of locks
 */
/*
class LockArray {
  private:
    atomic_u64* locks;
    u64 size;

  public:
    LockArray(u64 size);
    ~LockArray();

    u64 getSize();
    void tryLock(u64 n);
    void unlock(u64 n);
};
*/

/**
  * Reentrant FIFO. Unix datagram-based implementation. Ideal for complex communication with
  * signal handlers (see Signal::ReentrantHandler).
  */
/*
class ReentrantFifo : public DgramSocketPair {
  private:
    u64 bufSize;

  public:
    ReentrantFifo(u64 bufSize = PAGE_SIZE);
    ~ReentrantFifo();

    u64 getBufSize() const;
    void setBufSize(u64 bufSize);
};
*/

};
#endif
