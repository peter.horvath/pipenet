#ifndef nsntl_graph_h
#define nsntl_graph_h

#include "exception.h"
#include "list_core.h"
#include "types.h"

namespace nsntl {

enum class GraphError : uint64_t {
  VERTEX_NOT_EXISTS = 0x944ec74eb39b455cULL,
  GRAPH_NOT_EXISTS = 0x0016dadc0d20b5daULL,
  CROSS_GRAPH_EDGE_NOT_ALLOWED = 0x41f5ebd644e48f06ULL,
  HALF_ATTACHED_EDGE_NOT_ALLOWED = 0xc537b9d07168aae0ULL,
  CAN_NOT_DETACH_VERTEX_WITH_EDGES = 0x540c850dd63ae674ULL
};

class GraphException : public ErrorCodeException {
  public:
    GraphException(GraphError errorCode);
};

class Graph;
class Vertex;

class Edge {
  friend class Vertex;
  friend class Graph;

  private:
    CoreListEntry srcEntry, dstEntry, graphEntry;

  public:
    Edge(Vertex *src = 0, Vertex *dst = 0);
    virtual ~Edge();

    Graph* getGraph() const;
    Vertex* getSrc() const;
    Vertex* getDst() const;

    bool isAttached() const;
    void attach(Vertex *src, Vertex *dst);
    void detach();

  private:
    static Edge* fromSrcEntry(CoreListEntry* srcEntry);
    static Edge* fromDstEntry(CoreListEntry* dstEntry);
    static Edge* fromGraphEntry(CoreListEntry* graphEntry);
};

class Vertex {
  friend class Edge;
  friend class Graph;
  private:
    CoreList srcList, dstList;
    CoreListEntry graphEntry;

  public:
    Vertex(Graph* graph = 0);
    virtual ~Vertex();

    Graph* getGraph() const;
    bool isAttached() const;
    void attach(Graph *graph);
    void detach();

  private:
    static Vertex* fromSrcList(CoreList* srcList);
    static Vertex* fromDstList(CoreList* dstList);
    static Vertex* fromGraphEntry(CoreListEntry* graphEntry);
};

class Graph { // TODO: this should be a Collection
  friend class Edge;
  friend class Vertex;

  private:
    CoreList edgeList, vertexList;

  public:
    Graph();
    virtual ~Graph();

  private:
    static Graph* fromEdgeList(CoreList* edgeList);
    static Graph* fromVertexList(CoreList* vertexList);
};

};

#endif
