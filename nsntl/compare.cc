#include <stdlib.h>

#include "compare.h"

using namespace nsntl;

nsntl::Comparable::~Comparable() {
  // nothing now
};

int nsntl::compare(const void* a, const void* b) {
  if (a < b)
    return -1;
  else if (a > b)
    return 1;
  else
    return 0;
};

int nsntl::compare(int64_t a, int64_t b) {
  if (a < b)
    return -1;
  else if (a > b)
    return 1;
  else
    return 0;
};

int nsntl::compare(uint64_t a, uint64_t b) {
  if (a < b)
    return -1;
  else if (a > b)
    return 1;
  else
    return 0;
};

int nsntl::compare(const Comparable* a, const Comparable* b) {
  if (!a) {
    if (!b)
      return 0;
    else
      return -1;
  } else {
    if (!b)
      return 1;
    else
      return a->compare(b);
  }
};
