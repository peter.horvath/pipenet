#ifndef nsntl_util_h
#define nsntl_util_h

#include "types.h"

namespace nsntl {

extern const uint64_t PAGE_SIZE;

uint64_t min(uint64_t a, uint64_t b);
uint64_t max(uint64_t a, uint64_t b);
uint128_t min(const uint128_t a, const uint128_t b);
uint128_t max(const uint128_t a, const uint128_t b);

void Bzero(void *s, uint64_t size);
void* Memcpy(void *dst, const void* src, uint64_t size);
void* Memmove(void *dst, const void* src, uint64_t size);
uint64_t Strlen(const char *s);
bool Strcmp(const char* const p1, const char* const p2);
char* Strdup(const char* p);
void Strcpy(char* dst, const char* src);
void Strncpy(char* dst, const char* src, uint64_t n);

};

#endif
