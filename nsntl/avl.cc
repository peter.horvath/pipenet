/*
An AVL tree (Adel'son-Vel'skii & Landis) is a binary search
tree in which the heights of the left and right subtrees
of the root differ by at most one and in which the left
and right subtrees are again AVL trees.

With each node of an AVL tree is associated a balance factor
that is Left High, Equal, or Right High according,
respectively, as the left subtree has height greater than,
equal to, or less than that of the right subtree.

The AVL tree is, in practice, balanced quite well.  It can
(at the worst case) become skewed to the left or right,
but never so much that it becomes inefficient.  The
balancing is done as items are added or deleted.

It is safe to iterate or search a tree from multiple threads
provided that no threads are modifying the tree.

See also:	Robert L. Kruse, Data Structures and Program Design, 2nd Ed., Prentice-Hall
*/

#include <stdlib.h>

#include "avl.h"
#include "compare.h"
#include "exception.h"

using namespace nsntl;

nsntl::Avl::Avl(uint8_t flags) {
  this->root = 0;
  this->flags = flags;
};

nsntl::Avl::~Avl() {
  if (!isEmpty())
    abort();
};

void nsntl::Avl::forEach(void f(AvlEntry* a)) {
  if (root)
    root->forEachRec(f);
};

int nsntl::Avl::compare(const AvlEntry* a, const AvlEntry* b) const {
  return nsntl::compare((void*)a, (void*)b);
};

bool nsntl::Avl::isEmpty() const {
  return !root;
};

AvlEntry* nsntl::Avl::firstEntry() const {
  if (!root)
    return 0;
  else
    return root->firstChild();
};

AvlEntry* nsntl::Avl::lastEntry() const {
  if (!root)
    return 0;
  else
    return root->lastChild();
};

void nsntl::Avl::attach(AvlEntry* v) {
  v->attach(this);
};

void nsntl::Avl::detach(AvlEntry* v) {
  if (this != v->t)
    throw LogicException(LogicError::NOT_MY_ENTRY);
  v->detach();
};

AvlEntry* nsntl::AvlEntry::firstChild() const {
  AvlEntry* i;
  for (i = (AvlEntry*)this; i->left; i = i->left);
  return i;
};

AvlEntry* nsntl::AvlEntry::lastChild() const {
  AvlEntry* i;
  for (i = (AvlEntry*)this; i->right; i = i->right);
  return i;
};

void nsntl::AvlEntry::forEachRec(void f(AvlEntry *a)) {
  f(this);
  if (left)
    left->forEachRec(f);
  if (right)
    right->forEachRec(f);
};

nsntl::AvlEntry::AvlEntry(Avl* t) {
  this->t = 0;
  left = 0;
  right = 0;
  parent = 0;
  size = 1;
  height = 1;
  if (t)
    attach(t);
};

nsntl::AvlEntry::~AvlEntry() {
  if (t)
    detach();
};

Avl* nsntl::AvlEntry::avl() const {
  return t;
};

AvlEntry* nsntl::AvlEntry::next() const {
  if (right)
    return right->firstChild();
  else if (parent)
    return parent;
  else
    return 0;
};

AvlEntry* nsntl::AvlEntry::prev() const {
  if (left)
    return left->lastChild();
  else if (parent)
    return parent;
  else
    return 0;
};

void nsntl::AvlEntry::moveTo(Avl* t) {
  if (this->t)
    detach();
  attach(t);
};

uint64_t nsntl::AvlEntry::getSize() const {
  return size;
};
