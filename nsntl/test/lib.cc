// utility functions for testcases

package avltree

import (
  "bytes"
  "encoding/json"
  "strconv"
  "fmt"
)

func JsonPrettyPrint(in string) string {
  var out bytes.Buffer
  err := json.Indent(&out, []byte(in), "", "    ")
  if err != nil {
    return in
  }
  return out.String()
}

func compare_int64_int64(a interface{}, b interface{}) int {
  A := a.(int64)
  B := b.(int64)
  if A < B {
    return -1
  } else if A > B {
    return 1
  } else {
    return 0
  }
}

func int64Serializer(i interface{}) string {
  if i == nil {
    return "\"BUG! NIL!\""
  }
  return strconv.FormatInt(i.(int64), 10)
}

func (t *AvlTree) Dump() {
  s := t.ToStringFn(int64Serializer)
  fmt.Println(JsonPrettyPrint(s))
}

func nilOrValue(n *AvlNode) string {
  if n == nil {
    return "nil"
  } else {
    return strconv.FormatInt(n.value.(int64), 10)
  }
}

func panicNode(n *AvlNode, msg string) {
  fmt.Printf("panicNode: %d (parent: %s, left: %s, right: %s)\n",
    n.value, nilOrValue(n.parent), nilOrValue(n.left), nilOrValue(n.right))
  if n.left != nil {
    fmt.Println("n.left.parent: " + nilOrValue(n.left.parent))
  }
  if n.right != nil {
    fmt.Println("n.right.parent: " + nilOrValue(n.right.parent))
  }
  panic(msg)
}
