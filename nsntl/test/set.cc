#include <stdio.h>
#include <stdlib.h>

#include "../exception.h"
#include "../set.h"
#include "../wrap.h"

#define SIZE 10
#define ITER 100000

using namespace nsntl;

int main() {
  Uint64Set s;
  bool in[SIZE];
  uint64_t size = 0;

  for (int i=0; i<SIZE; i++)
    in[i] = false;

  for (int i=0; i<ITER; i++) {
    if (s.size() != size || bool(size) == s.isEmpty()) {
      printf ("size: %lu, s.size: %lu, isEmpty: %s\n", size, s.size(), s.isEmpty()?"true":"false");
      throw "size/isEmpty inconsistent";
    }

    int n = rand() % SIZE;
    if (in[n]) {
      if (!s.remove(n))
        throw "multiple remove";
      in[n] = false;
      size--;
    } else {
      if (!s.add(n))
        throw "multiple add";
      in[n] = true;
      size++;
    }

    FindRelation rel = FindRelation(rand() % 5);

    int testFor = rand() % SIZE;

    int wanted;

    if (rel == FIND_LT) {
      for (wanted = testFor - 1; wanted >= 0 && !in[wanted]; wanted--);
    } else if (rel == FIND_LE) {
      for (wanted = testFor; wanted >= 0 && !in[wanted]; wanted--);
    } else if (rel == FIND_EQ) {
      wanted = in[testFor] ? testFor : -1;
    } else if (rel == FIND_GE) {
      for (wanted = testFor; wanted < SIZE && !in[wanted]; wanted++);
    } else if (rel == FIND_GT) {
      for (wanted = testFor + 1; wanted < SIZE && !in[wanted]; wanted++);
    } else
      throw "random relation bad";

    if (wanted == SIZE)
      wanted = -1;

    int result;

    try {
      result = s.find(testFor, rel);
    } catch (LogicException e) {
      if (e.getErrorCode() != uint64_t(LogicError::NOT_FOUND))
        throw "we should have not get this exception";
      else result = -1;
    }

    printf ("\nArray:\n");
    for (int d = 0; d < SIZE; d++)
      if (in[d])
        printf ("%i ", d);
    printf ("\n");

    printf ("i: %i, testFor: %i, relation: %i, wanted: %i, result: %i\n", i, testFor, int(rel), wanted, result);
    if (wanted != result) {
      throw "test fails";
    }

  }
  s.deleteAll();
  return 0;
};
