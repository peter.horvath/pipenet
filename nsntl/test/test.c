#include <stdatomic.h>

#if ATOMIC_LLONG_LOCK_FREE != 2
  #error "long long must be lock-free"
#endif

int main(int argc, char** argv) {
  if (sizeof(long long) != 8) {
    throw Exception("should be sizeof(long long) == 8");
  }
};
