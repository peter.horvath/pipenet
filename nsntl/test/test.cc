#include <stdio.h>
#include <stdlib.h>

#include "avl.h"

using namespace nsntl;

class IntAvlEntry : public AvlEntry {
  private:
    int v;
  public:
    IntAvlEntry(int v=0) : AvlEntry() {
#ifdef DEBUG
      printf("IntAvlEntry::IntAvlEntry(v=%i)\n", v);
#endif
      this->v = v;
    };
    int get() const {
      return v;
    };
    void set(int v) {
      if (this->v == v)
        return;
      Avl* t = this->t;
      if (t)
        detach();
      this->v = v;
      if (t)
        attach(t);
    };
    int compare(const AvlEntry* b) const {
      return compare(dynamic_cast<const IntAvlEntry*>(b));
    };
    int compare(const IntAvlEntry* b) const {
      return nsntl::compare(v, b->v);
    };
    void dumpRec(int d) {
      if (left)
        dynamic_cast<IntAvlEntry*>(left)->dumpRec(d+1);
      for (int i=0; i<d; i++)
        putchar(' ');
      dump();
      if (right)
        dynamic_cast<IntAvlEntry*>(right)->dumpRec(d+1);
    };
    void dump() {
      printf ("%i (this: 0x%016lx, avl: 0x%016lx, parent: 0x%016lx, left: 0x%016lx, right: 0x%016lx, height: %u, size: %lu)\n",
        v, (uint64_t)this, (uint64_t)t, (uint64_t)parent, (uint64_t)left, (uint64_t)right, height, size);
    };
};

class DumpAvl : public Avl {
  public:
    DumpAvl(uint8_t flags) : Avl(flags) {
      // nothing now
    };
    void dump() {
      putchar('\n');
      if (root)
        dynamic_cast<IntAvlEntry*>(root)->dumpRec(0);
      putchar('\n');
    };
};

int main(int argc, char** argv) {
  DumpAvl avl((uint8_t)(CollectionFlags::ALLOW_DUPLICATES));
  // simple visual test
  /*
  for (int a=0; a<10; a++) {
    IntAvlEntry* e = new IntAvlEntry(a);
    avl.attach(e);
    avl.dump();
  };
  */

  #define SIZE 100
  #define TRIES 10000000
  IntAvlEntry e[SIZE];
  bool in[SIZE];
  #ifdef DEBUG
  printf("start\n");
  #endif
  for (int a=0; a<SIZE; a++) {
    e[a].dump();
    in[a]=false;
    //e[a].set(a);
  };
  srand(0);
  for (int a=0; a<TRIES; a++) {
    #ifdef DEBUG
    avl.dump();
    #endif
    int i = rand() % SIZE;
    if (in[i]) {
      #ifdef DEBUG
      printf ("detaching %i (%p)\n", i, &e[i]);
      #endif
      e[i].set(i);
      avl.detach(&e[i]);
      in[i]=false;
    } else {
      #ifdef DEBUG
      printf ("attaching %i (%p)\n", i, &e[i]);
      #endif
      avl.attach(&e[i]);
      in[i]=true;
    }
  };
  avl.dump();
  return 0;
};
