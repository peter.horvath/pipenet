#include "exception.h"
#include "malloc.h"
#include "map.h"

using namespace nsntl;

nsntl::Map::Map() {
  hotEntry = 0;
  this->allocator = const_cast<Allocator*>(static_cast<const Allocator*>(&defaultAllocator));
};

nsntl::Map::Map(Allocator& allocator) {
  hotEntry = 0;
  this->allocator = &allocator;
};

nsntl::Map::~Map() {
  if (hotEntry && !hotEntry->map())
    deleteEntry(hotEntry);

  while (!isEmpty())
    deleteEntry(firstEntry());
};

int nsntl::Map::compare(const AvlEntry* a, const AvlEntry* b) const {
  void* keyA = getKeyP(static_cast<const MapEntry*>(a));
  void* keyB = getKeyP(static_cast<const MapEntry*>(b));
  return compareKeys(keyA, keyB);
};

int nsntl::Map::compareEntryKey(const AvlEntry* a, const void* b) {
  const MapEntry* mapEntryA = static_cast<const MapEntry*>(a);
  const Map* mapOfA = mapEntryA->map();
  const void* keyOfA = mapOfA->getKeyP(mapEntryA);
  return mapOfA->compareKeys(keyOfA, b);
};

MapEntry* nsntl::Map::firstEntry() const {
  return static_cast<MapEntry*>(Avl::firstEntry());
};

MapEntry* nsntl::Map::lastEntry() const {
  return static_cast<MapEntry*>(Avl::lastEntry());
};

void nsntl::Map::initKey(void* p) const {
  // nothing now
};

void nsntl::Map::uninitKey(void* p) const {
  // nothing now
};

void* nsntl::Map::getKeyP(const MapEntry* e) const {
  const uint8_t* byteEntryAddr = reinterpret_cast<const uint8_t*>(e);
  const uint8_t* byteKeyAddr = byteEntryAddr + sizeof(MapEntry);
  const void* voidKeyAddr = reinterpret_cast<const void*>(byteKeyAddr);
  return const_cast<void*>(voidKeyAddr);
};

void nsntl::Map::initValue(void* p) const {
  // nothing now
};

void nsntl::Map::uninitValue(void* p) const {
  // nothing now
};

void* nsntl::Map::getValueP(const MapEntry* e) const {
  void* voidKeyAddr = getKeyP(e);
  uint8_t* byteKeyAddr = reinterpret_cast<uint8_t*>(voidKeyAddr);
  uint8_t* byteValueAddr = byteKeyAddr + sizeofKey();
  return reinterpret_cast<void*>(byteValueAddr);
};

uint64_t nsntl::Map::sizeofEntry() const {
  return sizeof(MapEntry) + sizeofKey() + sizeofValue();
};

MapEntry* nsntl::Map::buildEntry() {
  MapEntry* ret = new(allocator->alloc(sizeofEntry())) MapEntry();
  initKey(getKeyP(ret));
  initValue(getValueP(ret));
  return ret;
};

void nsntl::Map::deleteEntry(MapEntry* e) {
  if ((e->map() && e->map() != this) || (!e->map() && e != hotEntry))
    throw LogicException(LogicError::NOT_MY_ENTRY);

  if (e->map())
    e->detach();

  uninitValue(getValueP(e));
  uninitKey(getKeyP(e));
  e->~MapEntry();
  allocator->free(e);
};

void nsntl::Map::checkHot() const {
  if (!hotEntry)
    throw LogicException(LogicError::NOT_FOUND);
  if (hotEntry->map() && hotEntry->map() != this)
    throw LogicException(LogicError::NOT_MY_ENTRY);
};

void nsntl::Map::setHot(MapEntry* e) {
  if (e && e->map() && e->map() != this)
    throw LogicException(LogicError::NOT_MY_ENTRY);
  if (hotEntry && !hotEntry->map()) {
    deleteEntry(hotEntry);
  }
  if (e && !e->map())
    e->attach(this);
  hotEntry = e;
};

MapEntry* nsntl::Map::genFindEntry(const void* key, FindRelation rel) const {
  AvlEntry* ret = Avl::find(key, rel, Map::compareEntryKey);
  return static_cast<MapEntry*>(ret);
};

void nsntl::Map::genFind(const void* key, FindRelation rel) {
  setHot(genFindEntry(key, rel));
};

nsntl::MapEntry::MapEntry() {
  // nothing now
};

nsntl::MapEntry::~MapEntry() {
  // nothing now - ~AvlEntry detaches
};

Map* nsntl::Map::fromAvl(Avl* avl) {
  return static_cast<Map*>(avl);
};

Map* nsntl::MapEntry::map() const {
  return Map::fromAvl(avl());
};

MapEntry* nsntl::MapEntry::next() const {
  return static_cast<MapEntry*>(AvlEntry::next());
};

MapEntry* nsntl::MapEntry::prev() const {
  return static_cast<MapEntry*>(AvlEntry::prev());
};

void* nsntl::MapEntry::operator new(size_t size, void* p) {
  return p;
};

uint64_t nsntl::MapKeyUint64::sizeofKey() const {
  return sizeof(uint64_t);
};

int nsntl::MapKeyUint64::compareKeys(const void* a, const void* b) const {
  return nsntl::compare(*reinterpret_cast<const uint64_t*>(a), *reinterpret_cast<const uint64_t*>(b));
};

Map& nsntl::MapKeyUint64::operator[](uint64_t i) {
  genFind(&i);
  return *this;
};

/*
SoftPtr nsntl::MapValueSoftPtr::operator*() const {
  return v;
};
*/
