#ifndef nsntl_avl_h
#define nsntl_avl_h

#include <stdint.h>

#include "compare.h"

namespace nsntl {

class AvlEntry;

/**
 * Avl-tree implementation. It is only an algorithm, but not a container. Set, Map and similar containers
 * are all wrappers around this. They are containers. This, and AvlEntry are independent from container.h,
 * AvlEntry is not an Item, and it uses void* and not Object* .
 */
class Avl {
  friend AvlEntry;

  protected:
    AvlEntry* root;
    uint8_t flags;

  private:
    // avl_find.cc
    int _compare(int compare(const AvlEntry*, const void*), const AvlEntry* a, const void* b) const;

  public:
    // avl.cc
    Avl(uint8_t flags = 0);

    /** If not empty, abort() will be called! */
    virtual ~Avl();

    void forEach(void f(AvlEntry*));

    /** comparison method for the ordering. Default is simple pointer comparison, i.e. entries are ordered
        by their addresses. Any data manipulation uses this, but we can find(...) entries by alternate
        comparion functions. These functions use always "this" as their first argument, but the second can
        by any time (hence void*). */
    virtual int compare(const AvlEntry* a, const AvlEntry* b) const;

    bool isEmpty() const;

    AvlEntry* firstEntry() const;
    AvlEntry* lastEntry() const;

    void attach(AvlEntry* v);
    void detach(AvlEntry* v);

    // avl_find.cc
    AvlEntry* find(const void* v, FindRelation relation = FIND_EQ, int compare(const AvlEntry*, const void*) = 0) const;
    AvlEntry* find(const void* v, int compare(const AvlEntry*, const void*)) const;
};

class AvlEntry {
  friend Avl;
  protected:
    Avl* t;
    AvlEntry *left, *right, *parent;
    uint64_t size;
    uint32_t height;

    // avl_util.cc
    bool isLeftChild() const;
    bool isRightChild() const;
    uint64_t leftSize() const;
    uint64_t rightSize() const;
    uint32_t leftHeight() const;
    uint32_t rightHeight() const;
    AvlEntry* smallestChild() const;
    AvlEntry* highestChild() const;
    void addLeafLeft(AvlEntry* i);
    void addLeafRight(AvlEntry* i);

    // avl_balance.cc
    bool fixHs();
    void fixHsRec();
    void rotateLeft();
    void rotateRight();
    bool balance();
    void balanceRec();
    void fixRoot();

    // avl_detach.cc
    void exchange(AvlEntry* nxt);

    // avl.cc
    AvlEntry* firstChild() const;
    AvlEntry* lastChild() const;
    void forEachRec(void f(AvlEntry* a));

    // avl_find.cc
    AvlEntry* findLt(const void* v, int compare(const AvlEntry*, const void*)) const;
    AvlEntry* findLe(const void* v, int compare(const AvlEntry*, const void*)) const;
    AvlEntry* findGe(const void* v, int compare(const AvlEntry*, const void*)) const;
    AvlEntry* findGt(const void* v, int compare(const AvlEntry*, const void*)) const;

  public:
    // avl.cc
    AvlEntry(Avl* t = 0);
    virtual ~AvlEntry();

    Avl* avl() const;
    AvlEntry* next() const;
    AvlEntry* prev() const;

    // avl_attach.cc
    void attach(Avl* t);

    // avl_detach.cc
    void detach();

    // avl.cc
    void moveTo(Avl* t);
    uint64_t getSize() const;
};

};

#endif
