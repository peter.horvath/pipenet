#include <stddef.h>

#include "smartptr.h"

#pragma GCC diagnostic ignored "-Winvalid-offsetof"

using namespace nsntl;

nsntl::SmartPtd::SmartPtd() : SmartPtd(defaultAllocator, false) {
  // nothing now
};

nsntl::SmartPtd::SmartPtd(const Allocator& allocator) : SmartPtd(allocator, false) {
  // nothing now
};

nsntl::SmartPtd::SmartPtd(bool fixed) : SmartPtd(defaultAllocator, fixed) {
  // nothing now
};

nsntl::SmartPtd::SmartPtd(const Allocator& allocator, bool fixed) : allocator{&allocator}, fixed{fixed} {
  // nothing now
};

nsntl::SmartPtd::~SmartPtd() {
  fixed = true;
  while (CoreListEntry* le = pointedBy.firstEntry()) {
    SmartPtr* ptr = SmartPtr::fromEntry(le);
    SmartPtr::set(ptr, 0);
  };
};

bool nsntl::SmartPtd::isFixed() const {
  return fixed;
};

void nsntl::SmartPtd::setFixed(bool fixed) {
  this->fixed = true;
};

const Allocator& nsntl::SmartPtd::getAllocator() const {
  return *allocator;
};

void nsntl::SmartPtd::setAllocator(const Allocator& allocator) {
  this->allocator = &allocator;
};

void nsntl::SmartPtd::del(SmartPtd& smartPtd) {
  del(&smartPtd);
};

void nsntl::SmartPtd::del(SmartPtd* smartPtd) {
  if (smartPtd->fixed)
    throw LogicException(LogicError::CANNOT_DEL_FIXED_DATA);
  Allocator* allocator = smartPtd->allocator;
  smartPtd->~SmartPtd();
  allocator->free(static_cast<void*>(smartPtd));
};

nsntl::SmartPtd* nsntl::SmartPtd::fromSmartPtdBy(CoreList* pointedBy) {
  size_t offset = offsetof(SmartPtd, pointedBy);
  return reinterpret_cast<SmartPtd*>(reinterpret_cast<uint8_t*>(pointedBy) - offset);
};

nsntl::SmartPtr::SmartPtr() {
  // nothing now - CoreListEntry will be 0
};

nsntl::SmartPtr::SmartPtr(SmartPtd& target) {
  SmartPtr::set(this, &target);
};

nsntl::SmartPtr::SmartPtr(SmartPtd* target) {
  SmmrtPtr::set(this, target);
};

nsntl::SmartPtr::SmartPtr(const SmartPtr& p) : SmartPtr(p.get()) {
  // nothing now
};

nsntl::SmartPtr::~SmartPtr() {
  set(0);
};

nsntl::SmartPtd& nsntl::SmartPtr::get() const {
  return *getPtr();
};

nsntl::SmartPtd* nsntl::SmartPtr::getPtr() const {
  CoreList* pointedBy = entry.list();
  if (!pointedBy)
    return 0;
  else
    return SmartPtd::fromSmartPtdBy(pointedBy);
};

void nsntl::SmartPtr::set(SmartPtd& tgt) {
  SmartPtr::set(this, &tgt);
};

void nsntl::SmartPtr::set(SmartPtd* tgt) {
  SmartPtr::set(this, tgt);
};

bool nsntl::SmartPtr::isNull() const {
  return entry.list();
};

void nsntl::SmartPtr::setNull() {
  set(this, 0);
};

void nsntl::SmartPtr::defaultSetter(SmartPtr* ptr, SmartPtd* newTarget) {
  SmartPtd* oldTarget = ptr->getPtr();
  if (oldTarget == newTarget)
    return;
  if (oldTarget) {
    oldTarget->pointedBy.detach(&entry);
    if (oldTarget->pointedBy.isEmpty() && !oldTarget->fixed)
      SmartPtd::del(oldTarget);
  };
  if (newTarget)
    newTarget->pointedBy.attach(&entry);
};

nsntl::SmartPtr::Setter nsntl::SmartPtr::getSetter() const {
  return defaultSetter;
};

void nsntl::SmartPtr::set(SmartPtr& ptr, SmartPtd& target) {
  set(&ptr, &target);
};

void nsntl::SmartPtr::set(SmartPtr& ptr, SmartPtd* target) {
  set(&ptr, target);
};

void nsntl::SmartPtr::set(SmartPtr* ptr, SmartPtd& target) {
  set(ptr, &target);
};

void nsntl::SmartPtr::set(SmartPtr* ptr, SmartPtd* target) {
  Setter setter = ptr->getSetter();
  setter(ptr, target);
};

nsntl::SmartPtr* nsntl::SmartPtr::fromEntry(CoreListEntry* entry) {
  size_t offset = offsetof(SmartPtr, entry);
  return reinterpret_cast<SmartPtr*>(reinterpret_cast<uint8_t*>(entry) - offset);
};

nsntl::ComparableSmartPtd::ComparableSmartPtd() : SmartPtd() {
  // nothing now
};

nsntl::ComparableSmartPtd::ComparableSmartPtd(bool fixed) : SmartPtd(fixed) {
  // nothing now
};

nsntl::ComparableSmartPtd::ComparableSmartPtd(const Allocator& allocator) : SmartPtd(allocator) {
  // nothing now
};

nsntl::ComparableSmartPtd::ComparableSmartPtd(const Allocator& allocator, bool fixed) : SmartPtd(allocator, fixed) {
  // nothing now
};

nsntl::ComparableSmartPtd::~ComparableSmartPtd() {
  // nothing now
};

nsntl::ComparableSmartPtr::ComparableSmartPtr(ComparableSmartPtd* v) {
  // nothing now
};

nsntl::ComparableSmartPtr::~ComparableSmartPtr() {
  // nothing now
};

int nsntl::ComparableSmartPtr::compare(const Comparable* b) const {
  SmartPtd* thisPtd = get();
  ComparableSmartPtd* thisCPtd = static_cast<ComparableSmartPtd*>(thisPtd);
  const ComparableSmartPtr* bCPtr = static_cast<const ComparableSmartPtr*>(b);
  SmartPtd* bPtd = bCPtr->get();
  ComparableSmartPtd* bCPtd = static_cast<ComparableSmartPtd*>(bPtd);
  return nsntl::compare(thisCPtd, bCPtd);
};

nsntl::ComparableSmartPtd* nsntl::ComparableSmartPtr::get() const {
  return static_cast<ComparableSmartPtd*>(SmartPtr::get());
};

void nsntl::ComparableSmartPtr::set(ComparableSmartPtd* target) {
  SmartPtr::set(target);
};

CollectionSmartPtr::CollectionSmartPtr(CollectionEntry* entry, ComparableSmartPtd& tgt) {
  this->entry = entry;
  getSetter()(&tgt);
};

CollectionSmartPtr::~CollectionSmartPtr() {
  getSetter()(0);
};

uint64_t nsntl::SmartPtrContent::getSizeofContent() const {
  return sizeof(CollectionSmartPtr);
};

void nsntl::SmartPtrContent::initContent(void* p, const void* src) {
  if (src)
    throw LogicException(LogicError::NULLPTR_IN_COLLECTION);
  else {
    CollectionEntry* entry = getEntryFromContent(p);
    new(p) CollectionSmartPtr(entry, *reinterpret_cast<SmartPtd*>(src));
  }
};

void nsntl::SmartPtrContent::uninitContent(void* p) {
  reinterpret_cast<SmartPtr*>(p)->~SmartPtr();
};

int nsntl::SmartPtrContent::compareContent(const void* a, const void* b) const {
  const CollectionSmartPtr* aC = reinterpret_cast<const CollectionSmartPtr*>(a);
  const CollectionSmartPtr* bC = reinterpret_cast<const CollectionSmartPtr*>(b);
  return aC->compare(b);
};

bool nsntl::SmartPtrContent::has(const Comparable& p, FindRelation rel) const {
  return genFindEntry(&p, relation);
};

CollectionEntry* nsntl::SmartPtrContent::findEntry(const Comparable& p, FindRelation rel) const {
  return genFindEntry(&p, relation);
};

CollectionEntry* nsntl::SmartPtrContent::add(const ComparableSmartPtd& p) {
  return genAdd(&p);
};

bool nsntl::SmartPtrContent::remove(const Comparable& p) {
  return genRemove(&p);
};

ComparableSmartPtd& nsntl::SmartPtrContent::get(const CollectionEntry* e) const {
  const SmartPtr* p = reinterpret_cast<const SmartPtr*>(getContentP(e));
  SmartPtd* tgt = p->get();
  ComparableSmartPtd* tgtC = static_cast<ComparableSmartPtd*>(tgt);
  return *tgtC;
};

void nsntl::SmartPtrContent::set(CollectionEntry* e, ComparableSmartPtd& p) {
  genSet(&p);
};
