#include <stdlib.h>
#include <string.h>

#include "exception.h"
#include "util.h"

using namespace nsntl;

// page size detection

// problematic in a library in the lack of global initalization
// const uint64_t posxx::PAGE_SIZE = sysconf(_SC_PAGE_SIZE);

#if __i386__ || __amd64__ || __arm__

const uint64_t nsntl::PAGE_SIZE = 4096;

#else

const uint64_t nsntl::PAGE_SIZE = 4096;

#endif

// end of page size detection

uint64_t nsntl::min(uint64_t a, uint64_t b) {
  return a < b ? a : b;
}

uint64_t nsntl::max(uint64_t a, uint64_t b) {
  return a > b ? a : b;
}

uint128_t nsntl::min(const uint128_t a, const uint128_t b) {
  return a < b ? a : b;
}

uint128_t nsntl::max(const uint128_t a, const uint128_t b) {
  return a > b ? a : b;
}

void nsntl::Bzero(void *s, uint64_t size) {
  bzero(s, size);
};

void* nsntl::Memcpy(void* dst, const void* src, uint64_t size) {
  uint8_t* dstBegin = (uint8_t*)dst;
  uint8_t* dstEnd = dstBegin + size;
  uint8_t* srcBegin = (uint8_t*)src;
  uint8_t* srcEnd = srcBegin + size;
  if (srcEnd <= dstBegin || dstEnd <= srcBegin)
    return memcpy(dst, src, size);
  else
    throw LogicException(LogicError::MEMORY_OVERLAP);
};

void* nsntl::Memmove(void* dst, const void* src, uint64_t size) {
  return memmove(dst, src, size);
};

uint64_t nsntl::Strlen(const char *s) {
  return strlen(s);
};

bool nsntl::Strcmp(const char* const p1, const char* const p2) {
  return !::strcmp(p1, p2);
};

char* nsntl::Strdup(const char* s) {
  char* ret = strdup(s);
  if (!ret) {
    throw LogicException(LogicError::STRDUP_FAILS);
  }
  return ret;
};

void nsntl::Strcpy(char* dst, const char* src) {
  strcpy(dst, src);
};

void nsntl::Strncpy(char* dst, const char* src, uint64_t n) {
  strncpy(dst, src, n);
};
