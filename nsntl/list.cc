#include <stdlib.h>

#include "exception.h"
#include "list.h"

using namespace nsntl;

static const void* const listEntryType = 0;

nsntl::List::List(Allocator* entryAllocator) : Collection(entryAllocator), CoreList() {
  _size = 0;
};

nsntl::List::~List() {
  // wonderful c++... no virtual functions neither exceptions can be used in destructors...
  if (!this->isEmpty())
    abort();

  //while (!this->isEmpty())
  //  deleteEntry(this->firstEntry()->getCollectionEntry());
};

void nsntl::List::derAttach(CollectionEntry* entry) {
  attach(static_cast<ListEntry*>(entry));
};

void nsntl::List::derDetach(CollectionEntry* entry) {
  detach(static_cast<ListEntry*>(entry));
};

const void* nsntl::List::getEntryType() const {
  return reinterpret_cast<const void*>(&listEntryType);
};

uint64_t nsntl::List::getSizeofEntry() const {
  return sizeof(ListEntry);
};

bool nsntl::List::isEmpty() const {
  return !_size;
};

uint64_t nsntl::List::size() const {
  return _size;
};

Collection* nsntl::List::getCollection() const {
  return const_cast<List*>(this);
};

CollectionEntry* nsntl::List::nextOf(const CollectionEntry* entry) const {
  return static_cast<const ListEntry*>(entry)->next();
};

CollectionEntry* nsntl::List::prevOf(const CollectionEntry* entry) const {
  return static_cast<const ListEntry*>(entry)->prev();
};

void nsntl::List::initEntry(void* p) {
  new(p) ListEntry();
};

void nsntl::List::uninitEntry(void* p) {
  ListEntry* le = reinterpret_cast<ListEntry*>(p);
  le->~ListEntry();
};

CollectionEntry* nsntl::List::genFindEntry(const void* p, FindRelation relation) const {
  ListEntry* best = NULL;
  for (ListEntry* cur = static_cast<ListEntry*>(firstEntry()); cur; cur=cur->next()) {
    int cmp = compareContent(getContentP(cur), p);
    if (relation == FIND_EQ)
      return cur;
    else if ((relation == FIND_LT && cmp < 0)
        || (relation == FIND_LE && cmp <= 0)
        || (relation == FIND_GE && cmp >= 0)
        || (relation == FIND_GT && cmp > 0)) {
      if (!best)
        best = cur;
      else {
        int cmpRet = compareContent(getContentP(best), getContentP(cur));
        if ((relation == FIND_LT && cmpRet > 0)
            || (relation == FIND_LE && cmpRet >= 0)
            || (relation == FIND_GE && cmpRet <= 0)
            || (relation == FIND_GT && cmpRet < 0)) {
          best = cur;
        }
      }
    }
  };
  if (relation == FIND_EQ)
    return 0;
  else
    return best;
};

CollectionEntry* nsntl::List::getEntryFromContent(const void* p) const {
  return reinterpret_cast<const ListEntry*>(reinterpret_cast<const uint8_t*>(p) - getSizeofEntry());
};

void nsntl::List::attach(ListEntry* entry) {
  attach(entry, false);
};

void nsntl::List::attach(ListEntry* entry, bool toFront) {
  CoreList::attach(entry, toFront);
  _size++;
};

void nsntl::List::attachBefore(ListEntry* entry, ListEntry* before) {
  CoreList::attachBefore(entry, before);
  _size++;
};

void nsntl::List::attachAfter(ListEntry* entry, ListEntry* after) {
  CoreList::attachAfter(entry, after);
  _size++;
};

void nsntl::List::detach(ListEntry* entry) {
  if (!_size)
    throw LogicException(LogicError::INTERNAL_STATE_INCONSISTENT);
  _size--;
  CoreList::detach(entry);
};

nsntl::CollectionEntry* nsntl::List::firstEntry() const {
  return static_cast<ListEntry*>(CoreList::firstEntry());
};

nsntl::CollectionEntry* nsntl::List::lastEntry() const {
  return static_cast<ListEntry*>(CoreList::lastEntry());
};

nsntl::ListEntry::ListEntry(List* list) : CoreListEntry(list) {
  // nothing now
};

nsntl::ListEntry::~ListEntry() {
  if (lst)
    lst->detach(this);
};

nsntl::Collection* nsntl::ListEntry::getCollection() const {
  if (List* l = list())
    return l->getCollection();
  else
    return 0;
};

nsntl::List* nsntl::ListEntry::list() const {
  return static_cast<List*>(CoreListEntry::list());
};

nsntl::ListEntry* nsntl::ListEntry::next() const {
  return static_cast<ListEntry*>(CoreListEntry::next());
};

nsntl::ListEntry* nsntl::ListEntry::prev() const {
  return static_cast<ListEntry*>(CoreListEntry::prev());
};

const void* nsntl::ListEntry::getEntryType() const {
  return reinterpret_cast<const void*>(&listEntryType);
};

nsntl::CollectionEntry* nsntl::ListEntry::getCollectionEntry() const {
  return const_cast<ListEntry*>(this);
};
