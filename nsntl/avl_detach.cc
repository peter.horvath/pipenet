/**
 * Complex avl method to remove an element from the avl tree while keeping its balance.
 */

#include "avl.h"
#include "exception.h"

using namespace nsntl;

void nsntl::AvlEntry::detach() {
  if (!t)
    throw LogicException(LogicError::ALREADY_DETACHED);

  if (!left && !right) {
    if (parent) {
      if (isLeftChild())
        parent->left = 0;
      else
        parent->right = 0;
      parent->fixHsRec();
      parent->balanceRec();
      fixRoot();
    } else {
      t->root = 0;
    }
  } else if (!left && right) {
    if (!parent) {
      t->root = right;
      right->parent = 0;
    } else {
      if (isLeftChild())
        parent->left = right;
      else
        parent->right = right;
      right->parent = parent;
      parent->fixHsRec();
      parent->balanceRec();
      parent->fixRoot();
    }
  } else if (left && !right) {
    if (!parent) {
      t->root = left;
      left->parent = 0;
    } else {
      if (isLeftChild())
        parent->left = left;
      else
        parent->right = left;
      left->parent = parent;
      parent->fixHsRec();
      parent->balanceRec();
      parent->fixRoot();
    }
  } else {
    uint64_t ls = left->size;
    uint64_t rs = right->size;
    AvlEntry* nxt;
    AvlEntry* nextRootFrom;
    if (ls < rs) {
      nextRootFrom = left;
      nxt = right;
      if (nxt->left)
        nxt = nxt->smallestChild();
    } else {
      nextRootFrom = right;
      nxt = left;
      if (nxt->right)
        nxt = nxt->highestChild();
    }
    nxt->detach();
    exchange(nxt);
    nextRootFrom->fixRoot();
  }

  t = 0;
  parent = 0;
  right = 0;
  left = 0;
  size = 1;
  height = 1;
};

/**
 * Exchanges the AvlEntry with another - all their pointers (with the other AvlEntry) are exchanged.
 * The pointers of the remote objects pointing to them are also exchanged.
 * (Derived classes, etc are no-issue, because they can point only to the AvlEntries, which remain the same, and not to their members.)
 * It is used currently only from the detach() method, but it is usable everywhere.
 * In theory, it could even exchange entries between different Avl trees.
 */
void nsntl::AvlEntry::exchange(AvlEntry* nxt) {
  AvlEntry* this_parent = parent;
  AvlEntry* this_left = left;
  AvlEntry* this_right = right;
  Avl* this_t = t;
  uint64_t this_size = size;
  uint32_t this_height = height;

  AvlEntry* nxt_parent = nxt->parent;
  AvlEntry* nxt_left = nxt->left;
  AvlEntry* nxt_right = nxt->right;
  Avl* nxt_t = nxt->t;
  uint64_t nxt_size = nxt->size;
  uint32_t nxt_height = nxt->height;

  // possible pointers to an AvlEntry object:
  // - left of parent
  if (this_parent && this_parent->left == this)
    this_parent->left = nxt;
  if (nxt_parent && nxt_parent->left == nxt)
    nxt_parent->left = this;

  // - right of parent
  if (this_parent && this_parent->right == this)
    this_parent->right = nxt;
  if (nxt_parent && nxt_parent->right == nxt)
    nxt_parent->right = this;

  // - parent of left
  if (this_left)
    this_left->parent = nxt;
  if (nxt_left)
    nxt_left->parent = this;

  // - parent of right
  if (this_right)
    this_right->parent = nxt;
  if (nxt_right)
    nxt_right->parent = this;

  // - root of tree
  if (this_t && this_t->root == this)
    this_t->root = nxt;
  if (nxt_t && nxt_t->root == nxt)
    nxt_t->root = this;

  // swap local pointers between this and nxt
  parent = nxt_parent;
  left = nxt_left;
  right = nxt_right;
  t = nxt_t;
  size = nxt_size;
  height = nxt_height;

  nxt->parent = this_parent;
  nxt->left = this_left;
  nxt->right = this_right;
  nxt->t = this_t;
  nxt->size = this_size;
  nxt->height = this_height;
};
