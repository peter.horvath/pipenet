/**
 * Utility methods used by the core logic
 */

#define avl_util_internal

#include "avl.h"
#include "exception.h"

using namespace nsntl;

bool nsntl::AvlEntry::isLeftChild() const {
  return parent && this == parent->left;
};

bool nsntl::AvlEntry::isRightChild() const {
  return parent && this == parent->right;
};

uint64_t nsntl::AvlEntry::leftSize() const {
  return left ? left->size : 0;
};

uint64_t nsntl::AvlEntry::rightSize() const {
  return right ? right->size : 0;
};

uint32_t nsntl::AvlEntry::leftHeight() const {
  return left ? left->height : 0;
};

uint32_t nsntl::AvlEntry::rightHeight() const {
  return right ? right->height : 0;
};

// gives back the smallest node _below_ the node. If there is none,
// returns nil
AvlEntry* nsntl::AvlEntry::smallestChild() const {
  if (!left)
    return 0;

  const AvlEntry* i = this;
  for (;;) {
    i = i->left;
    if (!i->left)
      return const_cast<AvlEntry*>(i);
  }
};

// gives back the highest node _below_ the node. If there is none,
// returns nil
AvlEntry* nsntl::AvlEntry::highestChild() const {
  if (!right)
    return 0;

  const AvlEntry* i = this;
  for (;;) {
    i = i->right;
    if (!i->right)
      return const_cast<AvlEntry*>(i);
  }
};

// can be called **only** if left == null !
void nsntl::AvlEntry::addLeafLeft(AvlEntry* i) {
  left = i;
  i->parent = this;
  fixHsRec();
  balanceRec();
  fixRoot();
};

// can be called **only** if right == null !
void nsntl::AvlEntry::addLeafRight(AvlEntry* i) {
  right = i;
  i->parent = this;
  fixHsRec();
  balanceRec();
  fixRoot();
};
