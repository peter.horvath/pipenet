#ifndef nsntl_catom_h
#define nsntl_catom_h

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void nsntl_atomic_u16_init(volatile uint16_t* p, uint16_t desired);
uint16_t nsntl_atomic_u16_load(volatile uint16_t* p);
void nsntl_atomic_u16_store(volatile uint16_t* p, uint16_t n);
uint16_t nsntl_atomic_u16_inc(volatile uint16_t* p);
uint16_t nsntl_atomic_u16_exchange(volatile uint16_t* p, uint16_t v);

void nsntl_atomic_u32_init(volatile uint32_t* p, uint32_t desired);
uint32_t nsntl_atomic_u32_load(volatile uint32_t* p);
void nsntl_atomic_u32_store(volatile uint32_t* p, uint32_t n);
uint32_t nsntl_atomic_u32_inc(volatile uint32_t* p);
uint32_t nsntl_atomic_u32_exchange(volatile uint32_t* p, uint32_t v);

void nsntl_atomic_u64_init(volatile uint64_t* p, uint64_t desired);
uint64_t nsntl_atomic_u64_load(volatile uint64_t* p);
void nsntl_atomic_u64_store(volatile uint64_t* p, uint64_t n);
uint64_t nsntl_atomic_u64_inc(volatile uint64_t* p);
uint64_t nsntl_atomic_u64_exchange(volatile uint64_t* p, uint64_t v);

#ifdef __cplusplus
}
#endif

#endif
