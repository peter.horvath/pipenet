#ifndef nsntl_block_h
#define nsntl_block_h

#include <stdint.h>

#include "string.h"

namespace nsntl {

/** c++ interface over a (pointer;size) tuple. */
class Block {
  protected:
    uint64_t size;
    void *addr;

  public:
    Block();
    Block(void* addr, uint64_t size);
    Block(nsntl::string s);
    virtual ~Block();

    void *getAddr() const;
    virtual void setAddr(void* addr);
    uint64_t getSize() const;
    virtual void setSize(uint64_t size);

    operator string() const;
};

/** It is a Block, but allocates memory dynamically. We can only set its size. */
class DynBlock : public virtual Block {
  public:
    DynBlock(uint64_t size=0);
    DynBlock(const Block& block);
    virtual ~DynBlock();

    void setAddr(void* addr); // not allowed, throws exception
    void setSize(uint64_t size);
};

};

#endif
