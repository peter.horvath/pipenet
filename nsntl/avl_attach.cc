/**
 * Core functionality: adds an element to the avl tree, and balances it if needed.
 */

#include "avl.h"
#include "collection_core.h"
#include "exception.h"

using namespace nsntl;

void AvlEntry::attach(Avl* t) {
  if (this->t)
    throw LogicException(LogicError::ALREADY_ATTACHED);
  this->t = t;
  
  if (!t->root) {
    t->root = this;
    return;
  }

  bool toLeft;
  AvlEntry* n = t->root;
  
  for (;;) {
    int cr = t->compare(this, n);
    if (cr < 0) {
      toLeft = true;
    } else if (cr > 0) {
      toLeft = false;
    } else {
      if ((uint8_t)t->flags & (uint8_t)CollectionFlags::ALLOW_DUPLICATES)
        toLeft = leftSize() < rightSize();
      else
        throw LogicException(LogicError::NO_DUPES);
    }

    if (toLeft) {
      if (!n->left) {
        n->addLeafLeft(this);
        return;
      } else {
        n = n->left;
      }
    } else {
      if (!n->right) {
        n->addLeafRight(this);
        return;
      } else {
        n = n->right;
      }
    }
  };
};
