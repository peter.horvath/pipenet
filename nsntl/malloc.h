#ifndef nsntl_malloc_h
#define nsntl_malloc_h

#include <stddef.h>

#include "list_core.h"

namespace nsntl {

// mostly c++-ish wrappers to the libc malloc-related calls
void* Memdup(void* src, uint64_t size);
void* Malloc(uint64_t len);
void* Calloc(uint64_t len);
void* Realloc(void*& addr, uint64_t size);
void Free(void* addr); 

class Allocator {
  public:
    virtual void* alloc(size_t size)=0;
    virtual void free(void* p)=0;
};

class DefaultAllocator : public Allocator {
  public:
    void* alloc(size_t size);
    void free(void* p);
};

/** Throws a LogicError::UNREACHABLE_CODE on any try. Useful for collections where the entries are managed by the user. */
class AbortAllocator : public Allocator {
  public:
    void* alloc(size_t size);
    void free(void* p);
};

extern const DefaultAllocator defaultAllocator;
extern const AbortAllocator abortAllocator;

/** Makes a class inplace-constructable. */
class InplaceNew {
  public:
    void* operator new(size_t size, void* p);
};

/** Simple, NOT THREAD-SAFE accelerator for malloc(). */
class MallocPool;

class MallocPoolEntry : public CoreListEntry {
  friend class MallocPool;
  friend class MallocPooled;

  private:
    MallocPool* getPool() const;
    void* operator new(size_t size, uint64_t itemSize);
    void operator delete(void* p);
};

class MallocPool : public Allocator {
  friend MallocPoolEntry;

  private:
    uint64_t itemSize;

    // we start to physically free elements, if
    // used.size() < free.size() * spareMul
    //   &&
    // free.size() >= spareMax
    // We physically free at most spareOnce elements in a bunch

    uint64_t spareMul;
    uint64_t spareMax;
    uint64_t spareOnce;

    CoreList freePool, allocPool;
    uint64_t freeSize, allocSize;

    void freeBunch();

  public:
    MallocPool(uint64_t itemSize);
    ~MallocPool();

    // these affect only the behavior in the future. They don't start freeBunch()
    uint64_t getSpareMul() const;
    void setSpareMul(uint64_t spareMul);
    uint64_t getSpareMax() const;
    void setSpareMax(uint64_t spareMax);
    uint64_t getSpareOnce() const;
    void setSpareOnce(uint64_t spareOnce);

    void* get(uint64_t itemSize);
    void release(void* p);
    void release(MallocPoolEntry* pe);

    static MallocPoolEntry* poolEntryOf(void* p);
    static void Free(void* p);
    static void Free(MallocPoolEntry* pe);

    // we are also an Allocator
    void* alloc(size_t size);
    void free(void* p);

  private:
    static MallocPool* fromAllocPool(CoreList* allocPool);
};

class MallocPooled : public InplaceNew {
  public:
    void* operator new(size_t size, MallocPool& pool);
    void operator delete(void* p);
};

};

#endif
