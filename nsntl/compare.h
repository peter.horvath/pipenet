#ifndef nsntl_compare_h
#define nsntl_compare_h

#include <stdint.h>

namespace nsntl {

typedef enum {
  /** Finds the largest AvlEntry lighter than the parameter */
  FIND_LT,

  /** Finds the largest AvlEntry lighter than or equal to the parameter */
  FIND_LE,

  /** Finds an AvlEntry equal to the parameter */
  FIND_EQ,

  /** Finds the smallest AvlEntry greater than or equal to the parameter */
  FIND_GE,

  /** Finds the smallest AvlEntry greater than the parameter */
  FIND_GT
} FindRelation;

/**
 * Anything comparable.
 */
class Comparable {
  public:
    virtual int compare(const Comparable* b) const = 0;

  protected:
    virtual ~Comparable();
};

int compare(const void* a, const void* b);
int compare(int64_t a, int64_t b);
int compare(uint64_t a, uint64_t b);
int compare(const Comparable* a, const Comparable* b);

};

#endif
