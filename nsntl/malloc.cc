#include <stdlib.h>
#include <string.h>

#include "exception.h"
#include "malloc.h"
#include "util.h"

#pragma GCC diagnostic ignored "-Winvalid-offsetof"

using namespace nsntl;

const nsntl::DefaultAllocator nsntl::defaultAllocator;
const nsntl::AbortAllocator nsntl::abortAllocator;

void* nsntl::Memdup(void* src, uint64_t size) {
  void* ret = Malloc(size);
  memcpy(ret, src, size);
  return ret;
};

void* nsntl::Malloc(uint64_t len) {
  void *r=malloc(len);
  if (!r)
    throw LogicException(LogicError::MALLOC_FAILS);
  return r;
};

void* nsntl::Calloc(uint64_t len) {
  void* r = Malloc(len);
  Bzero(r, len);
  return r;
};

void* nsntl::Realloc(void*& addr, uint64_t size) {
  if (!size) {
    if (addr)
      free(addr);
    addr = NULL;
  } else {
    if (!addr)
      addr = Malloc(size);
    else {
      void *newAddr=realloc(addr, size);
      if (!newAddr)
        throw LogicException(LogicError::REALLOC_FAILS);
      addr = newAddr;
    }
  }
  return addr;
};

void nsntl::Free(void* addr) {
  free(addr);
};

void* nsntl::DefaultAllocator::alloc(size_t size) {
  return Malloc(size);
};

void nsntl::DefaultAllocator::free(void* p) {
  return Free(p);
};

void* nsntl::AbortAllocator::alloc(size_t size) {
  throw LogicException(LogicError::UNREACHABLE_CODE);
};

void nsntl::AbortAllocator::free(void* p) {
  throw LogicException(LogicError::UNREACHABLE_CODE);
};

void* nsntl::InplaceNew::operator new(size_t size, void* p) {
  return p;
};

MallocPool* nsntl::MallocPoolEntry::getPool() const {
  return MallocPool::fromAllocPool(this->list());
};

void* nsntl::MallocPoolEntry::operator new(size_t size, uint64_t itemSize) {
  return Malloc(size + itemSize);
};

void nsntl::MallocPoolEntry::operator delete(void* p) {
  Free(p);
};

void nsntl::MallocPool::freeBunch() {
  uint64_t deld = 0;

  while (allocSize <= freeSize * spareMul && freeSize >= spareMax && deld < spareOnce) {
    delete static_cast<MallocPoolEntry*>(freePool.firstEntry());
    deld++;
    freeSize--;
  }
};

nsntl::MallocPool::MallocPool(uint64_t itemSize) : freePool(), allocPool() {
  this->itemSize = itemSize;
  spareMul = 3;
  spareMax = 3;
  spareOnce = 3;
  freeSize = 0;
  allocSize = 0;
};

nsntl::MallocPool::~MallocPool() {
  if (!allocPool.isEmpty()) {
    abort();
    //throw LogicException(LogicError::POOL_WITH_USED_DATA);
  }
  while (!freePool.isEmpty())
    delete static_cast<MallocPoolEntry*>(freePool.firstEntry());
};

uint64_t nsntl::MallocPool::getSpareMul() const {
  return spareMul;
};

void nsntl::MallocPool::setSpareMul(uint64_t spareMul) {
  this->spareMul = spareMul;
};

uint64_t nsntl::MallocPool::getSpareMax() const {
  return spareMax;
};

void nsntl::MallocPool::setSpareMax(uint64_t spareMax) {
  this->spareMax = spareMax;
};

uint64_t nsntl::MallocPool::getSpareOnce() const {
  return spareOnce;
};

void nsntl::MallocPool::setSpareOnce(uint64_t spareOnce) {
  this->spareOnce = spareOnce;
};

void* nsntl::MallocPool::get(uint64_t itemSize) {
  if (itemSize != this->itemSize)
    throw LogicException(LogicError::INVALID_SIZE);

  MallocPoolEntry* poolEntry;

  if (freePool.isEmpty()) {
    poolEntry = new(itemSize) MallocPoolEntry();
    allocPool.attach(poolEntry);
    allocSize++;
  } else {
    poolEntry = static_cast<MallocPoolEntry*>(freePool.lastEntry());
    freePool.detach(poolEntry);
    allocPool.attach(poolEntry);
    allocSize++;
    freeSize--;
  }

  freeBunch();

  return reinterpret_cast<void*>(reinterpret_cast<uint8_t*>(poolEntry) + sizeof(MallocPoolEntry));
};

void nsntl::MallocPool::release(void* p) {
  MallocPoolEntry* pe = poolEntryOf(p);
  release(pe);
};

void nsntl::MallocPool::release(MallocPoolEntry* pe) {
  if (pe->getPool() != this)
    throw LogicException(LogicError::NOT_MY_ENTRY);
  allocPool.detach(pe);
  freePool.attach(pe);
  allocSize--;
  freeSize++;
  freeBunch();
};

void* nsntl::MallocPool::alloc(size_t size) {
  return get(size);
};

void nsntl::MallocPool::free(void* p) {
  return release(p);
};

MallocPoolEntry* nsntl::MallocPool::poolEntryOf(void* p) {
  return reinterpret_cast<MallocPoolEntry*>(reinterpret_cast<uint8_t*>(p) - sizeof(MallocPoolEntry));
};

void nsntl::MallocPool::Free(void* p) {
  MallocPoolEntry* pe = MallocPool::poolEntryOf(p);
  MallocPool::Free(pe);
};

void nsntl::MallocPool::Free(MallocPoolEntry* pe) {
  MallocPool* pool = pe->getPool();
  pool->release(pe);
};

nsntl::MallocPool* nsntl::MallocPool::fromAllocPool(CoreList* allocPool) {
  size_t offset = offsetof(MallocPool, allocPool);
  return reinterpret_cast<MallocPool*>(reinterpret_cast<uint8_t*>(allocPool) - offset);
};

void* nsntl::MallocPooled::operator new(size_t size, MallocPool& pool) {
  return pool.get(size);
};

void nsntl::MallocPooled::operator delete(void* p) {
  MallocPoolEntry* pe = MallocPool::poolEntryOf(p);
  pe->getPool()->release(pe);
};
