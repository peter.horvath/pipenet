#ifndef nsntl_set_h
#define nsntl_set_h

#include "avl.h"
#include "collection_core.h"
#include "malloc.h"

namespace nsntl {

class SetEntry;

/**
 * A simple wrapper class around Avl.
 */
class Set : protected virtual Collection, protected Avl {
  friend SetEntry;

  protected:
    Set(uint8_t flags);
    Set(Allocator* entryAllocator = 0, uint8_t flags = 0);
    virtual ~Set() override;

    void genSet(CollectionEntry* entry, const void* p) override;
    void derAttach(CollectionEntry* entry) override;
    void derDetach(CollectionEntry* entry) override;

  public:
    const void* getEntryType() const override;
    uint64_t getSizeofEntry() const override;
    bool isEmpty() const override;
    uint64_t size() const override;
    Collection* getCollection() const override;
    CollectionEntry* firstEntry() const override;
    CollectionEntry* lastEntry() const override;
    CollectionEntry* nextOf(const CollectionEntry* entry) const override;
    CollectionEntry* prevOf(const CollectionEntry* entry) const override;

  private:
    void initEntry(void* p) override;
    void uninitEntry(void* p) override;
    CollectionEntry* genFindEntry(const void* p, FindRelation relation = FIND_EQ) const override;
    CollectionEntry* getEntryFromContent(const void* p) const override;

    void attach(SetEntry* entry);
    void detach(SetEntry* entry);

    void forEach(CollectionEntry* f(SetEntry* list)); // TODO
    void forEach(void* self, CollectionEntry* f(void* self, SetEntry* li)); // TODO

    static int findHelper(const AvlEntry* a, const void* v);

  private:
    int compare(const AvlEntry* a, const AvlEntry* b) const override final;
    Avl* getAvl() const;
};

class SetEntry : protected CollectionEntry, protected AvlEntry {
  friend Set;

  public:
    SetEntry();
    SetEntry(Set* s);
    virtual ~SetEntry() override;

    Set* getSet() const;
    SetEntry* next() const;
    SetEntry* prev() const;

    Collection* getCollection() const override;
    const void* getEntryType() const override;

  private:
    static SetEntry* fromAvlEntry(AvlEntry* a);
    CollectionEntry* getCollectionEntry() const override;
};

};

#endif
