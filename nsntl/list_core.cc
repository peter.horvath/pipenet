#include "exception.h"
#include "list_core.h"

using namespace nsntl;

nsntl::CoreList::CoreList() {
  frst = 0;
  lst = 0;
};

nsntl::CoreList::~CoreList() {
  while (!this->isEmpty()) {
    detach(firstEntry());
  }
};

void nsntl::CoreList::attach(CoreListEntry* entry, bool toFront) {
  if (entry->lst)
    throw LogicException(LogicError::ALREADY_ATTACHED);
  if (!frst) {
    frst = entry;
    lst = entry;
    entry->prv = 0;
    entry->nxt = 0;
  } else {
    if (toFront) {
      entry->nxt = frst;
      entry->prv = 0;
      frst->prv = entry;
      frst = entry;
    } else {
      entry->nxt = 0;
      entry->prv = lst;
      lst->nxt = entry;
      lst = entry;
    }
  }
  entry->lst = this;
};

void nsntl::CoreList::attachBefore(CoreListEntry* entry, CoreListEntry* before) {
  if (entry->lst)
    throw LogicException(LogicError::ALREADY_ATTACHED);
  if (!before || before == frst) {
    attach(entry, true);
    return;
  }
  if (before->lst != this)
    throw LogicException(LogicError::NOT_MY_ENTRY);
  entry->lst=this;
  entry->prv=before->prv;
  before->prv->nxt=entry;
  before->prv=entry;
  entry->nxt=before;
};

void nsntl::CoreList::attachAfter(CoreListEntry* entry, CoreListEntry* after) {
  if (entry->lst)
    throw LogicException(LogicError::ALREADY_ATTACHED);
  if (!after || after == lst) {
    attach(entry, false);
    return;
  }
  if (after->lst != this)
    throw LogicException(LogicError::NOT_MY_ENTRY);
  entry->lst=this;
  entry->nxt=after->nxt;
  after->nxt->prv=entry;
  after->nxt=entry;
  entry->prv=after;
};

void nsntl::CoreList::detach(CoreListEntry* entry) {
  if (!entry->lst)
    throw LogicException(LogicError::ALREADY_DETACHED);
  if (entry->lst != this)
    throw LogicException(LogicError::NOT_MY_ENTRY);

  if (entry->prv)
    entry->prv->nxt = entry->nxt;
  else
    frst = entry->nxt;
  if (entry->nxt)
    entry->nxt->prv = entry->prv;
  else
    lst = entry->prv;
  entry->prv = 0;
  entry->nxt = 0;
  entry->lst = 0;
};
nsntl::CoreListEntry* nsntl::CoreList::firstEntry() const {
  return this->frst;
};

nsntl::CoreListEntry* nsntl::CoreList::lastEntry() const {
  return this->lst;
};

bool nsntl::CoreList::isEmpty() const {
  return !this->frst;
};

static int fProxy(void* self, CoreListEntry* li) {
  int (*f)(CoreListEntry*) = (int (*)(CoreListEntry*))self;
  return f(li);
};

void nsntl::CoreList::forEach(int f(CoreListEntry* li)) {
  forEach((void*)f, fProxy);
};

void nsntl::CoreList::forEach(void* self, int f(void*, CoreListEntry*)) {
  for (CoreListEntry* li=frst; li; li=li->nxt) {
    int ret = f(self, li);
    bool remove = false;
    CoreListEntry* nxt;
    if (ret) {
      remove = true;
      nxt = li->nxt;
      if (ret > 0)
        detach(li);
      else {
        detach(li);
        delete li;
      }
    }
    if (remove)
      li = nxt;
  };
};

nsntl::CoreListEntry::CoreListEntry(CoreList* list) {
  this->lst = 0;
  nxt = 0;
  prv = 0;
  if (list)
    list->attach(this);
};

nsntl::CoreListEntry::~CoreListEntry() {
  if (lst)
    lst->detach(this);
};

nsntl::CoreList* nsntl::CoreListEntry::list() const {
  return this->lst;
};

nsntl::CoreListEntry* nsntl::CoreListEntry::next() const {
  return this->nxt;
};

nsntl::CoreListEntry* nsntl::CoreListEntry::prev() const {
  return this->prv;
};
