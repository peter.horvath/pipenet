#include "exception.h"
#include "types.h"

using namespace nsntl;

nsntl::uint128_t::uint128_t() {
  h = 0;
  l = 0;
};

nsntl::uint128_t::uint128_t(uint64_t l) : uint128_t(0, l) {
  // nothing now
};

nsntl::uint128_t::uint128_t(uint64_t h, uint64_t l) {
  this->h = h;
  this->l = l;
};

nsntl::uint128_t::uint128_t(int32_t i) : uint128_t((uint64_t)i) {
  // nothing now
};

nsntl::uint128_t::uint128_t(double f) {
  h = f/4294967296/4294967296;
  l = f-(double)h*4294967296*4294967296;
};

/*
nsntl::uint128_t::uint128_t(long long unsigned int l) : uint128_t((uint64_t)l) {
  // nothing now
};
*/

nsntl::uint128_t& nsntl::uint128_t::operator=(int32_t p) {
  h = 0;
  l = p;
  return *this;
};

nsntl::uint128_t& nsntl::uint128_t::operator=(uint64_t p) {
  h = 0;
  l = p;
  return *this;
};

nsntl::uint128_t& nsntl::uint128_t::operator=(const uint128_t& p) {
  h = p.h;
  l = p.l;
  return *this;
};

nsntl::uint128_t& nsntl::uint128_t::operator=(double f) {
  *this = uint128_t(f);
  return *this;
};

bool nsntl::uint128_t::operator==(const uint128_t& p) const {
  return h == p.h && l == p.l;
};

bool nsntl::uint128_t::operator<(const uint128_t& p) const {
  if (h<p.h)
    return true;
  if (h>p.h)
    return false;
  return l<p.l;
};

bool nsntl::uint128_t::operator<=(const uint128_t& p) const {
  if (h<p.h)
    return true;
  if (h>p.h)
    return false;
  return l<=p.l;
};

bool nsntl::uint128_t::operator>(const uint128_t& p) const {
  if (h>p.h)
    return true;
  if (h<p.h)
    return false;
  return l>p.l;
};

bool nsntl::uint128_t::operator>=(const uint128_t& p) const {
  if (h>p.h)
    return true;
  if (h<p.h)
    return false;
  return l>=p.l;
};

uint128_t nsntl::uint128_t::operator+(const uint128_t& p) const {
  uint128_t r(h+p.h, l+p.l);
  if (r.l<l)
    r.h++;
  return r;
};

uint128_t& nsntl::uint128_t::operator+=(const uint128_t& p) {
  uint128_t tmp = *this + p;
  *this = tmp;
  return *this;
};

uint128_t nsntl::uint128_t::operator-(const uint128_t& p) const {
  uint128_t r(h-p.h, l-p.l);
  if (r.l>l)
    r.h--;
  return r;
};

uint128_t& nsntl::uint128_t::operator-=(const uint128_t& p) {
  uint128_t tmp = *this - p;
  *this = tmp;
  return *this;
};

uint128_t& nsntl::uint128_t::operator++() {
  if (l == ~0ULL) {
    l=0;
    h++;
  } else {
    l++;
  }
  return *this;
};

uint128_t nsntl::uint128_t::operator++(int) {
  uint128_t ret = *this;
  (*this)++;
  return ret;
};

uint128_t& nsntl::uint128_t::operator--() {
  if (!l) {
    h--;
    l = ~0ULL;
  } else {
    l--;
  }
  return *this;
};

uint128_t nsntl::uint128_t::operator--(int) {
  uint128_t ret = *this;
  (*this)--;
  return ret;
};

uint128_t nsntl::uint128_t::operator>>(int n) const {
  if (!n) {
    return *this;
  } else if (n<64) {
    uint128_t r(h>>n, l>>n);
    uint64_t trans = h << (64-n);
    r.l |= trans;
    return r;
  } else
    return uint128_t(h>>(n-64));
};

uint128_t& nsntl::uint128_t::operator>>=(int n) {
  *this = *this>>n;
  return *this;
};

uint128_t nsntl::uint128_t::operator<<(int n) const {
  if (!n) {
    return *this;
  } else if (n<64) {
    uint128_t r(h<<n, l<<n);
    uint64_t trans = l >> (64-n);
    r.h |= trans;
    return r;
  } else
    return uint128_t(l<<(n-64),0);
};

uint128_t& nsntl::uint128_t::operator<<=(int n) {
  *this = *this<<n;
  return *this;
};

uint128_t nsntl::uint128_t::operator*(const uint128_t& p) const {
  return ((*this * p.h) << 64) + *this * p.l;
};

uint128_t nsntl::uint128_t::operator*(uint64_t p) const {
  return (mul64(h, p)<<64) + mul64(l, p);
};

uint128_t nsntl::uint128_t::operator/(const uint128_t& p) const {
  uint128_t div, mod;
  divmod(*this, p, div, mod);
  return div;
};

uint128_t nsntl::uint128_t::operator%(const uint128_t& p) const {
  uint128_t div, mod;
  divmod(*this, p, div, mod);
  return mod;
};

uint128_t nsntl::uint128_t::operator~() const {
  return uint128_t(~h, ~l);
};

string nsntl::uint128_t::toString() const {
  char buf[64];
  buf[63]=0;
  char* bufp = &(buf[63]);
  uint128_t me = *this, div, mod;
  while (me) {
    divmod(me, uint128_t(10), div, mod);
    me = div;
    bufp--;
    *bufp = '0' + mod.l;
  }
  return nsntl::string(bufp);
};

nsntl::uint128_t::operator nsntl::string() const {
  return this->toString();
};

nsntl::uint128_t::operator bool() const {
  return h || l;
};

nsntl::uint128_t::operator double() const {
  return ((double)h)*4294967296*4294967296+l;
};

nsntl::uint128_t uint128_t::mul64(uint64_t a, uint64_t b) {
  uint64_t ah = a>>32, al = a&0xffffffff, bh = b>>32, bl = b&0xffffffff;
  return (uint128_t(ah*bh)<<64) + (uint128_t(ah*bl)<<32) + (uint128_t(al*bh)<<32) + uint128_t(al*bl);
};

void nsntl::uint128_t::divmod(uint128_t a, uint128_t p, uint128_t& div, uint128_t& mod) {
  double dp = p;
  uint128_t left = a;
  div = 0;
  while (left >= p) {
    uint128_t dec;
    if (left.h) {
      double dleft = left;
      dec = dleft/dp/2;
    } else if (p.h) {
      throw nsntl::LogicException(nsntl::LogicError::UNREACHABLE_CODE);
    } else {
      dec = left.l/p.l;
    }
    div+=dec;
    left-=dec*p;
  };
  mod = left;
};

uint128_t operator*(uint64_t a, const nsntl::uint128_t& b) {
  return b*a;
};

int nsntl::compare(const uint128_t& a, const uint128_t& b) {
  if (int r = compare(a.h, b.h))
    return r;
  else
    return compare(a.l, b.l);
};

nsntl::Uint64::Uint64(uint64_t v) {
  this->v = v;
};

nsntl::Uint64::operator uint64_t() {
  return v;
};

int nsntl::Uint64::compare(uint64_t v) const {
  return nsntl::compare(this->v, v);
};

int nsntl::Uint64::compare(const Comparable* v) const {
  const Uint64* vv = static_cast<const Uint64*>(v);
  return nsntl::compare(this->v, vv->v);
};

nsntl::Int64::Int64(int64_t v) {
  this->v = v;
};

nsntl::Int64::operator int64_t() {
  return v;
};

int nsntl::Int64::compare(int64_t v) const {
  return nsntl::compare(this->v, v);
};

int nsntl::Int64::compare(const Comparable* v) const {
  const Int64* vv = static_cast<const Int64*>(v);
  return nsntl::compare(this->v, vv->v);
};

uint64_t nsntl::Uint64Content::getSizeofContent() const {
  return sizeof(uint64_t);
};

void nsntl::Uint64Content::initContent(void* p, const void* src) {
  if (src)
    *reinterpret_cast<uint64_t*>(p) = *reinterpret_cast<const uint64_t*>(src);
};

void nsntl::Uint64Content::uninitContent(void* p) {
  // nothing now
};

int nsntl::Uint64Content::compareContent(const void* a, const void* b) const {
  uint64_t aa = *reinterpret_cast<const uint64_t*>(a);
  uint64_t bb = *reinterpret_cast<const uint64_t*>(b);
  return nsntl::compare(aa, bb);
};

bool nsntl::Uint64Content::has(uint64_t i, FindRelation relation) const {
  return genHas(&i, relation);
};

uint64_t nsntl::Uint64Content::find(uint64_t i, FindRelation relation) const {
  CollectionEntry* e = findEntry(i, relation);
  if (e)
    return get(e);
  else
    throw LogicException(LogicError::NOT_FOUND);
};

CollectionEntry* nsntl::Uint64Content::findEntry(uint64_t i, FindRelation relation) const {
  return genFindEntry(&i, relation);
};

CollectionEntry* nsntl::Uint64Content::add(uint64_t i) {
  return genAdd(&i);
};

bool nsntl::Uint64Content::remove(uint64_t i) {
  return genRemove(&i);
};

uint64_t nsntl::Uint64Content::get(const CollectionEntry* entry) const {
  return *reinterpret_cast<uint64_t*>(getContentP(entry));
};

void nsntl::Uint64Content::set(CollectionEntry* entry, uint64_t i) {
  genSet(entry, &i);
};

uint64_t nsntl::PtrContent::getSizeofContent() const {
  return sizeof(void*);
};

void nsntl::PtrContent::initContent(void* p, const void* src) {
  if (src) {
    void* p1 = const_cast<void*>(src);
    void** p2 = reinterpret_cast<void**>(p1);
    void* p3 = *p2;
    *reinterpret_cast<void**>(p)=p3;
  } else
    throw LogicException(LogicError::NULLPTR_IN_COLLECTION);
};

void nsntl::PtrContent::uninitContent(void* p) {
  *static_cast<void**>(p)=0;
};

int nsntl::PtrContent::compareContent(const void* a, const void* b) const {
  void* a2 = const_cast<void*>(a);
  void* b2 = const_cast<void*>(b);
  void** a3 = reinterpret_cast<void**>(a2);
  void** b3 = reinterpret_cast<void**>(b2);
  void* a4 = *a3;
  void* b4 = *b3;
  return nsntl::compare(a4, b4);
};

bool nsntl::PtrContent::has(void* p, FindRelation relation) const {
  return genFindEntry(&p, relation);
};

void* nsntl::PtrContent::find(void* p, FindRelation relation) const {
  CollectionEntry* e = findEntry(p, relation);
  if (e)
    return get(e);
  else
    throw LogicException(LogicError::NOT_FOUND);
};

CollectionEntry* nsntl::PtrContent::findEntry(void* p, FindRelation relation) const {
  return genFindEntry(&p, relation);
};

CollectionEntry* nsntl::PtrContent::add(const void* p) {
  return genAdd(&p);
};

bool nsntl::PtrContent::remove(const void* p) {
  return genRemove(&p);
};

void* nsntl::PtrContent::get(const CollectionEntry* entry) const {
  return *reinterpret_cast<void**>(getContentP(entry));
};

void nsntl::PtrContent::set(CollectionEntry* entry, const void* p) {
  genSet(entry, &p);
};
