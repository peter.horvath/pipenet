#ifndef nsntl_types_h
#define nsntl_types_h

#include <stdint.h>

#include "string.h"

namespace nsntl {

class string;

/**
 * Unsigned 128bit integer implementation
 */
class uint128_t {
  public:
    uint64_t h, l;

    uint128_t();
    explicit uint128_t(uint64_t l);
    uint128_t(uint64_t h, uint64_t l);
    explicit uint128_t(int32_t i);
    explicit uint128_t(double f);
    uint128_t& operator=(int32_t i);
    uint128_t& operator=(uint64_t p);
    uint128_t& operator=(const uint128_t& p);
    uint128_t& operator=(double f);
    bool operator==(const uint128_t& p) const;
    bool operator<(const uint128_t& p) const;
    bool operator<=(const uint128_t& p) const;
    bool operator>(const uint128_t& p) const;
    bool operator>=(const uint128_t& p) const;
    int operator<=>(const uint128_t& p) const;
    uint128_t operator+(const uint128_t& p) const;
    uint128_t& operator+=(const uint128_t& p);
    uint128_t operator-(const uint128_t& p) const;
    uint128_t& operator-=(const uint128_t& p);
    uint128_t& operator++();
    uint128_t operator++(int);
    uint128_t& operator--();
    uint128_t operator--(int);
    uint128_t operator>>(int n) const;
    uint128_t& operator>>=(int n);
    uint128_t operator<<(int n) const;
    uint128_t& operator<<=(int n);
    uint128_t operator*(const uint128_t& p) const;
    uint128_t operator*(uint64_t p) const;
    uint128_t operator/(const uint128_t& p) const;
    uint128_t operator%(const uint128_t& p) const;
    uint128_t operator~() const;
    operator bool() const;
    operator nsntl::string() const;
    operator double() const;
    string toString() const;

    static uint128_t mul64(uint64_t a, uint64_t b);
    static void divmod(uint128_t a, uint128_t b, uint128_t& div, uint128_t& mod);
};

int compare(const uint128_t& a, const uint128_t& b);

class Uint64 : public Comparable {
  private:
    uint64_t v;

  public:
    Uint64(uint64_t v=0);
    operator uint64_t();
    int compare(uint64_t v) const;
    int compare(const Comparable* v) const;
};

class Int64 : public Comparable {
  private:
    int64_t v;

  public:
    Int64(int64_t v=0);
    operator int64_t();
    int compare(int64_t v) const;
    int compare(const Comparable* v) const;
};

/** Example: class UintList : public List, public Uint64Collection --> List of Uint64 */
class Uint64Content : public virtual Collection {
  public:
    uint64_t getSizeofContent() const override;
    void initContent(void* p, const void* src) override;
    void uninitContent(void* p) override;
    int compareContent(const void* a, const void* b) const override;

    bool has(uint64_t i, FindRelation relation = FIND_EQ) const;
    uint64_t find(uint64_t i, FindRelation relation = FIND_EQ) const;
    CollectionEntry* findEntry(uint64_t i, FindRelation relation = FIND_EQ) const;
    CollectionEntry* add(uint64_t i);
    bool remove(uint64_t i);
    uint64_t get(const CollectionEntry* e) const;
    void set(CollectionEntry* e, uint64_t i);
};

class PtrContent : public virtual Collection {
  public:
    uint64_t getSizeofContent() const override;
    void initContent(void* p, const void* src) override;
    void uninitContent(void* p) override;
    int compareContent(const void* a, const void* b) const override;

    bool has(void* p, FindRelation relation = FIND_EQ) const;
    void* find(void* p, FindRelation relation = FIND_EQ) const;
    CollectionEntry* findEntry(void* p, FindRelation relation = FIND_EQ) const;
    CollectionEntry* add(const void* p);
    bool remove(const void* p);
    ComparablePtd& get(const CollectionEntry* e) const;
    void set(CollectionEntry* e, ComparablePtd& tgt);
};

}; // namespace nsntl

nsntl::uint128_t operator*(uint64_t a, const nsntl::uint128_t& b);

#endif
