#ifndef nsntl_string_h
#define nsntl_string_h

#include <stdint.h>

#include "collection_core.h"
#include "malloc.h"
#include "types.h"

namespace nsntl {

class uint128_t;

class string;

int compare(const string a, const string b);

class string_core {
  friend class string;
  friend int compare(const nsntl::string a, const nsntl::string b);
  private:
    char* p;
    uint64_t ref;
    uint64_t len;
    string_core();
    string_core(const char* p);
    ~string_core();
};

class string : public InplaceNew { // TODO: it should be a refcounted block
  friend int compare(const nsntl::string a, const nsntl::string b);
  private:
    string_core* c;

  public:
    string();
    string(const char* p);
    string(const uint128_t& n);
    string(uint64_t n);

    ~string();

    const string& operator=(const string& s);
    const string& operator=(char const * s);
    const char* c_str() const;
    uint64_t length() const;
    const string operator+(const string& s) const;

  private:
    void detach_core();
};

class StringContent : public virtual Collection {
  public:
    uint64_t getSizeofContent() const override;
    void initContent(void* p, const void* src) override;
    void uninitContent(void* p) override;
    int compareContent(const void* a, const void* b) const override;

    bool has(const string p, FindRelation relation = FIND_EQ) const;
    string find(const string p, FindRelation relation = FIND_EQ) const;
    CollectionEntry* findEntry(const string p, FindRelation relation = FIND_EQ) const;
    CollectionEntry* add(const string p);
    bool remove(const string p);
    string get(const CollectionEntry* e) const;
    void set(CollectionEntry* e, const string p);
};

};

#endif
