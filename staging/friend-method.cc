// demonstrates that also class methods can be friends

#include <stdio.h>

class A;

class B {
  public:
    void theMethod(A* a);
    void nonono(A* a);
};

class A {
  friend void B::theMethod(A* a);
  private:
    void priv();
};

void A::priv() {
  printf ("done!\n");
};

void B::theMethod(A* a) {
  a->priv();
};

void B::nonono(A* a) {
  // a->priv(); // won't compile
};

int main(int argc, char** argv) {
  A a;
  B b;
  b.theMethod(&a);
  b.nonono(&a);
  return 0;
};
