#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>

int main() {
    using namespace std;
    string sentence = "And I feel fine...";
    //istringstream iss(sentence);
    //istringstream iss(cin);
    //copy(istream_iterator<string>(iss),
    copy(istream_iterator<string>(cin),
         istream_iterator<string>(),
         ostream_iterator<string>(cout, "\n"));
}
