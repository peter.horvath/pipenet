class GCClass;

class GCP {
  private:
    GCClass *src, *dst;
    u64 srcPos;
    std::list<GCP*>::iterator srcIter, dstIter;
};

class GCClass {
  private:
    std::list<GCP*> srcList, dstList;
    u64 ref;
    bool root;
    int gen;
};

class GCContainer;

class GCContainerItem {
  private:
    GCContainer* container;
    GCClass* elem;
    
};

class GCContainer : protected GCClass {
  private:
    std::list<GCContainerItem*> item;
};

class Cl1 {
  public:
    int a;
    GCP b;

    Cl1() {
      b.srcPos = ;
    };
};
