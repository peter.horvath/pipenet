#ifndef nsntl_list_h
#define nsntl_list_h

namespace Nsntl {

class ListItem {
  private:
    ListItem *prev, *next;

  public:
    ListItem();
    virtual ~ListItem();

    ListItem* getPrev();
    ListItem* getNext();
};

class List {
  private:
    ListItem *first, *last;

  public:
    List();
    virtual ~List();

    ListItem& getFirst();
    ListItem& getLast();

    void add(ListItem& listItem);
    void addBefore(ListItem& item, ListItem& next);
    void addAfter(ListItem& prev, ListItem& item);
    void remove(ListItem& listItem);
};

class ListMember {
};

class ListItemMember : public ListItem {
  private:
    ListMember *parent;

  public:
    ListItemMember(ListMember* parent);
    ListItemMember(ListMember& parent);
    ListMember& getParent();
    virtual ~ListItemMember();
};

};

#endif
