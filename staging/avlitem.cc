nsntl::ItemAvl::ItemAvl(uint8_t flags) : Avl(flags) {
  // nothing now
};

nsntl::ItemAvl::~ItemAvl() {
  // nothing now
};

int nsntl::ItemAvl::compare(AvlItem* a, AvlItem* b) const {
  return *(a->get()) <=> *(b->get());
};

Object* nsntl::ItemAvl::first() const {
  return firstItem()->get();
};

Object* nsntl::ItemAvl::last() const {
  return lastItem()->get();
};

Item* nsntl::ItemAvl::firstItem() const {
  return dynamic_cast<Item*>(firstEntry());
};

Item* nsntl::ItemAvl::lastItem() const {
  return dynamic_cast<Item*>(lastEntry());
};

void nsntl::ItemAvl::forEach(void f(Object* v)) {
  TODO();
};

nsntl::AvlItem::AvlItem(Avl* t, Object* v) : AvlEntry(t), Item(v) {
  // nothing now
};

nsntl::AvlItem::~AvlItem() {
  // nothing now
};

Object* nsntl::AvlItem::get() const {
  return v;
};

void nsntl::AvlItem::set(Object* v) {
  Avl* tree = avl();
  if (tree)
    detach();
  this->v = v;
  if (tree)
    attach(tree);
};
