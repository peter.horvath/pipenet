#ifndef Pipenet_Core_h
#define Pipenet_Core_h

#include <list>

namespace Pipenet {

class Pipenet;

class Node;

class Pipe;

class Event {
  public:
    typedef enum {
      PipeReady = 1,
      PipeBlocked = 2,
      PipeClosed = 3,
      DeleteMe = 4
    } Type;
  public:
    Type type;
    Pipe* pipe;
    Event(Type type, const Pipe* pipe);
};

class Message {
  private:
    Pipenet* pipenet;
    Pipe* pipe;
    std::list<Message*>::iterator pipenetIter;

    void attachPipenet(Pipenet* pipenet);

  public:
    Message();
    virtual ~Message();

    void send(Pipe& pipe);
};

class Pipe {
  friend class Pipenet;
  private:
    bool srcRdy, dstRdy;
    Pipenet *pipenet;
    Node *src, *dst;
    std::list<Pipe*>::iterator pipenetIter, activeIter, srcIter, dstIter;
    Pipe();
    ~Pipe();

  protected:
    void attachPipenet(Pipenet *pipenet);
    void detachPipenet();
    void attachSrc(Node *node);
    void detachSrc();
    void attachDst(Node *node);
    void detachDst();

    void readySrc();
    void blockSrc();
    void readyDst();
    void blockDst();

    Message* recv();
    void send(Message *pipe);

  public:
    virtual void ready()=0;
    virtual void block()=0;
    virtual void close()=0;
};

class SrcPipe : private Pipe {
  public:
    void ready();
    void block();
    void close();
    Message* recv();
};

class DstPipe : private Pipe {
  public:
    void ready();
    void block();
    void close();
    void send(Message* msg);
};

class Node {
  friend class Pipenet;
  private:
    Pipenet *pipenet;
    std::list<Node*>::iterator pipenetIter, zombieIter;
    std::list<Pipe*> pipes;

  protected:
    Node(Pipenet& pipenet);
    virtual ~Node();

    virtual eventHandler(const Event& e)=0;
};

class Pipenet {
  private:
    std::list<Node*> nodes, zombies;
    std::list<Pipe*> pipes, activePipes;
    std::list<Message*> msgs;

  public:
    Pipenet();
    virtual ~Pipenet();
};

};
#endif
