#include <stdio.h>
#include <stdlib.h>

int glob;

int main(int argc, char** argv, char** envp) {
  int stack;
  void* heap = malloc(1);
  printf("glob: %p, stack: %p, heap: %p, argv: %p, &(argv[1]): %p, envp: %p, &(envp[1])=%p\n", &glob, &stack, heap, argv, &(argv[1]), envp, &(envp[1]));
  return 0;
};
