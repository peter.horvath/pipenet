#include <assert.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <ucontext.h>

#include "ss.h"

struct ss_hit {
  int signum;
  siginfo_t siginfo;
  ucontext_t *ucontext;
  struct timeval tv;
  struct timezone tz;
  struct ss_hit *next;
};

struct ss_handler {
  void (*handler)(int);
  void (*action)(int, siginfo_t*, void*);
};

// our array of the real (not wrapped) signal handlers
static struct ss_handler ss_handlers[_NSIG];

// currently executed hit, exclusively in signal handler isn't it NULL
static struct ss_hit *ss_current = NULL;

// if we are in a signal handler, shows our exact start time as gettimeofday(3)
int ss_gettimeofday(struct timeval *tv, struct timezone *tz) {
  if (ss_current) {
    memcpy(tv, &ss_current->tv, sizeof(struct timeval));
    memcpy(tz, &ss_current->tz, sizeof(struct timezone));
    return 0;
  } else {
    errno = EINVAL;
    return -1;
  }
}

static struct ss_hit *ss_newhit(int signum, siginfo_t *siginfo, ucontext_t* ucontext) {
  struct timeval tv;
  struct timezone tz;
  struct ss_hit *hit;
  int r = gettimeofday(&tv, &tz);
  assert(r != -1);

  hit = (struct ss_hit*)malloc(sizeof(struct ss_hit));
  assert(hit);

  hit->signum = signum;
  memcpy(&hit->siginfo, &siginfo, sizeof(siginfo_t));
  hit->ucontext = ucontext;
  memcpy(&hit->tv, &tv, sizeof(struct timeval));
  memcpy(&hit->tz, &tz, sizeof(struct timezone));
  hit->next = NULL;
  return hit;
}

static void ss_fire(struct ss_hit *hit) {
  ss_current = hit;
  if (ss_handlers[hit->signum].handler)
    ss_handlers[hit->signum].handler(hit->signum);
  else
    ss_handlers[hit->signum].action(hit->signum, &hit->siginfo, hit->ucontext);
  ss_current = NULL;
}

// action handler wrapper - here is the reentrant logic
static void ss_wrapper(int signum, siginfo_t* siginfo, void *ucontext) {
  // currently top element on the signal stack
  static struct ss_hit* ss_top = NULL;

  struct ss_hit* hit = ss_newhit(signum, siginfo, (ucontext_t*)ucontext);
  struct ss_hit *bkp;

  again:

  bkp = hit;
  hit = __atomic_exchange_n(&ss_top, hit, __ATOMIC_SEQ_CST);
  if (!hit) { // we got the lock, we are the master
    ss_fire(bkp);
    
    // release the lock, find out if there is new element
    bkp->next = __atomic_exchange_n(&ss_top, bkp->next, __ATOMIC_SEQ_CST);
    if (bkp->next) { // there IS

      hit = bkp;
      free(bkp);
      goto again;

    } else
      free(bkp);
  } else { // we didn't got the lock, but we got the top in hit
    __atomic_store(&hit->next, &bkp, __ATOMIC_SEQ_CST);
  }
}

static int ss_install_action(
    int signum,
    const struct sigaction *act,
    struct sigaction *oldact,
    void (*sa_handler_)(int),
    void (*sa_sigaction_)(int, siginfo_t*, void*)) {
  /* install action */
  struct sigaction wrapper;
  ss_handlers[signum].handler = NULL;
  ss_handlers[signum].action = act->sa_sigaction;
  memcpy(&wrapper, act, sizeof(struct sigaction));
  wrapper.sa_sigaction = ss_wrapper;
  //wrapper.sa_flags |= SA_SIGINFO | SA_NODEFER | SA_RESTART;
  return sigaction(signum, &wrapper, oldact);
}

static void ss_uninstall_action(int signum) {
  ss_handlers[signum].handler = NULL;
  ss_handlers[signum].action = NULL;
}

// sigaction (2) wrapper
int ss_sigaction(int signum, const struct sigaction *act, struct sigaction *oldact) {
  int ret=0;

  if (signum<1 || signum>=_NSIG) {
    errno = EINVAL;
    return -1;
  }

  // if act isn't NULL, we install our wrapped sigaction
  if (act) {
    if (act->sa_handler && act->sa_sigaction) { // both of them can't be specified
      errno = EINVAL;
      return -1;
    }

    if (act->sa_flags & SA_SIGINFO) {
      /* install new action */
      ret = ss_install_action(signum, act, oldact, NULL, act->sa_sigaction);
    } else if (act->sa_handler == SIG_IGN || act->sa_handler == SIG_DFL) { /* uninstall action/handler */
      ss_uninstall_action(signum);
      ret = sigaction(signum, act, oldact);
    } else {
      /* install new handler */
      ret = ss_install_action(signum, act, oldact, act->sa_handler, NULL);
    }
  }

  // if oldact isn't NULL and points to our wrapper, we give the wrapped functions back
  if (oldact && oldact->sa_sigaction == ss_wrapper) {
    oldact->sa_handler = ss_handlers[signum].handler;
    oldact->sa_sigaction = ss_handlers[signum].action;
  }

  return ret;
}

// signal (2) wrapper
void (*ss_signal(int signum, void (*handler)(int)))(int signum) { // well...
  if (handler == SIG_IGN || handler == SIG_DFL) { // defaulting signal, wrapper uninstall
    //int (*fp1)(double) = func1;
    void (*prev_handler)(int) = ss_handlers[signum].handler;
    void (*r)(int signum);

    ss_uninstall_action(signum);
    r=signal(signum, handler);
    if (r==SIG_ERR)
      return SIG_ERR;
    else {
      // if we wrapped it already, we give back the wrapped handler
      if (prev_handler == NULL)
        return r;
      else
        return prev_handler;
    }
  } else { // installing handler by sigaction()
    struct sigaction act, oldact;

    // we save the old handler if we had (will be NULL if hadn't)
    void (*oldhandler)(int signum) = ss_handlers[signum].handler;

    // constructing sigaction() structure
    act.sa_handler = NULL;
    act.sa_sigaction = ss_wrapper;
    sigemptyset(&act.sa_mask);
    act.sa_flags = SA_RESTART | SA_NODEFER;
    act.sa_restorer = NULL;

    ss_handlers[signum].handler = handler;
    ss_handlers[signum].action = NULL;

    int ret = sigaction(signum, &act, &oldact);
 
    // if there was an error, we give back the error
    if (ret == -1)
      return SIG_ERR;
    // if the old handler was our wrapper, we give back the wrapped function
    else if (oldact.sa_sigaction == ss_wrapper)
      return oldhandler;
    // else we give back the old handler from the kernel
    else 
      return oldact.sa_handler;
  }
}
