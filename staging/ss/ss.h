//TODO: make this to use rs.h instead of our own atomicity handling
#ifndef SignalSerializer_h
#define SignalSerializer_h

#include <sys/time.h>

// if we are in a signal handler, shows nearly exact start time as gettimeofday (3)
// (gives back the result of a gettimeofday() called in the first line of the wrapper function)
int ss_gettimeofday(struct timeval *tv, struct timezone *tz);

// wrapper around sigaction (2)
int ss_sigaction(int signum, const struct sigaction *act, struct sigaction *oldact);

// wrapper around signal (2) - sorry, but sighandler_t isn't a c11 thing
void (*ss_signal(int signum, void (*handler)(int)))(int);

#endif
