// a trick to overcome the inavailability of casting over non-public inheritances

#include <iostream>

class A {
};

class B : protected A {
  public:
    A* getA() {
      return static_cast<A*>(this);
    };

    static B* fromA(A* a) {
      return static_cast<B*>(a);
    };
};

int main() {
  B b;

  // Does not even compile
  //std::cout << static_cast<A*>(&b);

  // works like charm
  std::cout << b.getA() << '\n';

  // works also in the reverse direction, although it needs a static method
  std::cout << B::fromA(b.getA()) << '\n';
};
