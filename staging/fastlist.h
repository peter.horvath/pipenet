template<class T>
class FastList {
  private:
    T *first, *last;
    size_t size;

  public:
    class iterator {
      private:
        T *prev, *next;
        T content;
        
      public:
        iterator<T> iterator();
        ~iterator();
    };

    FastList<T>();
    ~FastList<T>();

    bool empty() const;
};
