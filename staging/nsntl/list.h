#ifndef Nsntl_list_h
#define Nsntl_list_h

namespace Nsntl {

class List;

class ListItem {
  private:
    ListItem* prev;
    ListItem* next;
    List* list;

  public:
    ListItem();
    virtual ~ListItem();

    List* getList() const;
    void setList(List* list);

    ListItem* getPrev() const;
    void setPrev(ListItem* prev);

    ListItem* getNext() const;
    void setNext(ListItem* next);

    void remove();
    void addFirst(List* list);
    void addLast(List* list);
    void addBefore(ListItem* item);
    void addAfter(ListIterm* item);
};

class List {
  private:
    ListItem* first;
    ListItem* last;

  public:
    List();
    virtual ~List();

    ListItem* getFirst() const;
    void setFirst(ListItem* first);

    ListItem* getLast() const;
    void setLast(ListItem* last);

    void remove(ListItem* item);
    void addFirst(ListItem* item);
    void addLast(ListItem* item);
    void addBefore(ListItem* item, ListItem* before);
    void addAfter(ListItem* after, ListItem* item);
};

};

#endif
