#ifndef Nsntl_list_h
#define Nsntl_list_h

namespace Nsntl {

class Tree {

public:

class Item {
  private:
    Item* parent;
    Item* left;
    Item* right;
    u64 size;
    u64 lvl;
};

typedef int (Comparator)(Item* a, Item* b);

private:
  TreeItem* top;
  u64 size;
  u64 lvl;

};

};

#endif
