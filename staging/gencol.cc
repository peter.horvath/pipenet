/*

class Uint64List : public Uint64Collection, public List {
};

Plan:

1. Uint64Collection provides an implicit constructor call to cast from uint64 to TmpEntry.
2. List provides an "add" method for TmpEntries.

So, Uint64List myList.add(5) will first call the implicit constructor, and then List.add for that.

So, what is needed:

1. Uint64Collection::TmpEntry -- constructor from an uint64_t
2. myList.add(TmpEntry& ...)

Question: is C++ enough smart to call the TmpEntry constructor automatically?

...very likely, no.

YES, IT CAN!!!!!!!!!!!!!!!!!!!!!!!!!! Problem solved.

*/

#include <iostream>

class A {
  public:
    int a;
    A(int a) {
      this->a = a;
    };
};

class B {
  public:
    int b;
    B(int b) {
      this->b = b;
    };
    void inc(const A& a) {
      b+=a.a;
    };
    void dump() {
      std::cout << b << '\n';
    };
};

int main() {
  B b(3);
  b.inc(5);
  b.dump();
};
