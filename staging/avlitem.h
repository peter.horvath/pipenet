
class AvlItem;

class ItemAvl : public virtual Avl {
  public:
    ItemAvl(uint8_t flags = 0);
    ~ItemAvl();

    // compares items by calling comparator of the pointed objects
    virtual int compare(AvlItem* a, AvlItem* b) const;

    Object* first() const;
    Object* last() const;
    Item* firstItem() const;
    Item* lastItem() const;
    void forEach(void f(Object* v)); // TODO
};

class AvlItem : public virtual AvlEntry, public Item {
  public:
    AvlItem(Avl* avl = 0, Object* v = 0);
    AvlItem(Object* v);
    ~AvlItem() noexcept(false);

    virtual Object* get() const;
    virtual void set(Object* v);
};

