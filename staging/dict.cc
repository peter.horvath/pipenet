nsntl::Dict::Dict() : Avl::Avl() {
  // nothing now
};

nsntl::Dict::~Dict() {
  while (root)
    delete root;
};

int nsntl::Dict::compare(const AvlEntry* a, const AvlEntry* b) const {
  const DictEntry *da = static_cast<const DictEntry*>(a);
  const DictEntry *db = static_cast<const DictEntry*>(b);
  return nsntl::compare(da->name, db->name);
};

DictEntry* nsntl::Dict::find(const string name) const {
  DictEntry e(name, string());
  return static_cast<DictEntry*>(Avl::find(&e));
};

bool nsntl::Dict::has(const string name) const {
  return find(name);
};

string nsntl::Dict::get(const string name) const {
  DictEntry* e = find(name);
  if (!e)
    throw LogicException(LogicError::NOT_FOUND);
  else
    return e->value;
};

Dict& nsntl::Dict::set(const string name, const string value) {
  DictEntry* e = find(name);
  if (!e) {
    e = new DictEntry(name, value);
    e->attach(this);
  } else {
    e->detach();
    e->value = value;
    e->attach(this);
  };
  return *this;
};

string nsntl::Dict::operator[](const string name) const {
  return get(name);
};

nsntl::DictEntry::DictEntry(string name, string value) {
  this->name = name;
  this->value = value;
};

nsntl::DictEntry::~DictEntry() {
  // nothing now
};

Dict* nsntl::DictEntry::dict() const {
  return static_cast<Dict*>(avl());
};

DictEntry* nsntl::DictEntry::next() const {
  return static_cast<DictEntry*>(AvlEntry::next());
};

DictEntry* nsntl::DictEntry::prev() const {
  return static_cast<DictEntry*>(AvlEntry::prev());
};
