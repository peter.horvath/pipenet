#include <cstdio>

class A {
  private:
    int a;

  public:
    A() {
      printf ("A::A() on %p\n", this);
    };
};

int main() {
  A a[5];
};
