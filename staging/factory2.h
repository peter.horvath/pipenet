
/*
 * Inherit this class _private_ non-virtually and your class will be subject of dynamic_casts.
 * This C++ standard optimization increases the class size with 1 pointer length.
 */
class IAmPolymorphicNow {
  private:
    virtual void iAmPolymorphicNow();

  protected:
    IAmPolymorphicNow();
    virtual ~IAmPolymorphicNow();
};

/*
 * Factory objects. TODO: write doc.
 */
class Factory {
  // nothing now
};

class FactoryMediator;

class FactoryResult {
  public:
    virtual FactoryResult& operator=(FactoryMediator& fm)=0;
};

typedef void (FactoryMethod)(Factory& factory, FactoryResult& result);

class FactoryMediator {
  private:
    Factory* factory;
    FactoryMethod* factoryMethod;

  public:
    FactoryMediator(Factory& factory, FactoryMethod& factoryMethod);
    void call(FactoryResult& result);
};

typedef void (FactoryFunction)(FactoryResult& result);

