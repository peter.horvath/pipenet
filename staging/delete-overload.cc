#include <cstdio>
#include <cstdlib>

class A {
  public:
    int a;
    A(int a) {
      this->a = a;
    };
    void* operator new(size_t size) {
      void* ret=malloc(size);
      printf ("A::operator new, size: %i, returns %p\n", size, ret);
      return ret;
    };
    void operator delete(void* p) {
      printf ("A::operator delete, p: %p\n", p);
      free(p);
    };
};

class B : public A {
  public:
    int b;
    B(int b) : A(b) {
      this->b = b;
    };
};

int main() {
  A* a = new A(5);
  B* b = new B(6);
  delete a;
  delete b;
};
