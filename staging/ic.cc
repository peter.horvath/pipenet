#include <iostream>

using namespace std;

class A {
  private:
    int a;
  public:
    A() {
      a=0;
    };
    A(void* p) {
      if (p)
        a=1;
      else
        a=0; 
    };
    A(int a) {
      this->a = a;
    };
    int getA() const {
      return a;
    };
};

class B {
  private:
    int b;
  public:
    /*
    B(A* a) {
      if (!a)
        b = 0;
      else
        b = a->getA();
    };
    */
    B(A* p) {
      if (p)
        b=1;
      else
        b=0; 
    };
    B(A& a) {
      b = a.getA();
    };
    int getB() const {
      return b;
    };
};

void showB(const B& b) {
  cout << b.getB() << "\n";
};

int main(int argc, char** argv) {
  A a(5);
  B b(a);
  showB(NULL);
  return 0;
};
