#include <cstdio>

class A {
  public:
    int a;

    A(int a) {
      this->a = a;
    }
};

int main() {
  A a(sizeof(A));
  printf ("%i\n", a.a);
  return 0;
};
