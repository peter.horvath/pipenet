#include <iostream>

class A {
  public:
    virtual A* fun() {
      std::cout << "A::fun()\n";
      return this;
    }
};

class B : public A {
  public:
    B* fun() override {
      std::cout << "B::fun()\n";
      return this;
    };
};

int main() {
  B b;
  B* p = b.fun();
  std::cout << p << '\n';
  return 0;
};
