#ifndef Reentrant_Serializer_h
#define Reentrant_Serializer_h

/* reentrant serializer logic */

#include <stdatomic.h>

struct rs_action;

typedef _Atomic struct rs_action *rs_action_lock;

struct rs_action {
  // id of the action
  unsigned long long id;

  // both of a spinlock and the pointer to the next element in the stack. Used internally only!
  rs_action_lock next;

  // action handler - will be called reentrantly
  void (*handler)(struct rs_action*);
} rs_action;

// process-wide init function
void rs_init();

// must be called on every thread creation
void rs_thread_init();

// initializes an rs_action struct. Prompt can be NULL, it won't be called then.
void rs_action_init(struct rs_action *action, void (*handler)(struct rs_action*));

// calls an rs_action struct - reentrantly
void rs_call(struct rs_action *action);

#endif
