#ifndef Pipenet_Tfba_h
#define Pipenet_Tfba_h

#include <inttypes.h>
#include <stdbool.h>

/*
 * Thread-free buffer array
 * 
 * Buffers can have 3 states: 0. error (0) 1. free (1) 2. locked (2) 3. used (3)
 */

/*
 * Base type
 */
typedef struct {
  volatile uint64_t locks; // array of the locks
  int num; // number of buffers ( 1 <= num <= 31 )
  uint8_t* bufs; // byte-block of the buffers (internal structure is hidden)
} tfba;

/*
 * Allocates an array with malloc() and initializes it. Returns the allocated array
 * @param bufnum The number of the buffers to allocate (max 31)
 * @param buflen The size of the allocated buffers
 * @return pointer to the newly allocated tfba, or NULL on error.
 *         Allowed error if the internal malloc() gives a failure, in this cases errno set as common.
 */
tfba* tfba_init(int bufnum, size_t buflen);

/*
 * Allocates a free, unlocked buffer and locks it.
 * @param tfba Pointer to the buffer array or NULL on error. The
 *        Possible errors: if there is no free buffer, in this case errno == ENOMEM.
 *                         if there is a problematic data structure found, errno == EINVAL.
 * @return Pointer to the allocated buffer
 */
uint8_t* tfba_alloc_free(volatile tfba* ba);

/*
 * Allocates an used, unlocked buffer and locks it.
 * @param tfba Pointer to the buffer array or NULL on error. The
 *        Possible errors: if there is no free buffer, in this case errno == ENOMEM.
 *                         if there is a problematic data structure found, errno == EINVAL.
 * @return Pointer to the allocated buffer
 */
uint8_t* tfba_alloc_used(volatile tfba* ba);

/*
 * Unlocks a locked buffer.
 * @param buf Pointer to the buffer
 * @param locked If true, the buffer is in used state after unlock. If false, it is in unlocked state after unlock.
 * @return 0, if there was no error. If there was an error, returns -1 and errno is set as follows:
 *           EINVAL, if there is a data structure corruption
 *           ENOLCK, if the buffer wasn't locked
 */
int tfba_unlock(uint8_t* buf, bool locked);

#endif
