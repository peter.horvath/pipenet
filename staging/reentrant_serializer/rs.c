#include "rs.h"

static atomic_ullong _nextid;

static _Atomic _Thread_local struct rs_action *_lock;

void rs_init() {
  atomic_init(&_nextid, 1);
  rs_thread_init();
}

void rs_thread_init() {
  atomic_init(&_lock, (rs_action_lock)0);
}

void rs_action_init(struct rs_action *action, void (*handler)(struct rs_action*)) {
  action->id = 0;
  atomic_init(&action->next, (rs_action_lock)0);
  action->handler = handler;
  action->arg = (void*)0;
}

void rs_call(struct rs_action *action) {
  rs_action_lock lock;

  action->id = atomic_fetch_add(&_nextid, 1);
  atomic_init(&lock, action);

  again:

  lock = atomic_exchange(&_lock, lock);
  if (!lock) { // we got the lock, we are the master
    action->handler(action);

    // release the lock, find out if there is a new element
    action->next = atomic_exchange(&_lock, action->next);
    if (action->next) { // there IS
      struct rs_action *oldact = action;
      action = (struct rs_action*)(action->next);
      atomic_store(&oldact->next, (rs_action_lock)0);
      goto again;
    }
  } else { // we didn't got the lock, but we got the top element of the stack
    atomic_store(&lock->next, action);
  }
}
