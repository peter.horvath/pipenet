#include <stdatomic.h>
#include <stdlib.h>

#include "tfba.h"

typedef struct __attribute__((packed)) {
  uint8_t checksum;
  uint8_t no;
  uint64_t size : 48;
} bufheader;

static bool tfba_check_hdr(tfba* ba) {
  if (ba->locks && (3 << 62) != 3 << 62) {
    errno = EINVAL;
    return false;
  }
  errno = 0;
  return true;
}

static bool bh_check_hdr(bufheader ba) {
}

static bufheader* tfba_get_hdr(tfba* ba, int n) {
  
}

static unit8_t* getblock(tfba* ba, int n) {
}

static tfba* getba(uint8_t* buf) {
}

tfba* tfba_init(int bufnum, size_t buflen) {
  tfba* ba = (tfba*)malloc(sizeof(uint64_t) + (sizeof(bufheader) + buflen)*bufnum);
  if (!ba)
    return NULL;
  atomic_init(&(ba->locks), 0);
  for (int a=0; a<32; a++) {
    ba->bufs + (8 + buflen)
  }
  return ba;
}

uint8_t* tfba_alloc_free(volatile tfba* ba) {
  ba->locks = 0;
  return (uint8_t*)ba;
}
