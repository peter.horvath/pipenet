#include <iostream>

class B1 {
  public:
    int b1;
};

class B2 {
  public:
    int b2;
    void* getThis() {
      return reinterpret_cast<void*>(this);
    };
};

class D : public B1, public B2 {
  public:
    int d;
    void* getThis() {
      return reinterpret_cast<void*>(this);
    };
};

int main() {
  D d;
  B1* b1 = static_cast<B1*>(&d);
  B2* b2 = static_cast<B2*>(&d);
  D* dp1 = static_cast<D*>(b1);
  D* dp2 = static_cast<D*>(b2);
  std::cout << "&d:  " << &d << '\n';
  std::cout << "b1:  " << b1 << '\n';
  std::cout << "b2:  " << b2 << '\n';
  std::cout << "dp1: " << dp1 << '\n';
  std::cout << "dp2: " << dp2 << '\n';
  std::cout << "b2::this " << d.(B2::getThis()) << '\n';
  //std::cout << "b2->d: " << ((D*)b2) << '\n';
  //std::cout << "d->b2: " << ((B2*)d) << '\n';
};
