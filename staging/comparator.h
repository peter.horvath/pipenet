#ifndef nsntl_comparator_h
#define nsntl_comparator_h

#include "object.h"
#include "types.h"

namespace nsntl {

int compare(const void* a, const void* b);
int compare(const int a, const int b);
int compare(const uint64_t a, const uint64_t b);

/**
 * Java-like comparator abstract class of two Objects.
 */
class Comparator : public Object {
  public:
    Comparator();
    virtual ~Comparator();

    virtual int compare(Object*, Object*) = 0;
};

class FunctionComparator : public Comparator {
  private:
    int (*_compare)(Object*,Object*);

  public:
    FunctionComparator(int (*compare)(Object*,Object*));
    virtual ~FunctionComparator();

    int compare(Object*,Object*);
};

int pointerCompare(Object* a,Object* b);

class PointerComparator : public FunctionComparator {
  public:
    PointerComparator();
    virtual ~PointerComparator();
};

#ifndef nsntl_comparator_cc_internal
extern
#endif
PointerComparator pointerComparator;

class AlwaysEqualsComparator : public Comparator {
  public:
    AlwaysEqualsComparator();
    virtual ~AlwaysEqualsComparator();

    int compare(Object*, Object*);
};

#ifndef nsntl_comparator_cc_internal
extern
#endif
AlwaysEqualsComparator alwaysEqualsComparator;

class PairComparator : public Comparator {
  private:
    Comparator *keyComparator, *valueComparator;
    bool dynKeyC, dynValueC;

  public:
    PairComparator();
    PairComparator(int (*keyCompare)(Object*, Object*));
    PairComparator(Comparator* keyComparator);
    PairComparator(int (*keyCompare)(Object*, Object*), int (*valueCompare)(Object*,Object*));
    PairComparator(Comparator* keyComparator, int (*valueCompare)(Object*,Object*));
    PairComparator(int (*keyCompare)(Object*, Object*), Comparator* valueComparator);
    PairComparator(Comparator* keyComparator, Comparator* valueComparator, bool dynKeyC = false, bool dynValueC = false);
    virtual ~PairComparator();

    int compare(Object* a, Object* b);
};

};

#endif
