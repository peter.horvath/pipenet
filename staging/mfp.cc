#include <iostream>

class A {
  public:
    int a;
    A(int a = 5) {
      this->a = a;
    };
    void dump() {
      std::cout << "a: " << a << '\n';
    };
    static void dump(A* a) {
      a->dump();
    };
};

void call(void fn(A*), A* a) {
  fn(a);
};

int main() {
  A a(5);
  call(A::dump, &a);
  return 0;
};
