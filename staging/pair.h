#ifndef nsntl_pair_h
#define nsntl_pair_h

#include "collection.h"

namespace nsntl {

class Pair : public Comparable {
  public:
    Pair();
    virtual ~Pair();

    virtual ComparableObject* getKey() const = 0;
    virtual void setKey(const ComparableObject* o) = 0;
    virtual Object* getValue() const = 0;
    virtual void setValue(Object* o) = 0;

    virtual int compare(const Comparable* p) const override;
};

/**
 * It should be done differently - it should be some like a "BasePtrPair" or so
 */
/*
class ObjectPair : public virtual Pair {
  private:
    Object* key;
    Object* value;

  public:
    ObjectPair(Object* key = 0, Object* value = 0);
    ~ObjectPair();

    Object* getKey() const;
    void setKey(Object* v);
    Object* getValue() const;
    void setValue(Object* v);
};
*/

};

#endif
