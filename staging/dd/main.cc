#include <stdlib.h>
#include <stdio.h>

#include <nsntl/list.h>

#include "cpp.h"

extern "C" {

void *DiamondDetectorAlloc(void* (*allocProc)(size_t));
void DiamondDetector(void*, int, const char*);
void DiamondDetectorFree(void*, void(*freeProc)(void*));

}

// Diamond Detector

int main(int argc, char **argv) {
  void *parser = DiamondDetectorAlloc(malloc);
  //ParserState state;
  DiamondDetector(parser, CPP_INTEGER, "6");
  DiamondDetector(parser, CPP_PLUS, "+");
  DiamondDetector(parser, CPP_INTEGER, "12");
  DiamondDetector(parser, 0, "null");
  DiamondDetectorFree(parser, free);
  return 0;
}
