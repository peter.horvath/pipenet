/*!re2c
re2c:define:YYCTYPE = "char";
re2c:define:YYCURSOR = m_cursor;
re2c:define:YYMARKER = m_marker;
re2c:define:YYLIMIT = m_limit;
re2c:define:YYFILL:naked = 1;

"//" { return SLCOMMENT; }
"/*" { return COMMENT_START; }
"*/" { return COMMENT_END; }
"#" { return HASH; }
"(" { return B_P_ROUND; }
")" { return E_P_ROUND; }
"<" { return B_P_ANGLE; }
">" { return E_P_ANGLE; }
"{" { return B_P_CURLY; }
"}" { return E_P_CURLY; }
"[" { return B_P_SQUARE; }
"]" { return E_P_SQUARE; }
[ \t] { return WSPACE;}
[\r] { return BSR; }
[\n] { return NEWLINE;}
*/
