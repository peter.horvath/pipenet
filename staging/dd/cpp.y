%name DiamondDetector
%token_type {int}  
%token_prefix CPP_
   
%left PLUS MINUS.   
%left DIVIDE TIMES.  
%left HASH.
   
%include {   
//#include <iostream>
//#include <assert.h>
//#include "cpp.h"
}  

%default_destructor {  
  printf("default_destructor!\n");
}
   
%syntax_error {  
  printf("Syntax error!\n");
}

%parse_accept {
  printf("Parsing accepted!\n");
}

program ::= expr(A) . {
  printf("Result=%i\n", A);
}

expr(A) ::= expr(A) HASH expr(B) . { A = B; }
expr(A) ::= expr(B) MINUS  expr(C). { A = B - C; }
expr(A) ::= expr(B) PLUS  expr(C). { A = B + C; printf ("plus!\n"); }
expr(A) ::= expr(B) TIMES  expr(C). { A = B * C; }
expr(A) ::= expr(B) DIVIDE expr(C). {
  if(C != 0) {
    A = B / C;
  } else {
    printf("divide by zero\n");
  }
}
expr(A) ::= INTEGER(B) . { A = B; }
