#include <stdio.h>

#include <memory>

int main(int argc, char** argv) {
  std::shared_ptr<int> a(new int(5));
  printf ("it is: %i, sizeof: %lu\n", *a, sizeof(std::shared_ptr<int>));
  return 0;
};
