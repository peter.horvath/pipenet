// surprising, but it works

#include <cstdio>
#include <cstdlib>

void f(int n) {
  char buf[n];
  printf ("&(buf[0])=%p, &(buf[%i])=%p\n", &(buf[0]), n-1, &(buf[n-1]));
};

int main(int argc, char** argv) {
  f(atol(argv[1]));
  return 0;
};
