#ifndef pipenet_staging_factory_h
#define pipenet_staging_factory_h

class IAmPolymorphicNow {
  private:
    virtual void iAmPolymorphicNow();

  protected:
    IAmPolymorphicNow();
};

class Factory : private IAmPolymorphicNow {
  public:
    class Mediator;

    class Result : private IAmPolymorphicNow {
    };

    typedef void (MakeMethod)(Factory& factory, Result& result);

    class Mediator {
      private:
        Factory* factory;
        MakeMethod* makeMethod;

      public:
        Mediator(Factory& factory, MakeMethod& makeMethod);
        void call(Result& result);
    };
};

#endif
