#ifndef Posxx_Signal_h
#define Posxx_Signal_h

#include <signal.h>

#include <list>
#include <map>
#include <set>

#include "atom.h"

namespace Posxx {

namespace Signal {

enum class SignalError : u64 {
  HANDLER_ALREADY_ADDED_TO_RQUEUE = 0x031b0e2fa8e08620ULL,
  HANDLER_IS_NOT_IN_RQUEUE = 0xff2ab8185f52c41cULL,
  INVALID_SIGNUM = 0x8026fe39596dc953ULL,
  SIGNUM_IS_NOT_REGISTERED = 0xc6691b363d667cb0ULL,
  HANDLER_ALREADY_REGISTERED = 0x5d9072b8de793be6ULL,
  HANDLER_NOT_REGISTERED = 0xb6281a613fdcebdcULL,
  CAN_NOT_ALLOC_SAME_SIGNUM_AGAIN = 0x4adaf7a9d690e1f8ULL,
  SIGNUM_ALREADY_REGISTERED = 0x1e4107b4e7175ffdULL,
  CAN_NOT_DEALLOC_SAME_SIGNUM_AGAIN = 0x205adb6268a357d1ULL,
  SIGNAL_RQUEUE_IS_FULL = 0xaf51fc5e70f8b8c6ULL,
  NO_SIGINFO_IN_ACTION = 0x582a70d74e38f997ULL,
  NO_UCONTEXT_IN_ACTION = 0x2ac1cc7583d093ffULL,
  SYSHANDLER_CALLED_FOR_NOT_ALLOCATED_SIGNAL = 0x14f1b0a8ff66e019ULL,
  SINGLE_ACTIONPARAM_NEEDED = 0x79c76d7e23dbea70ULL
};

class SignalException : public PosxxException {
  public:
    SignalException(SignalError errorCode);
};

typedef enum : u8 {
  AP_ID = 0x1,
  AP_SIGNUM = 0x2,
  AP_TIMESTAMP = 0x4,
  AP_SIGINFO = 0x8,
  AP_UCONTEXT = 0x10,
  AP_MAX = 5,
  AP_DEFAULT = AP_ID|AP_SIGNUM|AP_TIMESTAMP,
  AP_NONE = 0
} ActionParam;

// Describes the occurence of a signal. It is actually an interpreter around a variable-sized Block.
class Action : public virtual Block {
  public:
    const static u64 MAXSIZE = sizeof(u64) + sizeof(int) + sizeof(u128) + sizeof(siginfo_t) + sizeof(ucontext_t);

  private:
    u8 byteBlock[MAXSIZE];

  public:
    Action(bool hasId = false, int signum = -1, u128 timestamp = 0, siginfo_t* siginfo = NULL, ucontext_t* ucontext = NULL);
    ~Action();

    bool has(ActionParam mask) const;
    ActionParam getMask() const;

    u64 getId() const;
    int getSignum() const;
    u128 getTimestamp() const;
    siginfo_t& getSiginfo() const;
    ucontext_t& getUcontext() const;

    u64 getSize() const;
    u64 getMaxSize() const;

    class ActionComparator  {
      public:
        bool operator()(const Action& a1, const Action& a2) const;
    };
};

class Signum;
class HandlerList;

// describes a signal handler
class Handler : private virtual Id {
  friend class HandlerList;

  public:
    class HandlerComparator  {
      public:
        bool operator()(Handler* const a1, Handler* const a2) const;
    };

  private:
    // signal handlers will be executed in prio-order (lower will be called earlier)
    int prio;

    // if disabled, it still exists in the SignalHandlerList, but won't be executed
    bool enabled;

    ActionParam wantsMask;

    // list of handler-lists on which it is registered.
    //    - On every system-wide signums, there is a handler list.
    //    - In every system-wide reentrant queue, there is also one.
    std::set<HandlerList*> handlerLists;

    // internal initialization function
    void init(int signum, bool enabled, int prio, ActionParam wantsMask);

  public:
    Handler(bool enabled = true, int prio = 0, ActionParam wantsMask = AP_DEFAULT);
    Handler(int signum, bool enabled = true, int prio = 0, ActionParam wantsMask = AP_DEFAULT);
    virtual ~Handler();

    // registers handler on signum
    void reg(int signum);

    // registers handler in a HandlerList
    void reg(HandlerList& handlerList);

    // registers in a set of a HandlerList (is used mainly for re-registering for out setters)
    void reg(std::set<HandlerList*>& handlerLists);

    // unregs handler from signum
    void unreg(int signum);

    // unregs handler from a HandlerList
    void unreg(HandlerList& handlerList);

    // unregs handler from all signums
    void unregAll();

    u64 getId() const;

    int getPrio() const;
    void setPrio(int prio);

    ActionParam getWantsMask() const;
    void setWantsMask(ActionParam mask);

    void enable();
    void disable();
    bool isEnabled() const;
    void setEnabled(bool enabled);

    virtual void handler(const Action& action)=0;
};

// An ordered list of signal handlers.
// It is an ordered list, this is why it is implement with std::set.
class HandlerList : protected std::set<Handler*, Handler::HandlerComparator> {
  friend class Signum;

  private:
    // if it is false, our Signum::sysHandler() will return without
    // doing anything
    bool enabled;

    // no of handlers requiring extended data
    u64 wantsNo[AP_MAX];

    u64 getWantsNo(ActionParam param) const;
    ActionParam getWantsMask() const;

  public:
    HandlerList();
    ~HandlerList();

    // calls the elements of HandlerList with action - if they are enabled, in the required order, etc
    void action(const Action& action);

    // enables this signal - works only if it is alloc()-ated.
    //
    // It does nothing with the posix signal syscalls, only in our data structures makes
    // the signal enabled (i.e. our handlers will be called from that point).
    void enable();

    // disables this signal - works only if it is alloc()-ated.
    //
    // It does nothing with the posix signal syscalls, only in our data structures makes
    // the signal enabled (i.e. our handlers will be called from that point).
    void disable();

    // getter/setter if the signal is enabled in our data structures by the enable() method
    bool isEnabled() const;
    void setEnabled(bool enabled);

    // adds/removes a Hander from the list. If there would be an inconsistency, an exception is thrown.
    bool hasHandler(Handler& handler);
    void addHandler(Handler& handler);
    void removeHandler(Handler& handler);

    bool empty();
};

class Signum {
  private:
    int signum;
    static HandlerList* handlerLists[NSIG];
    
    // It is the system-wide handler from the Posix
    static void sysHandler(int signum, siginfo_t* siginfo, void* p);

  public:
    Signum(int signum);

    // reference to the HandlerList belonging to the actual system-wide signum
    // Throws exception in case of not alloc()-ated signals.
    HandlerList& getHandlerList() const;
    void addHandler(Handler& handler); // proxy to HandlerList
    void removeHandler(Handler& handler); // proxy to HandlerList

    // blocks a signal - system wide
    //
    // It is only a wrapper around the posix signal handling ( pthread_sigprocmask() )
    // and it does nothing in our data structures
    void block() const;

    // unlocks a signal - system wide
    //
    // It is only a wrapper around the posix signal handling ( pthread_sigprocmask() )
    // and it does nothing in our data structures
    void unblock() const;

    // true if signal is blocked in the current thread and false if not
    //
    // It is only a wrapper around the posix signal handling ( pthread_sigprocmask() )
    // and it does nothing in our data structures.
    //
    // It is a setter/getter. block() and unblock() is a wrapper around setBlocked().
    bool isBlocked() const;
    void setBlocked(bool blocked) const;

    // allocates a unix signal signum for the use of this class - called automatically if needed
    void alloc();

    // deallocates it - signum is set back to SIG_DFL
    void free();

    // gets/sets if a signal is our or not
    bool isAllocated() const;
    void setAllocated(bool allocated);
};

// Syntactic sugar around the system-wide HandlerList, to enable nice-looking calls
// like Signal(15).block();
Signum Signal(int signum);

/*

Method calling order:

1. Normal signals:

system -> sysHandler(int signum, siginfo_t*, ucontext_t*) 

sysHandler is the normal system signal handler. It packs the signal data, and the
normal handler list, and calls with them the callHandlers.

callHandlers(Action& action, handlerList);

2. Reentrant signals:

unreg/reg() are overriden, only a single handler is registered per ReentrantQueue with
minimal prioritiy (~0).
*/

// Classes for reentrant signal handlers. They collect the arriving signals in a ReentrantQueue
// (implemented by ReentrantFifo, implemented by a socketpair() of unix dgrams),
// and call their handlers only by the tic() method.

class SignalQueue {
  private:
    u64 size;
    Action* actions;
    u8* alloc;
    std::atomic<u64>* locks;
    ActionParam params;

  public:
    SignalQueue(u64 size, ActionParam params);
    ~SignalQueue();

    ActionParam getActionParams();
    Action* popAction();
    void pushAction(Action* action);
    void freeAction(Action* action);
};

enum class ReentrantError : u64 {
  RHANDLER_NEEDS_SIGNUM = 0xa073dfc8738a03a9ULL,
  RHANDLER_UNREGD_SIGNUM = 0x7475efa50606dcbbULL,
  RHANDLER_ALREADY_REGD = 0x296cb9403f42df08ULL,
  RHANDLER_NOT_YET_REGD = 0xe7e7079187182233ULL
};

class ReentrantException : public PosxxException {
  public:
    ReentrantException(ReentrantError errorCode);
};

class ReentrantHandler;

class ReentrantQueue : private Handler {
  friend class ReentrantHandler;
  friend class Signum;

  private:
    ReentrantFifo* reentrantFifo;
    HandlerList* sigRegs[NSIG];
    u64 handlerNo;

    virtual void handler(const Action& action);

  public:
    ReentrantQueue();
    virtual ~ReentrantQueue();

    void addHandler(ReentrantHandler& handler, int signum);
    void removeHandler(ReentrantHandler& handler, int signum);

    void tic();
};

class ReentrantHandler : protected Handler {
  friend class ReentrantQueue;

  private:
    static ReentrantQueue globalQueue;
    ReentrantQueue* queue;
    std::set<int> signums;

    void init(ReentrantQueue* queue, int signum, bool enabled, int prio, ActionParam wantsMask);

  public:
    ReentrantHandler(ReentrantQueue& queue, int signum, bool enabled=true, int prio=0, ActionParam wantsMask = AP_DEFAULT);
    ReentrantHandler(int signum, bool enabled=true, int prio=0, ActionParam wantsMask = AP_DEFAULT);
    ReentrantHandler(ReentrantQueue& queue, bool enabled=true, int prio=0, ActionParam wantsMask = AP_DEFAULT);
    ReentrantHandler(bool enabled=true, int prio=0, ActionParam wantsMask = AP_DEFAULT);
    ~ReentrantHandler();

    void reg(int signum);
    void unreg(int signum);
    void unregAll();
};

};

};
#endif
