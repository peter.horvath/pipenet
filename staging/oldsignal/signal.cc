#include <fcntl.h>
#include <string.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <algorithm>
#include <string>

#include "signal.h"
#include "timer.h"

using namespace Posxx;

using namespace Posxx::Signal;

/*
std::vector<Signal::Signum*> sigRegs = []{
  std::vector<Signal::Signum*> r;
  for (int i=1; i<NSIG; i++)
    r.push_back(NULL);
  return r;
}();
*/

SignalException::SignalException(SignalError errorCode) : PosxxException((u64)errorCode) {
  // nothing now
};

HandlerList* sigRegs[NSIG];

static void check_signum_valid(int signum) {
  if (signum <1 || signum >= NSIG)
    throw SignalException(SignalError::INVALID_SIGNUM);
};

static void check_signum_alloc(int signum) {
  if (!sigRegs[signum])
    throw SignalException(SignalError::SYSHANDLER_CALLED_FOR_NOT_ALLOCATED_SIGNAL);
};

Action::Action(bool hasId, int signum, u128 timestamp, siginfo_t* siginfo, ucontext_t* ucontext)
    : Block(byteBlock, MAXSIZE) {

  u8* p = byteBlock;

  *p = (hasId ? AP_ID : 0) | (signum > 0 ? AP_SIGNUM : 0) | (timestamp ? AP_TIMESTAMP : 0) | (siginfo ? AP_SIGINFO : 0) | (ucontext ? AP_UCONTEXT : 0);
  *p += sizeof(u8);

  if (has(AP_ID)) {
    Id id;
    *((u64*)p) = id.getId();
    p += sizeof(u64);
  };

  if (has(AP_SIGNUM)) {
    *((int*)p) = signum;
    p += sizeof(int);
  };

  if (has(AP_TIMESTAMP)) {
    *((u128*)p) = timestamp;
    p += sizeof(u128);
  };

  if (has(AP_SIGINFO)) {
    Memcpy(p, siginfo, sizeof(siginfo_t));
    p += sizeof(siginfo_t);
  };

  if (has(AP_UCONTEXT)) {
    Memcpy(p, ucontext, sizeof(ucontext_t));
    p += sizeof(ucontext);
  };

  setSize(p - byteBlock);
};

Action::~Action() {
  // nothing now
};

bool Action::has(ActionParam mask) const {
  return byteBlock[0] & mask;
};

ActionParam Action::getMask() const {
  return ActionParam(byteBlock[0]);
};

u64 Action::getId() const {
  if (has(AP_ID))
    return 0;

  u8* p = (u8*)this;
  return *(u64*)(p+1);
};

int Action::getSignum() const {
  if (!has(AP_SIGNUM))
    return 0;

  return *(int*) ( ((u8*)this) + sizeof(u8) + (has(AP_ID)?sizeof(u64):0));
};

u128 Action::getTimestamp() const {
  if (!has(AP_TIMESTAMP))
    return 0;

  return *(u128*) (
    ((u8*)this) +
    sizeof(u8) +
    (has(AP_ID) ? sizeof(u64) : 0) +
    (has(AP_SIGNUM) ? sizeof(int) : 0)
  );
};

siginfo_t& Action::getSiginfo() const {
  if (!has(AP_SIGINFO))
    throw SignalException(SignalError::NO_SIGINFO_IN_ACTION);

  return *(siginfo_t*) (
    ((u8*)this) +
    sizeof(u8) +
    (has(AP_ID) ? sizeof(u64) : 0) +
    (has(AP_SIGNUM) ? sizeof(int) : 0) +
    (has(AP_TIMESTAMP) ? sizeof(u128) : 0)
  );
};

ucontext_t& Action::getUcontext() const {
  if (!has(AP_UCONTEXT))
    throw SignalException(SignalError::NO_UCONTEXT_IN_ACTION);

  return *(ucontext_t*) (
    ((u8*)this) +
    sizeof(u8) +
    (has(AP_ID) ? sizeof(u64) : 0) +
    (has(AP_SIGNUM) ? sizeof(int) : 0) +
    (has(AP_TIMESTAMP) ? sizeof(u128) : 0) +
    (has(AP_SIGINFO) ? sizeof(siginfo_t) : 0)
  );
};

u64 Action::getSize() const {
  return
    1 +
    (has(AP_ID) ? sizeof(u64) : 0) +
    (has(AP_SIGNUM) ? sizeof(int) : 0) +
    (has(AP_TIMESTAMP) ? sizeof(u128) : 0) +
    (has(AP_SIGINFO) ? sizeof(siginfo_t) : 0) +
    (has(AP_UCONTEXT) ? sizeof(ucontext_t) : 0);
};

bool Action::ActionComparator::operator()(const Action& a1, const Action& a2) const {
  if (a1.has(AP_TIMESTAMP) && a2.has(AP_TIMESTAMP)) {
    if (a1.getTimestamp() < a2.getTimestamp())
      return false;
    else if (a1.getTimestamp() > a2.getTimestamp())
      return true;
  };

  if (a1.has(AP_ID) && a2.has(AP_ID)) {
    if (a1.getId() < a2.getId())
      return false;
    else if (a1.getId() > a2.getId())
      return true;
    else
      throw LogicException(LogicError::UNREACHABLE_CODE);
  };

  if (a1.has(AP_SIGNUM) && a2.has(AP_SIGNUM)) {
    if (a1.getSignum() < a2.getSignum())
      return false;
    else if (a1.getSignum() > a2.getSignum())
      return true;
  };

  return &a1 > &a2;
};

bool Handler::HandlerComparator::operator()(Handler* const a1, Handler* const a2) const {
  if (a1->getPrio() < a2->getPrio())
    return true;
  if (a1->getPrio() > a2->getPrio())
    return false;
  if (a1->getId() < a2->getId())
    return true;
  if (a1->getId() > a2->getId())
    return false;
  throw LogicException(LogicError::UNREACHABLE_CODE);
};

void Handler::init(int signum, bool enabled, int prio, ActionParam wantsMask) {
  setEnabled(enabled);
  setPrio(prio);
  setWantsMask(wantsMask);
  if (signum)
    reg(signum);
};

Handler::Handler(bool enabled, int prio, ActionParam wantsMask) {
  init(0, enabled, prio, wantsMask);
};

Handler::Handler(int signum, bool enabled, int prio, ActionParam wantsMask) {
  check_signum_valid(signum);

  init(signum, enabled, prio, wantsMask);
};

Handler::~Handler() {
  unregAll();
};

void Handler::reg(int signum) {
  check_signum_valid(signum);

  reg(Signal(signum).getHandlerList());
};
 
void Handler::reg(HandlerList& handlerList) {
  handlerList.addHandler(*this);
};

void Handler::reg(std::set<HandlerList*>& handlerLists) {
  std::for_each(handlerLists.begin(), handlerLists.end(), [this](HandlerList* list) {
    list->addHandler(*this);
  });
};

void Handler::unreg(int signum) {
  check_signum_valid(signum);

  Signal(signum).removeHandler(*this);
};
 
void Handler::unreg(HandlerList& handlerList) {
  handlerList.removeHandler(*this);
};

void Handler::unregAll() {
  while (!handlerLists.empty())
    unreg(**(handlerLists.begin()));
};

u64 Handler::getId() const {
  return Id::getId();
};

int Handler::getPrio() const {
  return this->prio;
};

void Handler::setPrio(int prio) {
  std::set<HandlerList*> lists(handlerLists);
  unregAll();
  this->prio = prio;
  reg(lists);
};

ActionParam Handler::getWantsMask() const {
  return wantsMask;
};

void Handler::setWantsMask(ActionParam wantsMask) {
  if (this->wantsMask == wantsMask)
    return;

  std::set<HandlerList*> lists(handlerLists);
  unregAll();
  this->wantsMask = wantsMask;
  reg(lists);
};

bool Handler::isEnabled() const {
  return this->enabled;
};

void Handler::enable() {
  this->enabled = true;
};

void Handler::disable() {
  this->enabled = false;
};

/**
  * HandlerList
  */

u64 HandlerList::getWantsNo(ActionParam param) const {
  for (int a=0; a<AP_MAX; a++)
    if (param == (1<<a))
      return wantsNo[a];
  throw SignalException(SignalError::SINGLE_ACTIONPARAM_NEEDED);
};

ActionParam HandlerList::getWantsMask() const {
  u8 mask = AP_NONE;
  for (int a=0; a<AP_MAX; a++)
    if (wantsNo[a])
      mask |= (1<<a);
  return ActionParam(mask);
};

HandlerList::HandlerList() {
  enabled = true;
  for (int a=0; a<AP_MAX; a++)
    wantsNo[a] = 0;
};

HandlerList::~HandlerList() {
  while (!this->empty())
    removeHandler(**(this->begin()));
};

void HandlerList::action(const Action& action) {
  if (!isEnabled())
    return;

  std::for_each(this->begin(), this->end(), [this, action](Handler* handler){
    if (!handler->isEnabled()) {
      // nothing now
    } else if (handler->getWantsMask() & ~action.getMask()) {
      // nothing now
    } else {
      handler->handler(action);
    };
  });
};

void HandlerList::enable() {
  setEnabled(true);
};

void HandlerList::disable() {
  setEnabled(false);
};

bool HandlerList::isEnabled() const {
  return enabled;
};

void HandlerList::setEnabled(bool enabled) {
  this->enabled = enabled;
};

bool HandlerList::hasHandler(Handler& handler) {
  return handler.handlerLists.find(this) != handler.handlerLists.end();
};

void HandlerList::addHandler(Handler& handler) {
  if (handler.handlerLists.find(this) != handler.handlerLists.end())
    throw SignalException(SignalError::HANDLER_ALREADY_REGISTERED);

  insert(&handler);
  handler.handlerLists.insert(this);

  for (int a=0; a<AP_MAX; a++)
    if (handler.getWantsMask() & (1<<a))
      wantsNo[a]++;
};

void HandlerList::removeHandler(Handler& handler) {
  if (handler.handlerLists.find(this) == handler.handlerLists.end())
    throw SignalException(SignalError::HANDLER_NOT_REGISTERED);

  erase(&handler);
  handler.handlerLists.erase(this);

  for (int a=0; a<AP_MAX; a++)
    if (handler.getWantsMask() & (1<<a))
      wantsNo[a]--;
};

bool HandlerList::empty() {
  return this->empty();
};

/**
  * Signum
  *
  * Functions to make system-wide signal operations. They are actually static singleton functions,
  * with a little bit of syntactic sugar (because Signal(15).block() looks so beautiful).
  */
Signum::Signum(int signum) {
  check_signum_valid(signum);
  this->signum = signum;
};

void Signum::sysHandler(int signum, siginfo_t* siginfo, void* ucontext) {
  check_signum_valid(signum);
  check_signum_alloc(signum);

  HandlerList* hl = sigRegs[signum];
  ActionParam hlMask = hl->getWantsMask();

  bool aHasId = hlMask & AP_ID;
  int aSignum = (hlMask & AP_SIGNUM) ? signum : 0;
  u128 aTimestamp = (hlMask & AP_TIMESTAMP) ? Timer::now() : u128(0);
  siginfo_t* aSiginfo = (hlMask & AP_SIGINFO) ? siginfo : NULL;
  ucontext_t* aUcontext = (hlMask & AP_UCONTEXT) ? (ucontext_t*)ucontext : (ucontext_t*)NULL;

  Action action(aHasId, aSignum, aTimestamp, aSiginfo, aUcontext);

  hl->action(action);
}

HandlerList& Signum::getHandlerList() const {
  check_signum_valid(signum);
  check_signum_alloc(signum);

  return *sigRegs[signum];
};

void Signum::addHandler(Handler& handler) {
  check_signum_valid(signum);
  check_signum_alloc(signum);

  sigRegs[signum]->addHandler(handler);
};

void Signum::removeHandler(Handler& handler) {
  check_signum_valid(signum);
  check_signum_alloc(signum);

  sigRegs[signum]->removeHandler(handler);
};

void Signum::block() const {
  setBlocked(true);
};

void Signum::unblock() const {
  setBlocked(true);
};

bool Signum::isBlocked() const {
  sigset_t sigset;
  sigemptyset(&sigset);
  if (sigprocmask(SIG_BLOCK, NULL, &sigset)==-1)
    throw PosxxException(errno);
  int r = sigismember(&sigset, signum);
  if (r == -1)
    throw PosxxException(errno);
  return r;
};

void Signum::setBlocked(bool block) const {
  sigset_t sigset;
  sigemptyset(&sigset);
  sigaddset(&sigset, signum);
  if (sigprocmask(block ? SIG_BLOCK : SIG_UNBLOCK, &sigset, NULL)==-1)
    throw PosxxException(errno);
};

void Signum::alloc() {
  if (isAllocated())
    throw SignalException(SignalError::CAN_NOT_ALLOC_SAME_SIGNUM_AGAIN);

  struct sigaction action;
  bzero(&action, sizeof(struct sigaction));
  action.sa_sigaction = sysHandler;
  action.sa_flags = SA_SIGINFO | SA_NODEFER | SA_RESTART;
  int r=sigaction(signum, &action, NULL);
  if (r==-1)
    throw PosxxException(errno);

  if (siginterrupt(signum, 1) == -1)
    throw PosxxException(errno);

  sigRegs[signum] = new HandlerList();
};

void Signum::free() {
  if (!isAllocated())
    throw SignalException(SignalError::CAN_NOT_DEALLOC_SAME_SIGNUM_AGAIN);

  struct sigaction action;
  bzero(&action, sizeof(struct sigaction));
  action.sa_handler = SIG_DFL;
  
  int r=sigaction(signum, &action, NULL);
  if (r==-1)
    throw PosxxException(errno);
  
  if (siginterrupt(signum, 0) == -1)
    throw PosxxException(errno);
  
  delete sigRegs[signum];
  sigRegs[signum] = NULL;
};

bool Signum::isAllocated() const {
  return sigRegs[signum];
};

void Signum::setAllocated(bool allocated) {
  if (!isAllocated() && allocated)
    alloc();
  else if (isAllocated() && !allocated)
    free();
};

// It is NOT a constructor, it gets a Signum reference from our register
Signum Signal(int signum) {
  return Signum(signum);
};

/**
 * Reentrant Signal Queue handling
 */
ReentrantException::ReentrantException(ReentrantError errorCode) : PosxxException((u64)errorCode) {
  // nothing now
};

void ReentrantQueue::handler(const Action& action) {
  if (!action.has(AP_SIGNUM))
    throw ReentrantException(ReentrantError::RHANDLER_NEEDS_SIGNUM);
  int signum = action.getSignum();
  check_signum_valid(signum);
  if (!sigRegs[signum])
    throw ReentrantException(ReentrantError::RHANDLER_UNREGD_SIGNUM);

  Block(action.getAddr(), action.getSize()) >> *reentrantFifo;
};

ReentrantQueue::ReentrantQueue() {
  reentrantFifo = NULL;
  Bzero(sigRegs, NSIG*sizeof(HandlerList*));
};

ReentrantQueue::~ReentrantQueue() {
  Handler::disable();
  for (int a=0; a<NSIG; a++)
    if (sigRegs[a])
      delete sigRegs[a];
  delete reentrantFifo;
};

void ReentrantQueue::addHandler(ReentrantHandler& handler, int signum) {
  check_signum_valid(signum);
  check_signum_alloc(signum);

  if (!reentrantFifo)
    reentrantFifo = new ReentrantFifo();

  if (!sigRegs[signum]) {
    sigRegs[signum] = new HandlerList();
    reg(signum);
  };

  sigRegs[signum]->addHandler(handler);

  handlerNo++;
};

void ReentrantQueue::removeHandler(ReentrantHandler& handler, int signum) {
  check_signum_valid(signum);

  if (!sigRegs[signum])
    throw ReentrantException(ReentrantError::RHANDLER_UNREGD_SIGNUM);

  if (sigRegs[signum]->hasHandler(handler))
    throw ReentrantException(ReentrantError::RHANDLER_UNREGD_SIGNUM);

  sigRegs[signum]->removeHandler(handler);

  if (sigRegs[signum]->empty()) {
    unreg(signum);
    delete sigRegs[signum];
    sigRegs[signum] = NULL;
  };

  if (!--handlerNo) {
    delete reentrantFifo;
    reentrantFifo = NULL;
  };
};

void ReentrantQueue::tic() {
  u8 block[Action::MAXSIZE];
  Block recvBlock(block, Action::MAXSIZE);
  Action* action = (Action*)block;

  while (1) {
    u64 recvSize = *reentrantFifo >> recvBlock;
    if (!recvSize)
      break;
    if (!action->has(AP_SIGNUM))
      throw ReentrantException(ReentrantError::RHANDLER_NEEDS_SIGNUM);
    int signum = action->getSignum();
    if (!sigRegs[signum])
      throw ReentrantException(ReentrantError::RHANDLER_UNREGD_SIGNUM);
    sigRegs[signum]->action(*action);
  };
};

ReentrantQueue ReentrantHandler::globalQueue;

void ReentrantHandler::init(ReentrantQueue* queue, int signum, bool enabled, int prio, ActionParam wantsMask) {
  if (signum != -1)
    check_signum_valid(signum);
  check_signum_alloc(signum);

  if (!queue)
    queue = &globalQueue;

  this->queue = queue;

  setEnabled(enabled);
  setPrio(prio);

  if (!(wantsMask & AP_SIGNUM))
    throw ReentrantException(ReentrantError::RHANDLER_NEEDS_SIGNUM);
  setWantsMask(wantsMask);
  
  if (signum != -1)
    reg(signum);
};

ReentrantHandler::ReentrantHandler(ReentrantQueue& queue, int signum, bool enabled, int prio, ActionParam wantsMask) {
  init(&queue, signum, enabled, prio, wantsMask);
};

ReentrantHandler::ReentrantHandler(int signum, bool enabled, int prio, ActionParam wantsMask) {
  init(NULL, signum, enabled, prio, wantsMask);
};

ReentrantHandler::ReentrantHandler(ReentrantQueue& queue, bool enabled, int prio, ActionParam wantsMask) {
  init(&queue, -1, enabled, prio, wantsMask);
};

ReentrantHandler::ReentrantHandler(bool enabled, int prio, ActionParam wantsMask) {
  init(NULL, -1, enabled, prio, wantsMask);
};

ReentrantHandler::~ReentrantHandler() {
  disable();
  unregAll();
};

void ReentrantHandler::reg(int signum) {
  if (signums.find(signum) != signums.end())
    throw ReentrantException(ReentrantError::RHANDLER_ALREADY_REGD);
  queue->addHandler(*this, signum);
  signums.insert(signum);
};

void ReentrantHandler::unreg(int signum) {
  if (signums.find(signum) == signums.end())
    throw ReentrantException(ReentrantError::RHANDLER_NOT_YET_REGD);
  queue->removeHandler(*this, signum);
  signums.erase(signum);
};

void ReentrantHandler::unregAll() {
  while (!signums.empty())
    unreg(*(signums.begin()));
};
