#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

void die(char* msg) {
  int e = errno;
  fprintf (stderr, "FATAL: %s (%s, errno=%i)\n", msg, strerror(e), e);
  exit (-e);
};

int main(int argc, char **argv) {
  int fd[2];
  char test[65536];
  int r = socketpair(AF_UNIX, SOCK_DGRAM, 0, fd);
  if (r == -1)
    die ("socketpair()");

  if (fcntl(fd[1], F_SETFL, O_NONBLOCK) == -1)
    die ("fcntl()");

  r = send(fd[0], test, 65536, 0);
  if (r < 65536)
    die ("send()");
  r = send(fd[0], test, 65536, 0);
  if (r < 65536)
    die ("send() #2");
  r = recv(fd[1], test, 65536, 0);
  if (r < 65536)
    die ("recv()");
  r = recv(fd[1], test, 65536, 0);
  if (r < 65536)
    die ("recv()");
  r = recv(fd[1], test, 65536, 0);
  if (r < 65536)
    die ("recv()");
  return 0;
}
