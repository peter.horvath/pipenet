#include <stdio.h>

int main(int argc, char** argv) {
  static const int SIZE = sizeof(FILE) + sizeof(int);
  char test[SIZE];
  printf ("%p\n", &(test[0]));
  return 0;
};
