// it was so beautiful...
Block::Block(void *addr, u64 size) {
  if (!addr && !size) {
    this->size=0;
    this->addr=NULL;
  } else if (!addr && size) {
    this->size=size;
    this->addr=Malloc(size);
    Bzero(this->addr, size);
  } else if (addr && !size) {
    this->size = Strlen((char*)addr);
    this->addr = Malloc(this->size);
    Memcpy(this->addr, addr, this->size);
  } else if (addr && size) {
    this->size = size;
    this->addr = Malloc(size);
    Memcpy(this->addr, addr, size);
  }
};

Buffer::Buffer(u64 size) : Block(size + 1) {
  dataFrom = 0;
  dataTo = 0;
};

u64 Buffer::getUsed() const {
  if (dataFrom <= dataTo)
    return dataTo - dataFrom;
  else
    return dataFrom + size - dataTo;
};

u64 Buffer::getFree() const {
  return getSize() - getUsed();
};

void Buffer::read(void* addr, u64 len) {
  if (len > getUsed())
    throw PosxxException(BUFFER_TOO_SMALL);

  if (dataFrom <= dataTo) {
    Memcpy(addr, this->addr + dataFrom, len);
    dataFrom += len;
  } else {
    if (len <= size - dataFrom) {
      Memcpy(addr, this->addr + dataFrom, len);
      dataFrom += len;
    } else {
      Memcpy(addr, this->addr + dataFrom, size - dataFrom);
      Memcpy(addr + (size - dataFrom), this->addr, len - (size - dataFrom));
      dataFrom = dataFrom + len - size;
    }
  }
};

void Buffer::insert(void* addr, u64 len) {
  if (len > getFree())
    throw PosxxException(BUFFER_TOO_SMALL);

  if (dataFrom <= dataTo) {
    if (len < size - dataTo) {
      Memcpy(this->addr + dataTo, addr, len);
      dataTo += len;
    } else {
      Memcpy(this->addr + dataTo, addr, size - dataTo);
      Memcpy(this->addr, addr + size - dataTo, len - (size - dataTo));
    }
  } else {
    Memcpy(this->addr + dataTo, addr, len);
    dataTo += len;
  };
};

void Buffer::moveDataTo(Buffer& buffer, u64 len) {
  if (len > buffer.getFree() || len > getUsed())
    throw PosxxException(BUFFER_TOO_SMALL);

  if (dataFrom <= dataTo) {
    buffer.insert(this->addr + dataFrom, len);
    dataFrom += len;
  } else {
    if (len <= size - dataFrom) {
      buffer.insert(this->addr + dataFrom, len);
      dataFrom += len;
    } else {
      buffer.insert(this->addr + dataFrom, size - dataFrom);
      buffer.insert(this->addr, len - (size - dataFrom));
      dataFrom = dataFrom + len - size;
    }
  }
};

void Buffer::moveDataTo(Buffer& buffer) {
  moveDataTo(buffer, std::min(getUsed(), buffer.getFree());
};

void Buffer::setSize(u64 size) {
  if (!size)
    size = getUsed();

  if (size < getUsed())
    throw PosxxException(BUFFER_TOO_SMALL);

  if (size == this->size) {
    return;
  } else if (size < this->size) { // shrink
    if (dataFrom <= dataTo) {
      TODO;
    } else {
      TODO;
    };
  } else { // grow
    TODO;
  };
};

Buffer& Buffer::operator<<(Buffer& buffer) {
  buffer.moveDataTo(this);
  return *this;
};

Buffer& Buffer::operator>>(Buffer& buffer) {
  moveDataTo(buffer);
  return buffer;
};
