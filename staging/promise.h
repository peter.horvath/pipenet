#ifndef posxx_promise_h
#define posxx_promise_h

namespace Posxx {

namespace Event {

class Event;
class Emitter;
class Collector;
class Net;

class Event {
  private:
    Channel* channel;
    std::list<Event*>::iterator netIter, channelIter;

  public:
    Event();
    ~Event();
};

class Emitter {
  private:
    
  public:
};

class Collector {
  private:
  public:
};

class Channel {
  friend class Emitter;
  friend class Collector;
  friend class Net;

  private:
    Emitter* emitter;
    Collector* collector;
    std::list<Channel*>::iterator emitterIter, collectorIter, netIter;
    std::list<Event*> events;

    Channel(Emitter& emitter, Collector& collector);
    ~Channel();
};

class Net {
  private:
    std::list<Emitter*> emitters;
    std::list<Collector*> collectors;
    std::list<Channel*> channels;
    std::list<Event*> events;

  public:
};

};

};

#endif
