#ifndef Posxx_Ip4_h
#define Posxx_Ip4_h

#include <netinet/in.h>

#include "posxx.h"
#include "socket.h"

namespace Posxx {

/**
  * Ipv4 address. Everything is in host byte order.
  */
class Ip4 {
  private:
    u32 ip;

  public:
    Ip4();
    Ip4(u8 a, u8 b, u8 c, u8 d);
    Ip4(u32 ip);
    Ip4(const Ip4& ip);

    u32 getIp() const;
    void setIp(u32 ip);
    void setIp(Ip4 ip);
    bool operator==(u32 ip) const;
    bool operator==(Ip4 ip) const;
    Ip4& operator=(u32 ip);
    Ip4& operator=(Ip4 ip);
    bool isNull() const;
    void clear();
};

/**
  * An IP port (0 - 65535). Everything is in host byte order.
  */
class IpPort {
  private:
    u16 port; // host byte order

  public:
    IpPort();
    IpPort(u16 port);
    IpPort(const IpPort& port);

    u16 getPort() const;
    void setPort(u16 port);
    void setPort(IpPort port);
    bool operator==(u16 port) const;
    bool operator==(IpPort port) const;
    IpPort& operator=(u16 port);
    IpPort& operator=(IpPort port);
    bool isNull() const;
    void clear();
};

/**
  * An ipv4 socket address (ip + port).
  */
class Ip4Addr : private sockaddr_in {
  public:
    Ip4Addr();
    Ip4Addr(Ip4 ip);
    Ip4Addr(Ip4 ip, IpPort port);
    Ip4Addr(Ip4 ip, u16 port);
    Ip4Addr(IpPort port);
    Ip4Addr(const Ip4Addr& ip4Addr);
    Ip4Addr(const struct sockaddr_in* const addr);
    Ip4Addr(const struct sockaddr_in& addr);

    const struct sockaddr_in* const getAddr() const;
    void setAddr(const struct sockaddr_in& addr);
    void setAddr(struct sockaddr_in* const addr);
    void setAddr(const Ip4Addr& ip4Addr);
    Ip4 getIp() const;
    void setIp(u32 ip);
    void setIp(Ip4 ip);
    IpPort getPort() const;
    void setPort(u16 port);
    void setPort(IpPort port);

    bool isNull() const;
    void clear();

    bool operator==(const Ip4Addr& addr) const;
    bool operator==(const struct sockaddr_in& addr) const;
    Ip4Addr& operator=(const Ip4Addr& addr);
    Ip4Addr& operator=(const struct sockaddr_in& addr);
};

/**
  * Class handling {UDP,TCP}/IPv4 (non-listening) sockets
  */
class BaseIp4Fd : public virtual SocketFd {
  private:
    Ip4Addr localAddr;

    virtual void mksock();

  protected:
    BaseIp4Fd();
    BaseIp4Fd(int fd);
    BaseIp4Fd(const Fd& fd);
    BaseIp4Fd(const Ip4Addr& localAddr);
    virtual ~BaseIp4Fd();

  public:
    const Ip4Addr& getLocalAddr();

    // connects to a randomly selected local port
    void bind();

    // connects to the given local port
    void bind(const Ip4Addr& localAddr);
    void bind(IpPort port);
    void bind(u16 port);
};

class BaseTcp4Fd : public virtual BaseIp4Fd {
  private:
    void mksock();

  protected:
    BaseTcp4Fd();
    BaseTcp4Fd(int fd);
    BaseTcp4Fd(const Fd& fd);
    BaseTcp4Fd(const Ip4Addr& localAddr);
    virtual ~BaseTcp4Fd();
};

/**
  * {UDP,TCP}/IPv4 sockets where there is also a possible remote address,
  * i.e. connect() can be called, i.e. they are not listening sockets, but normal
  * sockets for communication
  */
class BaseIp4ConnectableFd : public virtual BaseIp4Fd {
  private:
    Ip4Addr remoteAddr;

  protected:
    BaseIp4ConnectableFd();
    BaseIp4ConnectableFd(int fd);
    BaseIp4ConnectableFd(const Fd& fd);

    // connect() from the constructor. But beware, they are BLOCKING connections
    // (except if you construct them from an Fd previously set to nonblocking,
    //    but it is a little bit dirty)
    BaseIp4ConnectableFd(const Ip4Addr& remoteAddr);
    BaseIp4ConnectableFd(const Ip4Addr& localAddr, const Ip4Addr& remoteAddr);
    virtual ~BaseIp4ConnectableFd();

  public:
    const Ip4Addr& getRemoteAddr();
    void connect(const Ip4Addr& addr);
};

/**
  * TCP/IPv4 connection-oriented socket
  */

class Tcp4Fd : private virtual FactoryResult, public virtual StreamFd, public virtual BaseIp4ConnectableFd, public virtual BaseTcp4Fd {
  friend class Tcp4ListenerFd;

  private:
    void mksock();

  public:
    Tcp4Fd();
    Tcp4Fd(int fd);
    Tcp4Fd(const Fd& fd);
    Tcp4Fd(const Tcp4Fd& fd);
    Tcp4Fd(const Ip4Addr& remoteAddr);
    Tcp4Fd(const Ip4Addr& localAddr, const Ip4Addr& remoteAddr);
    Tcp4Fd(FactoryMediator fm); // required for Tcp4Fd fd = listenerFd.accept();
    virtual ~Tcp4Fd();

    Tcp4Fd& operator=(FactoryMediator& fm);
};

/**
  * UDP/IPv4 datagram socket
  */
class Udp4Fd;

class Udp4Dgram : private virtual FactoryResult, public DynamicBlock {
  private:
    Ip4Addr srcAddr;
    Ip4Addr dstAddr;

  public:
    Udp4Dgram(u64 size = 0);
    Udp4Dgram(const Ip4Addr& dstAddr, u64 size = 0);
    Udp4Dgram(const Ip4Addr& srcAddr, const Ip4Addr& dstAddr, u64 size = 0);
    Udp4Dgram(const Block& data);
    Udp4Dgram(const Block& data, const Ip4Addr& dstAddr);
    Udp4Dgram(const Block& data, const Ip4Addr& srcAddr, const Ip4Addr& dstAddr);
    Udp4Dgram(FactoryMediator fm);

    Ip4Addr& getSrcAddr() const;
    void setSrcAddr(const Ip4Addr& addr);
    Ip4Addr& getDstAddr() const;
    void setDstAddr(const Ip4Addr& addr);

    void sendTo(Udp4Fd& fd);
    void sendTo(Udp4Fd& fd, const Ip4Addr& dstAddr);

    Udp4Dgram& operator=(FactoryMediator& fm);
};

/* It is simpler as it seems. Simply use
 *
 * Udp4Dgram dgram = udp4Fd.recvFrom(...);
 *
 * These tricks are only to solve it without copy-by-value.
 */
class Udp4Fd : private virtual Factory, public virtual DgramFd, public virtual BaseIp4ConnectableFd {
  private:
    void mksock();

  public:
    Udp4Fd();
    Udp4Fd(int fd);
    Udp4Fd(const Fd& fd);
    Udp4Fd(const Ip4Addr& localAddr);
    Udp4Fd(const Ip4Addr& localAddr, const Ip4Addr& remoteAddr);
    virtual ~Udp4Fd();

    static void recvFrom(Factory& factory, FactoryResult& result);
    FactoryMediator recvFrom();
    int recvFrom(Block& block, Ip4Addr& srcAddr);
    int recvFrom(Block& block);
    void recvFrom(DynamicBlock& block, Ip4Addr& srcAddr);
    void recvFrom(DynamicBlock& block);
    void recvFrom(Udp4Dgram& dgram);

    void sendTo(const Block& block, const Ip4Addr& dstAddr);
    void send(const Udp4Dgram& pkt);
};

/**
  * Class describing a listening TCP/IPv4 socket.
  *
  * Most trivial example: Tcp4ListenerFd(Ip4Port(5020));
  */
class Tcp4ListenerFd : private virtual Factory, public virtual BaseTcp4Fd {
  private:
    void listen(int maxconn = 0);

  public:
    Tcp4ListenerFd(int maxconn = 0);
    Tcp4ListenerFd(const Fd& fd, int maxconn = 0);
    Tcp4ListenerFd(IpPort port, int maxconn = 0);
    Tcp4ListenerFd(const Ip4Addr& addr, int maxconn = 0);
    Tcp4ListenerFd(u16 port, int maxconn = 0);
    virtual ~Tcp4ListenerFd();

    void accept(Tcp4Fd& fd);
    static void accept(Factory& f, FactoryResult& r);
    FactoryMediator accept();
};

};
#endif
