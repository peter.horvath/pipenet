
/*
// Disabled, provided generically by Set & Uint64Collection

class Uint64SetEntry;

class Uint64Set : public Set {
  public:
    Uint64Set();
    ~Uint64Set();

  private:
    int compare(const SetEntry* a, const SetEntry* b) const override final;

  public:
    uint64_t first() const;
    uint64_t last() const;
    bool has(uint64_t i, FindRelation relation = FIND_EQ) const;
    uint64_t find(uint64_t i, FindRelation relation = FIND_EQ) const;
    bool add(uint64_t i);
    bool remove(uint64_t i);
};

class Uint64SetEntry : public SetEntry {
  friend class Uint64Set;

  private:
    uint64_t i;

  public:
    Uint64SetEntry(uint64_t i=0);
    ~Uint64SetEntry();

    Uint64Set* getUint64Set() const;
    uint64_t get() const;
    void set(uint64_t i);
    Uint64SetEntry* next() const;
    Uint64SetEntry* prev() const;
};
*/

/*
class SmartPtrSetEntry;

class SmartPtrSet : public Set {
  public:
    SmartPtrSet(uint8_t flags = 0);
    ~SmartPtrSet();

  private:
    int compare(const SetEntry* a, const SetEntry* b) const override;
    static int findCompareHelper(const AvlEntry* a, const void* b);

  public:
    bool has(const ComparableSmartPtr& v, FindRelation relation = FIND_EQ) const;
    bool has(const ComparableSmartPtd& v, FindRelation relation = FIND_EQ) const;
    bool has(const ComparableSmartPtd* v, FindRelation relation = FIND_EQ) const;
    SmartPtrSetEntry* find(const Comparable* v, FindRelation relation = FIND_EQ) const;
    SmartPtrSetEntry* find(const Comparable& v, FindRelation relation = FIND_EQ) const;
    SmartPtrSetEntry* add(ComparableSmartPtd& v);
    void remove(const Comparable& v);
    SmartPtrSetEntry* firstEntry() const;
    SmartPtrSetEntry* lastEntry() const;
    ComparableSmartPtd& first() const;
    ComparableSmartPtd& last() const;
};

class SmartPtrSetEntry : public SetEntry, public ComparableSmartPtr {
  friend SmartPtrSet;

  public:
    SmartPtrSetEntry(SmartPtrSet& s, ComparableSmartPtd& v);
    SmartPtrSetEntry(ComparableSmartPtd& v);
    virtual ~SmartPtrSetEntry();

    SmartPtrSet& smartPtrSet() const;
    SmartPtrSetEntry* next() const;
    SmartPtrSetEntry* prev() const;
    void moveTo(SmartPtrSet* set);

  protected:
    static SmartPtrSetEntry* fromSetEntry(SetEntry* e);
};
*/
