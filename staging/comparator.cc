#define nsntl_comparator_cc_internal

#include "comparator.h"
#include "pair.h"

using namespace nsntl;

int nsntl::compare(const void* a, const void* b) {
  if (a < b)
    return -1;
  else if (a > b)
    return 1;
  else
    return 0;
};

int nsntl::compare(const int a, const int b) {
  if (a < b)
    return -1;
  else if (a > b)
    return 1;
  else
    return 0;
};

int nsntl::compare(const uint64_t a, const uint64_t b) {
  if (a < b)
    return -1;
  else if (a > b)
    return 1;
  else
    return 0;
};

nsntl::Comparator::Comparator() {
  // nothing now
};

nsntl::Comparator::~Comparator() {
  // nothing now
};

nsntl::FunctionComparator::FunctionComparator(int (*compare)(Object* a,Object* b)) {
  this->_compare=compare;
};

nsntl::FunctionComparator::~FunctionComparator() {
  // nothing now
};

int nsntl::FunctionComparator::compare(Object* a,Object* b) {
  return this->_compare(a,b);
};

int nsntl::pointerCompare(Object* a,Object* b) {
  if (a<b)
    return -1;
  else if (a>b)
    return 1;
  else
    return 0;
};

nsntl::PointerComparator::PointerComparator() : FunctionComparator(pointerCompare) {
  // nothing now
};

nsntl::PointerComparator::~PointerComparator() {
  // nothing now
};

class AlwaysEqualsComparator : public Comparator {
  public:
    int compare(Object*, Object*);
    virtual ~AlwaysEqualsComparator();
};

nsntl::AlwaysEqualsComparator::AlwaysEqualsComparator() : Comparator() {
  // nothing now
};

nsntl::AlwaysEqualsComparator::~AlwaysEqualsComparator() {
  // nothing now
};

int nsntl::AlwaysEqualsComparator::compare(Object* a, Object* b) {
  return 0;
};

nsntl::PairComparator::PairComparator()
  : PairComparator(&pointerComparator, &pointerComparator, false, false) {

  // nothing now
};

nsntl::PairComparator::PairComparator(int (*keyCompare)(Object*, Object*))
  : PairComparator(new FunctionComparator(keyCompare), &pointerComparator, true, false) {

  // nothing now
};

nsntl::PairComparator::PairComparator(Comparator* keyComparator)
  : PairComparator(keyComparator, &pointerComparator, false, false) {

  // nothing now
};

nsntl::PairComparator::PairComparator(int (*keyCompare)(Object*, Object*), int (*valueCompare)(Object*, Object*))
  : PairComparator(new FunctionComparator(keyCompare), new FunctionComparator(valueCompare), true, true) {

  // nothing now
};

nsntl::PairComparator::PairComparator(int (*keyCompare)(Object*, Object*), Comparator* valueComparator)
  : PairComparator(new FunctionComparator(keyCompare), valueComparator, true, false) {

  // nothing now
};

nsntl::PairComparator::PairComparator(Comparator* keyComparator, int (*valueCompare)(Object*, Object*))
  : PairComparator(keyComparator, new FunctionComparator(valueCompare), false, true) {
  // nothing now
};

nsntl::PairComparator::PairComparator(Comparator* keyComparator, Comparator* valueComparator, bool dynKeyC, bool dynValueC) {
  this->keyComparator = keyComparator;
  this->valueComparator = valueComparator;
  this->dynKeyC = dynKeyC;
  this->dynValueC = dynValueC;
};

nsntl::PairComparator::~PairComparator() {
  if (this->dynKeyC)
    delete this->keyComparator;
  if (this->dynValueC)
    delete this->valueComparator;
};

int nsntl::PairComparator::compare(Object* a, Object* b) {
  Pair* aPair = (Pair*)a;
  Pair* bPair = (Pair*)b;
  int ret = keyComparator->compare(aPair->getKey(), bPair->getKey());
  if (ret)
    return ret;
  else {
    ret = valueComparator->compare(aPair->getValue(), bPair->getValue());
    return ret;
  }
};
