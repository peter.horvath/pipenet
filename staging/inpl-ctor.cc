#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

class A {
  public:
    int a,b,c;
    void* operator new(size_t size) {
      printf("operator new: size=0x%016lx\n", size);
      return (A*)malloc(size);
    }
    void operator delete(void* p) {
      printf("operator delete: p=0x%016lx\n", (uint64_t)p);
      free(p);
    };
    A() {
      printf("A::A(0x%016lx)\n", (uint64_t)this);
      a = 5;
    };
    ~A() {
      printf("A::~A(0x%016lx)\n", (uint64_t)this);
    };
};

int main() {
  void* p=(void*)0xc0deebedfce2ea31ULL;
  A* a = new A();
  printf("p=0x%016lx a=0x%016lx\n", (uint64_t)p, (uint64_t)a);
  delete a;
  return 0;
};
