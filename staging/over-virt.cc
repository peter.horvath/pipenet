// demonstrates that specifying the class is stronger than virtual overrides

#include <stdio.h>

class A {
  public:
    virtual void pukk() {
      printf("A::pukk\n");
    };
};

class B : public A {
  public:
    void pukk() {
      printf("B::pukk\n");
      A::pukk();
    };
};

int main(int argc, char** argv) {
  B b;
  b.pukk();
  return 0;
};
