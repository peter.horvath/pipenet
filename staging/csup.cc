#include <stdlib.h>
#include <unistd.h>

#include <nsntl/atom.h>
#include <nsntl/util.h>

#include "csup.h"
#include "posxx.h"

void __gxx_personality_v0() {
  // nothing now
};

void operator delete(void* ptr) {
  nsntl::Free(ptr);
};

struct __cxa_exception
{
  // Manage the exception object itself.
  //std::type_info *exceptionType;
  void (_GLIBCXX_CDTOR_CALLABI *exceptionDestructor)(void *);

  // The C++ standard has entertaining rules wrt calling set_terminate
  // and set_unexpected in the middle of the exception cleanup process.
  std::unexpected_handler unexpectedHandler;
  std::terminate_handler terminateHandler;

  // The caught exception stack threads through here.
  __cxa_exception *nextException;

  // How many nested handlers have caught this exception.  A negated
  // value is a signal that this object has been rethrown.
  int handlerCount;

  // Cache parsed handler data from the personality routine Phase 1
  // for Phase 2 and __cxa_call_unexpected.
  int handlerSwitchValue;
  const unsigned char *actionRecord;
  const unsigned char *languageSpecificData;
  _Unwind_Ptr catchTemp;
  void *adjustedPtr;

  // The generic exception header.  Must be last.
  _Unwind_Exception unwindHeader;
};

struct __cxa_refcounted_exception {
  nsntl::atomic_u16 referenceCount;
  __cxa_exception exc;
};

extern "C" void* __cxxabiv1::__cxa_allocate_exception(size_t thrown_size) { 
  void *ret;
  
  thrown_size += sizeof (__cxa_refcounted_exception);
  ret = nsntl::Malloc(thrown_size);
  
  if (!ret)
    ret = nsntl::Malloc(thrown_size);
  
  if (!ret)
    Posxx::terminate ();
  
  nsntl::Bzero(ret, sizeof (__cxa_refcounted_exception));
  
  return (void *)((char *)ret + sizeof (__cxa_refcounted_exception));
};


extern "C" void __cxxabiv1::__cxa_free_exception(void *vptr) {
  char *ptr = (char *) vptr - sizeof (__cxa_refcounted_exception);
    nsntl::Free (ptr);
};

void __attribute__((weak)) Posxx::terminate() {
  static const char* terminate_msg = "Posxx::terminate\n";
  write(2, terminate_msg, nsntl::Strlen(terminate_msg));
  exit(1);
};
