#ifndef Pipenet_refptr_h
#define Pipenet_refptr_h

template <class T> class ref_ptr {
  private:
    class C {
      public:
        T* t;
        unsigned long long n;

        C(const T* t) {
          this->t = t;
          n = 1;
        };
        ~C() {
          delete t;
        };
    };
    C *c;
    void __attach(C* c) {
      __detach();
      this->c = c;
      c->n++;
    };
    void __detach() {
      if (c) {
        if (c->n > 1)
          c->n--;
        else{
          delete c;
        }
        c = (C*)0;
      }
    };
  public:
    ref_ptr() {
      c = (C*)0;
    };
    ref_ptr(const T* t) {
      if (t)
        c = new C(t);
      else
        c = (C*)0;
    };
    ref_ptr(const ref_ptr<T>& t) {
      if (t)
        __attach(t.c);
      else
        c = (C*)0;
    };
    ~ref_ptr() {
      __detach();
    };

    const ref_ptr<T>& operator=(const T* t) {
      __detach();
      if (t)
        c=new C(t);
    };
    const ref_ptr<T>& operator=(ref_ptr<T>& t) {
      __detach();
      if (t)
        __attach(t.c);
    };
    bool operator==(const T* t) const {
      if (t)
        return c.t == t;
      else
        return this;
    };
    bool operator==(ref_ptr<T>& t) const {
      if (t)
        return c->t == t.c->t;
      else
        return this;
    };
    T* get() const {
      if (c)
        return c->t;
      else
        return (C*)0;
    };
    T& operator*() const {
      return *(c->t);
    };
    unsigned long long use_count() const {
      return c->n;
    };
    bool unique() const {
      return c->n == 1;
    };
    operator bool() const {
      return c;
    };
    unsigned long long operator-(const T* t) const {
      return get()-t;
    };
    unsigned long long operator-(ref_ptr<T>& t) const {
      return get()-t.get();
    };
    bool operator<(const T* t) const {
      return get() < t;
    };
    bool operator<(ref_ptr<T>& t) const {
      return get() < t.get();
    };
    bool operator<=(const T* t) const {
      return get() <= t;
    };
    bool operator<=(ref_ptr<T>& t) const {
      return get() <= t.get();
    };
    bool operator!=(const T* t) const {
      return get() != t;
    };
    bool operator!=(ref_ptr<T>& t) const {
      return get() != t.get();
    };
    bool operator>(const T* t) const {
      return get() > t;
    };
    bool operator>(ref_ptr<T>& t) const {
      return get() > t.get();
    };
    bool operator>=(const T* t) const {
      return get() >= t;
    };
    bool operator>=(ref_ptr<T>& t) const {
      return get() >= t.get();
    };
};

#endif
