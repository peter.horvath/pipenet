#include <ucontext.h>
#include <signal.h>
#include <stdio.h>

int main(int argc, char** argv) {
  printf ("siginfo_t: %li, ucontext_t: %li\n", sizeof(siginfo_t), sizeof(ucontext_t));
  return 0;
};
