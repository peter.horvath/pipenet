#include <cstddef>

class C {
  public:
    int n;
};

class A {
  public:
    int f(int x);
};

class S : protected virtual C, protected A {
  public:
};

class Uc : protected virtual C {
  public:
    int f(void* x);
};

class Us : public S, public Uc {
  using Uc::f;
  public:
    void g() {
      f(NULL);
    };
};
