
IAmPolymorphicNow::IAmPolymorphicNow() {
  // nothing now
};

IAmPolymorphicNow::~IAmPolymorphicNow() {
  // nothing now
};

void IAmPolymorphicNow::iAmPolymorphicNow() {
  throw LogicException(LogicError::UNREACHABLE_CODE);
};

FactoryMediator::FactoryMediator(Factory& factory, FactoryMethod& factoryMethod) {
  this->factory = &factory;
  this->factoryMethod = &factoryMethod;
};

void FactoryMediator::call(FactoryResult& result) {
  (factoryMethod)(*factory, result);
};

