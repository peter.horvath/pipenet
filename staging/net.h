#ifndef Posxx_Socket_h
#define Posxx_Socket_h

class SockAddr {
  public:
    virtual int getDomain()=0;
    virtual int getSockAddrLen()=0;
};

class UnixAddr : public SockAddr {
  public:
    int getDomain();
    int getSockAddrLen();
};

class Ip4Addr : public Ip4Addr {
  public:
    int getDomain();
    int getSockAddrLen();
};

class Socket {
  public:
    
};

class DgramSocket {
};

class StreamSocket {
};

class ListenerSocket {
};

class SocketDomain {
  public:
    virtual int getDomain();
};

class UnixDomain : public SocketDomain {
  public:
    int getDomain();
};

class Ip4Domain : public SocketDomain {
  public:
    int getDomain();
};

class Tcp4Fd : public Socket, public Ip4Domain, public StreamSocket {
};

#endif
