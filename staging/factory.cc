#include <iostream>
#include "factory.h"

#ifdef DOTRACE
  #define TRACE(x) x
#else
  #define TRACE(x)
#endif

using namespace std;

void IAmPolymorphicNow::iAmPolymorphicNow() {
  // nothing now
};

IAmPolymorphicNow::IAmPolymorphicNow() {
  // nothing now
};

Factory::Mediator::Mediator(Factory& factory, MakeMethod& makeMethod) {
  TRACE(cout << "Factory::Mediator::Mediator(Factory*, MakeMethod)\n");
  this->factory = &factory;
  this->makeMethod = &makeMethod;
};

void Factory::Mediator::call(Result& result) {
  TRACE(cout << "Factory::Mediator::call(Result*)\n");
  (makeMethod)(*factory, result);
};

class A;

class B : public virtual Factory {
  private:
    int v;

  public:
    B(int v);
    int getV() const;
    static void makeCb(Factory& f, Factory::Result& a);
    Factory::Mediator make();
    void make(A& a);
};

class A : public virtual Factory::Result {
  friend class B;

  private:
    int v;

  public:
    A();
    A(Factory::Mediator fm);
    int getV() const;
    void setV(int v);
};

B::B(int v) {
  TRACE(cout << "B::B()\n");
  this->v = v;
};

int B::getV() const {
  TRACE(cout << "B::getV()\n");
  return v;
};

void B::makeCb(Factory& f, Factory::Result& r) {
  TRACE(cout << "B::makeCb(Factory&, Factory::Result&)\n");
  B& b = dynamic_cast<B&>(f);
  A& a = dynamic_cast<A&>(r);
  b.make(a);
};

void B::make(A& a) {
  TRACE(cout << "B::make(A&)\n");
  a.setV(getV()+1);
};

Factory::Mediator B::make() {
  TRACE(cout << "Factory::Mediator B::make()\n");
  return Factory::Mediator(dynamic_cast<Factory&>(*this), B::makeCb);
};

A::A() {
  TRACE(cout << "A::A()\n");
  v = 0;
};

A::A(Factory::Mediator fm) {
  TRACE(cout << "A::A(Factory::Mediator)\n");
  fm.call(*this);
};

int A::getV() const {
  TRACE(cout << "A::getV()\n");
  return v;
};

void A::setV(int v) {
  TRACE(cout << "A::setV(" << v << ")\n");
  this->v = v;
};

int main(int argc, char **argv) {
  B b(42);
  A a = b.make();
  cout << "a.v = " << a.getV() << "\n";
  return 0;
}
