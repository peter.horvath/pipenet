#include "ll.h"

LinkedListItem::LinkedListItem() {
  prev = NULL;
  next = NULL;
};

void CLinkedList::add(LinkedListItem *item) {
  if (!head) {
    item->prev = NULL;
    item->next = NULL;
    head = item;
  } else {
    head->prev = item;
    item->next = head;
    item->prev = NULL;
    head = item;
  };
};

void CLinkedList::remove(LinkedListItem *item) {
  if (item->prev)
    item->prev->next = item->next;
  if (item->next)
    item->next->prev = item->prev;
  if (item == head)
    head = item->next;
  item->prev = NULL;
  item->next = NULL;
};
