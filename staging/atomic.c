#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

typedef unsigned long long int u64;
typedef unsigned char u8;

void theExchange(u64* a, u64 b) {
  __atomic_exchange_n(a, b, __ATOMIC_SEQ_CST);
  printf ("%llu %llu\n", a, b);
};

void* arr[256];
u64 a, b;

void put(void* p) {
  arr[b++] = p;
};

u64* get() {
  return arr[a--];
};

u64 func(u64* p, u64* q) {
  return (u64)(*(p++)=(u64)q);
  //printf("pointer: %016x\n", (u64)p);
  //return 
};

int main(int argc, char** argv) {
  u64 p=getpid();
  u64 q=getppid();
  printf ("result: %llu\n", func(&p, &q));
  return 0;
};
