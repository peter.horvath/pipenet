class Buffer : public Block {
  private:
    u64 dataFrom;
    u64 dataTo;

  public:
    Buffer(u64 size);

    u64 getSize() const;
    void setSize(u64 size) const;

    u64 getUsed() const;
    u64 getFree() const;

    void read(void *addr, u64 len);
    void insert(void *addr, u64 len);

    /**
     * Moves given number of bytes from this into another buffer. If not possible,
     * PosxxException(BUFFER_TOO_SMALL) is thrown.
     */
    void moveDataTo(Buffer& buffer, u64 len);

    /**
      * Moves as many bytes from buffer to this as possible.
      */
    void moveDataTo(Buffer& buffer);

    /**
      * Changes the maximal size of the buffer. If zero, changes to the minimal
      * possible size. If not possible, PosxxException(BUFFER_TOO_SMALL) is thrown.
      */
    void setSize(u64 size=0);

    /**
      * After that, data is no more fragmented in the buffer (i.e. dataFrom <= dataTo).
      * If onStart == true, data will begin exactly on the start (i.e. dataFrom == 0).
      */
    void defrag(bool onStart = false);

    /**
      * Syntactic sugar around Buffer.moveDataTo(Buffer&). Don't chain them, the result will
      * be misleading.
      */
    Buffer& operator<<(Buffer& buffer);
    Buffer& operator>>(Buffer& buffer);
};

