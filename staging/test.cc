#include <stdio.h>

#include "core.h"

int main(int argc, char **argv) {
  PipeNet pipeNet;
  //UnixNode unixNode(&pipeNet);

  //TcpProxyFactory tcpProxyFactory(IpAddress(127,0,0,1,80));

  //TcpListenerNode tcpListenerNode(&pipeNet, 8080, &tcpProxyFactory);
  
  printf ("Message: %li, Pipe: %lu, Node: %lu, PipeNet: %lu, iterator: %lu\n",
    sizeof(Message), sizeof(Pipe), sizeof(Node), sizeof(PipeNet), sizeof(std::list<void*>::iterator));

  pipeNet.loop();

  return 0;
}
