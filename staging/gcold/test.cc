#include <algorithm>
#include <iostream>

#include "gc.h"

using namespace MaXXGC;

class TestObj : public Object {
  private:
    u64 id;

  public:
    TestObj(u64 id) {
      this->id = id;
    };
    virtual ~TestObj() {
      std::cout << "TestObj #" << id << " eliminated\n";
    };
};

TestObj testObj(0);

void test1() {
  TestObj t1(173);
  TestObj t2(149);
  TestObj *t3 = new TestObj(193);

  t1.onStack();
  t2.onStack();
  t3->onHeap();

  new Ptr<TestObj>(t1, t3);

  std::cout << "Test1, GC start\n";
  gc();
  std::cout << "Test1, GC end\n";
};

void test2() {
  std::vector<TestObj*> testObjs(100);

  testObj.onStack();
  testObjs[0] = &testObj;

  for (int a=1; a<100; a++) {
    testObjs[a] = new TestObj(a);
    testObjs[a]->onHeap();
  };

  int p=0;
  for (int a=1; a<200; a++) {
    //new Ptr<TestObj>(testObjs[a], testObjs[a+1]);
    new Ptr<TestObj>(testObjs[rand()%100], testObjs[rand()%100]);
  };

  std::cout << "Test2, Starting gc\n";
  gc();
  std::cout << "Test2, GC ends\n";
};

int main(int argc, char** argv) {
  std::cout << "Object size: " << sizeof(Object) << " Ptr size: " << sizeof(Ptr<TestObj>) << "\n";
  test1();
  test2();
  return 0;
};
