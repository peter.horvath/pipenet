#include <algorithm>

#include "gc.h"

using namespace MaXXGC;

Object::Graph *Object::Graph::globalGraph(NULL);

GcException::GcException(ErrorCode errorCode) {
  this->errorCode = errorCode;
};

Object::iPtr::iPtr(Object* srcObj, Object* dstObj) {
  this->srcObj = srcObj;
  this->dstObj = NULL;
  srcIter = srcObj->outPtrs.insert(srcObj->outPtrs.end(), this);
  graphIter = graph().ptrs.insert(graph().ptrs.end(), this);
  setDst(dstObj);
};

// We all love the wonderfully indeterministic c++ static initialization ordering
Object::Graph& Object::graph() {
  if (!Object::Graph::globalGraph)
    Object::Graph::globalGraph = new Object::Graph();
  return *Object::Graph::globalGraph;
};

Object::iPtr::~iPtr() {
  this->srcObj->outPtrs.erase(srcIter);
  if (this->dstObj)
    setDst(NULL);
  graph().ptrs.erase(graphIter);
};

Object* Object::iPtr::getDst() const {
  return dstObj;
};

void Object::iPtr::setDst(Object* dstObj) {
  if (this->dstObj)
    this->dstObj->inPtrs.erase(dstIter);
  this->dstObj = dstObj;
  if (dstObj)
    dstIter = dstObj->inPtrs.insert(dstObj->inPtrs.end(), this);
};

Object::Object() {
  storage = Object::UNDEFINED;
  category = Object::UNSET;
  prevCat = NULL;
  nextCat = NULL;
  setCategory(UNSET);
  parentPtr = NULL;
  graphIter = graph().objs.insert(graph().objs.end(), this);
};

Object::~Object() {
  if (parentPtr) {
    delete parentPtr;
    parentPtr = NULL;
  };

  setCategory(UNSET);

  while (!outPtrs.empty())
    delete *(outPtrs.begin());
  while (!inPtrs.empty())
    (*inPtrs.begin())->setDst(NULL);
  graph().objs.erase(graphIter);
};

Object* Object::getParent() {
  if (storage != MEMBEROF && parentPtr)
    throw GcException(PARENT_MEMBEROF_INCONSISTENCY);

  if (parentPtr)
    return parentPtr->getDst();
  else
    return NULL;
};

Object* Object::getTop() {
  Object* obj = this;
  while (Object* parent = obj->getParent()) // yes, really "="
    obj = parent;
  return obj;
};

bool Object::isCategory(Category category) const {
  return this->category == category;
};

void Object::setCategory(Category category) {
  switch (this->category) {
    case UNSET:
      break;
    case UNKNOWN:
      graph().llRemove(&graph().unknown, this);
      break;
    case BORDER:
      graph().llRemove(&graph().border, this);
      break;
    case READY:
      graph().llRemove(&graph().ready, this);
      break;
    default:
      throw GcException(UNREACHABLE_CODE);
  };  

  this->category = category;

  switch (category) {
    case UNSET:
      break;
    case UNKNOWN:
      graph().llAdd(&graph().unknown, this);
      break;
    case BORDER:
      graph().llAdd(&graph().border, this);
      break;
    case READY:
      graph().llAdd(&graph().ready, this);
      break;
    default:
      throw GcException(UNREACHABLE_CODE);
  };
};

void Object::onHeap() {
  if (storage != Object::UNDEFINED)
    throw GcException(STORAGE_ALREADY_SET);
  storage = ONHEAP;
};

void Object::onStack() {
  if (storage != Object::UNDEFINED)
    throw GcException(STORAGE_ALREADY_SET);
  storage = ONSTACK;
};

void Object::memberOf(Object* parent) {
  if (storage != Object::UNDEFINED)
    throw GcException(STORAGE_ALREADY_SET);

  for (Object* top = parent; top->storage != Object::MEMBEROF; top = top->getParent())
    if (top == this)
      throw GcException(CIRCULAR_MEMBEROF);

  storage = MEMBEROF;
  parentPtr = new iPtr(this, parent);
  new iPtr(parent, this);
};

void Object::memberOf(Object& parent) {
  memberOf(&parent);
};

Members::Members(Object* parent, const std::initializer_list<Object*>& children) {
  std::for_each(children.begin(), children.end(), [parent](Object* child){
    child->memberOf(parent);
  });
};

Members::Members(Object& parent, const std::initializer_list<Object*>& children) : Members(&parent, children) {
};

Object::Graph::Graph() {
  // nothing now
};

Object::Graph::~Graph() {
  while (!objs.empty())
    delete *(objs.begin());
  while (!ptrs.empty())
    delete *(ptrs.begin());
};

void Object::Graph::assertGcRunning() {
  if (!isGcRunning)
    throw GcException(GC_SHOULD_RUN);
};

void Object::Graph::llAdd(Object **ll, Object *obj) {
  if (!*ll) {
    *ll = obj;
    obj->prevCat = NULL;
    obj->nextCat = NULL;
  } else {
    (*ll)->prevCat = obj;
    obj->nextCat = *ll;
    obj->prevCat = NULL;
    *ll = obj;
  }
};

void Object::Graph::llRemove(Object** ll, Object* obj) {
  if (*ll == obj)
    *ll = obj->nextCat;
  if (obj->prevCat)
    obj->prevCat->nextCat = obj->nextCat;
  if (obj->nextCat)
    obj->nextCat->prevCat = obj->prevCat;
  obj->prevCat = NULL;
  obj->nextCat = NULL;
};

void Object::Graph::gc() {
  isGcRunning = true;

  border = NULL;
  unknown = NULL;
  ready = NULL;

  std::for_each(objs.begin(), objs.end(), [](Object* obj){
    if (obj->storage == Object::ONSTACK || obj->storage == Object::UNDEFINED)
      obj->setCategory(Object::BORDER);
    else
      obj->setCategory(Object::UNKNOWN);
  });

  while (border) {
    Object *obj=border;
    std::for_each(obj->outPtrs.begin(), obj->outPtrs.end(), [](Object::iPtr *outPtr){
      Object *out = outPtr->getDst();
      switch (out->category) {
        case Object::UNKNOWN:
          out->setCategory(Object::BORDER);
          break;
        case Object::BORDER:
        case Object::READY:
          break;
        default:
          throw GcException(UNREACHABLE_CODE);
      }
    });
    obj->setCategory(Object::READY);
  };

  while (unknown) {
    switch (unknown->storage) {
      case Object::UNDEFINED:
      case Object::MEMBEROF:
      case Object::ONSTACK:
        unknown->setCategory(Object::UNSET);
        break;
      case Object::ONHEAP:
        delete unknown;
        break;
      default:
        throw GcException(UNREACHABLE_CODE);
    };
  };

  isGcRunning = false;
};

void ::MaXXGC::gc() {
  Object::graph().gc();
};
