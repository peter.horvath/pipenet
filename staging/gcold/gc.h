#ifndef MaXXGC_h
#define MaXXGC_h

// Ptr<Dog> myDog(this, targetObj);

/*
1. Classes must inherit virtual MaXXGC::Object:

class MyClass : private virtual MaXXGC::Object {
  MaXXGC::Members(this, member1, member2, NULL);
};

   virtual inherit is not a need, but you need to guarantee that MaXXGC::Object happen
   only _once_ in the inheritance graph (or it will be counted as two different object).

2. Classes must have a virtual destructor: the GC won't know how did you inherited them.

3. Members() should be set by you, at _end_ of the member declarations

4. Ptr is a template, but it is only syntactic sugar. Actually, the pointer tracking
   happens only between the MaXXGC::Object s. Its constructor must be parametrized also
   with the _source_ object from which it points to somewhere (which is "this" on most
   cases). Ptr s _must_ have a non-zero source Object. Their source can be set only by the
   constructor and is unchangeable after that. But their destination can be NULL and it
   can be changed.

5. Ptr<> s work exclusively between MaXXGC::Object s

6. If somebody is on the stack, is on the heap, or it is the member of an eclosing class,
  _you_ must give it, ideally just after you constructed it with one of the onHeap(),
  onStack(), memberOf() methods. They can be called only once. Unset classes won't be part of
  the gc.

7. There is _no_ guarantee for the destruction order of isolated subgraphes, thus in a destructor
  there is _no_ guarantee that a Ptr<> member weren't meanwhile zeroed.

8. Avoid too deep trees of memberOf() -trees. The look for the toplevel object happens in gc time,
  linearly. It doesn't mean you can't use recursive data structures:
  for example, in the case of an std::map member, every contained objects parent - from the view of
  the gc - is "this".
*/

#include <list>

//#include "../lib.h"

namespace MaXXGC {

typedef enum {
  STORAGE_ALREADY_SET = 0x3586,
  PARENT_MEMBEROF_INCONSISTENCY = 0x3587,
  GC_SHOULD_RUN = 0x3588,
  UNREACHABLE_CODE = 0x3589,
  CIRCULAR_MEMBEROF = 0x358a
} ErrorCode;

class GcException : public std::exception {
  public:
    ErrorCode errorCode;
    GcException(ErrorCode errorCode);
};

class Object {
  class Graph;
  friend Graph;
  friend void MaXXGC::gc();
  template<class Dst> class Ptr;
  template<class Dst> friend class Ptr;

  private:
    static Graph& graph();

    class iPtr;

    class Graph final {
      friend iPtr;
      friend Object;
      friend void ::MaXXGC::gc();

      private:
        static Graph *globalGraph;
        std::list<Object*> objs;
        std::list<Object::iPtr*> ptrs;
        bool isGcRunning = false;

        // for gc
        Object *border, *unknown, *ready;
        void llAdd(Object **ll, Object* obj);
        void llRemove(Object **ll, Object* obj);

        Graph();
        ~Graph();

        void assertGcRunning();
        void gc();
    };

    class iPtr {
      friend Object;
      friend Graph;

      private:
        Object *srcObj, *dstObj;
        std::list<iPtr*>::iterator srcIter, dstIter, graphIter;

      public:
        iPtr(Object* srcObj, Object* dstObj);
        ~iPtr();

        Object* getDst() const;
        void setDst(Object* dstObj);
    };

    std::list<iPtr*> inPtrs;
    std::list<iPtr*> outPtrs;
    std::list<Object*>::iterator graphIter;
    Object *prevCat, *nextCat;
    iPtr *parentPtr;

    typedef enum {
      UNDEFINED = 0, // storage isn't set up yet
      ONSTACK = 1, // is on the stack
      ONHEAP = 2, // is on the heap
      MEMBEROF = 3 // member of another Object
    } Storage;
    Storage storage;

    typedef enum {
      UNSET = 0, // category isn't set yet
      UNKNOWN = 1, // not yet checked object
      BORDER = 2, // object is referenced, but its outPtrs weren't checked yet
      READY = 3 // object is referenced, outPtrs checked - ready
    } Category;
    Category category;

    Object* getParent();
    Object* getTop();
    bool isCategory(Category category) const;
    void setCategory(Category category);

  public:
    Object();
    virtual ~Object();

    void onHeap();
    void onStack();
    void memberOf(Object* obj);
    void memberOf(Object& obj);
};

// Actually syntactic sugar to the type-less Object::iPtr
template<class Dst> class Ptr final : protected Object::iPtr {
  public:
    Ptr(Object* srcObj, Dst* dstObj) : Object::iPtr(srcObj, dstObj) { };
    Ptr(Object* srcObj, Dst& dstObj) : Object::iPtr(srcObj, &dstObj) { };
    Ptr(Object& srcObj, Dst* dstObj) : Object::iPtr(&srcObj, dstObj) { };
    Ptr(Object& srcObj, Dst& dstObj) : Object::iPtr(&srcObj, &dstObj) { };
    ~Ptr() { };

    Dst& operator*() const {
      return getDst();
    };

    Ptr<Dst>& operator=(Ptr<Dst>* const ptr) {
      setDst(ptr);
      return *this;
    };

    Ptr<Dst>& operator=(const Ptr<Dst>& ptr) {
      setDst(&ptr);
      return *this;
    };

    bool operator==(Ptr<Dst>* const ptr) const {
      return dstObj == ptr->dstObj;
    };

    bool operator==(const Ptr<Dst>& ptr) const {
      return dstObj == ptr.dstObj;
    };

    bool isNull() const {
      return dstObj;
    };
};

template<class Dst> bool operator==(Dst* ptr1, Ptr<Dst>& ptr2) {
  return ptr1 == ptr2.dstObj;
};

class Members final {
  public:
    Members(Object* parent, const std::initializer_list<Object*>& children);
    Members(Object& parent, const std::initializer_list<Object*>& children);
};

void gc();

};

#endif
