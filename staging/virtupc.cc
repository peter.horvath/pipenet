// upcasting derived class removes virtual overrides

#include <cstdio>

class Base {
  public:
    virtual void showWith(const Base& b) const {
      printf ("Base::showWith(Base&)\n");
    };
};

class Derived : public Base {
  public:
    /* // this is what I do not want
    void showWith(const Base& b) const {
      printf ("Derived::showWith(Base&)\n");
      showWith(dynamic_cast<const Derived&>(b));
    };
    */
    void showWith(const Derived& d) const {
      printf ("Derived::showWith(Derived&)\n");
    };
};

void show(const Base& b1, const Base& b2) {
  b1.showWith(b2);
};

int main() {
  Derived d;
  Base& b = dynamic_cast<Base&>(d);
  show(b,b);
  return 0;
};
