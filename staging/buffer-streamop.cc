uint64_t posxx::Buffer::operator<<(Block& block) {
  uint64_t dataToMove = nsntl::min(getFree(), block.getSize());

  if (pos + used <= size) {
    // no overflow
    if (pos + used + dataToMove <= size) {
      // even after the read won't be one
      Memcpy((uint8_t*)addr + pos, block.getAddr(), dataToMove);
    } else {
      // after the read there will be an overflow
      Memcpy((uint8_t*)addr + pos + used, block.getAddr(), size - pos - used);
      Memcpy((uint8_t*)addr, (uint8_t*)block.getAddr() + size - pos - used, dataToMove - (size - pos - used));
    }
  } else {
    // overflow
    Memcpy((uint8_t*)addr + pos + used - size, block.getAddr(), dataToMove);
  }
  used += dataToMove;

  return dataToMove;
};

uint64_t posxx::Buffer::operator>>(Block& block) {
  uint64_t dataToMove = nsntl::min(getUsed(), block.getSize());

  if (pos + block.getSize() > size) {
    // overflow, two-step copy
    Memcpy( block.getAddr(), (uint8_t*)addr + pos, size - pos );
    Memcpy( (uint8_t*)block.getAddr() + size - pos, addr, dataToMove - (size - pos) );
  } else {
    // no overflow, single-step copy
    Memcpy( block.getAddr(), (uint8_t*)addr + pos, dataToMove);
  }

  pos += dataToMove;
  if (pos >= size)
    pos -= size;
  used -= dataToMove;

  return dataToMove;
};

uint64_t posxx::Buffer::operator<<(DataSrc& src) {
  uint64_t read;
  if (pos + used < size) {
    // no overflow, and even there is at least a single byte at the end
    read = Block((uint8_t*)addr + pos + used, size - pos - used) << src;
  } else {
    // overflow
    read = Block((uint8_t*)addr + pos + used - size,  size - used) << src;
  }
  used += read;
  return read;
};

uint64_t posxx::Buffer::operator>>(DataDst& dst) {
  uint64_t written;
  if (pos + used <= size) {
    // no overflow
    written = Block((uint8_t*)addr + pos, used) >> dst;
  } else {
    // overflow
    written = Block((uint8_t*)addr + pos, size - pos) >> dst;
  }
  pos += written;

  if (pos > size)
    throw LogicException(LogicError::ASSERT_FAILED);

  if (pos == size)
    pos = 0;
  used -= written;
  return written;
};
