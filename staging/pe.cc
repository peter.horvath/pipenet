#include <iostream>

class A {
  public:
    int a;
    A(int a=0) {
      std::cout << "A::A(int a=0)\n";
      this->a = a;
    };
    A(const A& a) : A(a.a) {
      // nothing now
    };
    A operator+(int a) const {
      std::cout << "A A::operator+(int a)\n";
      return A(this->a + a);
    };
    A operator+(A& a) const {
      std::cout << "A A::operator+(A& a)\n";
      return A(this->a + a.a);
    };
    A& operator=(int a) {
      std::cout << "A& A::operator=(int a)\n";
      this->a = a;
      return *this;
    };
};

int main() {
  A a(5);
  a = 8;
  a += 3;
  std::cout << a.a << '\n';
};
