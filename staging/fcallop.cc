// shows that f*g functors are only syntax sugar without semantic meaning

#include <cstdio>

class A {
  private:
    int a;
  public:
    A(int a) {
      this->a = a;
    };
    void operator()() {
      printf("a=%i\n", a);
    };
};

void callfn(void f()) {
  printf("callfn\n");
  f();
  printf(".\n");
};

int main() {
  A a(6);

  a();

  callfn(a);
  return 0;
};
