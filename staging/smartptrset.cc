
/*
Uint64Set::Uint64Set() : Set() {
  // nothing now
};

Uint64Set::~Uint64Set() {
  // nothing now
};

uint64_t Uint64Set::first() const {
  return static_cast<Uint64SetEntry*>(firstEntry())->get();
};

uint64_t Uint64Set::last() const {
  return static_cast<Uint64SetEntry*>(lastEntry())->get();
};

bool Uint64Set::has(uint64_t i, FindRelation relation) const {
  Uint64SetEntry tmp(i);
  return Set::find(&tmp, relation);
};

uint64_t Uint64Set::find(uint64_t i, FindRelation relation) const {
  Uint64SetEntry tmp(i);
  Uint64SetEntry* result = static_cast<Uint64SetEntry*>(Set::find(&tmp, relation));
  if (!result)
    throw LogicException(LogicError::NOT_FOUND);
  else
    return result->get();
};

bool Uint64Set::add(uint64_t i) {
  if (has(i))
    return false;
  Uint64SetEntry* e = new(entryAllocator->alloc(sizeof(Uint64SetEntry))) Uint64SetEntry(i);
  e->attach(this);
  return true;
};

bool Uint64Set::remove(uint64_t i) {
  Uint64SetEntry tmp(i);
  AvlEntry* e = Avl::find(&tmp);
  if (!e)
    return false;
  else {
    e->~AvlEntry();
    entryAllocator->free(static_cast<Uint64SetEntry*>(e));
    return true;
  }
};

Uint64SetEntry::Uint64SetEntry(uint64_t i) : SetEntry(0) {
  set(i);
};

Uint64SetEntry::~Uint64SetEntry() {
  // nothing now
};

int Uint64Set::compare(const SetEntry* a, const SetEntry* b) const {
  uint64_t ai = static_cast<const Uint64SetEntry*>(a)->i;
  uint64_t bi = static_cast<const Uint64SetEntry*>(b)->i;
  return nsntl::compare(ai, bi);
};

Uint64Set* Uint64SetEntry::getUint64Set() const {
  return static_cast<Uint64Set*>(SetEntry::getSet());
};

uint64_t Uint64SetEntry::get() const {
  return i;
};

void Uint64SetEntry::set(uint64_t i) {
  if (i == this->i)
    return;
  Uint64Set* _set = getUint64Set();
  if (_set)
    detach();
  this->i = i;
  if (_set)
    attach(_set);
};

Uint64SetEntry* Uint64SetEntry::next() const {
  return static_cast<Uint64SetEntry*>(SetEntry::next());
};

Uint64SetEntry* Uint64SetEntry::prev() const {
  return static_cast<Uint64SetEntry*>(SetEntry::prev());
};
*/

/*
nsntl::SmartPtrSet::SmartPtrSet(uint8_t flags) : Set(flags) {
  // nothing now
};

nsntl::SmartPtrSet::~SmartPtrSet() {
  // nothing now
};

int nsntl::SmartPtrSet::compare(const SetEntry* a, const SetEntry* b) const {
  SmartPtrSetEntry* sa = SmartPtrSetEntry::fromSetEntry(const_cast<SetEntry*>(a));
  SmartPtrSetEntry* sb = SmartPtrSetEntry::fromSetEntry(const_cast<SetEntry*>(b));
  ComparableSmartPtd* coa = static_cast<ComparableSmartPtd*>(sa->get());
  ComparableSmartPtd* cob = static_cast<ComparableSmartPtd*>(sb->get());
  return coa->compare(cob);
};

int nsntl::SmartPtrSet::findCompareHelper(const AvlEntry* a, const void* b) {
  const SmartPtrSetEntry* a2 = static_cast<const SmartPtrSetEntry*>(a);
  const ComparableSmartPtr* a3 = static_cast<const ComparableSmartPtr*>(a2);
  ComparableSmartPtd* a4 = a3->get();
  const Comparable* bb = reinterpret_cast<const Comparable*>(b);
  return a4->compare(bb);
};

bool nsntl::SmartPtrSet::has(const ComparableSmartPtr& v, FindRelation relation) const {
  return has(v.get(), relation);
};

bool nsntl::SmartPtrSet::has(const ComparableSmartPtd* v, FindRelation relation) const {
  return find(v, relation);
};

bool nsntl::SmartPtrSet::has(const ComparableSmartPtd& v, FindRelation relation) const {
  return has(&v, relation);
};

SmartPtrSetEntry* nsntl::SmartPtrSet::find(const Comparable* v, FindRelation relation) const {
  return static_cast<SmartPtrSetEntry*>(Avl::find(v, relation, findCompareHelper));
};

SmartPtrSetEntry* nsntl::SmartPtrSet::find(const Comparable& v, FindRelation relation) const {
  return find(&v);
};

SmartPtrSetEntry* nsntl::SmartPtrSet::add(ComparableSmartPtd& v) {
  SmartPtrSetEntry* r = new(entryAllocator->alloc(sizeof(SmartPtrSetEntry))) SmartPtrSetEntry(v);
  r->attach(this);
  return r;
};

void nsntl::SmartPtrSet::remove(const Comparable& v) {
  SmartPtrSetEntry* si = find(v);
  if (!si)
    throw LogicException(LogicError::NOT_FOUND);
  si->~SmartPtrSetEntry();
  entryAllocator->free(si);
};

SmartPtrSetEntry* nsntl::SmartPtrSet::firstEntry() const {
  return static_cast<SmartPtrSetEntry*>(Set::firstEntry());
};

SmartPtrSetEntry* nsntl::SmartPtrSet::lastEntry() const {
  return static_cast<SmartPtrSetEntry*>(Set::lastEntry());
};

ComparableSmartPtd& nsntl::SmartPtrSet::first() const {
  return *firstEntry()->get();
};

ComparableSmartPtd& nsntl::SmartPtrSet::last() const {
  return *lastEntry()->get();
};

nsntl::SmartPtrSetEntry::SmartPtrSetEntry(SmartPtrSet& s, ComparableSmartPtd& v): SetEntry(&s), ComparableSmartPtr(&v) {
  // nothing now
};

nsntl::SmartPtrSetEntry::SmartPtrSetEntry(ComparableSmartPtd& v) : ComparableSmartPtr(&v) {
  // nothing now
};

nsntl::SmartPtrSetEntry::~SmartPtrSetEntry() {
  // nothing now
};

SmartPtrSetEntry* nsntl::SmartPtrSetEntry::next() const {
  return static_cast<SmartPtrSetEntry*>(AvlEntry::next());
};

SmartPtrSetEntry* nsntl::SmartPtrSetEntry::prev() const {
  return static_cast<SmartPtrSetEntry*>(AvlEntry::prev());
};

SmartPtrSetEntry* nsntl::SmartPtrSetEntry::fromSetEntry(SetEntry* e) {
  return static_cast<SmartPtrSetEntry*>(e);
};
*/
