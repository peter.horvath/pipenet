#include "pair.h"

using namespace nsntl;

nsntl::Pair::Pair() {
  // nothing now
};

nsntl::Pair::~Pair() noexcept(false) {
  // nothing now
};

int nsntl::Pair::compare(const Comparable* p) const {
  Pair* pc = static_cast<Pair*>(p);

  return getKey()->compare(pc->getKey());
};

/*
nsntl::ObjectPair::ObjectPair(Object* k, Object* v) {
  key = k;
  value = v;
};

nsntl::ObjectPair::~ObjectPair() {
  // nothing now
};

Object* nsntl::ObjectPair::getKey() const {
  return key;
};

void nsntl::ObjectPair::setKey(Object* o) {
  key = o;
};

Object* nsntl::ObjectPair::getValue() const {
  return value;
};

void nsntl::ObjectPair::setValue(Object* o) {
  value = o;
};
*/
