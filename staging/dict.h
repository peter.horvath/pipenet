

class Array : public MapKeyUint64t, public MapValueSoftPtr, public MapEntryHard {
  public:
    Object& operator[](int i) const;
    Object& operator[](const Int& i) const;
};

class ArrayEntry : public MapKeyUint64tEntry, public MapValueSoftPtrEntry {
  private:
    uint64_t i;
    SoftPtr value;

  public:
    ArrayEntry(uint64_t i, Object* value);
    ~ArrayEntry();

    uint64_t getKey() const;
    void setKey(const ComparableObject* key);
    Object* getValue() const override;
    void setValue(Object* value) override;
};

class DictEntry;

class Dict : public MapKeyString, public MapValueString, public MapEntryHard {
  public:
    Dict();
    ~Dict();

    int compare(const AvlEntry* a, const AvlEntry* b) const;
    bool has(const string str) const;
    string get(const string str) const;
    DictEntry* find(const string str) const;
    Dict& set(const string name, const string value);

    string operator[](const string name) const;
};

class DictEntry : public AvlEntry {
  friend Dict;

  private:
    string name;
    string value;

  public:
    DictEntry(string name, string value);
    ~DictEntry();

    Dict* dict() const;
    DictEntry* next() const;
    DictEntry* prev() const;
};
