// a try to work-around c++ incapability of downcasting over non-public inheritance

#include <iostream>

class A {
  public:
    int a;

    A(int a) {
      std::cout << "A::A(" << (void*)this << ", " << a << ")\n";
      this->a = a;
    };
    virtual ~A() {
      std::cout << "A::~A(" << (void*)this << ")\n";
    };
};

class B : public A {
  public:
    int b;

    B(int b) : A(b + 2) {
      std::cout << "B::B(" << (void*)this << ", " << b << ")\n";
      this->b = b;
    };
    virtual ~B() {
      std::cout << "B::~B(" << (void*)this << ")\n";
    };
};


int main() {
  B *b = new B(3);
  std::cout << "b= " << reinterpret_cast<void*>(b) << '\n';
  A *a = dynamic_cast<A*>(b);
  std::cout << "a= " << reinterpret_cast<void*>(a) << '\n';
  B *bb = (B*)a;
  std::cout << "bb=" << (void*)bb << '\n';
  delete b;
  return 0;
};
