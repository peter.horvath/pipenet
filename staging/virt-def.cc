#include <stdio.h>

class A {
  public:
    virtual void pukk(int n=5) {
      printf("A::pukk(%i)\n", 5);
    };
};

class B : public A {
  public:
    void pukk() {
      printf("B::pukk()");
    };
};

int main(int argc, char** argv) {
};
