#include <cstdint>
#include <cstdio>

#define PRINTTYPE(x) printf(#x ": %i\n", sizeof(x))

int main() {
  PRINTTYPE(unsigned long long int);
  PRINTTYPE(uint64_t);
  return 0;
};
