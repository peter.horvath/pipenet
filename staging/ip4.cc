#include <errno.h>

#include "ip4.h"

using namespace Posxx;

Ip4::Ip4() {
  this->ip=0;
};

Ip4::Ip4(u8 a, u8 b, u8 c, u8 d) {
  ip=(((u32)a)<<24) | (((u32)b)<<16) | (((u32)c)<<8) | ((u32)d);
};

Ip4::Ip4(u32 ip) {
  this->ip=ip;
};

Ip4::Ip4(const Ip4& ip) {
  this->ip=ip.ip;
};

u32 Ip4::getIp() const {
  return ip;
};

void Ip4::setIp(u32 ip) {
  this->ip=ip;
};

void Ip4::setIp(Ip4 ip) {
  this->ip=ip.ip;
};

bool Ip4::operator==(u32 ip) const {
  return this->ip == ip;
};

bool Ip4::operator==(Ip4 ip) const {
  return this->ip == ip.ip;
};

Ip4& Ip4::operator=(u32 ip) {
  setIp(ip);
  return *this;
};

Ip4& Ip4::operator=(Ip4 ip) {
  setIp(ip);
  return *this;
};

bool Ip4::isNull() const {
  return !ip;
};

void Ip4::clear() {
  ip = 0;
};

IpPort::IpPort() {
  this->port=0;
};

IpPort::IpPort(u16 port) {
  this->port=port;
};

IpPort::IpPort(const IpPort& port) {
  this->port=port.port;
};

u16 IpPort::getPort() const {
  return this->port;
};

void IpPort::setPort(u16 port) {
  this->port=port;
};

void IpPort::setPort(IpPort port) {
  this->port=port.port;
};

bool IpPort::operator==(u16 port) const {
  return this->port == port;
};

bool IpPort::operator==(IpPort port) const {
  return this->port == port.port;
};

IpPort& IpPort::operator=(u16 port) {
  setPort(port);
  return *this;
};

IpPort& IpPort::operator=(IpPort port) {
  setPort(port.port);
  return *this;
};

bool IpPort::isNull() const {
  return !port;
};

void IpPort::clear() {
  port = 0;
};

Ip4Addr::Ip4Addr()  {
  sin_family = AF_INET;
  setIp(0);
  setPort(0);
};

Ip4Addr::Ip4Addr(Ip4 ip) : Ip4Addr() {
  setIp(ip);
};

Ip4Addr::Ip4Addr(Ip4 ip, IpPort port) : Ip4Addr() {
  setIp(ip);
  setPort(port);
};

Ip4Addr::Ip4Addr(Ip4 ip, u16 port) : Ip4Addr(ip, IpPort(port)) {
  // nothing now
};

Ip4Addr::Ip4Addr(IpPort port) : Ip4Addr(Ip4(0), port) {
  // nothing now
};

Ip4Addr::Ip4Addr(const Ip4Addr& ip4Addr) {
  setAddr(ip4Addr);
};

Ip4Addr::Ip4Addr(const struct sockaddr_in* const addr) {
  setAddr(addr);
};

const struct sockaddr_in* const Ip4Addr::getAddr() const {
  return this;
};

void Ip4Addr::setAddr(const struct sockaddr_in& addr) {
  if (addr.sin_family != AF_INET)
    throw PosxxException(SIN_FAMILY_NOT_AF_INET);

  sin_family = addr.sin_family;
  sin_addr.s_addr = addr.sin_addr.s_addr;
  sin_port = addr.sin_port;
};

void Ip4Addr::setAddr(struct sockaddr_in* const addr) {
  setAddr(*addr);
};

void Ip4Addr::setAddr(const Ip4Addr& ip4Addr) {
  setAddr(ip4Addr.getAddr());
};

Ip4 Ip4Addr::getIp() const {
  return Ip4(ntohl(sin_addr.s_addr));
};

void Ip4Addr::setIp(u32 ip) {
  setIp(Ip4(ip));
};

void Ip4Addr::setIp(Ip4 ip) {
  sin_addr.s_addr = ntohl(ip.getIp());
};

IpPort Ip4Addr::getPort() const {
  return IpPort(ntohs(sin_port));
};

void Ip4Addr::setPort(u16 port) {
  setPort(IpPort(port));
};

void Ip4Addr::setPort(IpPort port) {
  sin_port = ntohs(port.getPort());
};

bool Ip4Addr::isNull() const {
  return getIp() == 0 && getPort() == 0;
};

void Ip4Addr::clear() {
  setIp(0);
  setPort(0);
};

bool Ip4Addr::operator==(const Ip4Addr& addr) const {
  return getIp() == addr.getIp() && getPort() == addr.getPort();
};

bool Ip4Addr::operator==(const struct sockaddr_in& addr) const {
  return *this == Ip4Addr(&addr);
};

Ip4Addr& Ip4Addr::operator=(const Ip4Addr& addr) {
  setIp(addr.getIp());
  setPort(addr.getPort());
  return *this;
};

Ip4Addr& Ip4Addr::operator=(const struct sockaddr_in& addr) {
  return *this = Ip4Addr(addr);
};

// to avoid warning: "pure virtual" "called from constructor"
void BaseIp4Fd::mksock() {
  throw PosxxException(UNREACHABLE_CODE);
};

BaseIp4Fd::BaseIp4Fd() {
  mksock();
};

BaseIp4Fd::BaseIp4Fd(int fd) : SocketFd(fd) {
  // nothing now
};

BaseIp4Fd::BaseIp4Fd(const Fd& fd) : BaseIp4Fd(fd.getFd()) {
  // nothing now
};

BaseIp4Fd::BaseIp4Fd(const Ip4Addr& localAddr) : SocketFd() {
  mksock();
  bind(localAddr);
};

const Ip4Addr& BaseIp4Fd::getLocalAddr() {
  if (localAddr.isNull()) {
    sockaddr_in addr;
    socklen_t len=sizeof(struct sockaddr_in);
    if (::getsockname(getFd(), reinterpret_cast<struct sockaddr*>(&addr), &len)==-1)
      throw PosxxException(errno);
    if (len!=sizeof(struct sockaddr_in))
      throw PosxxException(INVALID_SOCKADDR_LENGTH);
    localAddr.setAddr(addr);
  };
  return localAddr;
};

void BaseIp4Fd::bind() {
  bind(Ip4Addr());
};

void BaseIp4Fd::bind(const Ip4Addr& localAddr) {
  if (::bind(getFd(), reinterpret_cast<const struct sockaddr*>(localAddr.getAddr()), sizeof(struct sockaddr_in)) == -1)
    throw PosxxException(errno);
};

void BaseIp4Fd::bind(IpPort port) {
  bind(Ip4Addr(Ip4(0), port));
};

void BaseIp4Fd::bind(u16 port) {
  bind(IpPort(port));
};

void BaseTcp4Fd::mksock() {
  if (getFd() != -1)
    throw PosxxException(FD_ALREADY_INITIALIZED);

  int fd = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (fd == -1)
    throw PosxxException(errno);

  setFd(fd);
};

BaseTcp4Fd::BaseTcp4Fd() {
  // nothing now
};

BaseTcp4Fd::BaseTcp4Fd(int fd) : BaseIp4Fd(fd) {
};

BaseTcp4Fd::BaseTcp4Fd(const Fd& fd) : BaseIp4Fd(fd) {
  // nothing now
};

BaseTcp4Fd::BaseTcp4Fd(const Ip4Addr& localAddr) : BaseIp4Fd(localAddr) {
  // nothing now
};

BaseTcp4Fd::~BaseTcp4Fd() {
  // nothing now
};

BaseIp4ConnectableFd::BaseIp4ConnectableFd() : BaseIp4Fd() {
  remoteAddr.clear();
};

BaseIp4ConnectableFd::BaseIp4ConnectableFd(int fd) : SocketFd(fd) {
  remoteAddr.clear();
};

BaseIp4ConnectableFd::BaseIp4ConnectableFd(const Fd& fd) : BaseIp4ConnectableFd(fd.getFd()) {
  // nothing now
};

BaseIp4ConnectableFd::BaseIp4ConnectableFd(const Ip4Addr& remoteAddr) : BaseIp4ConnectableFd() {
  connect(remoteAddr);
};

BaseIp4ConnectableFd::BaseIp4ConnectableFd(const Ip4Addr& localAddr, const Ip4Addr& remoteAddr) : BaseIp4ConnectableFd() {
  bind(localAddr);
  connect(remoteAddr);
};

const Ip4Addr& BaseIp4ConnectableFd::getRemoteAddr() {
  if (remoteAddr.isNull()) {
    sockaddr_in addr;
    socklen_t len=sizeof(struct sockaddr_in);
    if (::getpeername(getFd(), reinterpret_cast<struct sockaddr*>(&addr), &len)==-1)
      throw PosxxException(errno);
    if (len!=sizeof(struct sockaddr_in))
      throw PosxxException(INVALID_SOCKADDR_LENGTH);
    remoteAddr.setAddr(addr);
  }
  return remoteAddr;
};

void BaseIp4ConnectableFd::connect(const Ip4Addr& addr) {
  if (::connect(getFd(), reinterpret_cast<const struct sockaddr*>(addr.getAddr()), sizeof(struct sockaddr_in))==-1)
    throw PosxxException(errno);
};

Tcp4Fd::Tcp4Fd() {
  // nothing now
};

Tcp4Fd::Tcp4Fd(int fd) : BaseIp4ConnectableFd(fd) {
  // nothing now
};

Tcp4Fd::Tcp4Fd(const Fd& fd) : BaseIp4ConnectableFd(fd) {
  // nothing now
};

Tcp4Fd::Tcp4Fd(const Tcp4Fd& fd) : Tcp4Fd(fd.getFd()) {
  // nothing now
};

Tcp4Fd::Tcp4Fd(const Ip4Addr& remoteAddr) : BaseIp4ConnectableFd(remoteAddr) {
  // nothing now
};

Tcp4Fd::Tcp4Fd(const Ip4Addr& localAddr, const Ip4Addr& remoteAddr) : BaseIp4ConnectableFd(localAddr, remoteAddr) {
  // nothing now
};

Tcp4Fd::Tcp4Fd(FactoryMediator fm) {
  *this = fm;
};

Tcp4Fd& Tcp4Fd::operator=(FactoryMediator& fm) {
  fm.call(*this);
  return *this;
};

Udp4Dgram::Udp4Dgram(u64 size) : DynamicBlock(size) {
  // nothing now
};

Udp4Dgram::Udp4Dgram(const Ip4Addr& dstAddr, u64 size) : DynamicBlock(size) {
  setDstAddr(dstAddr);
};

Udp4Dgram::Udp4Dgram(const Ip4Addr& srcAddr, const Ip4Addr& dstAddr, u64 size) : Udp4Dgram(dstAddr, size) {
  setSrcAddr(srcAddr);
};

Udp4Dgram::Udp4Dgram(const Block& data) : DynamicBlock(data) {
  // nothing now
};

Udp4Dgram::Udp4Dgram(const Block& data, const Ip4Addr& dstAddr) : DynamicBlock(data) {
  setDstAddr(dstAddr);
};

Udp4Dgram::Udp4Dgram(const Block& data, const Ip4Addr& srcAddr, const Ip4Addr& dstAddr) : Udp4Dgram(data, dstAddr) {
  setSrcAddr(srcAddr);
};

Ip4Addr& Udp4Dgram::getSrcAddr() const {
  return const_cast<Ip4Addr&>(srcAddr);
};

void Udp4Dgram::setSrcAddr(const Ip4Addr& addr) {
  srcAddr = addr;
};

Ip4Addr& Udp4Dgram::getDstAddr() const {
  return const_cast<Ip4Addr&>(dstAddr);
};

void Udp4Dgram::setDstAddr(const Ip4Addr& addr) {
  dstAddr = addr;
};

void Udp4Dgram::sendTo(Udp4Fd& fd) {
  fd.send(*this);
};

void Udp4Dgram::sendTo(Udp4Fd& fd, const Ip4Addr& dstAddr) {
  setDstAddr(dstAddr);
  fd.send(*this);
};

void Udp4Fd::mksock() {
  if (getFd() != -1)
    throw PosxxException(FD_ALREADY_INITIALIZED);

  int fd = ::socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if (fd == -1)
    throw PosxxException(errno);

  setFd(fd);
};

Udp4Fd::Udp4Fd() : BaseIp4ConnectableFd() {
  // nothing now
};

Udp4Fd::Udp4Fd(int fd) : BaseIp4ConnectableFd(fd) {
  // nothing now
};

Udp4Fd::Udp4Fd(const Fd& fd) : BaseIp4ConnectableFd(fd) {
  // nothing now
};

Udp4Fd::Udp4Fd(const Ip4Addr& localAddr) : Udp4Fd() {
  bind(localAddr);
};

Udp4Fd::Udp4Fd(const Ip4Addr& localAddr, const Ip4Addr& remoteAddr) : Udp4Fd(localAddr) {
  connect(remoteAddr);
};

void Udp4Fd::recvFrom(Factory& factory, FactoryResult& result) {
  Udp4Fd& fd = dynamic_cast<Udp4Fd&>(factory);
  Udp4Dgram& dgram = dynamic_cast<Udp4Dgram&>(result);
  fd.recvFrom(dgram);
};

FactoryMediator Udp4Fd::recvFrom() {
  return FactoryMediator(*this, recvFrom);
};

int Udp4Fd::recvFrom(Block& block, Ip4Addr& srcAddr) {
  unsigned int len = sizeof(struct sockaddr_in);
  int bytesGot = ::recvfrom(getFd(), block.getAddr(), block.getSize(), 0, reinterpret_cast<struct sockaddr*>(&srcAddr), &len);

  if (bytesGot == -1)
    throw PosxxException(errno);

  if (len != sizeof(struct sockaddr_in))
    throw PosxxException(INVALID_SOCKADDR_LENGTH);

  return bytesGot;
};

int Udp4Fd::recvFrom(Block& block) {
  Ip4Addr srcAddr;
  return recvFrom(block, srcAddr);
};

void Udp4Fd::recvFrom(DynamicBlock& block, Ip4Addr& srcAddr) {
  int bytesGot = recvFrom(static_cast<Block&>(block), srcAddr);
  block.setSize(bytesGot);
};

void Udp4Fd::recvFrom(DynamicBlock& block) {
  Ip4Addr srcAddr;
  recvFrom(block, srcAddr);
};

void Udp4Fd::recvFrom(Udp4Dgram& dgram) {
  recvFrom(static_cast<DynamicBlock&>(dgram), dgram.getSrcAddr());
};

void Udp4Fd::sendTo(const Block& block, const Ip4Addr& addr) {
  int bytesSent = ::sendto(getFd(), block.getAddr(), block.getSize(), 0, reinterpret_cast<const struct sockaddr*>(addr.getAddr()), sizeof(struct sockaddr_in));

  if (bytesSent == -1)
    throw PosxxException(errno);

  if ((unsigned int)bytesSent != block.getSize())
    throw PosxxException(COULD_NOT_SENT_WHOLE_PACKET);
};

void Udp4Fd::send(const Udp4Dgram& pkt) {
  sendTo(pkt, pkt.getDstAddr());
};

void Tcp4ListenerFd::listen(int maxconn) {
  if (!maxconn)
    maxconn = SOMAXCONN;

  if (getSockOptInt(SOL_SOCKET,SO_ACCEPTCONN))
    throw PosxxException(SOCKET_IS_ALREADY_LISTENING);

  if (::listen(getFd(), maxconn)==-1)
    throw PosxxException(errno);

  setSockOptInt(SOL_SOCKET, SO_REUSEADDR, 1);

  // maybe some SO_LINGER as well?
};

Tcp4ListenerFd::Tcp4ListenerFd(int maxconn) : BaseTcp4Fd() {
  listen(maxconn);
};

Tcp4ListenerFd::Tcp4ListenerFd(const Fd& fd, int maxconn) : BaseTcp4Fd(fd) {
  listen(maxconn);
};

Tcp4ListenerFd::Tcp4ListenerFd(const Ip4Addr& addr, int maxconn) : BaseTcp4Fd() {
  bind(addr);
  listen(maxconn);
};

Tcp4ListenerFd::Tcp4ListenerFd(IpPort port, int maxconn) : Tcp4ListenerFd(Ip4Addr(port), maxconn) {
  // nothing now
};

Tcp4ListenerFd::Tcp4ListenerFd(u16 port, int maxconn) : Tcp4ListenerFd(Ip4Addr(IpPort(port)), maxconn) {
  // nothing now
};

void Tcp4ListenerFd::accept(Tcp4Fd& tcp4fd) {
  struct sockaddr_in remoteAddr;
  unsigned int len = sizeof(struct sockaddr_in);
  int fd = ::accept(getFd(), reinterpret_cast<struct sockaddr*>(&remoteAddr), &len);

  if (fd == -1)
    throw PosxxException(errno);

  if (len != sizeof(struct sockaddr_in))
    throw PosxxException(INVALID_SOCKADDR_LENGTH);

  if (remoteAddr.sin_family != AF_INET)
    throw PosxxException(SOCKET_FAMILY_INVALID);

  tcp4fd.setFd(fd);
};

void Tcp4ListenerFd::accept(Factory& f, FactoryResult& r) {
  Tcp4ListenerFd& listener = dynamic_cast<Tcp4ListenerFd&>(f);
  Tcp4Fd& fd = dynamic_cast<Tcp4Fd&>(r);
  listener.accept(fd);
};

FactoryMediator Tcp4ListenerFd::accept() {
  return FactoryMediator(*this, Tcp4ListenerFd::accept);
};
