// Does not work, typed varargs _require_ templates in C++. S...

class A {
  private:
    int a;
  public:
    A(int a) {
      this->a = a;
    };
    operator int() {
      return a;
    };
};

int f(A... args) {
  return 0;
};
