#ifndef posxx_cxxsup_h
#define posxx_cxxsup_h

#include <stddef.h>

extern "C" {

void __gxx_personality_v0();

};

void operator delete(void* ptr);

namespace __cxxabiv1 {

extern "C" void* __cxa_allocate_exception(size_t thrown_size);

};

namespace Posxx {
  void terminate();
};

#endif
