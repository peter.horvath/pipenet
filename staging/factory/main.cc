#include <stdio.h>

#include "factory.h"

class Prod : public FactoryResult {
  private:
    int v;

  public:
    Prod(FactoryMediator fm) : FactoryResult(fm) {
      // nothing now
    };

    void* getThis() {
      return this;
    };
    void setV(int v) {
      this->v = v;
    };
    void show() {
      printf ("Prod.v=%i\n", v);
    };
};

class Fac : public Factory {
  public:
    int i=42;
    void make(void* _prod) {
      Prod* prod = (Prod*)_prod;
      prod->setV(++i);
    };
    FactoryMediator make() {
      return FactoryMediator(*this);
    };
};

int main(int argc, char** argv) {
  Fac fac;
  Prod prod = fac.make();
  prod.show();
  return 0;
};
