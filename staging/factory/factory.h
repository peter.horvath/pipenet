#ifndef pipenet_staging_factory_h
#define pipenet_staging_factory_h

/*

Fact fact;
Prod prod = fact.make();

-->

Try#1:

fact.make()=FactoryResult f;
prod.operator=(f);

---

Try#2: copy elision

Prod Fact::make() { ... ; return prod; }

Doesn't work with polymorphism.

*/

class FactoryMediator;

class FactoryResult {
  public:
    FactoryResult(FactoryMediator& fm);
    virtual void* getThis()=0;
};

class Factory {
  public:
    virtual void make(void* result)=0;
};

class FactoryMediator {
  private:
    Factory* factory;
    void* result;

  public:
    FactoryMediator(Factory& factory);

    Factory* getFactory();

    void make();
};

#endif
