#include "factory.h"

FactoryResult::FactoryResult(FactoryMediator& fm) {
  fm.getFactory()->make(this);
};

FactoryMediator::FactoryMediator(Factory& factory) {
  this->factory = &factory;
};

Factory* FactoryMediator::getFactory() {
  return factory;
};

void FactoryMediator::make() {
  factory->make(result);
};
