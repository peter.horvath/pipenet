#ifndef nsntl_aclist_h
#define nsntl_aclist_h

/* Adaptively sized, bidirectional, circular list */

#include "collection.h"

namespace nsntl {

class AclistItem;

class Aclist {
  friend AclistItem;
  private:
    AclistItem* frst;
    AclistItem* lst;
    int size;
    int free;

  public:
    Aclist();
    ~Aclist();

    void init();
    Object* first() const;
    Object* last() const;
    AclistItem* firstItem() const;
    AclistItem* lastItem() const;
    bool isEmpty() const;
    void for_each(void f(AclistItem* li));
    void for_each(void f(Object* v));
};

class AclistItem : public Item {
  friend Aclist;
  private:
    Aclist* lst;
    AclistItem* prv;
    AclistItem* nxt;

  public:
    AclistItem(Aclist* list = 0);
    ~AclistItem();

    void init(Object* v = 0);
    Aclist* list() const;
    AclistItem* next() const;
    AclistItem* prev() const;
    void attach(Aclist* tgt);
    void detach();
    void moveTo(Aclist* tgt);
};

};

#endif
