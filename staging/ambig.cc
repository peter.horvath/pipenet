#include <iostream>
#include <cstdint>

typedef enum {
  /** Finds the largest AvlEntry lighter than the parameter */
  FIND_LT,

  /** Finds the largest AvlEntry lighter than or equal to the parameter */
  FIND_LE,

  /** Finds an AvlEntry equal to the parameter */
  FIND_EQ,

  /** Finds the smallest AvlEntry greater than or equal to the parameter */
  FIND_GE,

  /** Finds the smallest AvlEntry greater than the parameter */
  FIND_GT
} FindRelation;

class B {
};

class A {
  public:
    void f(const void* v, FindRelation relation = FIND_EQ, int compare(const B*, const void*) = 0) const {
      std::cout << v << '\n';
    };
    void f(const void* v, int compare(const B*, const void*)) const {
      std::cout << v << '\n';
    };
    void f(uint64_t i, FindRelation relation = FIND_EQ) const {
      std::cout << i << '\n';
    };
};

int main() {
  A a;
  a.f(5, FIND_EQ);
  return 0;
};
