#include <signal.h>
#include <stdio.h>

int main(int argc, char** argv) {
  printf ("siginfo: %lu, ucontext: %lu\n", sizeof(siginfo_t), sizeof(ucontext_t));
  return 0;
};
