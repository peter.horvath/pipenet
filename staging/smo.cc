// tries & demonstrates static method overload - this works

#include <stdio.h>

class AA {
};

class BB : public AA {
};

class A {
  public:

  static void a(AA* aa) {
    printf ("A::a()\n");
  };

  static void b(AA* aa) {
    printf ("A::b()\n");
  };
};

class B : public A {
  public:

  static void a(BB* bb) {
    printf ("B::a()\n");
  };

  static void b(BB* bb) {
    printf ("B::b()\n");
  };
};

int main() {
  AA aa;
  BB bb;

  B::b(&bb);
  return 0;
};
