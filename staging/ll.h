#ifndef MaXX_LinkedList_h
#define MaXX_LinkedList_h

#include <stddef.h>

#include <stdio.h> // only for debug

/* Usage scenario:
 *
 * class Parent {
 *  ...
 *  LinkedListItem thatList;
 *  ...
 * }
 *
 * LinkedListHead thatList(offsetof(Parent, thatList));
 *
 * 
 */

// very minimalistic, very small, very fast and template-free C-like linkedlist implementation

/*
namespace ManyList {
  template <class T>
  class List;

  template <int N>
  class Item {
    template <class T> friend class List<T>;

    private:
      Item<N> *prevItems[N], *nextItems[N];
      Item<N>& getPrev(int idx);
      void setPrev(Item<N>& prev, int idx);
      Item<N>& getNext(int idx);
      void setNext(Item<N>& next, int idx);

    public:
      Item();
      ~Item();
  };

  template <class T> class List {
    friend template <int N> friend class Item<N>;

    private:
      Item<T> *head;

    public:
      
  };
*/

class CLinkedList;

class LinkedListItem {
  friend CLinkedList;
  private:
    LinkedListItem* prev;
    LinkedListItem* next;
  public:
    LinkedListItem();
};

class CLinkedList {
  private:
    LinkedListItem* head;

  public:
    void add(LinkedListItem *item);
    void remove(LinkedListItem *item);
};

};

// syntactic template-sugar for it
/*
template <class T> class LinkedListItemTpl {
  private:
    LinkedListItem t;
  public:
    
};*/

template <class T, LinkedListItem *T.itemMember> class LinkedList {
  private:
    ptrdiff_t offset;
    CLinkedList ll;

  public:
    LinkedList<T, itemMember>() {
      offset = offsetof(T, T::itemMember);
      printf ("%li\n", offset);
    };
};

class Example {
  public:
    int a;
    LinkedListItem b;
};

LinkedList<Example, &Example::b> theList;

};

#endif
