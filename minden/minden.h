#ifndef pipenet_minden_h
#define pipenet_minden_h

#include <posxx/fd.h>
#include <pipenet/core.h>

namespace minden {

class FdNode : public virtual Node, public virtual Fd {
  public:
    FdNode(const Pipe& src, const Pipe& dst, const Fd& fd);
};

class Tcp4Node : public virtual FdNode, public virtual Tcp4Fd {
  public:
    Tcp4Node(const Pipe& src, const Pipe& dst, const Tcp4Fd& fd);
};

class Tcp4ListenerNode : public virtual Node, public virtual Tcp4ListenerFd {
  public:
    Tcp4ListenerNode(const Pipe& src, const Pipe& dst, const Tcp4ListenerFd& fd);
};

class SignalNode : public virtual Node, public virtual SignalHandler {

};

class TimerNode : public virtual Node, public virtual Timer {

};

// Sits on a ListenerFd, converts incoming connections to FdNodes and outputs them
class AcceptorNode {
};

// gets incoming Nodes on its input and integrates them into the PipeNet
class IntegratorNode {
};

class SocketNode : protected virtual posxx::Fd, protected virtual posxx::Node {
  public:
    
};

class ConnectorStreamNode : protected virtual SocketNode {
  public:

};

class ListenerStreamNode : protected virtual SocketNode, protected virtual ListenerSocket {
  public:

};

class SignalNode : protected virtual
  public:
};

class TimeNode : protected virtual nsntl::

#endif
