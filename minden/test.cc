// roughly so will a tcp proxy work

int main() {
  Pipenet pn;
  EventNode *en = new EventNode();
  Tcp4ListenerNode *l = new Tcp4ListenerNode(Tcp4Port(64738));
  FactoryNode *f = new FactoryNode(&c, [en](Message* msg) {
    SocketMsg* sm = dynamic_cast<SocketMsg*>(msg);
    StreamSocketNode* ssn = new StreamSocketNode(sm->getSocket());
    Tcp4ConnectorNode* c = new Tcp4ConnectorNode(Tcp4Addr(127,0,0,1,22));
    pn.attachNode(ssn);
    pn.attachNode(c);
    pn.makePipe(en, ssn);
    pn.makePipe(ssn, en);
    pn.makePipe(en, c);
    pn.makePipe(c, en);
    pn.makePipe(ssn, c);
    pn.makePipe(c, ssn);
  });
  pn.attachNode(en);
  pn.attachNode(l);
  pn.attachNode(f);
  pn.makePipe(en, l);
  pn.makePipe(l, en);
  pn.makePipe(l, f);
  pn.tic();
  return 0;
}
