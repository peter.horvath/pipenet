#ifndef Posxx_ip4event_h
#define Posxx_ip4event.h

class Udp4EventSocket : public Udp4Socket, public FdEventHandler {
  public:
    TODO;
};

class Tcp4EventSocket : public Tcp4Socket, public FdEventHandler {
  public:
    typedef enum {
      CONNECTING,
      CONNECTED,
      CLOSED_IN,
      CLOSED_OUT,
      CLOSED,
      ERROR
    } State;

  private:
    Buffer in, out;
    State state;

  public:
    Tcp4EventSocket(int fd);
    Tcp4EventSocket(const Tcp4Socket& fd); // throws LogicError::COPY_ELISION_ENFORCED
    Tcp4EventSocket(const Ip4Addr& remoteAddr = nullptr);
    Tcp4EventSocket(const Ip4Addr& localAddr, const Ip4Addr& remoteAddr);

    virtual ~Tcp4EventSocket();

    Buffer& getInBuf() const;
    Buffer& getOutBuf() const;
};

class Tcp4ListenerEventSocket : public Tcp4ListenerSocket, public FdEventHandler {
  public:
    Tcp4ListenerEventSocket(const Tcp4ListenerEventSocket& socket); // throws LogicError::COPY_ELISION_ENFORCED
    Tcp4ListenerEventSocket(int maxconn = 0);
    Tcp4ListenerEventSocket(IpPort port, int maxconn = 0);
    Tcp4ListenerEventSocket(u16 port, int maxconn = 0);
    virtual ~Tcp4ListenerEventSocket();

    virtual void onConnect(Tcp4EventSocket* socket)=0;
};

#endif
