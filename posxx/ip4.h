#ifndef Posxx_Ip4_h
#define Posxx_Ip4_h

#include <netinet/in.h>

#include "exception.h"
#include "socket.h"

namespace posxx {

/**
  * Ipv4 address. Everything is in host byte order.
  */
class Ip4 {
  private:
    uint32_t ip;

    void init(uint8_t a, uint8_t b, uint8_t c, uint8_t d);

  public:
    Ip4();
    Ip4(uint8_t a, uint8_t b, uint8_t c, uint8_t d);
    Ip4(uint32_t ip);
    Ip4(int ip);
    Ip4(const Ip4& ip);
    Ip4(const char* ip);
    explicit Ip4(const nsntl::string& ip);

    uint32_t getIp() const;
    void setIp(Ip4 ip);
    bool operator==(Ip4 ip) const;
    Ip4& operator=(const Ip4 ip);
    bool isNull() const;
    void clear();
    operator nsntl::string() const;
};

/**
  * An IP port (0 - 65535). Everything is in host byte order.
  */
class IpPort {
  private:
    uint16_t port; // host byte order

  public:
    IpPort();
    IpPort(uint16_t port);
    IpPort(int port);
    IpPort(const IpPort& port);
    IpPort(const char* port);
    IpPort(const nsntl::string& port);

    uint16_t getPort() const;
    void setPort(uint16_t port);
    void setPort(IpPort port);
    bool operator==(uint16_t port) const;
    bool operator==(IpPort port) const;
    IpPort& operator=(uint16_t port);
    IpPort& operator=(IpPort port);
    bool isNull() const;
    void clear();
    operator nsntl::string() const;
};

class BaseIp4Addr : public virtual BaseSockAddr {
  public:
    sa_family_t getFamily() const;
    socklen_t getSockAddrLength() const;
};

/**
  * An ipv4 socket address (ip + port).
  */
class Ip4Addr : private sockaddr_in, public SockAddr, public BaseIp4Addr {
  public:
    Ip4Addr();
    Ip4Addr(Ip4 ip);
    Ip4Addr(Ip4 ip, IpPort port);
    Ip4Addr(Ip4 ip, uint16_t port);
    Ip4Addr(IpPort port);
    Ip4Addr(const Ip4Addr& ip4Addr);
    Ip4Addr(const SockAddr& sockAddr);
    Ip4Addr(const struct sockaddr_in* addr);
    Ip4Addr(const struct sockaddr* addr);
    Ip4Addr(const sockaddr_in& addr);
    Ip4Addr(const char* addr);
    Ip4Addr(const nsntl::string& addr);

    const struct sockaddr* getAddr() const;
    void setAddr(const Ip4 ip, const IpPort port);
    void setAddr(const struct sockaddr_in& addr);
    void setAddr(const sockaddr* addr);
    void setAddr(const Ip4Addr& ip4Addr);
    Ip4 getIp() const;
    void setIp(Ip4 ip);
    IpPort getPort() const;
    void setPort(IpPort port);

    bool isNull() const;
    void clear();

    bool operator==(const Ip4Addr& addr) const;
    Ip4Addr& operator=(const Ip4Addr& addr);

    operator nsntl::string() const;
};

class BaseTcp4Socket : public virtual BaseStreamSocket {
  protected:
    Ip4Addr localAddr;

    BaseTcp4Socket();
    virtual ~BaseTcp4Socket();

  public:
    int getProtocol() const;
};

/**
  * TCP/IPv4 connection-oriented socket
  */

class Tcp4Socket : public BaseConnectedSocket, public BaseTcp4Socket {
  friend class Tcp4ListenerSocket;
  private:
    Ip4Addr remoteAddr;
    void init();

  public:
    Tcp4Socket();
    Tcp4Socket(int fd);
    Tcp4Socket(const Tcp4Socket& fd); // throws LogicError::COPY_ELISION_ENFORCED
    Tcp4Socket(const Ip4Addr& remoteAddr);
    Tcp4Socket(const Ip4Addr& localAddr, const Ip4Addr& remoteAddr);

    virtual ~Tcp4Socket();
};

/**
  * UDP/IPv4 datagram socket
  */
class Udp4Dgram : public Dgram {
  private:
    Ip4Addr srcAddr;
    Ip4Addr dstAddr;

    void init();

  protected:

  public:
    Udp4Dgram(uint64_t size = 0);
    Udp4Dgram(const Ip4Addr& dstAddr, uint64_t size = 0);
    Udp4Dgram(const Ip4Addr& srcAddr, const Ip4Addr& dstAddr, uint64_t size = 0);
    Udp4Dgram(const Block& data);
    Udp4Dgram(const Block& data, const Ip4Addr& dstAddr);
    Udp4Dgram(const Block& data, const Ip4Addr& srcAddr, const Ip4Addr& dstAddr);
    virtual ~Udp4Dgram();
};

/* It is simpler as it seems. Simply use
 *
 * Udp4Dgram dgram = udp4Fd.recvFrom(...);
 *
 * These tricks are only to solve it without copy-by-value.
 */
class Udp4Socket : public BaseDgramSocket {
  private:
    Ip4Addr localAddr;
    Ip4Addr remoteAddr;

    void init();

  public:
    Udp4Socket();
    Udp4Socket(int fd);
    Udp4Socket(const Ip4Addr& remoteAddr);
    Udp4Socket(const Ip4Addr& localAddr, const Ip4Addr& remoteAddr);
    virtual ~Udp4Socket();

    int getProtocol() const;
};

/**
  * Class describing a listening TCP/IPv4 socket.
  *
  * Most trivial example: Tcp4ListenerSocket(IpPort(5020));
  */
class Tcp4ListenerSocket : public BaseTcp4Socket, public BaseListenerSocket {
  public:
    Tcp4ListenerSocket(const Tcp4ListenerSocket& socket); // throws LogicError::COPY_ELISION_ENFORCED
    Tcp4ListenerSocket(int maxconn = 0);
    Tcp4ListenerSocket(IpPort port, int maxconn = 0);
    Tcp4ListenerSocket(const Ip4Addr& addr, int maxconn = 0);
    Tcp4ListenerSocket(uint16_t port, int maxconn = 0);
    virtual ~Tcp4ListenerSocket();

    Tcp4Socket accept();
};

};
#endif
