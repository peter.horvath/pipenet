#ifndef Posxx_exception_h
#define Posxx_exception_h

#include <nsntl/exception.h>

namespace posxx {

/**
  * Exceptions from the Posxx. Uses the same uint64_t-based errorCodes.
  */

class Exception : public nsntl::ErrorCodeException {
  public:
    Exception(int posix_errno);
};

};
#endif
