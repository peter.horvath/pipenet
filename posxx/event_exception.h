#ifndef posxx_event_exception_h
#define posxx_event_exception_h

#include <stdint.h>

#include <nsntl/exception.h>

namespace posxx {

enum class EventError : uint64_t {
  DETACH_WITHOUT_ACTIVE_EM = 0x2fdb10a6a06d4578ULL,
  CANT_AIM_DETACHED_HANDLER = 0x0e27471860c60a42ULL,
  CANT_AIM_DISABLED_HANDLER = 0x7287483d64864b0aULL,
  CANT_CALL_TRIGGER_ON_DISABLED_HANDLER = 0x84e667ee0d6c7cf1ULL,
  ALREADY_HAS_EVENT_MANAGER = 0x48a1364eead0dcd8ULL,
  NO_EVENT_MANAGER = 0x0f73eab2259b01c7ULL,
  INVALID_EM_PHASE = 0xb64875cbfb7f959fULL,
  NOTHING_TO_WAIT = 0x8a6d1daa94e8e1f7ULL,
  EVTTYPE_HAS_ALREADY_HANDLER = 0x92882e1945ffae24ULL,
  NOT_MY_HANDLER = 0x3086b63e9b8cf622ULL,
  TIMER_WANTS_ALARM_IN_PAST = 0xac11770d8c5ad0aaULL,
  FD_AIMED_MULTIPLE_TIMES = 0xb4eee43ef19b0bccULL,
  AIM_REQUIRES_BEFORE_SELECT = 0x75472c7f11b2d976ULL,
  INVALID_HANDLER_STATE = 0xa48d4dc76de12af6ULL,
  TRIGGER_REQUIRES_AFTER_SELECT = 0x89b108add5d965c0ULL,
  FD_UNAIMED_WITHOUT_AIM = 0x0e42037c76388febULL,
  INVALID_PACKED_SIGNAL = 0x564f6e5e006ebd09ULL,
  MISSING_SIGNAL_EVENT_FIELD = 0x7bea8530be635b57ULL,
  MASK_NEEDS_SIGNUM_FIELD = 0x6c43069d5910a0c9ULL,
  SIGNAL_BUFFER_ERROR = 0x51b4cc3b505a17b3ULL
};

class EventException : nsntl::ErrorCodeException {
  public:
    EventException(EventError errorCode);
};

};

#endif
