using namespace nsntl;
using namespace posxx;

class Tcp4Proxy : public SetEntry {
  private:
    Tcp4EventSocket rec;
    Tcp4EventSocket snd;

  public:
    
};

class Tcp4ProxyListener : Tcp4Listener {
  private:
    Set conns;

  public:
    Tcp4ProxyListener(Tcp4Addr addr) : Tcp4ProxyListener(addr) {
      // nothing now
    };
    ~Tcp4ProxyListener() {
      // delete all instances
    };

    void onConnect(Tcp4Socket socket) {
      
    };
};

int main(int argc, char** argv) {
  EventManager em;
  Tcp4EventListener l(em, Tcp4Addr(127,0,0,1,64738));
};
