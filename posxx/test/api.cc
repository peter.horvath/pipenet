#include <stdio.h>

#include <nsntl/types.h>
#include <nsntl/util.h>

#include <posxx/file.h>
#include <posxx/filesystem.h>
#include <posxx/ip4.h>
#include <posxx/unix.h>

using namespace posxx;
using namespace nsntl;

StreamFd fd1;

void test_uint128_t() {
  uint128_t x(1000000000000000000ULL);
  uint128_t y(1000000000000000000ULL);
  uint128_t z = x*y;
  string l(string(z.h) + " " + z.l + "\n");
  fd1 << l;
  l = string(z) + "\n";
  fd1 << l;
};

void test_tcp4() {
  Block l(string("*** Testing Tcp4\n"));
  fd1 << l;
  printf ("sizeof(Tcp4Socket)==%lu sizeof(Tcp4ListenerSocket)=%lu sizeof(Ip4Addr)=%lu\n", sizeof(Tcp4Socket), sizeof(Tcp4ListenerSocket), sizeof(Ip4Addr));

  Tcp4ListenerSocket ls;
  Ip4Addr addr = ls.getLocalAddr();

  l = Block(string("Listening address: ") + string(addr) + "\n");
  
  fd1 << l;

  ls.setBlocking(false);

  Tcp4Socket c;
  c.setBlocking(false);

  c.connect(addr);

  Tcp4Socket d = ls.accept();
  d.setBlocking(false);

  l = string("Connecting socket: ") + string(c.getLocalAddr()) + string(" - ") + string(c.getRemoteAddr()) + string("\n");
  fd1 << l;
  l = string("Accepted socket: ") + string(d.getLocalAddr()) + string(" - ") + string(d.getRemoteAddr()) + string("\n");
  fd1 << l;

  string msg = "Sent through the socket.";

  Block( (void*)msg.c_str(), Strlen(msg.c_str())+1 ) >> d;

  l = string("Message sent.\n");
  fd1 << l;

  DynBlock blk(PAGE_SIZE);
  blk << c;

  l = string("Got message: ") + (string)((char*)blk.getAddr()) + string("\n");
  fd1 << l;
};

void test_udp4() {
  Block l(string("*** Testing Udp4\n"));
  fd1 << l;

  printf ("sizeof(Udp4Socket)==%lu sizeof(Udp4Dgram)=%lu\n", sizeof(Udp4Socket), sizeof(Udp4Dgram));

  Udp4Socket s1;
  Ip4Addr addr1 = s1.getLocalAddr();
  l = string("Udp4 #1 opened at address: ") + string(addr1) + string("\n");
  fd1 << l;
  s1.setBlocking(false);

  Udp4Socket s2;
  Ip4Addr addr2 = s2.getLocalAddr();
  l = string("Udp4 #2 opened at address: ") + string(addr2) + string("\n");
  fd1 << l;

  s2.setBlocking(false);

  s1.connect(addr2);

  nsntl::string msg = "Sent through the socket.";

  Block( (void*)msg.c_str(), Strlen(msg.c_str())+1 ) >> s1;

  l = string("Message sent.\n");
  fd1 << l;

  DynBlock blk(PAGE_SIZE);
  blk << s2;

  l = string("Got message: ") + blk + string("\n");
  l >> fd1;
};

void test_unixstream() {
  Block l(string("*** Testing Unix Stream\n"));
  fd1 << l;

  printf ("sizeof(UnixStreamSocket)==%lu sizeof(UnixListenerSocket)=%lu sizeof(UnixAddr)=%lu\n", sizeof(UnixStreamSocket), sizeof(UnixListenerSocket), sizeof(UnixAddr));

  if (Exists("test_l"))
    Delete("test_l");

  UnixListenerSocket li("test_l");
  UnixAddr addr = li.getLocalAddr();

  l = string("Listening address: ") + string(addr) + string("\n");
  fd1 << l;
  
  li.setBlocking(false);

  UnixStreamSocket c;

  c.setBlocking(false);
  if (Exists("test_c"))
    Delete("test_c");
  c.bind(UnixAddr("test_c"));

  c.connect(addr);

  UnixStreamSocket d = li.accept();
  d.setBlocking(false);

  l = string("Connecting socket: ") + string(c.getLocalAddr()) + string(" - ") + string(c.getRemoteAddr()) + string("\n");
  fd1 << l;

  l = string("Accepted socket: ") + string(d.getLocalAddr()) + string(" - ") + string(d.getRemoteAddr()) + string("\n");
  fd1 << l;

  string msg = "Sent through the socket.";

  Block( (void*)msg.c_str(), Strlen(msg.c_str())+1 ) >> d;

  l = string("Message sent.\n");
  fd1 << l;

  DynBlock blk(PAGE_SIZE);
  blk << c;

  l = string("Got message: ") + blk + string("\n");
  fd1 << l;

  Delete("test_l");
  Delete("test_c");
};

void test_unixdgram() {
  Block l(string("*** Testing Unix Dgram\n"));
  fd1 << l;

  printf ("sizeof(UnixDgramSocket)==%lu sizeof(UnixDgram)=%lu\n", sizeof(UnixDgramSocket), sizeof(UnixDgram));

  if (Exists("test_s1"))
    Delete("test_s1");

  UnixDgramSocket s1;
  s1.bind(UnixAddr("test_s1"));
  UnixAddr addr1 = s1.getLocalAddr();

  l = string("Udp4 #1 opened at address: ") + string(addr1) + string("\n");
  fd1 << l;

  s1.setBlocking(false);

  if (Exists("test_s2"))
    Delete("test_s2");
  UnixDgramSocket s2;
  s2.bind(UnixAddr("test_s2"));
  UnixAddr addr2 = s2.getLocalAddr();

  l = string("Udp4 #2 opened at address: ") + string(addr2) + string("\n");
  fd1 << l;

  s2.setBlocking(false);

  s1.connect(addr2);

  string msg = "Sent through the socket.";

  Block( (void*)msg.c_str(), Strlen(msg.c_str())+1 ) >> s1;

  l = string("Message sent.\n");
  fd1 << l;

  DynBlock blk(PAGE_SIZE);
  blk << s2;

  l = string("Got message: ") + blk + string("\n");
  fd1 << l;

  Delete("test_s1");
  Delete("test_s2");
};

int main(int argc, char** argv) {
  fd1.setFd(1);

  test_uint128_t();

  test_tcp4();
  test_udp4();

  test_unixstream();
  test_unixdgram();
};
