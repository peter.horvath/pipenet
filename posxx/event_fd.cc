#include "event_exception.h"
#include "event_fd.h"
#include "event_manager.h"

using namespace posxx;
using namespace nsntl;

posxx::FdEventHandler::FdEventHandler(EventManager* em, FdEventProcessor* fd, FdEventType evtType) : EventHandler(em) {
  this->evtType = evtType;
  this->fd = fd;
  if (evtType == EVT_EXCEPT)
    setPrio(true);
  fd->attachHandler(this);
};

posxx::FdEventHandler::~FdEventHandler() {
  fd->detachHandler(this);
};

FdEventType posxx::FdEventHandler::getType() const {
  return evtType;
};

void posxx::FdEventHandler::aim() {
  if (FD_ISSET(fd->getFd(), &em->fds[evtType]))
    throw EventException(EventError::FD_AIMED_MULTIPLE_TIMES);

  FD_SET(fd->getFd(), &em->fds[evtType]);
  em->fdSetCount[evtType]++;
  em->maxfd = nsntl::max(fd->getFd(), em->maxfd);
};

void posxx::FdEventHandler::unaim() {
  if (!FD_ISSET(fd->getFd(), &em->fds[evtType]))
    throw EventException(EventError::FD_UNAIMED_WITHOUT_AIM);

  FD_CLR(fd->getFd(), &em->fds[evtType]);
  em->fdSetCount[evtType]--;

  // we have no easy way to fix maxfd after the removal, but it should not cause any problem.
};

void posxx::FdEventHandler::trigger() {
  if (FD_ISSET(fd->getFd(), &getEventManager()->fds[evtType]))
    handler();
};

void posxx::FdEventHandler::handler() {
  if (evtType == EVT_READ)
    fd->onReadEvt();
  else if (evtType == EVT_WRITE)
    fd->onWriteEvt();
  else if (evtType == EVT_EXCEPT)
    fd->onExceptEvt();
  else
    throw LogicException(LogicError::UNREACHABLE_CODE);
};

posxx::FdEventProcessor::FdEventProcessor() {
  nsntl::Bzero(&(handlers[0]), sizeof(FdEventHandler*) * EVT_MAX);
};

posxx::FdEventProcessor::~FdEventProcessor() {
  for (int i = 0; i < EVT_MAX; i++) {
    if (handlers[i]) {
      delete handlers[i];
      handlers[i] = NULL;
    }
  };
};

FdEventHandler* FdEventProcessor::getHandler(FdEventType type) const {
  if (type < 0 || type >= EVT_MAX)
    throw LogicException(LogicError::INVALID_FLAGS);
  return handlers[type];
};

void posxx::FdEventProcessor::attachHandler(FdEventHandler* h) {
  if (handlers[h->getType()])
    throw EventException(EventError::EVTTYPE_HAS_ALREADY_HANDLER);
  handlers[h->getType()] = h;
};

void posxx::FdEventProcessor::detachHandler(FdEventHandler* h) {
  if (handlers[h->getType()] != h)
    throw EventException(EventError::NOT_MY_HANDLER);
  handlers[h->getType()] = NULL;
};

