#include "event_manager.h"
#include "event_timer.h"

using namespace posxx;

posxx::TimerEventHandler::TimerEventHandler(EventManager* em) : EventHandler(em), Timer() {
  // nothing now
};

posxx::TimerEventHandler::~TimerEventHandler() {
  // nothing now
};

void posxx::TimerEventHandler::aim() {
  Timer::silentSetNow(em->now);
  em->timeout = nsntl::min(em->timeout, getTimeLeft());
};

void posxx::TimerEventHandler::unaim() {
  // nothing - we have no easy & quick way to find the nearest TimerEventHandler after removal.
  // So we might cause an early, eventless pselect() return, but it should be harmless.
};

void posxx::TimerEventHandler::trigger() {
  Timer::setNow(em->now);
};

void posxx::TimerEventHandler::alarm(nsntl::uint128_t now) {
  handler();
};

