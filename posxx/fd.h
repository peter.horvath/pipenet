#ifndef Posxx_Fd_h
#define Posxx_Fd_h

// TODO:
//   - remove DynBlocks

#include <netinet/in.h>
#include <sys/un.h>

#include <nsntl/util.h>

#include "buffer.h"
#include "exception.h"

//#include "globalconst.h"

/**
  * C++ wrapper class around the unix file descriptor (fd).
  */

namespace posxx {

enum class FdError : uint64_t {
  BLOCK_TOO_SMALL = 0x09e75c28f43c4f0dULL,
  FD_DOES_NOT_EXIST = 0x2544e615cf63aa15ULL,
  FD_ALREADY_INITIALIZED = 0xf46c0570ebe0d96eULL,
  PACKET_TOO_BIG = 0xc295d4f16cc104d7ULL
};

class FdException : public posxx::Exception {
  public:
    FdException(FdError errorCode);
};

class Fd {
  private:
    //static std::map<int, Fd*> fdReg; // sometimes...

    int fd = -1;

  public:
    void setFd(int fd); // dirty!

    Fd();
    Fd(int fd);
    Fd(const Fd& fd);
    virtual ~Fd();

    int getFd() const;
    void close();
    int getFcntl();
    void setFcntl(int flags);
    bool isBlocking();
    void setBlocking(bool blocking);
};

class StreamFd : public virtual Fd, public DataSrc, public DataDst {
  public:
    StreamFd();
    StreamFd(int fd);

    virtual ~StreamFd() override;

    // this --> Block
    uint64_t read(nsntl::Block&& block);

    // this <-- Block
    uint64_t write(const nsntl::Block& block);
};

class DgramFd : public virtual Fd, public DataSrc, public DataDst {
  protected:
    uint64_t maxRecvSize;

    DgramFd();
    virtual ~DgramFd();

  public:
    DgramFd(int fd);

    uint64_t getMaxRecvSize() const;
    void setMaxRecvSize(uint64_t maxRecvSize = nsntl::PAGE_SIZE);
    // returns the size of the next packet available in the read buffer
    uint64_t peek() const;
    uint64_t recv(nsntl::Block& block);
    uint64_t send(const nsntl::Block& block);

    // this --> Block
    uint64_t read(nsntl::Block&& block);

    // this <-- Block
    uint64_t write(const nsntl::Block& block);
};

};
#endif
