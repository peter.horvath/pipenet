#include <nsntl/util.h>

#include "unix.h"

using namespace posxx;
using namespace nsntl;

/*
 * The Unix Socket World
 */
posxx::UnixSocketException::UnixSocketException(UnixSocketError errorCode) : posxx::Exception((uint64_t)errorCode) {
  // nothing now
};

sa_family_t posxx::BaseUnixAddr::getFamily() const {
  return AF_UNIX;
};

socklen_t posxx::BaseUnixAddr::getSockAddrLength() const {
  return sizeof(struct sockaddr_un);
};

posxx::UnixAddr::UnixAddr() {
  clear();
};

/*
posxx::UnixAddr::UnixAddr(std::nullptr_t ptr) : UnixAddr() {
  // nothing now
};
*/

posxx::UnixAddr::UnixAddr(const char* path) {
  clear();
  setPath(path);
};

posxx::UnixAddr::UnixAddr(const string path) {
  clear();
  setPath(path.c_str());
};

posxx::UnixAddr::UnixAddr(const sockaddr_un* addr) {
  if (addr->sun_family != AF_UNIX)
    throw SocketException(SocketError::INVALID_SOCKET_FAMILY);

  clear();
  setPath(addr->sun_path);
};

posxx::UnixAddr::UnixAddr(const sockaddr_un& addr) : UnixAddr(&addr) {
  // nothing now
};

posxx::UnixAddr::UnixAddr(const UnixAddr& addr) : UnixAddr((const sockaddr_un*)&addr) {
  // nothing now
};

posxx::UnixAddr::UnixAddr(const SockAddr& addr) : UnixAddr(dynamic_cast<const UnixAddr&>(addr)) {
  // nothing now
};

const struct sockaddr* posxx::UnixAddr::getAddr() const {
  return (struct sockaddr*)((sockaddr_un*)this);
};

void posxx::UnixAddr::setAddr(const struct sockaddr_un& addr) {
  setPath(addr.sun_path);
};

void posxx::UnixAddr::setAddr(const struct sockaddr* addr) {
  setPath(((sockaddr_un*)addr)->sun_path);
};

nsntl::string posxx::UnixAddr::getPath() const {
  return nsntl::string(&(sun_path[0]));
};

void posxx::UnixAddr::setPath(const nsntl::string& path) {
  setPath(path.c_str());
};

void posxx::UnixAddr::setPath(const char* path) {
  unsigned int len = Strlen(path);
  if (len >= sizeof(sun_path)-1)
    throw UnixSocketException(UnixSocketError::UNIX_PATH_TOO_LONG);

  clear();
  Memcpy(sun_path, path, len + 1);
};

bool posxx::UnixAddr::isNull() const {
  return !sun_path[0];
};

void posxx::UnixAddr::clear() {
  sun_family = AF_UNIX;
  Bzero(sun_path, sizeof(sun_path));
};

bool posxx::UnixAddr::operator==(const char* path) const {
  return Strcmp(sun_path, path);
};

bool posxx::UnixAddr::operator==(const nsntl::string& path) const {
  return *this == path.c_str();
};

bool posxx::UnixAddr::operator==(const sockaddr_un& addr) const {
  return *this == addr.sun_path;
};

bool posxx::UnixAddr::operator==(const UnixAddr& addr) const {
  return *this == addr.sun_path;
};

UnixAddr& posxx::UnixAddr::operator=(const nsntl::string& path) {
  setPath(path.c_str());
  return *this;
};

UnixAddr& posxx::UnixAddr::operator=(const sockaddr_un& addr) {
  setPath(addr.sun_path);
  return *this;
};

UnixAddr& posxx::UnixAddr::operator=(const UnixAddr& addr) {
  setPath(addr.getPath());
  return *this;
};

posxx::UnixAddr::operator nsntl::string() const {
  char ret[sizeof(sun_path)+1];
  int l=Strlen(sun_path);
  Memcpy(ret, (void*)(sun_path), l+1);
  return nsntl::string(ret);
};

posxx::BaseUnixStreamSocket::BaseUnixStreamSocket() {
  setLocalAddr(&localAddr);
};

posxx::BaseUnixStreamSocket::~BaseUnixStreamSocket() {
  // nothing now
};

int posxx::BaseUnixStreamSocket::getProtocol() const {
  return 0;
};

void posxx::UnixStreamSocket::init() {
  setRemoteAddr(&remoteAddr);
};

posxx::UnixStreamSocket::UnixStreamSocket() {
  init();
};

posxx::UnixStreamSocket::UnixStreamSocket(int fd) : BaseConnectedSocket(fd) {
  init();
};

posxx::UnixStreamSocket::UnixStreamSocket(const UnixStreamSocket& uss) {
  Memcpy(this, &uss, sizeof(UnixStreamSocket));
};

posxx::UnixStreamSocket::UnixStreamSocket(const UnixAddr& remoteAddr) {
  init();
  bind();
  connect(remoteAddr);
};

posxx::UnixStreamSocket::UnixStreamSocket(const UnixAddr& localAddr, const UnixAddr& remoteAddr) : BaseConnectedSocket(localAddr, remoteAddr) {
  init();
  if (!localAddr.isNull())
    bind(localAddr);

  if (!remoteAddr.isNull())
    connect(remoteAddr);
};

posxx::UnixStreamSocket::~UnixStreamSocket() {
  // nothing now
};

void posxx::UnixDgram::init(const UnixAddr& srcAddr, const UnixAddr& dstAddr) {
  Dgram::srcAddr = &(this->srcAddr);
  if (!srcAddr.isNull())
    setSrcAddr(srcAddr);

  Dgram::dstAddr = &(this->dstAddr);
  if (!dstAddr.isNull())
    setDstAddr(dstAddr);
};

posxx::UnixDgram::UnixDgram(uint64_t size) : Dgram(size) {
  // nothing now
};

posxx::UnixDgram::UnixDgram(const UnixAddr& srcAddr, const UnixAddr& dstAddr, uint64_t size) : Dgram(size) {
  init(srcAddr, dstAddr);
};

posxx::UnixDgram::UnixDgram(Block& data, const UnixAddr& srcAddr, const UnixAddr& dstAddr) : Dgram(data) {
  init(srcAddr, dstAddr);
};

posxx::UnixDgram::~UnixDgram() {
  // nothing now
};

void posxx::UnixDgramSocket::init() {
  setLocalAddr(&localAddr);
  setRemoteAddr(&remoteAddr);
};

posxx::UnixDgramSocket::UnixDgramSocket() {
  init();
};

posxx::UnixDgramSocket::UnixDgramSocket(int fd) : BaseDgramSocket(fd) {
  init();
};

posxx::UnixDgramSocket::UnixDgramSocket(const UnixAddr& remoteAddr) {
  init();
  if (!remoteAddr.isNull()) {
    connect(remoteAddr);
  }
};

posxx::UnixDgramSocket::UnixDgramSocket(const UnixAddr& localAddr, const UnixAddr& remoteAddr) {
  init();
  if (!localAddr.isNull())
    bind(localAddr);

  if (!remoteAddr.isNull())
    connect(remoteAddr);
};

posxx::UnixDgramSocket::~UnixDgramSocket() {
  // nothing now
};

int posxx::UnixDgramSocket::getProtocol() const {
  return 0;
};

posxx::UnixListenerSocket::UnixListenerSocket(const UnixListenerSocket& socket) {
  throw LogicException(LogicError::COPY_ELISION_ENFORCED);
};

posxx::UnixListenerSocket::UnixListenerSocket(int maxconn) : BaseUnixStreamSocket(), BaseListenerSocket(maxconn) {
  bind();
  BaseListenerSocket::init(maxconn);
};

posxx::UnixListenerSocket::UnixListenerSocket(const UnixAddr& addr, int maxconn) : BaseUnixStreamSocket() {
  bind(addr);
  BaseListenerSocket::init(maxconn);
};

posxx::UnixListenerSocket::~UnixListenerSocket() {
  // nothing now
};

posxx::UnixStreamSocket UnixListenerSocket::accept() {
  UnixStreamSocket ret;
  BaseListenerSocket::accept(ret);
  return ret;
};
