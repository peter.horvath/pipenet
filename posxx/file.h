#ifndef Posxx_File_h
#define Posxx_File_h

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "fd.h"

namespace posxx {

/**
  * A file in the filesystem
  */
class FileFd : public virtual StreamFd {
  private:
    nsntl::string path;

  public:
    FileFd();
    FileFd(const char* path, bool readable=true, bool writable=true, bool append=true);
    FileFd(const nsntl::string path, bool readable=true, bool writable=true, bool append=true);
    FileFd(const char* path, int flags, int mode=-1);
    FileFd(const nsntl::string path, int flags, int mode=-1);
    ~FileFd();

    void open(const char* path, bool readable=true, bool writable=true, bool append=true);
    void open(const nsntl::string path, bool readable=true, bool writable=true, bool append=true);
    void open(const char* path, int flags, int mode=-1);
    void open(const nsntl::string path, int flags, int mode=-1);

    uint64_t getSize() const;
    void setSize(uint64_t pos);

    uint64_t getPos() const;
    void setPos(int64_t pos, int whence = SEEK_SET);
};

};

#endif
