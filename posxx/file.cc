#include <errno.h>

#include "file.h"

using namespace posxx;
using namespace nsntl;

posxx::FileFd::FileFd(const char* path, int flags, int mode) {
  open(path, flags, mode);
};

posxx::FileFd::FileFd(const nsntl::string path, int flags, int mode) {
  open(path, flags, mode);
};

posxx::FileFd::FileFd(const char* path, bool readable, bool writable, bool append) {
  open(path, readable, writable, append);
};

posxx::FileFd::FileFd(const nsntl::string path, bool readable, bool writable, bool append) {
  open(path, readable, writable, append);
}

void posxx::FileFd::open(const char* path, bool readable, bool writable, bool append) {
  this->path = path;

  int flags;
  if (readable && writable)
    flags = O_RDWR;
  else if (readable && !writable)
    flags = O_RDONLY;
  else if (!readable && writable)
    flags = O_WRONLY;
  else
    throw LogicException(LogicError::UNREACHABLE_CODE);

  if (append)
    flags |= O_APPEND;

  open(path, flags);
};

void posxx::FileFd::open(const nsntl::string path, bool readable, bool writable, bool append) {
  open(path.c_str(), readable, writable, append);
};

void posxx::FileFd::open(const char* path, int flags, int mode) {
  int r;
  if (mode == -1)
    r = ::open(path, flags);
  else
    r = ::open(path, flags, mode);
  if (r == -1)
    throw posxx::Exception(errno);
  setFd(r);
};

void posxx::FileFd::open(const nsntl::string path, int flags, int mode) {
  open(path.c_str(), flags, mode);
};

uint64_t posxx::FileFd::getSize() const {
  struct stat st;
  if (::fstat(this->getFd(), &st)==-1)
    throw posxx::Exception(errno);
  return st.st_size;
};

void posxx::FileFd::setSize(uint64_t size) {
  if (::ftruncate(getFd(), size)==-1)
    throw posxx::Exception(errno);
};

uint64_t posxx::FileFd::getPos() const {
  int64_t r = ::lseek(this->getFd(), 0, SEEK_CUR);
  if (r==-1)
    throw posxx::Exception(errno);
  return (uint64_t)r;
};

void posxx::FileFd::setPos(int64_t pos, int whence) {
  int64_t r = ::lseek(this->getFd(), pos, whence);
  if (r == -1)
    throw posxx::Exception(errno);
};
