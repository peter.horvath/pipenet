#ifndef posxx_event_handler_h
#define posxx_event_handler_h

#include <nsntl/list.h>

namespace posxx {

class EventManager;

class EventHandler : public nsntl::ListEntry {
  friend class EventManager;

  private:
    typedef enum {
      STATE_NEW, // means also newly enabled EventHandlers
      STATE_AIMED,
      STATE_TRIGGERED,
      STATE_DISABLED,
      STATE_WANTSDEL,
      STATE_MAX
    } State;

  protected:
    EventManager* em;
    bool prio;

    EventHandler(EventManager* em);
    virtual ~EventHandler();

  private:
    // does general part of aim-ing, and calls more specific aim()-s
    void genAim();
    void genUnaim();

    // general part of tic(), calls tic
    void genTrigger();

  protected:
    bool getPrio() const;
    void setPrio(bool prio);

    State getState() const;
    void setState(State state);

  protected:
    void deleteMe();

    // marks handler for deletion. Requires consistent state. No more callback will be called, only the destructor.

    // modifies the event manager to watch for it on its next loop
    virtual void aim()=0;
    virtual void unaim()=0;

    // checks if the event happened, and fires handler() if yes
    virtual void trigger()=0;

    // provided by the derived classes, it will be called on fire
    virtual void handler()=0;

  public:
    EventManager* getEventManager() const;
    void setEventManager(EventManager* em);

    void attach(EventManager* em);
    void detach();

    bool isEnabled() const;
    virtual void enable();
    virtual void disable();
};

};

#endif
