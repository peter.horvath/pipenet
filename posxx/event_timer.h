#ifndef posxx_event_timer_h
#define posxx_event_timer_h

#include "event_handler.h"
#include "timer.h"

namespace posxx {

class TimerEventHandler : public EventHandler, public Timer {
  friend class EventManager;

  public:
    TimerEventHandler(EventManager* em);
    virtual ~TimerEventHandler();

  private:
    void aim();
    void unaim();
    void trigger();
    void alarm(nsntl::uint128_t now);

  protected:
    virtual void handler()=0;
};

};

#endif
