#ifndef posxx_event_fd_h
#define posxx_event_fd_h

#include "event_handler.h"
#include "fd.h"

namespace posxx {

class FdEventProcessor;

typedef enum {
  EVT_READ,
  EVT_WRITE,
  EVT_EXCEPT,
  EVT_MAX
} FdEventType;

class FdEventHandler : public EventHandler {
  friend class EventManager;

  private:
    FdEventProcessor* fd;
    FdEventType evtType;

  public:
    FdEventHandler(EventManager* em, FdEventProcessor* fd, FdEventType type);
    virtual ~FdEventHandler();

    FdEventType getType() const;

  private:
    void aim();
    void unaim();
    void trigger();
    void handler();
};

class FdEventProcessor : public virtual Fd {
  friend FdEventHandler;

  private:
    FdEventHandler* handlers[EVT_MAX];

  public:
    FdEventProcessor();
    virtual ~FdEventProcessor();

    FdEventHandler* getHandler(FdEventType type) const;

    void attachHandler(FdEventHandler* h);
    void detachHandler(FdEventHandler* h);

    virtual void onReadEvt()=0;
    virtual void onWriteEvt()=0;
    virtual void onExceptEvt()=0;
};

};

#endif
