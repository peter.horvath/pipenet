#ifndef Posxx_Socketpair_h
#define Posxx_Socketpair_h

#include "socket.h"
#include "unix.h"

// TODO: maybe "SocketPairBuilder" would be a better class name

namespace posxx {

/**
  * Socket pairs
  */

enum class SocketPairError : uint64_t {
  INCONSISTENT_STATE = 0xbe1b1df94612da0aULL,
  ALREADY_OPEN = 0x3a5d4b1d3ec7ec4dULL,
  ALREADY_CLOSED = 0x03b825f7bf0393fdULL
};

class SocketPairException : public nsntl::ErrorCodeException {
  public:
    SocketPairException(SocketPairError errorCode);
};

class SocketPair : public DataSrc, public DataDst {
  protected:
    Fd *_fd1, *_fd2;

    SocketPair();
    virtual ~SocketPair();

    virtual void makePair(int& fd1, int& fd2) const = 0;
    virtual void buildFds(int fd1, int fd2) = 0;

  public:
    virtual Fd& fd1() const;
    virtual Fd& fd2() const;

    void open();
    void close();
    bool isOpened() const;
    virtual void extract(Fd*& fd1, Fd*& fd2);
};

class PipeSocketPair : public SocketPair {
  public:
    PipeSocketPair();
    virtual ~PipeSocketPair();

  protected:
    void makePair(int& fd1, int& fd2) const;
    void buildFds(int fd1, int fd2);

  public:
    virtual StreamFd& fd1() const;
    virtual StreamFd& fd2() const;
    virtual void extract(StreamFd*& fd1, StreamFd*& fd2);

    uint64_t read(nsntl::Block&& block);
    uint64_t write(const nsntl::Block& block);
};

class DgramSocketPair : public SocketPair {
  public:
    DgramSocketPair();
    virtual ~DgramSocketPair();

  protected:
    virtual void makePair(int& fd1, int& fd2) const;
    virtual void buildFds(int fd1, int fd2);

  public:
    virtual UnixDgramSocket& fd1() const;
    virtual UnixDgramSocket& fd2() const;
    virtual void extract(UnixDgramSocket*& fd1, UnixDgramSocket*& fd2);

    uint64_t read(nsntl::Block&& block);
    uint64_t write(const nsntl::Block& block);
};

class StreamSocketPair : public SocketPair {
  public:
    StreamSocketPair();
    virtual ~StreamSocketPair();

  protected:
    void makePair(int& fd1, int& fd2) const;
    void buildFds(int fd1, int fd2);

  public:
    virtual UnixStreamSocket& fd1() const;
    virtual UnixStreamSocket& fd2() const;
    virtual void extract(UnixStreamSocket*& fd1, UnixStreamSocket*& fd2);

    uint64_t read(nsntl::Block&& block);
    uint64_t write(const nsntl::Block& block);
};

};
#endif
