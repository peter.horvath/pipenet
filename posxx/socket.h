#ifndef Posxx_Socket_h
#define Posxx_Socket_h

#include <nsntl/block.h>
#include <nsntl/string.h>

#include "fd.h"

namespace posxx {

enum class SocketError : uint64_t {
  INVALID_SOCKADDR_LENGTH = 0x2c75a8dd98260126ULL,
  COULD_NOT_SENT_WHOLE_PACKET = 0x2863032b6a840b38ULL,
  SOCKET_IS_ALREADY_LISTENING = 0x51117aadb1de00dcULL,
  INVALID_SOCKET_FAMILY = 0xa12bfbfe969b8bedULL,
  INVALID_SOCKET_ADDR = 0x97e31718a459fe2eULL
};

class SocketException : public posxx::Exception {
  public:
    SocketException(SocketError errorCode);
};

class SockAddr {
  public:
    virtual sa_family_t getFamily() const =0;
    virtual socklen_t getSockAddrLength() const =0;
};

/**
  * C++ wrapper around a file descriptor describing a socket.
  */
class BaseSocket : public virtual Fd {
  private:
    SockAddr *localAddr;

  protected:
    BaseSocket();
    BaseSocket(int fd);
    BaseSocket(const BaseSocket&) = delete;
    BaseSocket(const SockAddr& localAddr);
    virtual ~BaseSocket();

    void mksock();
    virtual sa_family_t getFamily() const;
    virtual int getProtocol() const;
    virtual socklen_t getSockAddrLength() const;

  public:
    const SockAddr& getLocalAddr();
    void bind();
    void bind(const SockAddr& localAddr);

    void setRcvBufSize(uint64_t size);
    void setSndBufSize(uint64_t size);

    // wrapper around getsockopt(). Returns the size of the answer,
    // throws exception with errno on error.
    unsigned int getSockOpt(int level, int optname, nsntl::Block& block);

    // getsockopt() for integer values. Returns the answer
    int getSockOptInt(int level, int optname);

    void setSockOpt(int level, int optname, nsntl::Block& block);
    void setSockOptInt(int level, int optname, int value);
};

/** Describing a stream-oriented socket. They have also remote addresses.

While Posix can have also a remote address for datagram sockets, we don't use this functionality.
*/
class BaseStreamSocket : public virtual BaseSocket, public StreamFd {
  protected:
    SockAddr *remoteAddr;

    BaseStreamSocket();
    BaseStreamSocket(int fd);
    BaseStreamSocket(const BaseStreamSocket&) = delete;
    BaseStreamSocket(const SockAddr& localAddr);

    int getSockType() const;

  public:
    const SockAddr& getRemoteAddr() const;
    const SockAddr& getRemoteAddr();
    virtual void setRemoteAddr(SockAddr* remoteAddr);
    void connect(const SockAddr& remoteAddr);
};

class BaseDgramSocket;

class Dgram : public nsntl::Block {
  protected:
    Dgram(uint64_t size=0);
    Dgram(const SockAddr& dstAddr, uint64_t size = 0);
    Dgram(const SockAddr& srcAddr, const SockAddr& dstAddr, uint64_t size = 0);
    Dgram(const Block& data);
    Dgram(const Block& data, const SockAddr& dstAddr);
    Dgram(const Block& data, const SockAddr& srcAddr, const SockAddr& dstAddr);

  public:
    virtual const SockAddr& getSrcAddr() const = 0;
    virtual void setSrcAddr(const SockAddr& srcAddr) = 0;
    virtual const SockAddr& getDstAddr() const = 0;
    virtual void setDstAddr(const SockAddr& dstAddr) = 0;

    void sendTo(BaseDgramSocket& dstSocket);
    void sendTo(BaseDgramSocket& dstSocket, const SockAddr& dstAddr);
};

class BaseDgramSocket : public virtual BaseSocket, public DgramFd {
  protected:
    int getSockType() const;

    BaseDgramSocket();
    BaseDgramSocket(int fd);
    BaseDgramSocket(const BaseDgramSocket&) = delete;
    BaseDgramSocket(const SockAddr& localAddr);
    BaseDgramSocket(const SockAddr& localAddr, const SockAddr& remoteAddr);
    virtual ~BaseDgramSocket();

    uint64_t recvFrom(nsntl::Block& block, const struct sockaddr* srcAddr, socklen_t len, sa_family_t family);

  public:
    uint64_t recvFrom(nsntl::Block& block, const SockAddr& srcAddr);
    uint64_t recvFrom(nsntl::Block& block);
    void recvFrom(nsntl::DynBlock& block, SockAddr& srcAddr);
    void recvFrom(nsntl::DynBlock& block);
    void recvFrom(Dgram& dgram);

    void sendTo(const nsntl::Block& block, const SockAddr& dstAddr);
    void send(const Dgram& dgram);
};

class BaseListenerSocket : public BaseSocket {
  protected:
    BaseListenerSocket(const BaseListenerSocket&) = delete;
    BaseListenerSocket(const SockAddr& localAddr, int maxconn = 0);
    BaseListenerSocket(int maxconn = 0);
    virtual ~BaseListenerSocket();

  private:
    virtual int accept() = 0;
};

};
#endif
