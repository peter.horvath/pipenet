#ifndef posxx_event_signal_h
#define posxx_event_signal_h

#include <nsntl/list.h>
#include <nsntl/malloc.h>

#include "event_fd.h"
#include "event_handler.h"
#include "signal.h"
#include "socketpair.h"

namespace posxx {

class SignalEventProcessor;

class SignalEvent : public nsntl::ListEntry, public nsntl::MallocPooled {
  private:
    uint8_t mask;
    uint64_t id;
    nsntl::uint128_t timestamp;
    int signum;
    siginfo_t *siginfo;
    ucontext_t *ucontext;

  public:
    static const int maxSize = sizeof(uint8_t) + sizeof(uint64_t) + sizeof(nsntl::uint128_t)
      + sizeof(int) + sizeof(siginfo_t) + sizeof(ucontext_t);

    typedef enum {
      MASK = 0,
      ID = 1,
      TIMESTAMP = 2,
      SIGNUM = 4,
      SIGINFO = 8,
      UCONTEXT = 16
    } SignalEventField;

    SignalEvent(uint8_t* src, int len, SignalEventProcessor* processor);
    SignalEvent(DataSrc& dataSrc, SignalEventProcessor* processor);
    ~SignalEvent();

    // throws exception if mask is invalid
    void checkMask() const;

    uint8_t getMask() const;

    bool hasId() const;
    uint64_t getId() const;

    bool hasTimestamp() const;
    nsntl::uint128_t getTimestamp() const;

    bool hasSignum() const;
    int getSignum() const;

    bool hasSiginfo() const;
    const siginfo_t* getSiginfo() const;

    bool hasUcontext() const;
    const ucontext_t* getUcontext() const;

    // says the size of this signal event in packed form
    int size() const;

    // says the size of a signal event belonging to a mask
    static int size(uint8_t mask);

    // packs signal data together in a buffer. Signal-safe
    static uint64_t pack(uint8_t* dst, uint8_t mask, uint64_t id, nsntl::uint128_t timestamp,
      int signum, siginfo_t* siginfo, ucontext_t* ucontext);
};

class SignalEventHandler : public EventHandler {
  friend class EventManager;
  friend class SignalEventProcessor;

  private:
    nsntl::ItemList pendingEvents;
    uint8_t mask;
    SignalEvent* currentEvent;
    int signum;
    nsntl::ListItem signalEventProcessorItem;

  protected:
    SignalEventHandler(EventManager* em, int signum, uint8_t mask = SignalEvent::ID|SignalEvent::TIMESTAMP|SignalEvent::SIGNUM);
    virtual ~SignalEventHandler();

    uint8_t getMask() const;
    void setMask(uint8_t mask);
    const SignalEvent* getEvent() const;
    int getSignum() const;

  private:
    void aim();
    void unaim();
    void trigger();

    void enable();
    void disable();

    // empties the local pendingEvents
    void flushQueue();

    bool isAttachedToProcessor() const;

  protected:
    virtual void handler()=0;
};

class SignalEventProcessor;

// it is the receiver side of the signal sockets - the sender side is a raw fd, filled by the signal handlers (in signal context)
class SignalSocketReceiver : public UnixDgramSocket, public FdEventProcessor {
  private:
    SignalEventProcessor* signalEventProcessor;

  public:
    SignalSocketReceiver(int fd, SignalEventProcessor* signalEventProcessor);
    ~SignalSocketReceiver();

    void onReadEvt();
    void onWriteEvt();
    void onExceptEvt();
};

class SignalEventProcessor : protected SignalHandler, protected DgramSocketPair {
  friend class EventManager;
  friend class SignalSocketReceiver;
  friend class SignalEventHandler;
  friend class SignalEvent;

  private:
    EventManager* em;
    int (*masks)[5];
    nsntl::ItemList *signalHandlerLists;
    int sighandlerCount;
    nsntl::List pendingEvents;
    nsntl::MallocPool *eventListEntryPool, *signalEventPool, *siginfoPool, *ucontextPool;

    SignalEventProcessor(EventManager* eventManager);
    ~SignalEventProcessor() noexcept(false);

    void attachHandler(SignalEventHandler* eh);
    void detachHandler(SignalEventHandler* eh);

    void distributeSignal(SignalEvent* signalEvent);

    /** empties PendingEvents, is called from EventManager::cleanup() */
    void flushQueue();
    void changeMask(int signum, uint8_t oldMask, uint8_t newMask);
    void buildFds(int fd1, int fd2);

    void handler(uint64_t id, nsntl::uint128_t timestamp, int signum, siginfo_t* siginfo, ucontext_t* ucontext);
};

};

#endif
