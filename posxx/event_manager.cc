#include <errno.h>

#include <nsntl/exception.h>
#include <nsntl/util.h>

#include "event_exception.h"
#include "event_manager.h"

using namespace posxx;
using namespace nsntl;

posxx::EventManager::EventManager() : signalEventProcessor{this} {
  for (int i = 0; i < EventHandler::STATE_MAX; i++)
    eventHandlers[i].init();
  reset();
};

posxx::EventManager::~EventManager() {
  for (;;) {
    bool found = false;
    for (int i = 0; i < EventHandler::STATE_MAX; i++)
      if (!eventHandlers[i].isEmpty()) {
        found = true;
        EventHandler* eh = getFirstHandler(EventHandler::State(i));
        delete eh;
      }
    if (!found)
      break;
  }
};

SignalEventProcessor* posxx::EventManager::getSignalEventProcessor() const {
  return const_cast<SignalEventProcessor*>(&signalEventProcessor);
};

EventHandler* posxx::EventManager::getFirstHandler(EventHandler::State state) const {
  int i = int(state);
  if (i<0 || i>=EventHandler::STATE_MAX)
    throw EventException(EventError::INVALID_HANDLER_STATE);
  return dynamic_cast<EventHandler*>(eventHandlers[i].firstEntry());
};

void posxx::EventManager::setHandlerPrio(EventHandler* handler, bool prio) {
  if (handler->em != this)
    throw EventException(EventError::NOT_MY_HANDLER);
  handler->setPrio(prio);
};

void posxx::EventManager::cleanup() {
  while (!eventHandlers[EventHandler::STATE_WANTSDEL].isEmpty())
    delete getFirstHandler(EventHandler::STATE_WANTSDEL);
  signalEventProcessor.flushQueue();
};

void posxx::EventManager::reset() {
  for (int i=0; i<EVT_MAX; i++) {
    FD_ZERO(&(fds[i]));
    fdSetCount[i]=0;
  };
  now = Timer::now();
  maxfd = -1;
  timeout = ~uint128_t();
  phase = BEFORE_SELECT;
};

void posxx::EventManager::tic() {
  reset();
  
  for (;;) {
    EventHandler* eh = getFirstHandler(EventHandler::STATE_NEW);
    if (eh) {
      eh->genAim();
      continue;
    }
    eh = getFirstHandler(EventHandler::STATE_TRIGGERED);
    if (eh) {
      eh->genAim();
      continue;
    }
    break;
  }

  cleanup();

  // pselect waits, processes results
  struct timespec timeoutTimespec;
  struct timespec* timeoutTimespecP;
  if (timeout == ~uint128_t())
    timeoutTimespecP = NULL;
  else {
    timeoutTimespec = Timer::ts2timespec(timeout);
    timeoutTimespecP = &timeoutTimespec;
  }

  fd_set* fd_set_p[EVT_MAX];
  for (int i=0; i<EVT_MAX; i++)
    fd_set_p[i] = fdSetCount[i]?&(fds[i]):NULL;

  int r = pselect(maxfd+1, fd_set_p[EVT_READ], fd_set_p[EVT_WRITE], fd_set_p[EVT_EXCEPT], timeoutTimespecP, NULL);
  if ((r == -1) && (errno != EINTR))
    throw Exception(errno);

  phase = AFTER_SELECT;
  now = Timer::now();

  while (EventHandler* eh = getFirstHandler(EventHandler::STATE_AIMED))
    eh->genTrigger();

  cleanup();
};
