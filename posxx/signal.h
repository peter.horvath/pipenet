#ifndef Posxx_Signal_h
#define Posxx_Signal_h

// Simple and clear C++ wrapper over the Posix signal api. handler()s are called in signal context!
// For more sophisticated functionality (serialization + passing the events to the main context), use event.h .

#include <signal.h>

#include <nsntl/collection.h>

#include "exception.h"

namespace posxx {

enum class SignalError : uint64_t {
  SIGNAL_ALREADY_REGISTERED = 0x031b0e2fa8e08620ULL,
  SIGNAL_NOT_YET_REGISTERED = 0xff2ab8185f52c41cULL,
  INVALID_SIGNAL_NUMBER = 0x8026fe39596dc953ULL,
  GLOBALS_NOT_INITED = 0xf62d1101f211bf34ULL,
  UNKNOWN_SIGNAL = 0x4ef2bbc91a486ebdULL
};

class SignalException : public posxx::Exception {
  public:
    SignalException(SignalError errorCode);
};

class SignalHandler {
  private:
    nsntl::Uint64Set signums;

  public:
    // none of these can be called in signal context. Nearly nothing can be called in signal context
    SignalHandler();
    SignalHandler(int signum);
    virtual ~SignalHandler() noexcept(false);

    const nsntl::Uint64Set& getSignums() const; // readonly set of the signal numbers we are registered to
    void attach(int signum); // throws exception if signum is already allocated by us
    void detach(int signum); // throws exception if signum is not allocated by this handler
    void detach(); // detaches all signums
    // none of these can be called in signal context. Nearly nothing can be called in signal context

    virtual void handler(uint64_t id, nsntl::uint128_t timestamp, int signum, siginfo_t* siginfo, ucontext_t* ucontext) = 0;

    static int getMaxSignum();
};

};
#endif
