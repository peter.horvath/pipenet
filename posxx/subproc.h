#ifndef posxx_exec_h
#define posxx_exec_h

/**

Standard use case:

PipeSocketPair lsOutput, lsErr;

Subprocess
  .path("/bin/ls")
  .noenv()
  .setenv("LOCALE", "en_US.UTF-8")
  .pwd("/tmp")
  .arg0("ls")
  .arg("-la")
  .arg("/tmp")
  .nofd()
  .fd(1, lsOutput.fd2())
  .fd(2, lsError.fd2())
  .exec()
  .wait();

*/

namespace posxx {

class SubprocessResult {
  private:
    bool bySignal;
    int exitCode;

  public:
    SubprocessResult(bool byExit, int exitCode);
    SubprocessResult(int libcResult);

    bool isExitedBySignal() const;
    int getExitCode() const;
};

class Subprocess {
  private:
    pid_t pid;
    nsntl::string path;
    nsntl::set arg;
    nsntl::dict environ;
    nsntl::array fd;
    
    static nsntl::array subprocs;

  public:
    typedef enum {
      SUBPROC_NONE,
      SUBPROC_RUNNING,
      SUBPROC_EXITED
    } State;

    Subprocess(nsntl::string path, ...);
    ~Subprocess();

    int getPid() const;
    nsntl::string getPath() const;
    const nsntl::array getFds() const;
    const nsntl::array getFd() const;
    const nsntl::dict getEnv() const;
    const nsntl::string getEnv(nsntl::string env) const;
    State getState() const;

    void start();
    void kill();
    SubprocessResult wait();

  private:
    void cloneFrom(const Subprocess& subprocess);
};

class SubprocessBuilder {
  private:
    typedef enum {
      TASK_PATH,
      TASK_NOENV,
      TASK_SETENV,
      TASK_PWD,
      TASK_ARG0,
      TASK_ARG,
      TASK_NOFD,
      TASK_FD,
      TASK_EXEC,
      TASK_DAEMON
    } BuildTaskType;

    class BuildTask : public nsntl::ListEntry {
      public:
        BuildTaskType type,
        int intParam,
        nsntl::string strParam1,
        nsntl::string strParam2
    };

    nsntl::List tasks;

  public:
    SubprocessBuilder();
    ~SubprocessBuilder();

    SubprocessBuilder& path(nsntl::string path);
    SubprocessBuilder& pwd(nsntl::string pwd);
    SubprocessBuilder& arg0(nsntl::string arg0);
    SubprocessBuilder& arg(nsntl::string arg);
    SubprocessBuilder& noenv();
    SubprocessBuilder& setenv(nsntl::string name, nsntl::string value);
    SubprocessBuilder& nofd();
    SubprocessBuilder& fd(int fd, Fd* fd);

    Subprocess exec();
};

};

#endif
