#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>

#include <nsntl/util.h>

#include "exception.h"

using namespace posxx;
using namespace nsntl;

posxx::Exception::Exception(int posix_errno) : ErrorCodeException(posix_errno < 0 ? -posix_errno : posix_errno) {
  // nothing now
};

