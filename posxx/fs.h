#ifndef posxx_fs_h
#define posxx_fs_h

#include <nsntl/string.h>

namespace posxx {
  bool Exists(const nsntl::string path);
  void Delete(const nsntl::string path);
};

#endif
