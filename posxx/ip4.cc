#include <stdio.h>
#include <errno.h>

#include <nsntl/util.h>

#include "ip4.h"

using namespace posxx;
using namespace nsntl;

void posxx::Ip4::init(uint8_t a, uint8_t b, uint8_t c, uint8_t d) {
  ip=(((uint32_t)a)<<24) | (((uint32_t)b)<<16) | (((uint32_t)c)<<8) | ((uint32_t)d);
};

posxx::Ip4::Ip4() {
  this->ip=0;
};

posxx::Ip4::Ip4(uint8_t a, uint8_t b, uint8_t c, uint8_t d) {
  init(a,b,c,d);
};

posxx::Ip4::Ip4(uint32_t ip) {
  this->ip=ip;
};

posxx::Ip4::Ip4(int ip) : Ip4((uint32_t)ip) {
  // nothing now
};

posxx::Ip4::Ip4(const Ip4& ip) {
  this->ip=ip.ip;
};

posxx::Ip4::Ip4(const char* ip) {
  int a=-1, b=-1, c=-1, d=-1;
  sscanf(ip, "%i.%i.%i.%i", &a, &b, &c, &d);
  if (errno)
    throw posxx::Exception(errno);
  if ((a<0) || (a>255) || (b<0) || (b>255) || (c<0) || (c>255) || (d<0) || (d>255))
    throw SocketException(SocketError::INVALID_SOCKET_ADDR);
  init((uint8_t)a, (uint8_t)b, (uint8_t)c, (uint8_t)d);
};

posxx::Ip4::Ip4(const nsntl::string& ip) : Ip4(ip.c_str()) {
  // nothing now
};

uint32_t posxx::Ip4::getIp() const {
  return ip;
};

void posxx::Ip4::setIp(Ip4 ip) {
  this->ip=ip.ip;
};

bool posxx::Ip4::operator==(Ip4 ip) const {
  return this->ip == ip.ip;
};

Ip4& posxx::Ip4::operator=(const Ip4 ip) {
  setIp(ip);
  return *this;
};

bool posxx::Ip4::isNull() const {
  return !ip;
};

void posxx::Ip4::clear() {
  ip = 0;
};

posxx::Ip4::operator nsntl::string() const {
  char result[16];
  snprintf(result, 16, "%3u.%3u.%3u.%3u", (uint8_t)(ip>>24), (uint8_t)((ip>>16)&0xff),(uint8_t)((ip>>8)&0xff), (uint8_t)(ip&0xff));
  return nsntl::string(result);
};

posxx::IpPort::IpPort() {
  this->port=0;
};

posxx::IpPort::IpPort(uint16_t port) {
  this->port=port;
};

posxx::IpPort::IpPort(int port) {
  if ((port<0) || (port>65535))
    throw SocketException(SocketError::INVALID_SOCKET_ADDR);
  this->port=(uint16_t)port;
};

posxx::IpPort::IpPort(const IpPort& port) {
  this->port=port.port;
};

posxx::IpPort::IpPort(const char* port) {
  int p = -1;
  sscanf(port, "%i", &p);
  if (p == -1)
    sscanf(port, ":%i", &p);
  if (p == -1)
    throw SocketException(SocketError::INVALID_SOCKET_ADDR);
  this->port = p;
};

posxx::IpPort::IpPort(const nsntl::string& port) : IpPort(port.c_str()) {
  // nothing now
};

uint16_t posxx::IpPort::getPort() const {
  return this->port;
};

void posxx::IpPort::setPort(uint16_t port) {
  this->port=port;
};

void posxx::IpPort::setPort(IpPort port) {
  this->port=port.port;
};

bool posxx::IpPort::operator==(uint16_t port) const {
  return this->port == port;
};

bool posxx::IpPort::operator==(IpPort port) const {
  return this->port == port.port;
};

IpPort& posxx::IpPort::operator=(uint16_t port) {
  setPort(port);
  return *this;
};

IpPort& posxx::IpPort::operator=(IpPort port) {
  setPort(port.port);
  return *this;
};

bool posxx::IpPort::isNull() const {
  return !port;
};

void posxx::IpPort::clear() {
  port = 0;
};

posxx::IpPort::operator nsntl::string() const {
  char result[7];
  snprintf(result, 6, "%i", getPort());
  return nsntl::string(result);
};

sa_family_t posxx::BaseIp4Addr::getFamily() const {
  return AF_INET;
}

socklen_t posxx::BaseIp4Addr::getSockAddrLength() const {
  return sizeof(struct sockaddr_in);
};

posxx::Ip4Addr::Ip4Addr()  {
  sin_family = getFamily();
  setIp(0);
  setPort(0);
};

posxx::Ip4Addr::Ip4Addr(Ip4 ip) : Ip4Addr() {
  setIp(ip);
};

posxx::Ip4Addr::Ip4Addr(Ip4 ip, IpPort port) : Ip4Addr() {
  setIp(ip);
  setPort(port);
};

posxx::Ip4Addr::Ip4Addr(Ip4 ip, uint16_t port) : Ip4Addr(ip, IpPort(port)) {
  // nothing now
};

posxx::Ip4Addr::Ip4Addr(IpPort port) : Ip4Addr(Ip4(0), port) {
  // nothing now
};

posxx::Ip4Addr::Ip4Addr(const Ip4Addr& ip4Addr) {
  setAddr(ip4Addr);
};

posxx::Ip4Addr::Ip4Addr(const SockAddr& sockAddr) : Ip4Addr(dynamic_cast<const Ip4Addr&>(sockAddr)) {
  // nothing now
};

posxx::Ip4Addr::Ip4Addr(const struct sockaddr_in* addr) {
  setAddr((const struct sockaddr*)addr);
};

posxx::Ip4Addr::Ip4Addr(const struct sockaddr* addr) {
  if (addr->sa_family != AF_INET)
    throw SocketException(SocketError::INVALID_SOCKET_ADDR);
  setAddr((const struct sockaddr_in*)addr);
};

posxx::Ip4Addr::Ip4Addr(const struct sockaddr_in& addr) {
  setAddr(&addr);
};

posxx::Ip4Addr::Ip4Addr(const char* addr) {
  if (!addr || !*addr) { // empty string
    clear();
    return;
  }
  
  if (Strlen(addr)>=22)
    throw SocketException(SocketError::INVALID_SOCKET_ADDR);

  if (*addr == ':') { // format ":80"
    setAddr(Ip4(0), IpPort(addr+1));
  } else if (!strchr(addr, ':')) { // format a.b.c.d
    setAddr(Ip4(0), IpPort(addr));
  } else { // format a.b.c.d:port
    char buf[23];
    strcpy(buf, addr);
    char* delim = strchr(buf, ':');
    *delim = 0;
    setAddr(buf, delim+1);
  }
};

posxx::Ip4Addr::Ip4Addr(const nsntl::string& addr) : Ip4Addr(addr.c_str()) {
  // nothing now
};

const struct sockaddr* posxx::Ip4Addr::getAddr() const {
  return (struct sockaddr*)((sockaddr_in*)this);
};

void posxx::Ip4Addr::setAddr(const Ip4 ip, const IpPort port) {
  clear();
  setIp(ip);
  setPort(port);
};

void posxx::Ip4Addr::setAddr(const struct sockaddr_in& addr) {
  if (addr.sin_family != AF_INET)
    throw SocketException(SocketError::INVALID_SOCKET_FAMILY);

  sin_family = addr.sin_family;
  sin_addr.s_addr = addr.sin_addr.s_addr;
  sin_port = addr.sin_port;
};

void posxx::Ip4Addr::setAddr(const struct sockaddr* addr) {
  setAddr(*((struct sockaddr_in*)addr));
};

void posxx::Ip4Addr::setAddr(const Ip4Addr& ip4Addr) {
  setAddr(ip4Addr.getAddr());
};

Ip4 posxx::Ip4Addr::getIp() const {
  return Ip4(ntohl(sin_addr.s_addr));
};

void posxx::Ip4Addr::setIp(Ip4 ip) {
  sin_addr.s_addr = ntohl(ip.getIp());
};

IpPort posxx::Ip4Addr::getPort() const {
  return IpPort(ntohs(sin_port));
};

void posxx::Ip4Addr::setPort(IpPort port) {
  sin_port = ntohs(port.getPort());
};

bool posxx::Ip4Addr::isNull() const {
  return getIp() == 0 && getPort() == 0;
};

void posxx::Ip4Addr::clear() {
  setIp(0);
  setPort(0);
};

bool posxx::Ip4Addr::operator==(const Ip4Addr& addr) const {
  return getIp() == addr.getIp() && getPort() == addr.getPort();
};

Ip4Addr& posxx::Ip4Addr::operator=(const Ip4Addr& addr) {
  setIp(addr.getIp());
  setPort(addr.getPort());
  return *this;
};

Ip4Addr::operator nsntl::string() const {
  return nsntl::string(getIp()) + nsntl::string(":") + nsntl::string(getPort());
};

posxx::BaseTcp4Socket::BaseTcp4Socket() {
  setLocalAddr(&localAddr);
};

posxx::BaseTcp4Socket::~BaseTcp4Socket() {
  // nothing now
};

int posxx::BaseTcp4Socket::getProtocol() const {
  return IPPROTO_TCP;
};

void posxx::Tcp4Socket::init() {
  setRemoteAddr(&remoteAddr);
};

posxx::Tcp4Socket::Tcp4Socket() : BaseConnectedSocket() {
  init();
};

posxx::Tcp4Socket::Tcp4Socket(const Tcp4Socket& socket) {
  throw LogicException(LogicError::COPY_ELISION_ENFORCED);
};

posxx::Tcp4Socket::Tcp4Socket(int fd) : BaseConnectedSocket(fd) {
  init();
};

posxx::Tcp4Socket::Tcp4Socket(const Ip4Addr& remoteAddr) : BaseConnectedSocket(remoteAddr) {
  init();
};

posxx::Tcp4Socket::Tcp4Socket(const Ip4Addr& localAddr, const Ip4Addr& remoteAddr) : BaseConnectedSocket(localAddr, remoteAddr) {
  init();
};

posxx::Tcp4Socket::~Tcp4Socket() {
  // nothing now
};

void posxx::Udp4Dgram::init() {
  Dgram::srcAddr = &srcAddr;
  Dgram::dstAddr = &dstAddr;
};

posxx::Udp4Dgram::Udp4Dgram(uint64_t size) : Dgram(size) {
  init();
};

posxx::Udp4Dgram::Udp4Dgram(const Ip4Addr& dstAddr, uint64_t size) : Udp4Dgram(size) {
  init();
  setDstAddr(dstAddr);
};

posxx::Udp4Dgram::Udp4Dgram(const Ip4Addr& srcAddr, const Ip4Addr& dstAddr, uint64_t size) : Udp4Dgram(dstAddr, size) {
  setSrcAddr(srcAddr);
};

posxx::Udp4Dgram::Udp4Dgram(const Block& data) : Dgram(data) {
  init();
};

posxx::Udp4Dgram::Udp4Dgram(const Block& data, const Ip4Addr& dstAddr) : Udp4Dgram(data) {
  setDstAddr(dstAddr);
};

posxx::Udp4Dgram::Udp4Dgram(const Block& data, const Ip4Addr& srcAddr, const Ip4Addr& dstAddr) : Udp4Dgram(data, dstAddr) {
  setSrcAddr(srcAddr);
};

posxx::Udp4Dgram::~Udp4Dgram() {
  // nothing now
};

void posxx::Udp4Socket::init() {
  setLocalAddr(&localAddr);
  setRemoteAddr(&remoteAddr);
};

posxx::Udp4Socket::Udp4Socket() : BaseDgramSocket() {
  init();
  bind();
};

posxx::Udp4Socket::Udp4Socket(int fd) : BaseDgramSocket(fd) {
  init();
  bind();
};

posxx::Udp4Socket::Udp4Socket(const Ip4Addr& remoteAddr) : BaseDgramSocket() {
  init();
  connect(localAddr);
};

posxx::Udp4Socket::Udp4Socket(const Ip4Addr& localAddr, const Ip4Addr& remoteAddr) {
  init();
  bind(localAddr);
  connect(remoteAddr);
};

posxx::Udp4Socket::~Udp4Socket() {
  // nothing now
};

int posxx::Udp4Socket::getProtocol() const {
  return IPPROTO_UDP;
};

posxx::Tcp4ListenerSocket::Tcp4ListenerSocket(const Tcp4ListenerSocket& socket) {
  throw LogicException(LogicError::COPY_ELISION_ENFORCED);
};

posxx::Tcp4ListenerSocket::Tcp4ListenerSocket(int maxconn) : BaseTcp4Socket(), BaseListenerSocket(maxconn) {
  BaseListenerSocket::init(maxconn);
};

posxx::Tcp4ListenerSocket::Tcp4ListenerSocket(const Ip4Addr& addr, int maxconn) : BaseTcp4Socket() {
  bind(addr);
  BaseListenerSocket::init(maxconn);
};

posxx::Tcp4ListenerSocket::Tcp4ListenerSocket(IpPort port, int maxconn) : Tcp4ListenerSocket(Ip4Addr(port), maxconn) {
  // nothing now
};

posxx::Tcp4ListenerSocket::Tcp4ListenerSocket(uint16_t port, int maxconn) : Tcp4ListenerSocket(Ip4Addr(IpPort(port)), maxconn) {
  // nothing now
};

posxx::Tcp4ListenerSocket::~Tcp4ListenerSocket() {
  // nothing now
};

Tcp4Socket posxx::Tcp4ListenerSocket::accept() {
  Tcp4Socket ret;
  BaseListenerSocket::accept(ret);
  return ret;
};
