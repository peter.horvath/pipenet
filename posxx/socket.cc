#include <errno.h>

#include "socket.h"

using namespace posxx;
using namespace nsntl;

posxx::SocketException::SocketException(SocketError errorCode) : posxx::Exception((uint64_t)errorCode) {
  // nothing now
};

posxx::BaseSocket::BaseSocket() : Fd() {
  // nothing now
};

posxx::BaseSocket::BaseSocket(int fd) : Fd(fd) {
  // nothing now
};

posxx::BaseSocket::BaseSocket(const SockAddr& localAddr) : Fd() {
  bind(localAddr);
};

posxx::BaseSocket::~BaseSocket() {
  // nothing now
};

void posxx::BaseSocket::mksock() {
  if (getFd() != -1)
    return;

  int fd = ::socket(getFamily(), getSockType(), getProtocol());
  if (fd == -1)
    throw posxx::Exception(errno);

  setFd(fd);
};

void posxx::BaseSocket::setLocalAddr(SockAddr* const localAddr) {
  this->localAddr = localAddr;
};

const SockAddr& posxx::BaseSocket::getLocalAddr() {
  mksock();

  if (localAddr->isNull()) {
    socklen_t len = getSockAddrLength();
    const sockaddr* addr = localAddr->getAddr();

    if (::getsockname(getFd(), const_cast<sockaddr*>(addr), &len) == -1)
      throw posxx::Exception(errno);

  // unix sockets hate this, they are variable sized......
  /*
    if (len != getSockAddrLength())
      throw SocketException(SocketError::INVALID_SOCKADDR_LENGTH);
  */

    if (addr->sa_family != localAddr->getFamily())
      throw SocketException(SocketError::INVALID_SOCKET_FAMILY);
  };

  return *localAddr;
};

void posxx::BaseSocket::bind() {
  localAddr->clear();
  bind(*localAddr);
};

void posxx::BaseSocket::bind(const SockAddr& localAddr) {
  mksock();

  if (::bind(getFd(), localAddr.getAddr(), getSockAddrLength()) == -1)
    throw posxx::Exception(errno);
};

void posxx::BaseSocket::setRcvBufSize(uint64_t size) {
  setSockOptInt(SOL_SOCKET, SO_RCVBUF, size);
};

void posxx::BaseSocket::setSndBufSize(uint64_t size) {
  setSockOptInt(SOL_SOCKET, SO_SNDBUF, size);
};

unsigned int posxx::BaseSocket::getSockOpt(int level, int optname, Block& block) {
  mksock();

  unsigned int len = block.getSize();
  if (::getsockopt(getFd(), level, optname, block.getAddr(), &len) == -1)
    throw posxx::Exception(errno);
  return len;
};

void posxx::BaseSocket::setSockOpt(int level, int optname, Block& block) {
  mksock();

  if (::setsockopt(getFd(), level, optname, block.getAddr(), block.getSize()) == -1)
    throw posxx::Exception(errno);
};

int posxx::BaseSocket::getSockOptInt(int level, int optname) {
  int ret;
  Block block(&ret, sizeof(int));
  getSockOpt(level, optname, block);
  return ret;
};

void posxx::BaseSocket::setSockOptInt(int level, int optname, int value) {
  Block block(&value, sizeof(int));
  setSockOpt(level, optname, block);
};

posxx::BaseStreamSocket::BaseStreamSocket() : BaseSocket() {
  // nothing now
};

posxx::BaseStreamSocket::BaseStreamSocket(int fd) : BaseSocket(fd) {
  // nothing now
};

posxx::BaseStreamSocket::BaseStreamSocket(const SockAddr& localAddr) : BaseSocket(localAddr) {
  // nothing now
};

int posxx::BaseStreamSocket::getSockType() const {
  return SOCK_STREAM;
};

posxx::BaseConnectableSocket::BaseConnectableSocket() : BaseSocket() {
  // nothing now
};

posxx::BaseConnectableSocket::BaseConnectableSocket(int fd): BaseSocket(fd) {
  // nothing now
};

posxx::BaseConnectableSocket::BaseConnectableSocket(const SockAddr& remoteAddr) : BaseSocket() {
  connect(remoteAddr);
};

posxx::BaseConnectableSocket::BaseConnectableSocket(const SockAddr& localAddr, const SockAddr& remoteAddr) : BaseSocket(localAddr) {
  connect(remoteAddr);
};

posxx::BaseConnectableSocket::~BaseConnectableSocket() {
  // nothing now
};

void posxx::BaseConnectableSocket::setRemoteAddr(SockAddr* remoteAddr) {
  this->remoteAddr = remoteAddr;
};

const SockAddr& posxx::BaseConnectableSocket::getRemoteAddr() const {
  return *remoteAddr;
};

const SockAddr& posxx::BaseConnectableSocket::getRemoteAddr() {
  mksock();

  if (remoteAddr->isNull()) {
    const sockaddr* addr = remoteAddr->getAddr();
    socklen_t len = getSockAddrLength();

    if (::getpeername(getFd(), const_cast<sockaddr*>(addr), &len) == -1)
      throw posxx::Exception(errno);

    // unix sockets hate this, they are variable sized......
    /*
    if (len != getSockAddrLength())
      throw SocketException(SocketError::INVALID_SOCKADDR_LENGTH);
    */

    if (addr->sa_family != getFamily())
      throw SocketException(SocketError::INVALID_SOCKET_FAMILY);
  }
  return const_cast<const BaseConnectableSocket*>(this)->getRemoteAddr();
};

void posxx::BaseConnectableSocket::connect(const SockAddr& remoteAddr) {
  mksock();

  if (::connect(getFd(), remoteAddr.getAddr(), getSockAddrLength()) == -1)
    throw posxx::Exception(errno);
};

posxx::BaseConnectedSocket::BaseConnectedSocket() : BaseSocket() {
  // nothing now
};

posxx::BaseConnectedSocket::BaseConnectedSocket(int fd) : BaseSocket(fd) {
  // nothing now
};

posxx::BaseConnectedSocket::BaseConnectedSocket(const BaseConnectedSocket& sck) {
  throw LogicException(LogicError::UNREACHABLE_CODE);
};

posxx::BaseConnectedSocket::BaseConnectedSocket(const SockAddr& remoteAddr) : BaseConnectableSocket(remoteAddr) {
  // nothing now
};

posxx::BaseConnectedSocket::BaseConnectedSocket(const SockAddr& localAddr, const SockAddr& remoteAddr) : BaseConnectableSocket(localAddr, remoteAddr) {
  // nothing now
};

posxx::BaseConnectedSocket::~BaseConnectedSocket() {
  // nothing now
};

posxx::Dgram::Dgram(uint64_t size) : DynBlock(size) {
  // nothing now
};

posxx::Dgram::Dgram(const SockAddr& dstAddr, uint64_t size) : DynBlock(size) {
  setDstAddr(dstAddr);
};

posxx::Dgram::Dgram(const SockAddr& srcAddr, const SockAddr& dstAddr, uint64_t size) : DynBlock(size) {
  setSrcAddr(srcAddr);
  setDstAddr(dstAddr);
};

posxx::Dgram::Dgram(const Block& data) : DynBlock(data) {
  // nothing now
};

posxx::Dgram::Dgram(const Block& data, const SockAddr& dstAddr) : DynBlock(data) {
  setDstAddr(dstAddr);
};

posxx::Dgram::Dgram(const Block& data, const SockAddr& srcAddr, const SockAddr& dstAddr) : DynBlock(data) {
  setSrcAddr(srcAddr);
  setDstAddr(dstAddr);
};

const SockAddr& posxx::Dgram::getSrcAddr() const {
  return *srcAddr;
};

void posxx::Dgram::setSrcAddr(const SockAddr& srcAddr) {
  *(this->srcAddr) = srcAddr;
};

const SockAddr& posxx::Dgram::getDstAddr() const {
  return *dstAddr;
};

void posxx::Dgram::setDstAddr(const SockAddr& dstAddr) {
  *(this->dstAddr) = dstAddr;
};

void posxx::Dgram::sendTo(BaseDgramSocket& dstSocket) {
  dstSocket.send(*this);
};

void posxx::Dgram::sendTo(BaseDgramSocket& dstSocket, const SockAddr& dstAddr) {
  setDstAddr(dstAddr);
  dstSocket.send(*this);
};

int posxx::BaseDgramSocket::getSockType() const {
  return SOCK_DGRAM;
};

posxx::BaseDgramSocket::BaseDgramSocket() : BaseConnectableSocket() {
  // nothing now
};

posxx::BaseDgramSocket::BaseDgramSocket(int fd) : BaseConnectableSocket(fd) {
  // nothing now
};

posxx::BaseDgramSocket::BaseDgramSocket(const SockAddr& localAddr) : BaseConnectableSocket(localAddr) {
  // nothing now
};

posxx::BaseDgramSocket::BaseDgramSocket(const SockAddr& localAddr, const SockAddr& remoteAddr) : BaseConnectableSocket(localAddr, remoteAddr) {
  // nothing now
};

posxx::BaseDgramSocket::~BaseDgramSocket() {
  // nothing now
};

uint64_t posxx::BaseDgramSocket::recvFrom(Block& block, const struct sockaddr* addr, socklen_t len, sa_family_t family) {
  socklen_t sockLen = len;

  int64_t bytesGot = ::recvfrom(getFd(), block.getAddr(), block.getSize(), 0, const_cast<sockaddr*>(addr), &sockLen);

  if (bytesGot == -1)
    throw posxx::Exception(errno);

  if (len != sockLen)
    throw SocketException(SocketError::INVALID_SOCKADDR_LENGTH);

  if (addr->sa_family != family)
    throw SocketException(SocketError::INVALID_SOCKET_FAMILY);

  return (uint64_t)bytesGot;
};

uint64_t posxx::BaseDgramSocket::recvFrom(Block& block, const SockAddr& sockAddr) {
  return recvFrom(block, sockAddr.getAddr(), getSockAddrLength(), getFamily());
};

uint64_t posxx::BaseDgramSocket::recvFrom(Block& block) {
  return recvFrom(block, NULL, getSockAddrLength(), getFamily());
};

void posxx::BaseDgramSocket::recvFrom(DynBlock& block, SockAddr& srcAddr) {
  int bytesGot = recvFrom(static_cast<Block&>(block), srcAddr);
  block.setSize(bytesGot);
};

void posxx::BaseDgramSocket::recvFrom(DynBlock& block) {
  int bytesGot = recvFrom(static_cast<Block&>(block));
  block.setSize(bytesGot);
};

void posxx::BaseDgramSocket::recvFrom(Dgram& dgram) {
  recvFrom(static_cast<Block&>(dgram), const_cast<SockAddr&>(dgram.getSrcAddr()));
};

void posxx::BaseDgramSocket::sendTo(const Block& block, const SockAddr& dstAddr) {
  int bytesSent = ::sendto(getFd(), block.getAddr(), block.getSize(), 0, dstAddr.getAddr(), getSockAddrLength());

  if (bytesSent == -1)
    throw posxx::Exception(errno);

  if ((unsigned int)bytesSent != block.getSize())
    throw SocketException(SocketError::COULD_NOT_SENT_WHOLE_PACKET);
};

void posxx::BaseDgramSocket::send(const Dgram& dgram) {
  sendTo(dgram, dgram.getDstAddr());
};

void posxx::BaseListenerSocket::init(int maxconn) {
  listen(maxconn);
};

posxx::BaseListenerSocket::BaseListenerSocket(const BaseListenerSocket& socket) {
  throw LogicException(LogicError::COPY_ELISION_ENFORCED);
};

posxx::BaseListenerSocket::BaseListenerSocket(const SockAddr& localAddr, int maxconn) : BaseStreamSocket(localAddr) {
  // nothing now
};

posxx::BaseListenerSocket::BaseListenerSocket(int maxconn) : BaseStreamSocket() {
  // nothing now
};

posxx::BaseListenerSocket::~BaseListenerSocket() {
  // nothing now
};

void posxx::BaseListenerSocket::listen(int maxconn) {
  if (!maxconn)
    maxconn = SOMAXCONN;

  mksock();

  if (getSockOptInt(SOL_SOCKET,SO_ACCEPTCONN))
    throw SocketException(SocketError::SOCKET_IS_ALREADY_LISTENING);

  if (::listen(getFd(), maxconn) == -1)
    throw posxx::Exception(errno);

  setSockOptInt(SOL_SOCKET, SO_REUSEADDR, 1);

  // maybe some SO_LINGER as well?
};

void posxx::BaseListenerSocket::accept(BaseConnectedSocket& socket) {
  const struct sockaddr* addr = const_cast<const BaseConnectedSocket&>(socket).getRemoteAddr().getAddr();
  unsigned int len = socket.getSockAddrLength();
  int fd = ::accept(getFd(), const_cast<sockaddr*>(addr), &len);

  if (fd == -1)
    throw posxx::Exception(errno);

  // unix sockets hate this, they are variable sized......
  /*
  if (len != socket.getSockAddrLength())
    throw SocketException(SocketError::INVALID_SOCKADDR_LENGTH);
  */

  if (addr->sa_family != socket.getFamily())
    throw SocketException(SocketError::INVALID_SOCKET_FAMILY);

  socket.setFd(fd);
};
