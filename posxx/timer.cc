#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#include <nsntl/util.h>

#include "timer.h"

using namespace posxx;
using namespace nsntl;

posxx::TimerException::TimerException(TimerError errorCode) : posxx::Exception((uint64_t)errorCode) {
  // nothing now
};
 
uint128_t posxx::Timer::now() {
  struct timespec tp;
  if (clock_gettime(CLOCK_REALTIME, &tp) == -1)
    throw posxx::Exception(errno);
  return timespec2ts(tp);
};

uint128_t posxx::Timer::timespec2ts(const struct timespec& tp) {
  return uint128_t((uint64_t)tp.tv_sec)*uint64_t(1000000000) + uint128_t((uint64_t)tp.tv_nsec);
};

struct timespec posxx::Timer::ts2timespec(const uint128_t& ts) {
  struct timespec tp;
  uint128_t sec, nsec;

  if (ts == ~uint128_t())
    throw TimerException(TimerError::INVALID_TIMESTAMP);

  uint128_t::divmod(ts, uint128_t(1000000000), sec, nsec);
  if (sec>uint128_t((uint64_t)0xffffffff))
    throw TimerException(TimerError::LINUS_WE_WANT_64BIT_TIMESPEC);
  tp.tv_sec = sec.l;
  tp.tv_nsec = nsec.l;
  return tp;
};

posxx::Timer::Timer(uint128_t now) {
  this->_now = now;
  this->lastAlarm = 0;
  this->recurInterval = 0;
  this->counter = 0;
};

posxx::Timer::~Timer() noexcept(false) {
  // nothing now
};

uint128_t posxx::Timer::getNow() const {
  return this->_now;
};

void posxx::Timer::setNow(uint128_t now) {
  if (!_now || !lastAlarm) {
    _now = now;
    lastAlarm = now;
    return;
  }
  if (!recurInterval || recurInterval == ~uint128_t()) {
    _now = now;
    lastAlarm = now;
    return;
  }
  for (;;) {
    if (!counter)
      break;
    uint128_t nextAlarm = lastAlarm + recurInterval;
    if (nextAlarm <= now) {
      _now = nextAlarm;
      alarm(now);
      lastAlarm = nextAlarm;
      if (counter && counter != ~uint128_t())
        counter--;
    } else {
      break;
    }
  }
  _now = now;
};

void posxx::Timer::silentSetNow(uint128_t now) {
  _now = now;
};

uint128_t posxx::Timer::getRecurInterval() const {
  return recurInterval;
};

void posxx::Timer::setRecurInterval(const uint128_t recurInterval) {
  this->recurInterval = recurInterval;
  lastAlarm = _now;
};

uint128_t posxx::Timer::getLastAlarm() const {
  return lastAlarm;
};

uint128_t posxx::Timer::getNextAlarm() const {
  if (!_now || !lastAlarm || !recurInterval || recurInterval == ~uint128_t() || !counter)
    return ~uint128_t();
  return lastAlarm + recurInterval;
};

void posxx::Timer::setNextAlarm(const uint128_t ts) {
  if (!_now)
    throw TimerException(TimerError::TIMER_NOT_INITED);
  if (ts < _now)
    throw TimerException(TimerError::ALARM_SET_TO_PAST);
  if (!counter)
    counter = 1;
  if (!recurInterval || recurInterval == ~uint128_t()) {
    recurInterval = ts - _now;
    lastAlarm = _now;
  } else {
    lastAlarm = ts - recurInterval;
  }
  counter = 1;
};

uint128_t posxx::Timer::getTimeLeft() const {
  uint128_t nxt = getNextAlarm();
  if (nxt == ~uint128_t())
    return ~uint128_t();

  if (nxt < _now)
    return uint128_t(0);

  return nxt - _now;
};

void posxx::Timer::setTimeLeft(uint128_t ns) {
  setNextAlarm(_now + ns);
};

uint128_t posxx::Timer::getCounter() const {
  return counter;
};

void posxx::Timer::setCounter(uint128_t counter) {
  this->counter = counter;
  if (counter)
    lastAlarm = _now;
};

void posxx::Timer::enable() {
  if (!_now || !recurInterval)
    throw TimerException(TimerError::TIMER_NOT_INITED);
  lastAlarm = _now;
  if (!counter)
    counter = 1;
};

void posxx::Timer::disable() {
  counter = 0;
};

bool posxx::Timer::isEnabled() const {
  return getNextAlarm() < ~uint128_t();
};
