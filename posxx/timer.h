#ifndef Posxx_timer_h
#define Posxx_timer_h

#include "exception.h"

namespace posxx {

/**
  * Exceptions from the Posxx. Uses the same uint64_t-based errorCodes.
  */
enum class TimerError : uint64_t {
  RECUR_WITHOUT_INTERVAL = 0x69c871eef1272705ULL,
  NO_ALARM_PLANNED = 0x495cd88486b29a4cULL,
  NOW_CANT_DECREASE = 0xd7409995ffa2638eULL,
  INVALID_TIMESTAMP = 0xd5fb7e70d0c1ae07ULL,
  LINUS_WE_WANT_64BIT_TIMESPEC = 0x4f0bda5817157b16ULL,
  TIMER_NOT_INITED = 0x72014cb1eefa7731ULL,
  ALARM_SET_TO_PAST = 0x77a58d7256aab2aaULL
};

class TimerException : public posxx::Exception {
  public:
    TimerException(TimerError errorCode);
};

// absolute times are in nanoseconds since the unix epoch
// relative times are in nanoseconds
// all time is in nsntl::uint128_t
class Timer {
  public:
    // static method, measures nsecs since unix epoch by the kernel api
    static nsntl::uint128_t now();
    static nsntl::uint128_t timespec2ts(const struct timespec& tp);
    static struct timespec ts2timespec(const nsntl::uint128_t& ts);

  private:
    // timer thinks the time is now
    nsntl::uint128_t _now;

    // ts of the last firing. Might be faked - we calculate from this the timestamp of the next alarm
    nsntl::uint128_t lastAlarm;

    // microseconds between recurrences. If 0 or !0, we are disabled.
    nsntl::uint128_t recurInterval;

    // how many times do we want to fire. If ~0ULLL, it means infinite times, otherways it is decreased. Reaching 0, the timer disable()s itself.
    nsntl::uint128_t counter;

  public:
    Timer(nsntl::uint128_t now = nsntl::uint128_t(0));
    virtual ~Timer();

    // getters-setters of the private fields
    nsntl::uint128_t getNow() const;

    // instructs Timer that the time is now "ts". Results fired alarms. Time can decrease only in disabled timers, otherways we get Exception.
    void setNow(const nsntl::uint128_t ts);

    // changes _now, but does not fire. Note, we might have a burst if we abuse it
    void silentSetNow(const nsntl::uint128_t ts);

    nsntl::uint128_t getRecurInterval() const;

    // if zero, Timer disables itself after the first firing. If ~0ULLL, timer disables itself.
    // Chainging this resets lastAlarm to _now without firing.
    void setRecurInterval(const nsntl::uint128_t iv);

    // when has the last alarm happened
    nsntl::uint128_t getLastAlarm() const;

    // when will happen the next alarm
    nsntl::uint128_t getNextAlarm() const;

    // enables timer, sets next alarm to ts. Sets also counter to 1.
    void setNextAlarm(const nsntl::uint128_t ts);

    // return usecs left before the next alarm - results ~0ULLL if there is none. Returns zero if next alarm should have happened in the past.
    nsntl::uint128_t getTimeLeft() const;
    
    // wrapper to setNextAlarm _now + ns
    void setTimeLeft(nsntl::uint128_t ns);

    // shows the alarms left
    nsntl::uint128_t getCounter() const;

    // if counter is not 0, it also enables the Timer. If 0, it disables the Timer.
    void setCounter(nsntl::uint128_t counter);

    // if counter is not 0, sets to 1
    void enable();

    // disables Timer by setting counter to 0
    void disable();

    // Timer is enabled iff it will ever fire
    bool isEnabled() const;
    
    virtual void alarm(nsntl::uint128_t now)=0;
};

};

#endif
