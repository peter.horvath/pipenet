#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "exception.h"
#include "fs.h"

using namespace posxx;
using namespace nsntl;

bool posxx::Exists(const nsntl::string path) {
  struct stat stat;
  int ret = ::stat(path.c_str(), &stat);
  if (ret == 0)
    return true;
  if (ret == -1) {
    if (errno == ENOENT)
      return false;
    else
      throw posxx::Exception(errno);
  };
  throw LogicException(LogicError::UNREACHABLE_CODE);
};

void posxx::Delete(const nsntl::string path) {
  if (::unlink(path.c_str()) == -1)
    throw posxx::Exception(errno);
};
