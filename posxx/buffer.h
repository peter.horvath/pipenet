#ifndef posxx_buffer_h
#define posxx_buffer_h

#include <unistd.h>

#include <nsntl/block.h>
#include <nsntl/exception.h>
#include <nsntl/string.h>
#include <nsntl/types.h>

namespace posxx {

enum class BufferError : uint64_t {
  BUFFER_TOO_SMALL = 0x491c8b1e5f3e512eULL,
  BUFFER_CANT_BE_RESIZED_BELOW_USAGE = 0x02758a4ae423dadbULL,
  TARGET_BLOCK_TOO_SMALL = 0xc6193e26231bf37eULL,
  CAN_NOT_READ = 0x532e08f323a49c0fULL,
  READ_INCOMPLETE = 0x5235a7ceef31d274ULL,
  WRITE_INCOMPLETE = 0xe4879c46085b5e5fULL
};

class BufferException : public nsntl::ErrorCodeException {
  public:
    BufferException(BufferError error);
};

class DataDst;

/** A class from which we can read bytes. */
class DataSrc {
  public:
    virtual ~DataSrc();

    // this --> Block
    virtual uint64_t read(nsntl::Block&& block) = 0;

    // this --> Block
    uint64_t read(nsntl::Block& block);

    // this --> DataDst
    uint64_t read(DataDst& dst, uint64_t len = ~0ULL);
};

/** A class in which can write bytes. */
class DataDst {
  public:
    virtual ~DataDst();

    // this <-- Block
    virtual uint64_t write(const nsntl::Block& block) = 0;

    // this <-- DataSrc
    uint64_t write(DataSrc& src, uint64_t len = ~0ULL);
};

/** A circular buffer implementation over a Block. */
class Buffer : private virtual nsntl::Block, public DataSrc, public DataDst {
  private:
    uint64_t pos;
    uint64_t used;

  public:
    Buffer(void* addr, uint64_t size);
    Buffer(const nsntl::Block& block);
    ~Buffer();

    uint64_t getPos() const;
    void setPos(uint64_t pos);

    uint64_t getUsed() const;
    uint64_t getFree() const;
    
    // makes the buffer a new one (internal position (pos) and used data (used) will be 0)
    void reset();

    // this --> Block
    virtual uint64_t read(nsntl::Block&& block);

    // this <-- Block
    virtual uint64_t write(const nsntl::Block& block);

    // since Buffer is both a Block and a Data{Src,Dst}, we need to make these operators non-ambiguous
    uint64_t operator<<(posxx::DataSrc& dataSrc);
    Buffer& operator<<=(posxx::DataSrc& dataSrc);
    uint64_t operator>>(posxx::DataDst& dataDst);
    Buffer& operator>>=(posxx::DataDst& dataDst);
};

/** A dynamically allocated circular buffer. Usable for byte storage. */
class DynBuffer : protected nsntl::DynBlock, protected Buffer {
  private:
    uint64_t pos;
    uint64_t used;

  public:
    DynBuffer(uint64_t size);

    //uint64_t getSize() const; // same as inherited
    void setSize(uint64_t size=0);

    /** If the data is non-contiguous in the buffer, it makes that continuous */
    void defrag();
};

/** It is a /dev/null. Read results always 0, written data is lost. */
class Sink : public DataSrc, public DataDst {
  public:
    // this --> Block
    uint64_t read(nsntl::Block&& block);

    // this <-- Block
    uint64_t write(const nsntl::Block& block);
};

};

/*
uint64_t operator>>(posxx::Block& src, posxx::Block& dst);
uint64_t operator<<(posxx::Block& dst, posxx::Block& src);
*/
uint64_t operator>>(const nsntl::Block& src, posxx::DataDst& dataDst);
uint64_t operator<<(nsntl::Block& dst, posxx::DataSrc& dataSrc);

uint64_t operator>>(posxx::DataSrc& dataSrc, nsntl::Block& dst);
posxx::DataSrc& operator>>=(posxx::DataSrc& dataSrc, nsntl::Block& dst);
uint64_t operator<<(posxx::DataDst& dataDst, const nsntl::Block& src);
posxx::DataDst& operator<<=(posxx::DataDst& dataDst, const nsntl::Block& src);

uint64_t operator>>(posxx::DataSrc& dataSrc, posxx::DataDst& dataDst);
posxx::DataSrc& operator>>=(posxx::DataSrc& dataSrc, posxx::DataDst& dataDst);
uint64_t operator<<(posxx::DataDst& dataDst, posxx::DataSrc& dataSrc);
posxx::DataDst& operator<<=(posxx::DataDst& dataDst, posxx::DataSrc& dataSrc);

//uint64_t operator<<(posxx::DataDst& dataDst, nsntl::string s);

#endif
