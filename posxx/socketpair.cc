#include <errno.h>

#include "socketpair.h"

using namespace posxx;
using namespace nsntl;

posxx::SocketPairException::SocketPairException(SocketPairError errorCode) : ErrorCodeException((uint64_t)errorCode) {
  // nothing now
};

posxx::SocketPair::SocketPair() {
  _fd1 = 0;
  _fd2 = 0;
};

posxx::SocketPair::~SocketPair() {
  if (_fd1)
    delete _fd1;
  if (_fd2)
    delete _fd2;
};

Fd& posxx::SocketPair::fd1() const {
  return *_fd1;
};

Fd& posxx::SocketPair::fd2() const {
  return *_fd2;
};

void posxx::SocketPair::open() {
  if (_fd1 && _fd2)
    throw SocketPairException(SocketPairError::ALREADY_OPEN);
  if (_fd1 || _fd2)
    throw SocketPairException(SocketPairError::INCONSISTENT_STATE);
  int __fd1, __fd2;
  makePair(__fd1, __fd2);
  buildFds(__fd1, __fd2);
};

void posxx::SocketPair::close() {
  if (!_fd1 && !_fd2)
    throw SocketPairException(SocketPairError::ALREADY_CLOSED);
  if (!_fd1 || !_fd2)
    throw SocketPairException(SocketPairError::INCONSISTENT_STATE);
  delete _fd1;
  delete _fd2;
  _fd1 = 0;
  _fd2 = 0;
};

bool posxx::SocketPair::isOpened() const {
  if (_fd1 && _fd2)
    return true;
  else if (!_fd1 && !_fd2)
    return false;
  else
    throw SocketPairException(SocketPairError::INCONSISTENT_STATE);
};

void posxx::SocketPair::extract(Fd*& fd1, Fd*& fd2) {
  fd1 = _fd1;
  this->_fd1 = 0;
  fd2 = _fd2;
  this->_fd2 = 0;
};

posxx::PipeSocketPair::PipeSocketPair() : SocketPair() {
  // nothing now
};

posxx::PipeSocketPair::~PipeSocketPair() {
  // nothing now
};

// currently we can consider pipes as sockets, but maybe it won't be ever so
void posxx::PipeSocketPair::makePair(int& fd1, int& fd2) const {
  int _fds[2];
  if (::pipe2(_fds, 0) == -1)
    throw posxx::Exception(errno);
  fd1 = _fds[0];
  fd2 = _fds[1];
};

void posxx::PipeSocketPair::buildFds(int fd1, int fd2) {
  _fd1 = new StreamFd(fd1);
  _fd2 = new StreamFd(fd2);
};

StreamFd& posxx::PipeSocketPair::fd1() const {
  return *dynamic_cast<StreamFd*>(_fd1);
};

StreamFd& posxx::PipeSocketPair::fd2() const {
  return *dynamic_cast<StreamFd*>(_fd2);
};

void posxx::PipeSocketPair::extract(StreamFd*& fd1, StreamFd*& fd2) {
  Fd *__fd1, *__fd2;
  SocketPair::extract(__fd1, __fd2);
  fd1 = dynamic_cast<StreamFd*>(__fd1);
  fd2 = dynamic_cast<StreamFd*>(__fd2);
};

uint64_t posxx::PipeSocketPair::read(Block&& block) {
  return dynamic_cast<StreamFd*>(_fd2)->read(static_cast<Block&&>(block));
};

uint64_t posxx::PipeSocketPair::write(const Block& block) {
  return dynamic_cast<StreamFd*>(_fd1)->write(block);
};

posxx::DgramSocketPair::DgramSocketPair() : SocketPair() {
  // nothing now
};

posxx::DgramSocketPair::~DgramSocketPair() {
  // nothing now
};

void posxx::DgramSocketPair::makePair(int& fd1, int& fd2) const {
  int _fds[2];
  if (::socketpair(AF_UNIX, SOCK_DGRAM, 0, _fds) == -1)
    throw posxx::Exception(errno);
  fd1 = _fds[0];
  fd2 = _fds[1];
};

void posxx::DgramSocketPair::buildFds(int fd1, int fd2) {
  _fd1 = new UnixDgramSocket(fd1);
  _fd2 = new UnixDgramSocket(fd2);
};

UnixDgramSocket& posxx::DgramSocketPair::fd1() const {
  return dynamic_cast<UnixDgramSocket&>(*_fd1);
};

UnixDgramSocket& posxx::DgramSocketPair::fd2() const {
  return dynamic_cast<UnixDgramSocket&>(*_fd2);
};

void posxx::DgramSocketPair::extract(UnixDgramSocket*& fd1, UnixDgramSocket*& fd2) {
  Fd *__fd1, *__fd2;
  SocketPair::extract(__fd1, __fd2);
  fd1 = dynamic_cast<UnixDgramSocket*>(__fd1);
  fd2 = dynamic_cast<UnixDgramSocket*>(__fd2);
};

uint64_t posxx::DgramSocketPair::read(Block&& block) {
  return dynamic_cast<DgramFd*>(_fd2)->read(static_cast<Block&&>(block));
};

uint64_t posxx::DgramSocketPair::write(const Block& block) {
  return dynamic_cast<DgramFd*>(_fd1)->write(block);
};

posxx::StreamSocketPair::StreamSocketPair() : SocketPair() {
  // nothing now
};

posxx::StreamSocketPair::~StreamSocketPair() {
  // nothing now
};

void posxx::StreamSocketPair::makePair(int& fd1, int& fd2) const {
  int _fds[2];
  if (::socketpair(AF_UNIX, SOCK_STREAM, 0, _fds) == -1)
    throw posxx::Exception(errno);
  fd1 = _fds[0];
  fd2 = _fds[1];
};

void posxx::StreamSocketPair::buildFds(int fd1, int fd2) {
  _fd1 = new UnixStreamSocket(fd1);
  _fd2 = new UnixStreamSocket(fd2);
};

UnixStreamSocket& posxx::StreamSocketPair::fd1() const {
  return dynamic_cast<UnixStreamSocket&>(*_fd1);
};

UnixStreamSocket& posxx::StreamSocketPair::fd2() const {
  return dynamic_cast<UnixStreamSocket&>(*_fd2);
};

void posxx::StreamSocketPair::extract(UnixStreamSocket*& fd1, UnixStreamSocket*& fd2) {
  Fd *__fd1, *__fd2;
  SocketPair::extract(__fd1, __fd2);
  fd1 = dynamic_cast<UnixStreamSocket*>(__fd1);
  fd2 = dynamic_cast<UnixStreamSocket*>(__fd2);
};

uint64_t posxx::StreamSocketPair::read(Block&& block) {
  return dynamic_cast<StreamFd*>(_fd2)->read(static_cast<Block&&>(block));
};

uint64_t posxx::StreamSocketPair::write(const Block& block) {
  return dynamic_cast<StreamFd*>(_fd1)->write(block);
};
