#include <nsntl/malloc.h>
#include <nsntl/util.h>

#include "buffer.h"

using namespace posxx;
using namespace nsntl;

posxx::BufferException::BufferException(BufferError error) : ErrorCodeException((uint64_t)error) {
  // nothing now
};

posxx::DataSrc::~DataSrc() {
  // nothing now
};

uint64_t posxx::DataSrc::read(Block& block) {
  return this->read(static_cast<Block&&>(block));
};

uint64_t posxx::DataSrc::read(DataDst& dataDst, uint64_t len) {
  uint8_t buf[len];
  Block block(buf, len);
  int len_read = this->read(block);
  if (len_read < 0) {
      throw BufferException(BufferError::CAN_NOT_READ);
  } else if (len_read > 0) {
    block.setSize(len_read);
    int len_write = dataDst.write(block);
    if (len_write < len_read)
      throw BufferException(BufferError::WRITE_INCOMPLETE);
  }
  return len_read;
};

posxx::DataDst::~DataDst() {
  // nothing now
};

uint64_t posxx::DataDst::write(DataSrc& src, uint64_t len) {
  return src.read(*this, len);
};

posxx::Buffer::Buffer(void* addr, uint64_t size) : Block(addr, size) {
  pos = 0;
  used = 0;
};

posxx::Buffer::Buffer(const Block& block) : Buffer(block.getAddr(), block.getSize()) {
  // nothing now
};

posxx::Buffer::~Buffer() {
  // nothing now
};

uint64_t posxx::Buffer::getPos() const {
  return pos;
};

void posxx::Buffer::setPos(uint64_t pos) {
  this->pos = pos;
};

uint64_t posxx::Buffer::getUsed() const {
  return used;
};

uint64_t posxx::Buffer::getFree() const {
  return size - used;
};

void posxx::Buffer::reset() {
  pos = 0;
  used = 0;
};

// this --> Block
uint64_t posxx::Buffer::read(Block&& block) {
  uint64_t dataToMove = nsntl::min(getUsed(), block.getSize());

  if (pos + block.getSize() > size) {
    // overflow, two-step copy
    Memcpy( block.getAddr(), (uint8_t*)addr + pos, size - pos );
    Memcpy( (uint8_t*)block.getAddr() + size - pos, addr, dataToMove - (size - pos) );
  } else {
    // no overflow, single-step copy
    Memcpy( block.getAddr(), (uint8_t*)addr + pos, dataToMove);
  }

  pos += dataToMove;
  if (pos >= size)
    pos -= size;
  used -= dataToMove;

  return dataToMove;
};

// this <-- Block
uint64_t posxx::Buffer::write(const Block& block) {
  uint64_t dataToMove = nsntl::min(getFree(), block.getSize());

  if (pos + used <= size) {
    // no overflow
    if (pos + used + dataToMove <= size) {
      // even after the read won't be one
      Memcpy((uint8_t*)addr + pos, block.getAddr(), dataToMove);
    } else {
      // after the read there will be an overflow
      Memcpy((uint8_t*)addr + pos + used, block.getAddr(), size - pos - used);
      Memcpy((uint8_t*)addr, (uint8_t*)block.getAddr() + size - pos - used, dataToMove - (size - pos - used));
    }
  } else {
    // overflow
    Memcpy((uint8_t*)addr + pos + used - size, block.getAddr(), dataToMove);
  }
  used += dataToMove;

  return dataToMove;
};

uint64_t posxx::Buffer::operator<<(posxx::DataSrc& dataSrc) {
  return ::operator<<(static_cast<DataDst&>(*this), dataSrc);
};

posxx::Buffer& posxx::Buffer::operator<<=(posxx::DataSrc& dataSrc) {
  ::operator<<=(static_cast<DataDst&>(*this), dataSrc);
  return *this;
};

uint64_t posxx::Buffer::operator>>(posxx::DataDst& dataDst) {
  return ::operator>>(static_cast<DataSrc&>(*this), dataDst);
};

posxx::Buffer& posxx::Buffer::operator>>=(posxx::DataDst& dataDst) {
  ::operator>>=(static_cast<DataSrc&>(*this), dataDst);
  return *this;
};

posxx::DynBuffer::DynBuffer(uint64_t size) : DynBlock(size), Buffer(addr, size) {
  pos = 0;
  used = 0;
};

void posxx::DynBuffer::setSize(uint64_t size) {
  uint64_t oldSize = this->size;
  uint64_t newSize = size;

  if (newSize < used)
    throw BufferException(BufferError::BUFFER_CANT_BE_RESIZED_BELOW_USAGE);

  if (oldSize < newSize) {
    // grow
    if (pos + used <= oldSize) // no overflow
      DynBlock::setSize(newSize);
    else {
      // overflow
      DynBlock::setSize(newSize);
      uint64_t toMove = nsntl::min(pos + used - oldSize, newSize - oldSize);
      Memcpy((uint8_t*)addr + oldSize, addr, toMove);
    }
  } else if (oldSize > newSize) {
    // shrink
    if (pos + used <= oldSize) {
      // no overflow
      if (pos + used <= newSize) {
        // no overflow even after the shrink
        DynBlock::setSize(newSize);
      } else {
        // overflow after the shrink
        uint64_t toMove = pos + used - newSize;
        Memcpy((uint8_t*)addr, (uint8_t*)addr + newSize, toMove);
        DynBlock::setSize(newSize);
      }
    } else {
      // overflow
      uint64_t moveBy = oldSize - newSize;
      Memmove((uint8_t*)addr + pos - moveBy, (uint8_t*)addr + pos, oldSize - pos);
      pos -= moveBy;
      DynBlock::setSize(size);
    }
  }
};

void posxx::DynBuffer::defrag() {
  if (!addr || !pos)
    return;

  if (!used) {
    pos = 0;
    return;
  }

  void* newAddr = Malloc(size);
  if (pos + used <= size) {
    // no overflow
    Memcpy((uint8_t*)newAddr, (uint8_t*)addr + pos, used);
  } else {
    // overflow
    Memcpy(newAddr, (uint8_t*)addr + pos, size - pos);
    Memcpy((uint8_t*)newAddr + size - pos, addr, pos + used - size);
  }
  Free(addr);
  addr = newAddr;
};

uint64_t posxx::Sink::read(Block&& block) {
  return 0;
};

uint64_t posxx::Sink::write(const Block& block) {
  return block.getSize();
};

/*
uint64_t operator>>(posxx::Block& src, posxx::Block& dst) {
  uint64_t len = nsntl::min(src.getSize(), dst.getSize());
  Memcpy(dst.getAddr(), src.getAddr(), len);
  return len;
};

uint64_t operator<<(posxx::Block& dst, posxx::Block& src) {
  return src >> dst;
};
*/

uint64_t operator>>(const nsntl::Block& src, posxx::DataDst& dataDst) {
  return dataDst.write(src);
};

uint64_t operator<<(nsntl::Block& dst, posxx::DataSrc& dataSrc) {
  return dataSrc.read(dst);
};

uint64_t operator>>(posxx::DataSrc& dataSrc, nsntl::Block& dst) {
  return dst << dataSrc;
};

posxx::DataSrc& operator>>=(posxx::DataSrc& dataSrc, nsntl::Block& dst) {
  uint64_t ret = dataSrc >> dst;
  if (ret != dst.getSize())
    throw BufferException(BufferError::READ_INCOMPLETE);
  return dataSrc;
};

uint64_t operator<<(posxx::DataDst& dataDst, const nsntl::Block& src) {
  return src >> dataDst;
};

posxx::DataDst& operator<<=(posxx::DataDst& dataDst, const nsntl::Block& src) {
  uint64_t ret = dataDst << src;
  if (ret != src.getSize())
    throw BufferException(BufferError::WRITE_INCOMPLETE);
  return dataDst;
};

uint64_t operator>>(posxx::DataSrc& dataSrc, posxx::DataDst& dataDst) {
  return dataSrc.read(dataDst, PAGE_SIZE);
};

posxx::DataSrc& operator>>=(posxx::DataSrc& dataSrc, posxx::DataDst& dataDst) {
  dataSrc >> dataDst;
  return dataSrc;
};

uint64_t operator<<(posxx::DataDst& dataDst, posxx::DataSrc& dataSrc) {
  return dataSrc >> dataDst;
};

posxx::DataDst& operator<<=(posxx::DataDst& dataDst, posxx::DataSrc& dataSrc) {
  dataSrc >> dataDst;
  return dataDst;
};
