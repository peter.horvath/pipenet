#ifndef Posxx_Unix_h
#define Posxx_Unix_h

#include <sys/un.h>

#include <nsntl/string.h>

#include "socket.h"

namespace posxx {

enum class UnixSocketError : uint64_t {
  UNIX_PATH_TOO_LONG = 0x885ec98f25f0a904ULL
};

class UnixSocketException : public posxx::Exception {
  public:
    UnixSocketException(UnixSocketError errorCode);
};

/**
  * World of the Unix Sockets
  */
class UnixAddr : public SockAddr {
  private:
    nsntl::string path;

  public:
    UnixAddr();
    UnixAddr(const char* path);
    UnixAddr(const nsntl::string path);
    UnixAddr(const sockaddr_un* addr);
    UnixAddr(const sockaddr_un& addr);
    UnixAddr(const UnixAddr& addr);
    UnixAddr(const SockAddr& addr);

    const struct sockaddr* getAddr() const;
    void setAddr(const struct sockaddr_un& addr);
    void setAddr(const sockaddr* addr);
    
    nsntl::string getPath() const;
    void setPath(const char* path);
    void setPath(const nsntl::string& path);
    
    bool isUnnamed() const;
    bool isBound() const;
    bool isAbstract() const;

    bool operator==(const char* path) const;
    bool operator==(const nsntl::string& addr) const;
    bool operator==(const sockaddr_un& addr) const;
    bool operator==(const UnixAddr& addr) const;
    UnixAddr& operator=(const nsntl::string& addr);
    UnixAddr& operator=(const sockaddr_un& addr);
    UnixAddr& operator=(const UnixAddr& addr);

    sa_family_t getFamily() const;
    socklen_t getSockAddrLength() const;
};

class BaseUnixSocket : public virtual BaseSocket {
  private:
    UnixAddr localAddr;

  public:
    bool isUnnamed() const;
    bool isBound() const;
    bool isAbstract() const;

    const UnixAddr& getLocalAddr() const;
};

class UnixListenerSocket;

class UnixStreamSocket : public BaseStreamSocket, public BaseUnixSocket {
  friend class UnixListenerSocket;
  private:
    UnixAddr remoteAddr;
    void init();


  public:
    UnixStreamSocket();
    UnixStreamSocket(int fd);
    UnixStreamSocket(const UnixStreamSocket& fd);
    UnixStreamSocket(const UnixAddr& remoteAddr);
    UnixStreamSocket(const UnixAddr& localAddr, const UnixAddr& remoteAddr);
    UnixStreamSocket(UnixListenerSocket& listener);

    virtual ~UnixStreamSocket();

    int getProtocol() const;
};

/*
 * Datagrams for sending-receiving on unix sockets
 */
class UnixDgram : public Dgram {
  private:
    UnixAddr srcAddr;
    UnixAddr dstAddr;

    void init(const UnixAddr& srcAddr, const UnixAddr& dstAddr);

  public:
    UnixDgram(uint64_t size = 0);
    UnixDgram(const UnixAddr& dstAddr, uint64_t size = 0);
    UnixDgram(const UnixAddr& srcAddr, const UnixAddr& dstAddr, uint64_t size = 0);
    UnixDgram(Block& data, const UnixAddr& dstAddr);
    UnixDgram(Block& data, const UnixAddr& srcAddr, const UnixAddr& dstAddr);
    virtual ~UnixDgram();
};

class UnixDgramSocket : public BaseDgramSocket, public BaseUnixSocket {
  private:
    UnixAddr remoteAddr;

    void init();

  public:
    UnixDgramSocket();
    UnixDgramSocket(int fd);
    UnixDgramSocket(const UnixAddr& remoteAddr);
    UnixDgramSocket(const UnixAddr& localAddr, const UnixAddr& remoteAddr);
    virtual ~UnixDgramSocket();

    int getProtocol() const;
};

class UnixListenerSocket : public BaseListenerSocket, public BaseUnixSocket {
  private:
    void listen(int maxconn = 0);

  public:
    UnixListenerSocket(const UnixListenerSocket& socket) = delete;
    UnixListenerSocket(int maxconn = 0);
    UnixListenerSocket(const UnixAddr& addr, int maxconn = 0);
    virtual ~UnixListenerSocket();

    int getProtocol() const;
};

};
#endif
