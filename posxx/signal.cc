#include <errno.h>
#include <signal.h>
#include <string.h>
#include <sys/mman.h>

#include <nsntl/atom.h>
#include <nsntl/malloc.h>
#include <nsntl/util.h>

#include "signal.h"
#include "timer.h"

using namespace posxx;
using namespace nsntl;

static SignalHandler** sigHandlers = NULL;
static struct sigaction** oldSa = NULL;
static int max_signum = -1;
static bool posxx_signal_inited = false;
static Counter signalCounter;
static unsigned int sigcount = 0;

static void init_globals() {
  if (!posxx_signal_inited) {
    signalCounter.init();
    posxx_signal_inited = true;
  }
  if (max_signum == -1)
    max_signum = SIGRTMAX;
  if (!sigHandlers) {
    sigHandlers = (SignalHandler**)nsntl::Malloc(sizeof(SignalHandler*) * max_signum);
    nsntl::Bzero(sigHandlers, sizeof(SignalHandler*) * max_signum);
  }
  if (!oldSa) {
    oldSa = (struct sigaction**)nsntl::Malloc(sizeof(SignalHandler*) * max_signum);
    nsntl::Bzero(oldSa, sizeof(struct sigaction*) * max_signum);
  }
};

static void uninit_globals() {
  if (sigcount)
    throw LogicException(LogicError::UNREACHABLE_CODE);
  nsntl::Free(sigHandlers);
  sigHandlers=NULL;
  nsntl::Free(oldSa);
  oldSa=NULL;
};

static void check_signum(int signum) {
  if (signum <= 0 || signum >= max_signum)
    throw SignalException(SignalError::INVALID_SIGNAL_NUMBER);
};

static void check_signum_registered(int signum, bool must) {
  check_signum(signum);
  if (must && !sigHandlers[signum])
    throw SignalException(SignalError::SIGNAL_NOT_YET_REGISTERED);
  else if (!must && sigHandlers[signum])
    throw SignalException(SignalError::SIGNAL_ALREADY_REGISTERED);
};

static void generalHandler(int signum, siginfo_t* siginfo, void* ucontext) {
  check_signum_registered(signum, true);

  if (!sigHandlers[signum])
    throw SignalException(SignalError::SIGNAL_NOT_YET_REGISTERED);

  uint64_t id = signalCounter.getId();
  uint128_t timestamp = Timer::now();

  sigHandlers[signum]->handler(id, timestamp, signum, siginfo, (ucontext_t*)ucontext);  
};

posxx::SignalException::SignalException(SignalError errorCode) : posxx::Exception((uint64_t)errorCode) {
  // nothing now
};

posxx::SignalHandler::SignalHandler() {
  init_globals();
  sigcount++;
};

posxx::SignalHandler::SignalHandler(int signum) : SignalHandler() {
  attach(signum);
};

posxx::SignalHandler::~SignalHandler() noexcept(false) {
  detach();
  if (!--sigcount)
    uninit_globals();
};

const IntSet& posxx::SignalHandler::getSignums() const {
  return signums;
};

void posxx::SignalHandler::attach(int signum) {
  check_signum_registered(signum, false);
  signums.add(signum);
  sigHandlers[signum] = this;

  struct sigaction sa, oldsa;
  nsntl::Bzero(&sa, sizeof(struct sigaction));
  sa.sa_handler = NULL;
  sa.sa_sigaction = &generalHandler;
  if (sigemptyset(&sa.sa_mask))
    throw posxx::Exception(errno);
  sa.sa_flags = SA_SIGINFO | SA_NODEFER | SA_RESTART;
  sa.sa_restorer = NULL;
  if (sigaction(signum, &sa, &oldsa))
    throw posxx::Exception(errno);

  oldSa[signum] = (struct sigaction*)nsntl::Malloc(sizeof(struct sigaction));
  nsntl::Memcpy(&oldSa[signum], &oldsa, sizeof(struct sigaction));
};

void posxx::SignalHandler::detach(int signum) {
  check_signum_registered(signum, true);
  
  if (sigaction(signum, oldSa[signum], NULL))
    throw posxx::Exception(errno);

  signums.remove(signum);
  nsntl::Free(oldSa[signum]);
  oldSa[signum] = NULL;
  sigHandlers[signum] = NULL;
};

void posxx::SignalHandler::detach() {
  while (!signums.isEmpty()) {
    detach(signums.first());
  };
};

int posxx::SignalHandler::getMaxSignum() {
  if (max_signum <= 0)
    throw SignalException(SignalError::GLOBALS_NOT_INITED);

  return max_signum;
};
