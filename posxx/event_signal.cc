#include "buffer.h"
#include "event_exception.h"
#include "event_manager.h"
#include "event_signal.h"

using namespace posxx;
using namespace nsntl;

static void check_mask(uint8_t mask) {
  if ((mask & 0xe0) != 0xa0)
    throw EventException(EventError::INVALID_PACKED_SIGNAL);
};

posxx::SignalEvent::SignalEvent(DataSrc& dataSrc, SignalEventProcessor* processor) {
  if (dataSrc.read(Block(&mask, sizeof(uint8_t))) < 1)
    throw EventException(EventError::INVALID_PACKED_SIGNAL);

  check_mask(mask);

  if (hasId()) {
    if (dataSrc.read(Block(&id, sizeof(uint64_t))) < sizeof(uint64_t))
      throw EventException(EventError::INVALID_PACKED_SIGNAL);
  };

  if (hasTimestamp()) {
    if (dataSrc.read(Block(&timestamp, sizeof(uint128_t))) < sizeof(uint128_t))
      throw EventException(EventError::INVALID_PACKED_SIGNAL);
  };

  if (hasSignum()) {
    if (dataSrc.read(Block(&signum, sizeof(int))) < sizeof(int))
      throw EventException(EventError::INVALID_PACKED_SIGNAL);
  };

  if (hasSiginfo()) {
    siginfo = reinterpret_cast<siginfo_t*>(processor->siginfoPool->get(sizeof(siginfo_t)));
    if (dataSrc.read(Block(siginfo, sizeof(siginfo_t))) < sizeof(siginfo_t))
      throw EventException(EventError::INVALID_PACKED_SIGNAL);
  };

  if (hasUcontext()) {
    ucontext = reinterpret_cast<ucontext_t*>(processor->ucontextPool->get(sizeof(ucontext_t)));
    if (dataSrc.read(Block(ucontext, sizeof(ucontext_t))) < sizeof(ucontext_t))
      throw EventException(EventError::INVALID_PACKED_SIGNAL);
  };
};

posxx::SignalEvent::~SignalEvent() {
  if (siginfo) {
    MallocPool::free(siginfo);
    siginfo = NULL;
  }
  if (ucontext) {
    MallocPool::free(ucontext);
    ucontext = NULL;
  }
};

void posxx::SignalEvent::checkMask() const {
  check_mask(mask);
};

uint8_t posxx::SignalEvent::getMask() const {
  checkMask();
  return mask;
};

bool posxx::SignalEvent::hasId() const {
  checkMask();
  return mask & ID;
};

uint64_t posxx::SignalEvent::getId() const {
  if (!hasId())
    throw EventException(EventError::MISSING_SIGNAL_EVENT_FIELD);
  return id;
};

bool posxx::SignalEvent::hasTimestamp() const {
  checkMask();
  return mask & TIMESTAMP;
};

nsntl::uint128_t posxx::SignalEvent::getTimestamp() const {
  if (!hasTimestamp())
    throw EventException(EventError::MISSING_SIGNAL_EVENT_FIELD);
  return timestamp;
};

bool posxx::SignalEvent::hasSignum() const {
  checkMask();
  return mask & SIGNUM;
};

int posxx::SignalEvent::getSignum() const {
  if (!hasSignum())
    throw EventException(EventError::MISSING_SIGNAL_EVENT_FIELD);
  return signum;
};

bool posxx::SignalEvent::hasSiginfo() const {
  checkMask();
  return mask & SignalEvent::SIGINFO;
};

const siginfo_t* posxx::SignalEvent::getSiginfo() const {
  if (!hasSiginfo())
    throw EventException(EventError::MISSING_SIGNAL_EVENT_FIELD);
  return siginfo;
};

bool posxx::SignalEvent::hasUcontext() const {
  checkMask();
  return mask & UCONTEXT;
};

const ucontext_t* posxx::SignalEvent::getUcontext() const {
  if (!hasUcontext())
    throw EventException(EventError::MISSING_SIGNAL_EVENT_FIELD);
  return ucontext;
};

int posxx::SignalEvent::size() const {
  checkMask();
  return size(mask);
};

int posxx::SignalEvent::size(uint8_t mask) {
  check_mask(mask);
  int ret = 1;
  if (mask & ID)
    ret += sizeof(uint64_t);
  if (mask & TIMESTAMP)
    ret += sizeof(uint128_t);
  if (mask & SIGNUM)
    ret += sizeof(int);
  if (mask & SignalEvent::SIGINFO)
    ret += sizeof(siginfo_t);
  if (mask & UCONTEXT)
    ret += sizeof(ucontext_t);
  return ret;
};

uint64_t posxx::SignalEvent::pack(uint8_t* dst, uint8_t mask, uint64_t id, nsntl::uint128_t timestamp, int signum, siginfo_t* siginfo, ucontext_t* ucontext) {
  int p=0;

  check_mask(mask);

  dst[p] = mask;
  p++;

  if (mask & SignalEvent::ID) {
    *(uint64_t*)(dst+p) = id;
    p += sizeof(uint64_t);
  };

  if (mask & SignalEvent::TIMESTAMP) {
    *(uint128_t*)(dst+p) = timestamp;
    p += sizeof(uint128_t);
  };

  if (mask & SignalEvent::SIGNUM) {
    *(int*)(dst+p) = signum;
    p += sizeof(int);
  };

  if (mask & SignalEvent::SIGINFO) {
    Memcpy(dst+p, siginfo, sizeof(siginfo_t));
    p += sizeof(siginfo_t);
  };

  if (mask & SignalEvent::UCONTEXT) {
    Memcpy(dst+p, ucontext, sizeof(ucontext_t));
    p += sizeof(ucontext_t);
  };

  return p;
};

posxx::SignalEventHandler::SignalEventHandler(EventManager* em, int signum, uint8_t mask) : EventHandler(em) {
  pendingEvents.init();
  currentEvent = NULL;
  this->signum = signum;
  signalEventProcessorItem.init(this);
  this->mask = 0xa0;
  mask = 0xa0 | (mask & 0x1f);
  setMask(mask);
};

posxx::SignalEventHandler::~SignalEventHandler() {
  while (!pendingEvents.isEmpty())
    delete pendingEvents.firstEntry();
};

uint8_t posxx::SignalEventHandler::getMask() const {
  return mask;
};

void posxx::SignalEventHandler::setMask(uint8_t mask) {
  check_mask(this->mask);
  check_mask(mask);
  if (! (mask & SignalEvent::SIGNUM))
    throw EventException(EventError::MASK_NEEDS_SIGNUM_FIELD);
  if (isAttachedToProcessor())
    em->signalEventProcessor.changeMask(this->signum, this->mask, mask);
  this->mask = 0xa0 | (mask & 0x1f);
};

const SignalEvent* posxx::SignalEventHandler::getEvent() const {
  return currentEvent;
};

bool posxx::SignalEventHandler::isAttachedToProcessor() const {
  return signalEventProcessorItem.list();
};

int posxx::SignalEventHandler::getSignum() const {
  return signum;
};

void posxx::SignalEventHandler::aim() {
  // nothing now - pendingEvents is filled by signalProcessor if we are not disabled
};

void posxx::SignalEventHandler::unaim() {
  flushQueue();
};

void posxx::SignalEventHandler::trigger() {
  for (ListItem* li = pendingEvents.firstItem(); li; li=li->next()) {
    currentEvent = dynamic_cast<SignalEvent*>(li->get());
    handler();
  }
  currentEvent = NULL;
  flushQueue();
};

void posxx::SignalEventHandler::enable() {
  EventHandler::enable();
  em->getSignalEventProcessor()->attachHandler(this);
};

void posxx::SignalEventHandler::disable() {
  EventHandler::disable();
  em->getSignalEventProcessor()->detachHandler(this);
};

void posxx::SignalEventHandler::flushQueue() {
  while (!pendingEvents.isEmpty())
    delete pendingEvents.firstItem();
};

posxx::SignalSocketReceiver::SignalSocketReceiver(int fd, SignalEventProcessor* signalEventProcessor)
    : Fd(fd), UnixDgramSocket(fd), FdEventProcessor() {
  this->signalEventProcessor = signalEventProcessor;
};

posxx::SignalSocketReceiver::~SignalSocketReceiver() {
  // nothing now
};

void posxx::SignalSocketReceiver::onReadEvt() {
  uint8_t buf[SignalEvent::maxSize];
  Buffer buffer(buf, SignalEvent::maxSize);
  for (;;) {
    if (!(buffer << *this))
      break;
    while (buffer.getUsed()) {
      SignalEvent* signalEvent = new(*(signalEventProcessor->signalEventPool)) SignalEvent(buffer, signalEventProcessor);
      signalEventProcessor->distributeSignal(signalEvent);
    }
    // needed to avoid circulation - it would cause to read a datagram in fragments
    buffer.reset();
  }
};

void posxx::SignalSocketReceiver::onWriteEvt() {
  throw LogicException(LogicError::UNREACHABLE_CODE);
};

void posxx::SignalSocketReceiver::onExceptEvt() {
  throw LogicException(LogicError::UNREACHABLE_CODE);
};

posxx::SignalEventProcessor::SignalEventProcessor(EventManager* em) : SignalHandler() {
  this->em = em;
  sighandlerCount = 0;
  pendingEvents.init();
};

posxx::SignalEventProcessor::~SignalEventProcessor() {
  while (!getSignums().isEmpty()) {
    int signum = getSignums().first();
    // on the last detachHandler, theoretically all the de-allocations should run
    while (!signalHandlerLists[signum].isEmpty())
      detachHandler(dynamic_cast<SignalEventHandler*>(signalHandlerLists[signum].first()));
  }
  while (!pendingEvents.isEmpty())
    delete pendingEvents.firstEntry();
  if (masks)
    Free(masks);
};

void posxx::SignalEventProcessor::attachHandler(SignalEventHandler* eh) {
  if (!em)
    throw LogicException(LogicError::UNREACHABLE_CODE);

  if (eh->isAttachedToProcessor())
    throw LogicException(LogicError::ALREADY_ATTACHED);

  if (!sighandlerCount) {
    if (masks || signalHandlerLists || !pendingEvents.isEmpty() || SocketPair::isOpened())
      throw LogicException(LogicError::INTERNAL_STATE_INCONSISTENT);
    masks = new int[SignalHandler::getMaxSignum()][5]();
    signalHandlerLists = new ItemList[SignalHandler::getMaxSignum()]();
    eventListEntryPool = new MallocPool(sizeof(PooledListItem));
    signalEventPool = new MallocPool(sizeof(SignalEvent));
    siginfoPool = new MallocPool(sizeof(siginfo_t));
    ucontextPool = new MallocPool(sizeof(ucontext_t));
    SocketPair::open();
  }

  sighandlerCount++;
  
  eh->signalEventProcessorItem.attach(signalHandlerLists + eh->signum);
  if (signalHandlerLists[eh->signum].isEmpty())
    SignalHandler::attach(eh->signum);

  changeMask(eh->signum, 0xa0, eh->mask);
};

void posxx::SignalEventProcessor::detachHandler(SignalEventHandler* eh) {
  if (!eh->isAttachedToProcessor())
    throw LogicException(LogicError::ALREADY_DETACHED);

  if (sighandlerCount <= 0)
    throw LogicException(LogicError::INTERNAL_STATE_INCONSISTENT);

  eh->flushQueue();

  changeMask(eh->signum, eh->mask, 0xa0);

  eh->signalEventProcessorItem.detach();
  if (signalHandlerLists[eh->signum].isEmpty())
    SignalHandler::detach(eh->signum);

  if (!--sighandlerCount) {
    SocketPair::close();
    delete masks;
    masks = NULL;
    delete signalHandlerLists;
    signalHandlerLists = NULL;
    delete eventListEntryPool;
    eventListEntryPool = NULL;
    delete signalEventPool;
    signalEventPool = NULL;
    delete siginfoPool;
    siginfoPool = NULL;
    delete ucontextPool;
    ucontextPool = NULL;
  };
};

void posxx::SignalEventProcessor::distributeSignal(SignalEvent* signalEvent) {
  if (!signalEvent->hasSignum()) {
    delete signalEvent;
    return;
  }

  int signum = signalEvent->getSignum();

  if (!signalHandlerLists)
    throw LogicException(LogicError::UNREACHABLE_CODE);

  signalEvent->attach(&pendingEvents);

  for (ListItem* li = signalHandlerLists[signum].firstItem(); li; li = li->next()) {
    SignalEventHandler* handler = dynamic_cast<SignalEventHandler*>(li->get());
    PooledListItem* pli = new(*eventListEntryPool) PooledListItem(signalEvent);
    pli->attach(&handler->pendingEvents);
  };
};

void posxx::SignalEventProcessor::flushQueue() {
  while (!pendingEvents.isEmpty())
    delete pendingEvents.firstEntry();
};

void posxx::SignalEventProcessor::changeMask(int signum, uint8_t oldMask, uint8_t newMask) {
  check_mask(oldMask);
  check_mask(newMask);

  for (int i=0; i<5; i++) {
    bool oldHas = oldMask & 1 << i;
    bool newHas = newMask & 1 << i;
    if (!oldHas && newHas) {
      masks[signum][i]++;
    } else if (oldHas && !newHas) {
      if (!masks[signum][i])
        throw LogicException(LogicError::UNREACHABLE_CODE);
      masks[signum][i]--;
    }
  };
};

void posxx::SignalEventProcessor::buildFds(int fd1, int fd2) {
  if (!em)
    throw LogicException(LogicError::UNREACHABLE_CODE);

  _fd1 = new UnixDgramSocket(fd1);
  SignalSocketReceiver* ssr = new SignalSocketReceiver(fd2, this);
  _fd2 = static_cast<Fd*>(ssr);
  FdEventHandler* rdHandler = new FdEventHandler(em, ssr, EVT_READ);
  rdHandler->attach(em);
  em->setHandlerPrio(rdHandler, true);
};

void posxx::SignalEventProcessor::handler(uint64_t id, nsntl::uint128_t timestamp, int signum, siginfo_t* siginfo, ucontext_t* ucontext) {
  uint8_t mask = 0xe0;
  for (int i=0; i<5; i++)
    if (masks[signum][i])
      mask |= 1 << i;

  uint8_t buf[SignalEvent::maxSize];
  uint64_t size = SignalEvent::pack(buf, mask, id, timestamp, signum, siginfo, ucontext);
  ssize_t ret = ::send(SocketPair::fd1().getFd(), buf, size, 0);
  if ((int64_t)ret < (int64_t)size) {
    throw EventException(EventError::SIGNAL_BUFFER_ERROR);
  };
};
