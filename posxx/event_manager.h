#ifndef posxx_event_manager_h
#define posxx_event_manager_h

#include "event_fd.h"
#include "event_timer.h"
#include "event_signal.h"

// Event handling for the interaction between the PipeNet and the Posix

namespace posxx {

class EventManager {
  friend class EventHandler;
  friend class FdEventHandler;
  friend class TimerEventHandler;
  friend class SignalEventHandler;
  friend class SignalEventProcessor;

  public:
    typedef enum {
      BEFORE_SELECT,
      AFTER_SELECT
    } Phase;

  private:
    nsntl::List eventHandlers[EventHandler::STATE_MAX];
    SignalEventProcessor signalEventProcessor;
    Phase phase;

    fd_set fds[EVT_MAX]; // array of rfds, wfds and efds
    int fdSetCount[EVT_MAX];
    int maxfd;
    nsntl::uint128_t now;
    nsntl::uint128_t timeout;

  public:
    EventManager();
    ~EventManager();

  private:
    SignalEventProcessor* getSignalEventProcessor() const;
    EventHandler* getFirstHandler(EventHandler::State state) const;
    void setHandlerPrio(EventHandler* handler, bool prio);
    void cleanup();
    void reset();

  public:
    // resets, aims handlers, waits an event and executes all handlers if they were triggered
    void tic();
};

};

#endif
