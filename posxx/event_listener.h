#ifndef posxx_event_listener_h
#define posxx_event_listener_h

namespace posxx {

class FdEventListener : protected class FdEventProcessor {
  public:
    FdEventListener();
    virtual ~FdEventListener();

    virtual void onConnect(BaseConnectedSocket* socket) = 0;
};

class UnixListenerFactory : public class UnixListenerSocket, protected class FdEventListener {
  public:
    UnixListenerFactory(UnixAddr& addr);
    virtual ~UnixListenerFactory();

    virtual void onConnect(UnixStreamSocket* socket) = 0;
};

class Tcp4ListenerFactory : public class Tcp4ListenerSocket, protected class FdEventListener {
  public:
    Tcp4ListenerFactory(Tcp4Addr& addr);
    virtual ~Tcp4ListenerFactory();

    virtual void onConnect(Tcp4StreamSocket* socket) = 0;
};

};

#endif
