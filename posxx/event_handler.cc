#include "event_exception.h"
#include "event_handler.h"
#include "event_manager.h"

using namespace posxx;

posxx::EventHandler::EventHandler(EventManager* em) : ListEntry() {
  this->em = 0;
  prio = false;
  if (em)
    attach(em);
};

posxx::EventHandler::~EventHandler() {
  if (em)
    detach();
};

void posxx::EventHandler::genAim() {
  State state = getState();
  if (state != STATE_NEW && state != STATE_TRIGGERED)
    throw EventException(EventError::CANT_AIM_DISABLED_HANDLER);
  if (em->phase != EventManager::BEFORE_SELECT)
    throw EventException(EventError::AIM_REQUIRES_BEFORE_SELECT);

  aim();

  state = getState();

  if (state == STATE_NEW || state == STATE_TRIGGERED)
    setState(STATE_AIMED);
  else if (state != STATE_DISABLED && state != STATE_WANTSDEL) {
    throw EventException(EventError::INVALID_HANDLER_STATE);
  };
};

void posxx::EventHandler::genUnaim() {
  State state = getState();
  if (state != STATE_AIMED)
    throw EventException(EventError::INVALID_HANDLER_STATE);
  if (em->phase != EventManager::BEFORE_SELECT)
    throw EventException(EventError::AIM_REQUIRES_BEFORE_SELECT);

  unaim();

  state = getState();

  if (state == STATE_AIMED)
    setState(STATE_DISABLED);
  else if (state != STATE_DISABLED && state != STATE_WANTSDEL)
    throw EventException(EventError::INVALID_HANDLER_STATE);
};

void posxx::EventHandler::genTrigger() {
  State state = getState();
  if (state != STATE_AIMED)
    throw EventException(EventError::INVALID_HANDLER_STATE);
  if (em->phase != EventManager::AFTER_SELECT)
    throw EventException(EventError::TRIGGER_REQUIRES_AFTER_SELECT);

  trigger();

  state = getState();

  if (state == STATE_AIMED)
    setState(STATE_TRIGGERED);
  else if (state != STATE_DISABLED && state != STATE_WANTSDEL)
    throw EventException(EventError::INVALID_HANDLER_STATE);
};

bool posxx::EventHandler::getPrio() const {
  return prio;
};

void posxx::EventHandler::setPrio(bool prio) {
  this->prio = prio;
};

EventHandler::State posxx::EventHandler::getState() const {
  if (!em)
    return STATE_DISABLED;
  int idx = ListEntry::list() - &em->eventHandlers[0];
  return (EventHandler::State)idx;
};

void posxx::EventHandler::setState(EventHandler::State newState) {
  State state = getState();

  if (state == newState)
    return;

  if (!em) {
    if (state != STATE_DISABLED || newState != STATE_DISABLED)
      throw EventException(EventError::INVALID_HANDLER_STATE);
    else
      return;
  }

  ListEntry::detach();
  ListEntry::attach(&em->eventHandlers[newState], prio);
};

void posxx::EventHandler::deleteMe() {
  if (!em)
    throw EventException(EventError::NO_EVENT_MANAGER);

  State state = getState();

  if (state != STATE_DISABLED && state != STATE_WANTSDEL)
    disable();

  setState(STATE_WANTSDEL);
};

EventManager* posxx::EventHandler::getEventManager() const {
  return em;
};

void posxx::EventHandler::setEventManager(EventManager* em) {
  if (this->em == em)
    return;
  if (this->em)
    detach();
  if (em)
    attach(em);
};

void posxx::EventHandler::attach(EventManager* em) {
  if (this->em)
    throw EventException(EventError::ALREADY_HAS_EVENT_MANAGER);
  this->em = em;

  ListEntry::attach(&em->eventHandlers[STATE_DISABLED], prio);
};

void posxx::EventHandler::detach() {
  if (!em)
    throw EventException(EventError::NO_EVENT_MANAGER);

  disable();
  ListEntry::detach();
  em = 0;
};

bool posxx::EventHandler::isEnabled() const {
  return getState() != STATE_DISABLED;
};

void posxx::EventHandler::enable() {
  State state = getState();

  if (state == STATE_DISABLED)
    throw EventException(EventError::INVALID_HANDLER_STATE);

  setState(STATE_NEW);
};

void posxx::EventHandler::disable() {
  State state = getState();

  if (state == STATE_DISABLED)
    throw EventException(EventError::INVALID_HANDLER_STATE);
  else if (getState() == STATE_AIMED) {
    if (em->phase == EventManager::BEFORE_SELECT)
      genUnaim();
    else if (em->phase == EventManager::AFTER_SELECT)
      genTrigger();
  };

  setState(STATE_DISABLED);
};

