#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <unistd.h>

//#include "csup.h"
#include "fd.h"

using namespace posxx;
using namespace nsntl;

posxx::FdException::FdException(FdError errorCode) : posxx::Exception((uint64_t)errorCode) {
  // nothing now
};

void posxx::Fd::setFd(const int fd) {
  this->fd=fd;
};

posxx::Fd::Fd() {
  // nothing now
};

posxx::Fd::Fd(int fd) {
  this->fd=fd;
};

posxx::Fd::Fd(const Fd& fd) {
  this->fd = fd.getFd();
};

posxx::Fd::~Fd() noexcept(false) {
  this->close();
};

int posxx::Fd::getFd() const {
  return fd;
};

void posxx::Fd::close() {
  if (fd == -1)
    throw FdException(FdError::FD_DOES_NOT_EXIST);

  if (::close(fd) == -1)
    throw posxx::Exception(errno);

  fd=-1;
}

int posxx::Fd::getFcntl() {
  int flags=fcntl(this->fd, F_GETFD);
  if (flags<0)
    throw posxx::Exception(errno);
  return flags;
}

void posxx::Fd::setFcntl(const int flags) {
  if (fcntl(this->fd, F_SETFD, flags)==-1)
    throw posxx::Exception(errno);
}

bool posxx::Fd::isBlocking() {
  return ! (getFcntl()&O_NONBLOCK);
};

void posxx::Fd::setBlocking(bool blocking) {
  int flags = getFcntl();
  bool setfl = false;
  if (blocking && (flags & O_NONBLOCK)) {
    flags &= ~O_NONBLOCK;
    setfl = true;
  } else if (!blocking && !(flags & O_NONBLOCK)) {
    flags |= O_NONBLOCK;
    setfl = true;
  }
  if (setfl)
    setFcntl(flags);
};

posxx::StreamFd::StreamFd() {
  // nothing now
};

posxx::StreamFd::StreamFd(int fd) : Fd(fd) {
  // nothing now
};

posxx::StreamFd::~StreamFd() {
  // nothing now
};

uint64_t posxx::StreamFd::read(Block&& block) {
  int len = block.getSize();

  int bytesRead = ::read(getFd(), block.getAddr(), len);
  int exErrno = errno;
  if (bytesRead == -1)
    throw posxx::Exception(exErrno);
  return bytesRead;
};

uint64_t posxx::StreamFd::write(const Block& block) {
  int len = block.getSize();

  int bytesWritten=::write(getFd(), block.getAddr(), len);
  int exErrno = errno;
  if (bytesWritten == -1)
    throw posxx::Exception(exErrno);

  return bytesWritten;
};

posxx::DgramFd::DgramFd() {
  setMaxRecvSize(PAGE_SIZE);
};

posxx::DgramFd::DgramFd(int fd) : Fd(fd) {
  // nothing now
};

posxx::DgramFd::~DgramFd() {
  // nothing now
};

uint64_t posxx::DgramFd::getMaxRecvSize() const {
  return maxRecvSize;
};

void posxx::DgramFd::setMaxRecvSize(uint64_t maxRecvSize) {
  this->maxRecvSize = maxRecvSize;
};

uint64_t posxx::DgramFd::peek() const {
  uint8_t buf[1];
  int ret = ::recv(this->getFd(), buf, 1, MSG_PEEK | MSG_TRUNC);
  if (ret < 0)
    throw ErrorCodeException(errno);
  else
    return ret;
};

uint64_t posxx::DgramFd::recv(Block& block) {
  if (block.getSize() < getMaxRecvSize())
    throw FdException(FdError::BLOCK_TOO_SMALL);

  int bytesRead = ::recv(getFd(), block.getAddr(), block.getSize(), 0);
  int exErrno = errno;
  if (bytesRead == -1)
    throw posxx::Exception(exErrno);
  return bytesRead;
};

uint64_t posxx::DgramFd::send(const Block& block) {
  int bytesSent = ::send(getFd(), block.getAddr(), block.getSize(), 0);
  if (bytesSent == -1)
    throw posxx::Exception(errno);
  return bytesSent;
};

uint64_t posxx::DgramFd::read(Block&& block) {
  uint64_t pktLen = peek();
  if (pktLen > block.getSize())
    throw FdException(FdError::PACKET_TOO_BIG);
  return this->recv(block);
};

uint64_t posxx::DgramFd::write(const Block& block) {
  return this->send(block);
};
