#ifndef Posxx_Dtree_h
#define Posxx_Dtree_h

typedef struct DTree_node __attribute__((packed)) {
  void* data;
  struct __DTree_node* childs;
  u8 nchild;
  u8 height;
} __DTree_node;

typedef struct DTree {
  int comp(void* d1, void* d2);
  DTree_node* root;
};

DTree* DTree_new(int comp(void*, void*));
void DTree_del(DTree* tree);
void DTree_add(DTree* tree, void* data);
void DTree_remove(DTree* tree, void* data);
void* DTree_find(void* data);

#endif
