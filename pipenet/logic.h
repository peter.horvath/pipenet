#ifndef pipenet_logic_h
#define pipenet_logic_h

#include "core.h"

namespace pipenet {

class Blackhole : public Node {
  public:
    Blackhole(Pipenet& pipenet);
    Message* getMessage(Pipe* p);
    void onMessage(Pipe* p, Message* msg);
    void onNewInput(Pipe* p);
    void onNewOutput(Pipe* p);

#ifdef DEBUG
    const char* getName() const;
#endif
};

class Whitehole : public Node {
  private:
    Pipe* output;

  public:
    Whitehole(Pipenet& pipenet);
    Message* getMessage(Pipe* p);
    void onMessage(Pipe* p, Message* msg);
    void onNewInput(Pipe* p);
    void onNewOutput(Pipe* pipe);
#ifdef DEBUG
    const char* getName() const;
#endif
};

class Filter : public Node {
  private:
    Pipe *src, *dst;
    Message *msg;

  public:
    Filter(Pipenet& pipenet);
    virtual ~Filter();

    Message* getMessage(Pipe* p);
    void onMessage(Pipe* p, Message* msg);
    void onNewInput(Pipe* src);
    void onNewOutput(Pipe* dst);
    void onOutputBlocked(Pipe* pipe);
    void onOutputUnblocked(Pipe* pipe);

    virtual Message* filter(Message* msg) = 0;
#ifdef DEBUG
    virtual const char* getName() const;
#endif
};

class FirstNFilter : public Filter {
  private:
    int n;

  public:
    FirstNFilter(Pipenet& pipenet, int n);
    Message* filter(Message* msg);
#ifdef DEBUG
    const char* getName() const;
#endif
};

class Forwarder : public Filter {
  public:
    Forwarder(Pipenet& pipenet);
    virtual Message* filter(Message* msg);
#ifdef DEBUG
    const char* getName() const;
#endif
};

};

#endif
