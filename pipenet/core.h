#ifndef pipenet_core_h
#define pipenet_core_h

// TODO: refactor it to have 1) sync message passing between the nodes
//        2) allow only the minimal needed interface to inherited / external classes
//        3) intuitive interface
//        4) differentiate between Src and Dst pipes

#include <nsntl/exception.h>
#include <nsntl/list.h>
#include <nsntl/malloc.h>
#include <nsntl/types.h>

namespace pipenet {

enum class Error : uint64_t {
  NOT_MY_SRC_PIPE = 0x305c9694beae93cbULL,
  NOT_MY_DST_PIPE = 0x13408136982d21ebULL,
  NOT_MY_PIPE = 0xca1c099df3c34fd4ULL,
  NOT_MY_NODE = 0x28515cdeaf750251ULL,
  CROSS_PIPENET_NOT_ALLOWED = 0xc3d177bdeca50bf3ULL,
  SELF_PIPE_NOT_ALLOWED = 0xdc41bdb458d68326ULL,
  DELETEME_SENT_MULTIPLE_TIMES = 0x3dc9246f5575284eULL,
  NO_PIPENET = 0x77f1fa52e19fe162ULL,
  INVALID_TASK_TYPE = 0x056fdba558feed6cULL,
  HAS_PIPES = 0xca89e46942eb5e72ULL,
  ALREADY_BLOCKED = 0x0d65d0ae25e3a4beULL,
  ALREADY_UNBLOCKED = 0x75c089fe1aead1a0ULL,
  OUTPUT_BLOCKED = 0x0b1b6c6a9b0a71efULL,
  HAS_ALREADY_PIPENET = 0x562eae4b6b65ddd8ULL,
  HAS_PENDING_MSG = 0x89e746a79bbba3f9ULL,
  EXCESS_PIPES = 0x3a08d017577ffb1cULL
};

class Exception : public nsntl::ErrorCodeException {
  public:
    Exception(Error code);
};

class Message;
class Pipe;
class Node;
class Pipenet;

class Message : private nsntl::Object {
  public:
    virtual ~Message();
};

class Task : public nsntl::Object, public nsntl::MallocPooled {
  friend class Pipe;
  friend class Node;
  friend class Pipenet;

  private:
    typedef enum {
      /** Pipe closure. Pipe is deleted, then Src and Dst get onInputClosed / onOutputClosed events. */
      CLOSE_PIPE,

      /** If src blocks, nothing. If dst blocks, src gets onOutputBlocked. */
      BLOCK_PIPE,

      /** If src unblocks, nothing. If dst blocks, src gets onOutputUnblocked. */
      UNBLOCK_PIPE,

      /** Both src and dst gets onNewInput / onNewOutput. */
      OPEN_PIPE,

      /** For beautiful array limit checks. */
      TASKS_MAX
    } TaskType;

    TaskType type;
    Pipe* pipe;
    nsntl::ListItem pipenetItem, pipeItem;

    Task(Pipe* pipe, TaskType type);
    ~Task();
};

class Pipe : public nsntl::Object {
  friend class Task;
  friend class Node;
  friend class Pipenet;

  private:
    typedef enum {
      BLOCK_BLOCK = 0,
      BLOCK_UNBLOCK = 1,
      UNBLOCK_BLOCK = 2,
      UNBLOCK_UNBLOCK = 3,
      BLOCKTYPES_MAX
    } BlockType;

    Pipenet* pipenet;
    Node *src, *dst;
    nsntl::ListItem srcItem, dstItem, pipenetItem;
    nsntl::ItemList tasks;

    Pipe(Node* src, Node* dst);
    ~Pipe();

    bool wantsDel;

    BlockType getBlockType() const;
    void setBlockType(BlockType blockType);

    bool isInputBlocked() const;
    void setInputBlocked(bool blocked);
    bool isOutputBlocked() const;
    void setOutputBlocked(bool blocked);

    /** Utility method, a new task is created */
    void makeTask(Task::TaskType type);
};

class Node : private nsntl::Object {
  friend class Pipenet;
  friend class Pipe;

  private:
    Pipenet* pipenet;
    nsntl::ListItem pipenetItem;

    nsntl::ItemList srcPipes;
    nsntl::ItemList dstPipes;

    bool wantsDel;

  protected:
    Node();
    Node(Pipenet& pipenet);
    virtual ~Node();

    // --- API ---

    /** blocks input pipe - remote side can not send messages any more. No more onMessage() will be called after it. */
    void blockInput(Pipe* srcPipe);

    /** unblocks input pipe - allows remote to send again. From that point, we can get onMessage()s from this srcPipe. */
    void unblockInput(Pipe* srcPipe);

    /** says if input is blocked (by us). */
    bool isInputBlocked(Pipe* srcPipe) const;

    /** closes a source pipe. If it is not empty, fatal exception. */
    void closeInputPipe(Pipe* srcPipe);

    /** blocks output pipe - no more getMessage() is called after it. */
    void blockOutput(Pipe* dstPipe);

    /** unblocks output pipe - getMessage() can be called after that with it. */
    void unblockOutput(Pipe* dstPipe);

    /** closes a destination pipe. Remote will learn it only after it is emptied. */
    void closeOutputPipe(Pipe* dstPipe);

    /** says if output is blocked (by us) */
    bool isOutputBlocked(Pipe* dstPipe) const;

    // Note: there is no way to block output from our side. Don't send if you don't want to.

    /** Asks framework to call our destructor. No more event handler will be called,
     * but the destructor will be called soon. Pending incoming and outgoing Messages are lost! */
    void deleteMe();

    // --- EVENT HANDLERS --
 
    /** event handler if we need to provide a Message on an output pipe. Called only if we don't block it */
    virtual Message* getMessage(Pipe* dstPipe) = 0;

    /** event handler if we got a message from an input pipe */
    virtual void onMessage(Pipe* srcPipe, Message* msg) = 0;

    /** Called on a new Input connection from the Pipenet. Default: ignore */
    virtual void onNewInput(Pipe* srcPipe);

    /** Called if an input Pipe is gone, forever. Default: if it was the last, then deleteMe()
     * Is called only after the Message was read out. */
    virtual void onInputClosed(Pipe* srcPipe);

    /** Called on a new Output connection from the Pipenet. Default: ignore */
    virtual void onNewOutput(Pipe* dstPipe);

    /** called on our output Pipe is forever gone. Default handler: deleteMe().
     * Remote can close a Pipe only if it is empty, so no Message loss can happen. */
    virtual void onOutputClosed(Pipe* dstPipe);

    /** called on remote Node blocks our output Pipe. Default: ignore */
    virtual void onOutputBlocked(Pipe* dstPipe);

    /** called on remote Node unblocks our output Pipe. Default: ignore */
    virtual void onOutputUnblocked(Pipe* dstPipe);

#ifdef DEBUG
    virtual const char* getName() const;
#endif
};

class Pipenet {
  friend class Task;
  friend class Pipe;
  friend class Node;

  private:
    nsntl::ItemList nodes, pipes[Pipe::BLOCKTYPES_MAX], tasks[Task::TASKS_MAX];
    nsntl::MallocPool taskPool;

    void doBlockPipe(Pipe* pipe);
    void doUnblockPipe(Pipe* pipe);
    void doClosePipe(Pipe* pipe);
    void doOpenPipe(Pipe* pipe);
    void doDeliverMsg(Pipe* pipe);

#ifdef DEBUG
    void debug_task(const char* msg, Task* task);
#endif

  public:
    Pipenet();
    ~Pipenet();

    Pipenet& addNode(Node* node);
    Pipenet& makePipe(Node* src, Node* dst);

    void tic();
};

};
#endif
