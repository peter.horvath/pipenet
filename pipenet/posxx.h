#ifndef pipenet_net_h
#define pipenet_net_h

#include <posxx/event_manager.h>

#include "core.h"

namespace pipenet {

class DgramNode : public posxx::DgramFd, public posxx::FdEventProcessor, public Node {
  public:
    DgramNode(Pipenet& pn, posxx::EventManager& em);
    ~DgramNode();
};

class StreamNode : public posxx::StreamFd, public posxx::FdEventProcessor, public Node {
  public:
    StreamNode(Pipenet& pn, posxx::EventManager& em);
    ~StreamNode();
};

class ListenerNode : public posxx::StreamFd, public posxx::FdEventProcessor, public Node {
  public:
    ListenerNode(Pipenet& pn, posxx::EventManager& em);
    ~ListenerNode();
};

class SignalNode : public posxx::SignalEventHandler, public Node {
  public:
    SignalNode(Pipenet& pn, posxx::EventManager& em, int signum, uint8_t mask);
    ~SignalNode();
};

class TimerNode : public posxx::TimerEventHandler, public Node {
  public:
    TimerNode(Pipenet& pn, posxx::EventManager& em);
    ~TimerNode();
};

};

#endif
