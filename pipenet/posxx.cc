#include "posxx.h"

using namespace pipenet;
using namespace posxx;

pipenet::DgramNode::DgramNode(Pipenet& pn, EventManager& em) : Node(pn) {
  // TODO
};

pipenet::DgramNode::~DgramNode() {
  // TODO
};

pipenet::StreamNode::StreamNode(Pipenet& pn, EventManager& em) : Node(pn) {
  // TODO
};

pipenet::StreamNode::~StreamNode() {
  // TODO
};

pipenet::ListenerNode::ListenerNode(Pipenet& pn, EventManager& em) : Node(pn) {
  // TODO
};

pipenet::ListenerNode::~ListenerNode() {
  // TODO
};

pipenet::SignalNode::SignalNode(Pipenet& pn, EventManager& em, int signum, uint8_t mask) : SignalEventHandler(&em, signum, mask), Node(pn) {
  // TODO
};

pipenet::SignalNode::~SignalNode() {
  // TODO
};

pipenet::TimerNode::TimerNode(Pipenet& pn, EventManager& em) : TimerEventHandler(&em), Node(pn) {
  // TODO
};

pipenet::TimerNode::~TimerNode() {
  // TODO
};
