#include <stdio.h>

#include "core.h"
#include "logic.h"

using namespace pipenet;
using namespace nsntl;

pipenet::Blackhole::Blackhole(Pipenet& pipenet) : Node(pipenet) {
#ifdef DEBUG
  printf("Blackhole::Blackhole(%p,%p)\n", this, &pipenet);
#endif
};

void pipenet::Blackhole::onMessage(Pipe* p, Message* msg) {
#ifdef DEBUG
  printf("Blackhole::onMessage(%p, %p, %p)\n", this, p, msg);
#endif
  delete msg;
};

Message* pipenet::Blackhole::getMessage(Pipe* p) {
  throw LogicException(LogicError::UNREACHABLE_CODE);
};

void pipenet::Blackhole::onNewInput(Pipe* p) {
  unblockInput(p);
};

void pipenet::Blackhole::onNewOutput(Pipe* p) {
  throw Exception(Error::EXCESS_PIPES);
};

#ifdef DEBUG
const char* pipenet::Blackhole::getName() const {
  return "BlackHole";
};
#endif

pipenet::Whitehole::Whitehole(Pipenet& pipenet) : Node(pipenet) {
#ifdef DEBUG
  printf("Whitehole::Whitehole(%p, %p)\n", this, &pipenet);
#endif
};

void pipenet::Whitehole::onMessage(Pipe* p, Message* msg) {
  throw LogicException(LogicError::UNREACHABLE_CODE);
};

Message* pipenet::Whitehole::getMessage(Pipe* p) {
#ifdef DEBUG
  printf("Whitehole::getMessage(%p, %p)\n", this, p);
#endif
  return new Message();
};

void pipenet::Whitehole::onNewInput(Pipe* p) {
  throw Exception(Error::EXCESS_PIPES);
};

void pipenet::Whitehole::onNewOutput(Pipe* p) {
#ifdef DEBUG
  printf("Whitehole::onNewOutput(%p, %p)\n", this, p);
#endif
  unblockOutput(p);
};

#ifdef DEBUG
const char* pipenet::Whitehole::getName() const {
  return "WhiteHole";
};
#endif

pipenet::Filter::Filter(Pipenet& pipenet) : Node(pipenet) {
#ifdef DEBUG
  printf("Filter::Filter(%p, %p)\n", this, &pipenet);
#endif
  src = NULL;
  dst = NULL;
  msg = NULL;
};

pipenet::Filter::~Filter() {
#ifdef DEBUG
  printf("Filter::~Filter(%p)\n", this);
#endif
  if (msg)
    delete msg;
};

Message* pipenet::Filter::getMessage(Pipe* p) {
#ifdef DEBUG
  printf("Filter::getMessage(%p, %p)\n", this, p);
#endif

  if (p != dst)
    throw LogicException(LogicError::UNREACHABLE_CODE);

  Message* msg = this->msg;
  this->msg = NULL;
  if (isInputBlocked(src))
    unblockInput(src);
  blockOutput(dst);
  return msg;
};

void pipenet::Filter::onMessage(Pipe* p, Message* msg) {
#ifdef DEBUG
  printf("Filter::onMessage(%p, %p, %p)\n", this, p, msg);
#endif

  if (!src)
    throw LogicException(LogicError::UNREACHABLE_CODE);
  if (!dst)
    throw LogicException(LogicError::UNREACHABLE_CODE);
  if (this->msg)
    throw LogicException(LogicError::UNREACHABLE_CODE);

  if (!isInputBlocked(src))
    blockInput(src);
  this->msg = filter(msg);
  if (isOutputBlocked(dst))
    unblockOutput(dst);
};

void pipenet::Filter::onNewInput(Pipe* p) {
#ifdef DEBUG
  printf("Filter::onNewInput(%p, %p)\n", this, p);
#endif

  if (src)
    throw Exception(Error::EXCESS_PIPES);

  src=p;
  unblockInput(src);
};

void pipenet::Filter::onNewOutput(Pipe* p) {
#ifdef DEBUG
  printf("Filter::onNewOutput(%p, %p)\n", this, p);
#endif

  if (dst)
    throw Exception(Error::EXCESS_PIPES);

  dst=p;
  if (msg)
    unblockOutput(dst);
};

void pipenet::Filter::onOutputBlocked(Pipe* p) {
#ifdef DEBUG
  printf("Filter::onOutputBlocked(%p, %p)\n", this, p);
#endif

  if (msg && !isInputBlocked(src))
    blockInput(src);
};

void pipenet::Filter::onOutputUnblocked(Pipe* p) {
#ifdef DEBUG
  printf("Filter::onOutputUnblocked(%p, %p)\n", this, p);
#endif

  if (dst != p)
    throw LogicException(LogicError::UNREACHABLE_CODE);

  if (!msg && !isOutputBlocked(dst)) {
#ifdef DEBUG
    printf("msg: %p, isOutputBlocked: %s", msg, isOutputBlocked(dst)?"true":"false");
#endif
    blockOutput(dst);
  }
};

#ifdef DEBUG
const char* pipenet::Filter::getName() const {
  return "Filter";
};
#endif

pipenet::FirstNFilter::FirstNFilter(Pipenet& pipenet, int n) : Filter(pipenet) {
  this->n = n;
};

Message* pipenet::FirstNFilter::filter(Message* msg) {
#ifdef DEBUG
  printf ("filter, n: %i\n", n);
#endif
  if (n<1)
    deleteMe();
  else
    n--;
  return msg;
};

#ifdef DEBUG
const char* pipenet::FirstNFilter::getName() const {
  return "FirstNFilter";
};
#endif

pipenet::Forwarder::Forwarder(Pipenet& pipenet) : Filter(pipenet) {
  // nothing now
};

Message* pipenet::Forwarder::filter(Message* msg) {
  return msg;
};

#ifdef DEBUG
const char* pipenet::Forwarder::getName() const {
  return "Forwarder";
};
#endif
