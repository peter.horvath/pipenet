#include <stdio.h>

#include <pipenet/nodelib.h>

using namespace pipenet;

int main(int argc, char** argv) {
  pipenet::Pipenet pipenet;
  Whitehole* wh = new Whitehole(pipenet);
  FirstNFilter* filter = new FirstNFilter(pipenet, /*1<<30*/ 1<<30);
  Blackhole* bh = new Blackhole(pipenet);
  pipenet.makePipe(wh, filter);
  pipenet.makePipe(filter, bh);
  try {
    pipenet.tic();
  } catch (nsntl::ErrorCodeException e) {
    printf ("Exception got: %016lx\n", e.getErrorCode());
    throw e;
  }
};
