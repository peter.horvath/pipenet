#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <nsntl/exception.h>
#include <nsntl/list.h>
#include <nsntl/malloc.h>

#include "core.h"

using namespace pipenet;
using namespace nsntl;

pipenet::Exception::Exception(Error code) : ErrorCodeException((uint64_t)code) {
  // nothing now
};

pipenet::Message::~Message() {
  // nothing now
};

pipenet::Task::Task(Pipe* pipe, TaskType type) {
  if (type >= Task::TASKS_MAX)
    throw Exception(Error::INVALID_TASK_TYPE);

  if (!pipe->pipenet)
    throw Exception(Error::NO_PIPENET);

  this->type = type;

  this->pipe = pipe;
  pipeItem.init(this);
  pipeItem.attach(&pipe->tasks);
  
  pipenetItem.init(this);
  pipenetItem.attach(&pipe->pipenet->tasks[type]);

#ifdef DEBUG
  pipe->pipenet->debug_task("new task", this);
#endif
};

pipenet::Task::~Task() {
  // nothing - any important (pool release) is in MallocPooled::operator delete
};

pipenet::Pipe::Pipe(Node* srcNode, Node* dstNode) {
  if (srcNode->pipenet != dstNode->pipenet) {
    throw Exception(Error::CROSS_PIPENET_NOT_ALLOWED);
  }
  if (srcNode == dstNode) { // it would not be hard to allow it
    throw Exception(Error::SELF_PIPE_NOT_ALLOWED);
  }

  pipenet = srcNode->pipenet;
  pipenetItem.init(this);
  pipenetItem.attach(&pipenet->pipes[BLOCK_BLOCK]);

  src = srcNode;
  srcItem.init(this);
  srcItem.attach(&srcNode->dstPipes);

  dst = dstNode;
  dstItem.init(this);
  dstItem.attach(&dstNode->srcPipes);

  tasks.init();

  wantsDel = srcNode->wantsDel || dstNode->wantsDel;

  if (wantsDel)
    makeTask(Task::CLOSE_PIPE);
};

pipenet::Pipe::~Pipe() {
  while (!tasks.isEmpty()) {
    delete tasks.first();
  }
};

Pipe::BlockType pipenet::Pipe::getBlockType() const {
  if (!pipenet)
    throw Exception(Error::NO_PIPENET);
  return Pipe::BlockType(pipenetItem.list() - &pipenet->pipes[0]);
};

void pipenet::Pipe::setBlockType(BlockType blockType) {
  if (!pipenet)
    throw Exception(Error::NO_PIPENET);

  if (blockType != getBlockType()) {
    pipenetItem.detach();
    pipenetItem.attach(&pipenet->pipes[blockType]);
  }
};

bool pipenet::Pipe::isInputBlocked() const {
  return !(getBlockType() & 2);
};

void pipenet::Pipe::setInputBlocked(bool blocked) {
  int blockType = getBlockType();
  blockType = (blockType & 1) | (blocked ? 0 : 2);
  setBlockType((BlockType)blockType);
};

bool pipenet::Pipe::isOutputBlocked() const {
  return !(getBlockType() & 1);
};

void pipenet::Pipe::setOutputBlocked(bool blocked) {
  int blockType = getBlockType();
  blockType = (blockType & 2) | (blocked ? 0 : 1);
  setBlockType((BlockType)blockType);
};

void pipenet::Pipe::makeTask(Task::TaskType type) {
  new(pipenet->taskPool) Task(this, type);
};

pipenet::Node::Node() {
  pipenet = NULL;
  pipenetItem.init(this);

  wantsDel = false;
};

pipenet::Node::Node(Pipenet& pipenet) : Node() {
  pipenet.addNode(this);
};

pipenet::Node::~Node() {
  // In theory, none of these loops should run, because Node deletion can happen only if it has no more Pipes.
  while (!srcPipes.isEmpty())
    delete srcPipes.first();
  while (!dstPipes.isEmpty()) 
    delete dstPipes.first();
};

void pipenet::Node::blockInput(Pipe* srcPipe) {
  if (!pipenet)
    throw Exception(Error::NO_PIPENET);
  if (srcPipe->dst != this)
    throw Exception(Error::NOT_MY_SRC_PIPE);
  if (srcPipe->isOutputBlocked())
    throw Exception(Error::ALREADY_BLOCKED);

  srcPipe->makeTask(Task::BLOCK_PIPE);
};

void pipenet::Node::unblockInput(Pipe* srcPipe) {
  if (!pipenet)
    throw Exception(Error::NO_PIPENET);
  if (srcPipe->dst != this)
    throw Exception(Error::NOT_MY_SRC_PIPE);
  if (!srcPipe->isOutputBlocked())
    throw Exception(Error::ALREADY_UNBLOCKED);

  srcPipe->makeTask(Task::UNBLOCK_PIPE);
};

bool pipenet::Node::isInputBlocked(Pipe* srcPipe) const {
  if (!pipenet)
    throw Exception(Error::NO_PIPENET);
  if (srcPipe->dst != this)
    throw Exception(Error::NOT_MY_SRC_PIPE);

  return srcPipe->isOutputBlocked(); // if the input pipe is blocked by us, then its output is blocked
};

void pipenet::Node::closeInputPipe(Pipe* srcPipe) {
  if (!pipenet)
    throw Exception(Error::NO_PIPENET);
  if (srcPipe->dst != this)
    throw Exception(Error::NOT_MY_SRC_PIPE);

  srcPipe->wantsDel = true;
  srcPipe->makeTask(Task::CLOSE_PIPE);
};

void pipenet::Node::blockOutput(Pipe* dstPipe) {
  if (!pipenet)
    throw Exception(Error::NO_PIPENET);
  if (dstPipe->src != this)
    throw Exception(Error::NOT_MY_DST_PIPE);
  if (dstPipe->isInputBlocked())
    throw Exception(Error::ALREADY_BLOCKED);

  dstPipe->setInputBlocked(true);
};

void pipenet::Node::unblockOutput(Pipe* dstPipe) {
  if (!pipenet)
    throw Exception(Error::NO_PIPENET);
  if (dstPipe->src != this)
    throw Exception(Error::NOT_MY_DST_PIPE);
  if (!dstPipe->isInputBlocked())
    throw Exception(Error::ALREADY_UNBLOCKED);

  dstPipe->setInputBlocked(false);
};

void pipenet::Node::closeOutputPipe(Pipe* dstPipe) {
  if (!pipenet)
    throw Exception(Error::NO_PIPENET);
  if (dstPipe->src != this)
    throw Exception(Error::NOT_MY_DST_PIPE);

  dstPipe->wantsDel = true;
  dstPipe->makeTask(Task::CLOSE_PIPE);
};

bool pipenet::Node::isOutputBlocked(Pipe* dstPipe) const {
  if (!pipenet)
    throw Exception(Error::NO_PIPENET);
  if (dstPipe->src != this)
    throw Exception(Error::NOT_MY_DST_PIPE);

  return dstPipe->isInputBlocked(); // if output is blocked by us, then its input is blocked
};

void pipenet::Node::deleteMe() {
  if (!pipenet)
    throw Exception(Error::NO_PIPENET);

  wantsDel = true;

  for (ListItem* li = srcPipes.firstItem(); li; li=li->next()) {
    closeInputPipe(static_cast<Pipe*>(li->get()));
  }
  for (ListItem* li = dstPipes.firstItem(); li; li=li->next()) {
    closeOutputPipe(static_cast<Pipe*>(li->get()));
  }
};

void pipenet::Node::onNewInput(Pipe* srcPipe) {
  // nothing now
};

void pipenet::Node::onInputClosed(Pipe* srcPipe) {
  if (srcPipes.isEmpty())
    deleteMe();
};

void pipenet::Node::onNewOutput(Pipe* dstPipe) {
  // nothing now
};

void pipenet::Node::onOutputClosed(Pipe* dstPipe) {
  deleteMe();
};

void pipenet::Node::onOutputBlocked(Pipe* dstPipe) {
  // nothing now
};

void pipenet::Node::onOutputUnblocked(Pipe* dstPipe) {
  // nothing now
};

#ifdef DEBUG
const char* pipenet::Node::getName() const {
  return "Node";
};
#endif

pipenet::Pipenet::Pipenet() : taskPool(sizeof(Task)) {
  // nothing now
};

pipenet::Pipenet::~Pipenet() {
  for (int i=0; i < Pipe::BLOCKTYPES_MAX; i++)
    while (!pipes[i].isEmpty())
      delete pipes[i].first();

  // should not run - tasks are already deleted on pipe deletions
  for (int i=0; i < Task::TASKS_MAX; i++)
    while (!tasks[i].isEmpty())
      delete tasks[i].first();

  while (!nodes.isEmpty()) {
    delete nodes.first();
  }
};

Pipenet& pipenet::Pipenet::addNode(Node* node) {
  if (node->pipenet)
    throw Exception(Error::HAS_ALREADY_PIPENET);
  else
    node->pipenet = this;
  node->pipenetItem.attach(&nodes);
  return *this;
};

Pipenet& pipenet::Pipenet::makePipe(Node* src, Node* dst) {
  if (src->pipenet != this || dst->pipenet != this)
    throw Exception(Error::NOT_MY_NODE);
  Pipe* p = new Pipe(src, dst);
  p->makeTask(Task::OPEN_PIPE);
  return *this;
};

void pipenet::Pipenet::doBlockPipe(Pipe* pipe) {
  if (pipe->pipenet != this)
    throw Exception(Error::NOT_MY_PIPE);
  if (pipe->isOutputBlocked())
    throw Exception(Error::ALREADY_BLOCKED);
  pipe->setOutputBlocked(true);
  pipe->src->onOutputBlocked(pipe);
}

void pipenet::Pipenet::doUnblockPipe(Pipe* pipe) {
  if (pipe->pipenet != this)
    throw Exception(Error::NOT_MY_PIPE);
  if (!pipe->isOutputBlocked())
    throw Exception(Error::ALREADY_UNBLOCKED);
  pipe->setOutputBlocked(false);
  pipe->src->onOutputUnblocked(pipe);
}

void pipenet::Pipenet::doClosePipe(Pipe* pipe) {
  if (pipe->pipenet != this)
    throw Exception(Error::NOT_MY_PIPE);

  Node* src = pipe->src;
  Node* dst = pipe->dst;
  delete pipe;
  if (src->wantsDel && src->srcPipes.isEmpty() && src->dstPipes.isEmpty())
    delete src;
  else
    src->onOutputClosed(pipe);

  if (dst->wantsDel && dst->srcPipes.isEmpty() && dst->dstPipes.isEmpty())
    delete dst;
  else
    dst->onInputClosed(pipe);
}

void pipenet::Pipenet::doOpenPipe(Pipe* pipe) {
  if (pipe->pipenet != this)
    throw Exception(Error::NOT_MY_PIPE);
  pipe->dst->onNewInput(pipe);
  pipe->src->onNewOutput(pipe);
}

void pipenet::Pipenet::doDeliverMsg(Pipe* pipe) {
  if (pipe->pipenet != this)
    throw Exception(Error::NOT_MY_PIPE);
  Message* msg = pipe->src->getMessage(pipe);
  pipe->dst->onMessage(pipe, msg);
  if (pipe->getBlockType() == Pipe::UNBLOCK_UNBLOCK) {
    pipe->pipenetItem.detach();
    pipe->pipenetItem.attach(&(pipes[Pipe::UNBLOCK_UNBLOCK]));
  }
}

#ifdef DEBUG
void pipenet::Pipenet::debug_task(const char* msg, Task* task) {
  const char *typeStr[5] = {
    "CLOSE_PIPE",
    "BLOCK_PIPE",
    "UNBLOCK_PIPE",
    "OPEN_PIPE"
  };
  printf ("%s: %s pipe: %p (%s %p -> %s %p)\n",
    msg,
    typeStr[task->type], task->pipe,
    task->pipe->src->getName(), task->pipe->src,
    task->pipe->dst->getName(), task->pipe->dst
    );
};
#endif

void pipenet::Pipenet::tic() {
  while (true) {
    int i;
    for (i=0; i < Task::TASKS_MAX; i++) {
      if (!tasks[i].isEmpty()) {
        Task* task = static_cast<Task*>(tasks[i].first());
        Task::TaskType type = task->type;
#ifdef DEBUG
        debug_task("execute task", task);
#endif
        switch (type) {
          case Task::BLOCK_PIPE:
            doBlockPipe(task->pipe);
            break;
          case Task::UNBLOCK_PIPE:
            doUnblockPipe(task->pipe);
            break;
          case Task::CLOSE_PIPE:
            doClosePipe(task->pipe);
            break;
          case Task::OPEN_PIPE:
            doOpenPipe(task->pipe);
            break;
          default:
            throw LogicException(LogicError::UNREACHABLE_CODE);
        }
        if (type != Task::CLOSE_PIPE)
          delete task;
        break;
      }
    }
    if (i == Task::TASKS_MAX) {
      if (pipes[Pipe::UNBLOCK_UNBLOCK].isEmpty())
        break;
      else
        doDeliverMsg((Pipe*)(pipes[Pipe::UNBLOCK_UNBLOCK].first()));
    }
  }
};
