#ifndef reentrant_signal_queue_h
#define reentrant_signal_queue_h

struct {
  uint64_t id,
  int signum,
  int64_t ts1,
  int64_t ts2,
  siginfo_t siginfo,
  ucontext_t context
} rsq_signal;

int rsq_alloc_signal(int signum);
int rsq_dealloc_signal(int signum);
int rsq_read_event(struct rsq_signal* dst);

#endif
