#ifndef MaXXGC_h
#define MaXXGC_h

#include <nsntl/list.h>
#include <nsntl/types.h>
#include <nsntl/exception.h>

namespace MaXXGC {

enum class Error : u64 {
  DIFFERENT_GRAPH = 0xbc5b009be832cb3e,
  FROZEN_ON_MEMBER = 0x0de27fe8e06eac89,
  CIRCULAR_MEMBEROF = 0x9003214b1feb3770
};

class Exception : nsntl::ErrorCodeException {
  public:
    Exception(Error code);
};

class Object;
class Ptr;
class Graph;

class Ptr {
  friend class Object;
  friend class Graph;

  private:
    Graph* graph;
    Object *src, *dst;
    nsntl::ListItem *graphIter, *srcIter, *dstIter;

  public:
    Ptr(Object* src, Object* dst);
    virtual ~Ptr();
};

class Object {
  friend class Ptr;
  friend class Graph;

  private:
    Graph* graph;
    nsntl::List in, out, members;
    nsntl::ListItem *graphIter, *memberIter;

    bool frozen, visited = false;
    Object* parent;

    Object* getTopParent() const;
    void recVisited();

  public:
    Object(Graph* graph = (Graph*)0, bool frozen = false);
    Object(bool frozen = false);
    virtual ~Object();

    bool isFrozen();
    void setFrozen(bool frozen);
};

class Graph {
  friend class Object;
  friend class Ptr;
  private:
    nsntl::List objs, ptrs;

  public:
    Graph();
    ~Graph();

    void gc();
};

};

#endif
