#include "gc.h"

using namespace MaXXGC;

static MaXXGC::Graph mainGraph;

MaXXGC::Exception::Exception(Error code) : nsntl::ErrorCodeException((u64)code) {
};

MaXXGC::Ptr::Ptr(Object* src, Object* dst) {
  if (src->graph != dst->graph)
    throw Exception(Error::DIFFERENT_GRAPH);
  this->graph = src->graph;
  this->graphIter = this->graph->ptrs.push(this);
  this->src = src;
  this->dst = dst;
  this->srcIter = this->src->out.push(this);
  this->dstIter = this->dst->in.push(this);
};

MaXXGC::Ptr::~Ptr() {
  delete this->srcIter;
  delete this->dstIter;
  delete this->graphIter;
};

MaXXGC::Object::Object(Graph* graph, bool frozen) {
  if (!graph)
    graph = &mainGraph;
  this->graphIter = this->graph->objs.push(this);
  this->frozen = frozen;
};

MaXXGC::Object::Object(bool frozen) : MaXXGC::Object::Object(&mainGraph, frozen) {
  // nothing now
};

MaXXGC::Object::~Object() {
  if (this->parent) {
    delete this->memberIter;
  }

  while (!this->in.isEmpty()) {
    delete *(this->in.begin());
  }

  while (!this->out.empty()) {
    delete *(this->out.begin());
  }

  this->graph->objs.erase(this->graphIter);
};

Object* MaXXGC::Object::getTopParent() const {
  Object* ret;
  for (ret = const_cast<Object*>(this); ret->parent; ret = ret->parent);
  return ret;
};

bool MaXXGC::Object::isFrozen() {
  return this->getTopParent()->frozen;
};

void MaXXGC::Object::setFrozen(bool frozen) {
  if (this->parent) {
    throw Exception(Error::FROZEN_ON_MEMBER);
  }
  this->frozen = frozen;
};

void MaXXGC::Object::recVisited() {
  for (std::list<Ptr*>::iterator cur = this->out.begin(); cur != this->out.end(); cur++) {
    Object* neighbor = (*cur)->dst;
    if (!neighbor->visited) {
      neighbor->visited = true;
      neighbor->recVisited();
    }
  }
};

MaXXGC::Graph::Graph() {
  // nothing now (yes, it is real)
};

MaXXGC::Graph::~Graph() {
  std::list<Object*>::iterator cur, nxt;
  for (cur = this->objs.begin(); cur != this->objs.end(); cur = nxt) {
    nxt = cur;
    nxt++;
    if ((*cur)->parent)
      continue;
    else
      while ((*nxt)->getTopParent() == *cur) nxt++;
    if (!(*cur)->frozen) {
      delete *cur;
    }
  }
  while (!this->ptrs.empty()) {
    delete *(this->ptrs.begin());
  }
};

void MaXXGC::Graph::gc() {
  std::list<Object*>::iterator cur, nxt;
  for (cur = this->objs.begin(); cur != this->objs.end(); cur++) {
    (*cur)->visited = (*cur)->isFrozen();
  }
  for (cur = this->objs.begin(); cur != this->objs.end(); cur++) {
    if ((*cur)->visited)
      (*cur)->recVisited();
  }
  for (cur = this->objs.begin(); cur != this->objs.end(); cur = nxt) {
    nxt = cur;
    nxt++;
    if ((*cur)->visited)
      continue;
    if ((*cur)->parent)
      continue;
    else
      while ((*nxt)->getTopParent() == *cur) nxt++;
    delete *cur;
  }
};
